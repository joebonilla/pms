@extends('layouts.master')

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="well">
                <div class="row">
                    <div class="col-md-3">
                        <label for="search_for" class="control-label">Search For:</label>
                        <select name="search_for" id="search_for" class="form-control adv-search">
                            @foreach($search_options as $value => $name)
                                <option @if($value == app('request')->input('search_for')) selected
                                        @elseif($value == 'name' && gettype(app('request')->input('search_for')) === "NULL" ) selected
                                        @endif value="{{ $value }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="sort_by" class="control-label">Sort By:</label>
                        <select name="sort" id="sort_by" class="form-control adv-search">
                            @foreach($search_options as $value => $name)
                                <option @if($value == app('request')->input('sort')) selected
                                        @elseif($value == 'id' && gettype(app('request')->input('sort')) === "NULL" ) selected
                                        @endif value="{{ $value }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="order_by" class="control-label">Order By:</label>
                        <select name="order" id="order_by" class="form-control adv-search">
                            @foreach($order_options as $value => $name)
                                <option @if($value == app('request')->input('order')) selected
                                        @elseif($value == 'DESC' && gettype(app('request')->input('order')) === "NULL") selected
                                        @endif value="{{ $value }}">{{ $name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label for="per_page" class="control-label">Per Page:</label>
                        <select name="per_page" id="per_page" class="form-control adv-search">
                            @foreach($per_page_options as $value)
                                <option @if($value == app('request')->input('per_page')) selected
                                        @elseif($value == '10' && gettype(app('request')->input('per_page')) === "NULL") selected
                                        @endif value="{{ $value }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            @if(count($campaigns->items()) > 0)
                <div class="col-md-12">
                    <h3>Campaigns</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Team</th>
                            <th>Account</th>
                            <th>Units</th>
                            {{--<th>Initial</th>--}}
                            {{--<th>Iteration</th>--}}
                            {{--<th>Revision</th>--}}
                            <th>Percentage</th>
                            <th>Status</th>
                            <th>Due Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($campaigns as $campaign)
                            <tr>
                                <td>{{ $campaign->id }}</td>
                                <td><a href="/campaign/{{ $campaign->id }}">{{ $campaign->name }}</a></td>
                                <td><a href="/team/{{ $campaign->team->id }}">{{ $campaign->team->name }}</a></td>
                                <td>{{ $campaign->partner->name }}</td>
                                <td>{{ $campaign->units->count() }}</td>
                                {{--<td>{{ $campaign->units()->taskType(1)->count() }}</td>--}}
                                {{--<td>{{ $campaign->units()->taskType(2)->count() }}</td>--}}
                                {{--<td>{{ $campaign->units()->taskType(3)->count() }}</td>--}}
                                <td>{{ $campaign->progress }}%</td>
                                <td>{{ $campaign->status }}</td>
                                @if(isset($campaign->committed_at))
                                    <td>{{ $campaign->committed_at->toDayDateTimeString() }}</td>
                                @else
                                    <td></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif

            @if(count($units->items()) > 0)
                <div class="col-md-12">
                    <h3>Units</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Campaign Name</th>
                            <th>Assigned to</th>
                            <th>Team</th>
                            <th>Account</th>
                            <th>Status</th>
                            <th>Due Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($units as $unit)
                            <tr>
                                <td>{{ $unit->id }}</td>
                                <td><a href="/unit/{{ $unit->id }}">{{ $unit->name }}</a></td>
                                <td><a href="/campaign/{{ $unit->campaign->id }}">{{ $unit->campaign->name }}</a></td>
                                <td>@if ($unit->assignee)<strong>{{ $unit->assignee->name }}</strong>@else<strong>Unassigned</strong>@endif
                                </td>
                                <td>
                                    <a href="/team/{{ $unit->campaign->team->id }}">{{ $unit->campaign->team->name }}</a>
                                </td>
                                <td>{{ $unit->campaign->partner->name }}</td>
                                <td>{{ $unit->status }}</td>
                                <td>{{ $unit->committed_at->toDayDateTimeString() }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            @if(count($campaigns->items()) > 0 || count($units->items()) > 0)
                <div style="text-align:center;">
                    @if(count($campaigns->items()) >= count($units->items()))
                        {!! $campaigns->appends(Request::except('page'))->render() !!}
                    @else
                        {!! $units->appends(Request::except('page'))->render() !!}
                    @endif
                </div>
            @endif
        </div>
    </div>
@stop
@section('scripts')
    <script>
        function removeParam(key, sourceURL) {
            var rtn = sourceURL.split("?")[0],
                    param,
                    params_arr = [],
                    queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
            if (queryString !== "") {
                params_arr = queryString.split("&");
                for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                    param = params_arr[i].split("=")[0];
                    if (param === key) {
                        params_arr.splice(i, 1);
                    }
                }
                rtn = rtn + "?" + params_arr.join("&");
            }
            return rtn;
        }

        function updateQueryStringParameter(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        }

        $(document).on('ready', function () {
            $('.adv-search').on('change', function () {
                var elem = $(this);
                var param = elem.attr('name');
                var value = elem.val();
                var url = removeParam(param, window.location.href);

                if (url.indexOf('?') == -1) {
                    url += '?';
                }

                window.location.href = url + '&' + param + '=' + value;
            });
        });
    </script>
@stop