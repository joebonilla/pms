@extends('layouts.master')

@section('styles')
    <!-- Sweet Alert -->
    {!! Html::style('css/plugins/sweetalert/sweetalert.css') !!}
    {!! Html::style('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}
@endsection

@section('scripts')
    <!-- Sweet alert -->
    {!! Html::script('js/plugins/sweetalert/sweetalert.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            swal({
                        title: "500",
                        text: "Page is currently under construction!",
                        type: "warning",
                        timer: 5000,
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.href = '/';
                        }
                    });
        });
    </script>
@endsection