@extends('layouts.master')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Create Benchmark</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/team">Benchmark</a>
            </li>
            <li class="active">
                <a>New</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-7">
            <div class="ibox float-e-margins">

                <div class="ibox-content">
                    <form method="POST" action="/benchmark" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Team Name:</label>
                            <input type="text" name="name" placeholder="Awesome Team.." class="form-control">
                        </div>
                        <div class="form-group text-center">
                            <div>Complexity</div>
                            <div>Hours</div>
                            <div class="benchmark-items">
                                <input type="text" name="complexity" value="" class="form-control"/>
                                <input type="text" name="benchmark" value="" class="form-control">
                            </div>
                            <div>
                                <span>Add</span> | <span>Remove</span>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Create</strong></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
