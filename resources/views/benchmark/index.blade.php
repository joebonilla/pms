@extends('layouts.master')

@section('styles')
<!-- Toastr style -->
{{--<link href="/css/plugins/toastr/toastr.min.css" rel="stylesheet">--}}
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Benchmark</h2>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <?php print_r($benchmark); ?>
    </div>
</div>
@endsection
