@extends('layouts.master')

@section('styles')
{!! Html::style('css/plugins/fullcalendar/fullcalendar.css') !!}
{!! Html::style('css/plugins/fullcalendar/fullcalendar.css',array('media' => 'print')) !!}
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{ $team->name }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/team">Teams</a>
            </li>
            <li class="active">
                <strong>{{ $team->name }}</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row m-b-lg m-t-lg">
        <div class="col-md-6">
            <div class="profile-image">
                <img src="{{ asset('img/no-image.jpg') }}" class="img-circle circle-border m-b-md" alt="profile">
            </div>
            <div class="profile-info">
                <div class="">
                    <div>
                        <h2 class="no-margins">{{ $team->name }}</h2>
                        <h4>{{ $team->poc->name }}</h4>
                        <small>
                            Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
                        </small>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <h5>Team stats</h5>
            <table class="table small m-b-xs">
                <tbody>
                <tr>
                    <td>
                        <strong>{{ $team->members->count() }}</strong> Members
                    </td>
                    <td>
                        <strong>{{ $team->accounts->count() }}</strong> Accounts
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{ $team->campaigns->count() }}</strong> Campaigns
                    </td>
                    <td>
                        <strong>{{ $team->units->count() }}</strong> Units
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Team Members</h5>
                        </div>
                        <div class="ibox-content">
                            <p class="small">
                                If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't
                                anything embarrassing
                            </p>
                            <div class="user-friends">
                                @foreach ($team->members as $member)
                                <a href="/employee/{{ $member->id }}" title="{{ $member->name }}"><img alt="image" class="img-circle" src="{{ asset('img/no-image.jpg') }}"></a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Members with task <small>Ordered by priority</small></h5>
                        </div>
                        <div class="ibox-content">
                            <table class="table table-hover no-margins">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Member</th>
                                        <th>Unit</th>
                                        <th>Status</th>
                                        <th>Priority</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($members_with_task as $key => $member)
                                        <tr>
                                            <th>{{ $key + 1 }}</th>
                                            <td><a href="/employee/{{ $member->assignee->id }}">{{ $member->assignee->nickname }}</a></td>
                                            <td class="text-navy"><a href="/unit/{{ $member->id }}" title="{{ $member->name }}">{{ $member->id }}</a></td>
                                            @if ($member->status_tag == 1)
                                                <td><span class="label label-primary">{{ $member->status }}</span></td>
                                            @elseif ($member->status_tag == 2 || $member->status_tag == 4 || $member->status_tag == 6)
                                                <td><span class="label label-warning">{{ $member->status }}</span></td>
                                            @elseif ($member->status_tag == 5)
                                                <td><span class="label label-info">{{ $member->status }}</span></td>
                                            @else
                                                <td><span class="label label-success">{{ $member->status }}</span></td>
                                            @endif
                                            <td><small>{{ $member->priority }}</small></td>
                                        </tr>    
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Members without task <small>Ordered by oldest update</small></h5>
                        </div>
                        <div class="ibox-content">
                            <table class="table table-hover no-margins">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Last Unit</th>
                                    <th>Last Update</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach ($members_without_task as $key => $member)
                                        <tr>
                                            <th>{{ $key + 1 }}</th>
                                            <td><a href="/employee/{{ $member->employee_id }}">{{ $member->employee->nickname }}</a></td>
                                            <td><a href="/unit/{{ $member->unit_id }}" title="">{{ $member->unit_id }}</a></td>
                                            <td><i class="fa fa-clock-o"></i> {{ $member->updated_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Team Week </h5>
                        </div>
                        <div class="ibox-content">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Unassigned Units <small>Ordered by earliest commit date</small></h5>
                        </div>
                        <div class="ibox-content">
                            <table class="table table-hover no-margins">
                                <thead>
                                    <tr>
                                        <th>Campaign ID</th>
                                        <th>Unit ID</th>
                                        <th>Due</th>
                                        <th>Queue</th>
                                        <th>Priority</th>
                                        <th>Complexity</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($unassigned_units as $unit)
                                        <tr>
                                            <td><a href="/campaign/{{ $unit->campaign->id }}">{{ $unit->campaign->id }}</a></td>
                                            <td><a href="#">{{ $unit->id }}</a></td>
                                            <td><i class="fa fa-clock-o"></i> {{ $unit->committed_at }}</td>
                                            <td><small>{{ $unit->queue }}</small> </td>
                                            <td>{{ $unit->priority }}</td>
                                            <td>{{ $unit->complexity }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
{!! Html::script('js/plugins/fullcalendar/moment.min.js') !!}
{!! Html::script('js/plugins/fullcalendar/fullcalendar.min.js') !!}
<script type="text/javascript">
    $(document).ready(function() {
        $.ajaxSetup({
           headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        /* initialize the calendar
         -----------------------------------------------------------------*/
        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: false,
            droppable: false, // this allows things to be dropped onto the calendar
            drop: function() {
                // is the "remove after drop" checkbox checked?
                if ($('#drop-remove').is(':checked')) {
                    // if so, remove the element from the "Draggable Events" list
                    $(this).remove();
                }
            },
            events: {
                type: "GET",
                url: "{{ url('team', $team->id) }}",
                cache: false
            },
            // eventClick: function(calEvent, jsEvent, view) {

            //     alert('Event: ' + calEvent.title);
            //     alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            //     alert('View: ' + view.name);

            //     // change the border color just for fun
            //     $(this).css('border-color', 'red');

            // }
        });


    });

</script>
@endsection
