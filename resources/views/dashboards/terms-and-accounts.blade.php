@extends('layouts.master')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Teams and Accounts</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/">Dashboards</a>
            </li>
            <li class="active">
                <strong>Teams and Accounts</strong>
            </li>
            <li></li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
@endsection

@section('scripts')

@endsection