@extends('layouts.master')

@section('styles')

@endsection

@section('overrides')
<style type="text/css">
    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 0px;
    }
</style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Main</h2>
        <ol class="breadcrumb">
            <li>
                <p>Dashboards</p>
            </li>
            <li class="active">
                <strong>Main</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Volume</h5>
                    <!-- <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-white active">Today</button>
                            <button type="button" class="btn btn-xs btn-white">Monthly</button>
                            <button type="button" class="btn btn-xs btn-white">Annual</button>
                        </div>
                    </div> -->
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="m-b-xs">
                                {{ $volume->volume }}
                            </h1>
                            <small>
                                Volume
                            </small>
                            <div id="sparkline1" class="m-b-sm"><canvas width="348" height="50" style="display: inline-block; width: 348px; height: 50px; vertical-align: top;"></canvas></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <small class="stats-label">This Week (Units)</small>
                                    <h4>{{ $volume->this_week_units }}</h4>
                                </div>

                                <div class="col-xs-4">
                                    <small class="stats-label">Last Week (Units)</small>
                                    <h4>{{ $volume->last_week_units }}</h4>
                                </div>
                                <div class="col-xs-4">
                                    <small class="stats-label">Variance</small>
                                    <h4>{{ $volume->variance }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <h5 class="m-b-xs">Volume (This Week)</h5>
                            <h1 class="no-margins">{{ $volume->percentage }}%</h1>
                            <table class="table small m-t-sm">
                                <tbody>
                                <tr>
                                    <td>
                                        <strong>{{ $volume->campaigns }}</strong> Campaigns
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>{{ $volume->initial_builds }}</strong> Initial Builds
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>{{ $volume->creative_iterations }}</strong> Creative Iterations
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Utilization</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="m-b-xs">
                                {{ $utilization->utilization }} hrs.
                            </h1>
                            <small>
                                Utilization
                            </small>
                            <div id="sparkline2" class="m-b-sm"><canvas width="348" height="50" style="display: inline-block; width: 348px; height: 50px; vertical-align: top;"></canvas></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <small class="stats-label">This Week (Hours)</small>
                                    <h4>{{ $utilization->this_week_hours }}</h4>
                                </div>

                                <div class="col-xs-4">
                                    <small class="stats-label">Last Week (Hours)</small>
                                    <h4>{{ $utilization->last_week_hours }}</h4>
                                </div>
                                <div class="col-xs-4">
                                    <small class="stats-label">Variance</small>
                                    <h4>{{ $utilization->variance }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <h5 class="m-b-xs">Utilization (This Week)</h5>
                            <h1 class="no-margins">{{ $utilization->percentage }}%</h1>
                            <!-- <div class="font-bold text-navy">XX% <i class="fa fa-bolt"></i></div> -->
                            <table class="table small m-t-sm">
                                <tbody>
                                <tr>
                                    <td>
                                        <strong>{{ $utilization->review_hours }}</strong> Review Hours
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>{{ $utilization->dev_hours }}</strong> Development Hours
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>{{ $utilization->qa_hours }}</strong> QA Hours
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Quality</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="m-b-xs">{{ $quality->quality }}%</h1>
                            <small>Quality</small>
                            <div id="sparkline3" class="m-b-sm"><canvas width="348" height="50" style="display: inline-block; width: 348px; height: 50px; vertical-align: top;"></canvas></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <small class="stats-label">This Week (Issues)</small>
                                    <h4>{{ $quality->this_week_issues }}</h4>
                                </div>

                                <div class="col-xs-4">
                                    <small class="stats-label">Last Week (Issues)</small>
                                    <h4>{{ $quality->last_week_issues }}</h4>
                                </div>
                                <div class="col-xs-4">
                                    <small class="stats-label">Variance</small>
                                    <h4>{{ $quality->variance }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <h5 class="m-b-xs">Quality (This Week)</h5>
                            <h1 class="no-margins">{{ $quality->percentage }}%</h1>
                            <table class="table small m-t-sm">
                                <tbody>
                                <tr>
                                    <td>
                                        <strong>{{ $quality->client_issues }}</strong> Client Issues
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>{{ $quality->units_with_issues }}</strong> Units w/ Issues
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>{{ $quality->campaigns_with_issues }}</strong> Campaigns w/ Issues
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Efficiency</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-8">
                            <h1 class="m-b-xs">{{ $efficiency->efficiency }}%</h1>
                            <small>Efficiency</small>
                            <div id="sparkline4" class="m-b-sm"><canvas width="348" height="50" style="display: inline-block; width: 348px; height: 50px; vertical-align: top;"></canvas></div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <small class="stats-label">This Week (Lates)</small>
                                    <h4>{{ $efficiency->this_week_lates }}</h4>
                                </div>

                                <div class="col-xs-4">
                                    <small class="stats-label">Last Week (Lates)</small>
                                    <h4>{{ $efficiency->this_week_lates }}</h4>
                                </div>
                                <div class="col-xs-4">
                                    <small class="stats-label">Variance</small>
                                    <h4>{{ $efficiency->variance }}</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <h5 class="m-b-xs">Efficiency (This Week)</h5>
                            <h1 class="no-margins">{{ $efficiency->percentage }}%</h1>
                            <table class="table small m-t-sm">
                                <tbody>
                                <tr>
                                    <td>
                                        <strong>{{ $efficiency->on_time_units }}</strong> On-Time Units
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>{{ $efficiency->otd_level }}</strong> OTD Level (%)
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>{{ $efficiency->avg_tat_hour }}</strong> Average TAT (Hr.)
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            var sparklineCharts = function(){
                $("#sparkline1").sparkline(new Array({{ implode(', ', $volume->volume_graph) }}), {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1ab394',
                    fillColor: "transparent"
                });

                $("#sparkline2").sparkline(new Array({{ implode(', ', $utilization->utilization_graph) }}), {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1ab394',
                    fillColor: "transparent"
                });

                $("#sparkline3").sparkline(new Array({{ implode(', ', $quality->quality_graph) }}), {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1C84C6',
                    fillColor: "transparent"
                });

                $("#sparkline4").sparkline(new Array({{ implode(', ', $efficiency->efficiency_graph) }}), {
                    type: 'line',
                    width: '100%',
                    height: '50',
                    lineColor: '#1ab394',
                    fillColor: "transparent"
                });
            };

            var sparkResize;

            $(window).resize(function(e) {
                clearTimeout(sparkResize);
                sparkResize = setTimeout(sparklineCharts, 500);
            });

            sparklineCharts();
        });
    </script>
@endsection