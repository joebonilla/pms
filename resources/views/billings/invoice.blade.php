<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Billing - Invoice</title>

    <style type="text/css">
        td {
            width: 20px;
        }

        .title {
            border-bottom: 10px solid red;
        }

        .title td {
            text-align: center;
            height: 30px;
        }
    </style>
</head>
<body>
    <table border="1">
        <thead>
            <tr class="title">
                <td colspan="8">
                    <h2>INVOICE</h2>
                </td>
            </tr>
            <tr>
                <td colspan="1">Name:</td>
                <td colspan="3">{{ $partner_name }}</td>
                <td>Invoice no:</td>
            </tr>
            <tr>
                <td colspan="1">Attention:</td>
                <td colspan="3">Matt Burgoon</td>
            </tr>
            <tr>
                <td colspan="1">Address:</td>
                <td colspan="3">261 Madison Avenue, 12th Floor</td>
                <td colspan="1">Date</td>
                <td colspan="3">{{ $coverage_dates['coverage_from'] .' - ' . $coverage_dates['coverage_to'] }}</td>
            </tr>
            <tr>
                <td colspan="1">City:</td>
                <td colspan="3">New York, NY 10016</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="3">DESCRIPTION</td>
                <td>DATE COMPLETED</td>
                <td>TOTAL NO. OF UNITS</td>
                <td>COMPLEXITY LEVEL</td>
                <td>RATE</td>
                <td>TOTAL</td>
            </tr>
            <tr>
                <td colspan="8">Services Rendered for September 2015</td>
            </tr>
            @foreach ($billings as $billing)
                <tr>
                    <td colspan="3">{{ $billing['client_contact'] }}</td>
                </tr>

                @foreach ($billing['units'] as $unit)
                    <tr>
                        <td colspan="3">{{ $unit['name'] }}</td>
                        <td>{{ $unit['closed_at'] }}</td>
                        <td>1</td>
                        <td>{{ $unit['complexity'] }}</td>
                    </tr>
                @endforeach
            @endforeach
        </tbody>
    </table>
</body>
</html>