@extends('layouts.master')

@section('styles')
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Generate Billing</h2>
        <ol class="breadcrumb">
            <li>
                <p>Billings</p>
            </li>
            <li class="active">
                <strong>Invoice</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filters</h5>
                </div>
                <div class="ibox-content">
                	<form action="/billing/invoice" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Partner:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="partner_id">
                                    {{-- <option value="0">-</option> --}}
                                    @foreach ($partners as $partner)
                                    <option value="{{ $partner->id }}">{{ $partner->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="created-at">
                            <label class="col-sm-2 control-label">Closed From:</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" name="closed_from" value="{{ $closed_dates['closed_from'] }}">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" name="closed_to" value="{{ $closed_dates['closed_to'] }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit" name="invoice" value="invoice"><i class="fa fa-th-large"></i> Invoice</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{--<div class="col-lg-3">--}}
            {{--<div class="ibox">--}}
                {{--<div class="ibox-title">--}}
                    {{--<h5>Instructions</h5>--}}
                {{--</div>--}}
                {{--<div class="ibox-content">--}}
                    {{--<p>Under Construction</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
@endsection

@section('scripts')
<!-- Data picker -->
{!! Html::script('js/plugins/datapicker/bootstrap-datepicker.js') !!}
<script type="text/javascript">
    $(document).ready(function () {
        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            orientation: "bottom auto"
        });
    });
</script>
@endsection
