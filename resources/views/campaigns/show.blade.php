@extends('layouts.master')

@section('styles')
    <!-- Sweet Alert -->
    {!! Html::style('css/plugins/sweetalert/sweetalert.css') !!}
    {!! Html::style('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}
@endsection

@section('overrides')
    <style type="text/css">
        .no-side-padding {
            padding-right: 0;
            padding-left: 0;
        }
    </style>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Campaign detail</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/campaign">Campaign</a>
                </li>
                <li class="active">
                    <strong>{{ str_limit($campaign->name, $limit = 32) }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4">
            <div class="title-action">
                <form action="/campaign/{{ $campaign->id }}" method="POST" class="form-horizontal"
                      id="campaign-show-form">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <a href="/unit/create?campaign={{ $campaign->id }}" class="btn btn-success">+Units(s)</a>
                    <a href="/campaign/{{ $campaign->id }}/edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit
                    </a>
                    @if (($employee->ic_tag != $employee::IC_QA) AND ($employee->ic_tag != $employee::IC_DEV))
                    <button type="button" class="btn btn-danger" id="campaign-delete"><i class="fa fa-times "></i>
                        Delete
                    </button>
                    @endif
                </form>
            </div>
        </div>
    </div>
    <div class="row wrapper wrapper-content">
        <form action="/unit/mass-update" method="POST" role="form" id="mass-update-form" class="form-horizontal">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="campaign_id" value="{{ $campaign->id }}">
            <div class="col-lg-9">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="m-b-md">
                                    <h2>{{ $campaign->name }}</h2>
                                </div>
                                <dl class="dl-horizontal">
                                    <dt>Status:</dt>
                                    <dd><span class="label label-primary">{{ $campaign->status }}</span></dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5">
                                <dl class="dl-horizontal">
                                    <dt>Created by:</dt>
                                    <dd><a href="/employee/{{ $campaign->creator->id }}"
                                           class="text-navy">{{ $campaign->creator->name }}</a></dd>
                                    <dt>Team:</dt>
                                    <dd><a href="/team/{{ $campaign->team->id }}"
                                           class="text-navy">{{ $campaign->team->name }}</a></dd>
                                    <dt>Account:</dt>
                                    <dd>  {{ $campaign->partner->name }} </dd>
                                    <dt>Units:</dt>
                                    <dd>  {{ $campaign->units->count() }} </dd>
                                </dl>
                            </div>
                            <div class="col-lg-7" id="cluster_info">
                                <dl class="dl-horizontal">
                                    <dt>Advertiser:</dt>
                                    <dd><a href="#" class="text-navy">{{ $campaign->advertiser->name }}</a></dd>
                                    <dt>Created:</dt>
                                    <dd>  {{ $campaign->created_at->toDayDateTimeString() }} </dd>
                                    <dt>Participants:</dt>
                                    <dd class="project-people">
                                        @foreach ($campaign->participants as $participant)
                                            <a href="/employee/{{ $participant->id }}"><img alt="image"
                                                                                            class="img-circle"
                                                                                            src="{{ $participant->avatar }}"></a>
                                        @endforeach
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <dl class="dl-horizontal">
                                    <dt>Creative brief:</dt>
                                    <dd>
                                        <a href="{{ $campaign->creative_brief }}">{{ $campaign->creative_brief }}</a>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <dl class="dl-horizontal">
                                    <dt>Progress:</dt>
                                    <dd>
                                        <div class="progress progress-striped active m-b-sm">
                                            <div style="width: {{ $campaign->progress }}%;" class="progress-bar"></div>
                                        </div>
                                        <small>Campaign completed in <strong>{{ $campaign->progress }}%</strong>.
                                            Remaining close the project, sign a contract and invoice.
                                        </small>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hidden">
                    @if ($chunks)
                        {{-- @foreach ($chunks as $units) --}}
                        <div class="row">
                            @foreach ($chunks as $unit)
                                <div class="col-lg-4">
                                    @if ($unit)
                                        <div class="ibox">
                                            <div class="ibox-content">
                                                <span class="label label-primary pull-right"><input type="checkbox"
                                                                                                    name="mass_units[{{ $unit->id }}]"
                                                                                                    value="{{ $unit->id }}"></span>
                                                <a href="/unit/{{ $unit->id }}" class="btn-link">
                                                    <h2>{{ str_limit($unit->name, $limit = 12) }}</h2>
                                                </a>
                                                <div class="small m-b">
                                                    @if ($unit->assignee)
                                                        <strong>{{ $unit->assignee->name }}</strong>
                                                    @else
                                                        <strong>Unassigned</strong>
                                                    @endif
                                                    <span class="text-muted"><i
                                                                class="fa fa-clock-o"></i> {{ $unit->committed_at->toDayDateTimeString() }}</span>
                                                </div>
                                                <div class="m-b">
                                                    <span>Unit progress:</span>
                                                    <div class="stat-percent">{{ $unit->progress }}%</div>
                                                    <div class="progress progress-mini">
                                                        <div style="width: {{ $unit->progress }}%;"
                                                             class="progress-bar"></div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h5>Task Type: {{ $unit->getTypeAttribute() }}</h5>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <h5>Status:</h5>
                                                        @if ($unit->queueNotes()->count())
                                                            <button class="btn btn-info btn-xs" disabled=""
                                                                    type="button">
                                                                Q: {{ $unit->queueNotes()->orderBy('created_at', 'desc')->first()->queue }}</button>
                                                        @else
                                                            <button class="btn btn-info btn-xs" disabled=""
                                                                    type="button">Q: Not started
                                                            </button>
                                                        @endif
                                                        <button class="btn btn-success btn-xs" disabled=""
                                                                type="button">S: {{ $unit->status }}</button>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="small text-right">
                                                            <h5>Stats:</h5>
                                                            <div>
                                                                <i class="fa fa-comments-o"> </i> {{ $unit->queueNotes()->whereNotNull('note')->count() }}
                                                                Notes
                                                            </div>
                                                            <i class="fa fa-eye"> </i> {{ $unit->unitTimes()->count() }}
                                                            Unit Time
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                        {{-- @endforeach --}}
                    @else
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="ibox">
                                    <div class="ibox-title">
                                        <h5>No unit(s) as the moment.</h5>
                                    </div>
                                    <div class="ibox-content">
                                        <p class="text-center">
                                            <a href="/team/create"><i class="fa fa-sign-in big-icon"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="table-responsive">
                    <h3>Units</h3>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox" class="check-all" data-target=".mass-units"></th>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Assigned to</th>
                            <th>Committed at</th>
                            <th>Progress</th>
                            <th>Task Type</th>
                            <th>Status</th>
                            <th>Phase</th>
                            <th>Unit Time</th>
                            <th>Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($units as $unit)
                            <tr>
                                <td><input type="checkbox" class="mass-units" name="mass_units[{{ $unit->id }}]"
                                           value="{{ $unit->id }}"></td>
                                <td>{{ $unit->id }}</td>
                                <td><a href="/unit/{{ $unit->id }}">{{ $unit->name }}</a></td>
                                <td>@if ($unit->assignee)<strong>{{ $unit->assignee->name }}</strong>@else<strong>Unassigned</strong>@endif
                                </td>
                                <td>{{ $unit->committed_at->toDayDateTimeString() }}</td>
                                <td>{{ $unit->progress }}%</td>
                                <td>{{ $unit->getTypeAttribute() }}</td>
                                <td>{{ $unit->status }}</td>
                                <td>@if ($unit->queueNotes()->count()) {{ $unit->queueNotes()->orderBy('created_at', 'desc')->first()->queue }} @else
                                        Review @endif</td>
                                <td>{{ $unit->queueNotes()->whereNotNull('note')->count() }}</td>
                                <td>{{ $unit->unitTimes()->count() }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox">
                    <div class="ibox-title">
                        <h4>Mass Update</h4>
                    </div>
                    <div class="ibox-content">
                        <p class="text-info">Assign Unit(s)</p>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">To:</label>
                                <div class="col-lg-8 no-side-padding">
                                    <select class="form-control" name="unit_assignee">
                                        <option>---Select---</option>
                                        @foreach ($team_members as $team_member)
                                            <option value="{{ $team_member->id }}">{{ $team_member->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" name="unit_assign" value="unit_assign"
                                        class="btn btn-primary btn-xs pull-right">GO
                                </button>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <p class="text-info">Unit Action</p>
                        @if (($employee->ic_tag > $employee::IC_QA) OR ($employee->role_tag > $employee::ROLE_IC))
                        <div class="row">
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Work Type:</label>
                                <div class="col-lg-6 no-side-padding">
                                    <select class="form-control" name="work_tag">
                                        <option value="4">Review</option>
                                        <option value="3">Qa</option>
                                        <option value="2">Dev</option>
                                        <option value="1">Creative</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="row m-b-xs">
                            <div class="col-lg-4">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_action_grab" value="2" name="unit_action_tag"
                                           checked="checked">
                                    <label for="unit_action_grab"> Grab</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_action_drop" value="3" name="unit_action_tag">
                                    <label for="unit_action_drop"> Drop</label>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_action_hold" value="6" name="unit_action_tag">
                                    <label for="unit_action_hold"> Hold</label>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b">
                            <div class="col-lg-5 col-lg-offset-1">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_action_in_prog" value="5" name="unit_action_tag">
                                    <label for="unit_action_in_prog"> In Prog</label>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_action_info_req" value="4" name="unit_action_tag">
                                    <label for="unit_action_info_req"> Info Req</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" name="unit_action" value="unit_action"
                                        class="btn btn-primary btn-xs pull-right">GO
                                </button>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <p class="text-info">Unit Phase</p>
                        <div class="row m-b-xs">
                            <div class="col-lg-6">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_queue_dev_ready" value="1" name="unit_queue_tag"
                                           checked="checked">
                                    <label for="unit_queue_dev_ready"> Development</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="inlineRadio7" value="2" name="unit_queue_tag">
                                    <label for="inlineRadio7">QA</label>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b-xs">
                            <div class="col-lg-6">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_queue_no_rev" value="4" name="unit_queue_tag">
                                    <label for="unit_queue_no_rev"> No Revision</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_queue_close" value="5" name="unit_queue_tag">
                                    <label for="unit_queue_close"> Close</label>
                                </div>
                            </div>
                        </div>
                        <div class="row m-b">
                            <div class="col-lg-6">
                                <div class="radio radio-info radio-inline">
                                    <input type="radio" id="unit_queue_rev" value="3" name="unit_queue_tag">
                                    <label for="unit_queue_rev"> Revision</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" name="revision_count" class="form-control input-sm">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <button type="submit" name="unit_queue" value="unit_queue"
                                        class="btn btn-primary btn-xs pull-right">GO
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('scripts')
    <!-- Sweet alert -->
    {!! Html::script('js/plugins/sweetalert/sweetalert.min.js') !!}
    <script type="text/javascript">
        $(document).ready(function () {
            $("#campaign-delete").on("click", function (event) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this campaign!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $("#campaign-show-form").submit();
                });
            });
            $("#mass-update-form").submit(function (e) {
                if (!$('.mass-units:checked').length) {
                    swal({
                        title: "No Units Selected",
                        text: "Atleast Pick Something!",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "OK!",
                        closeOnConfirm: false
                    });
                    return false;
                }

                return true;
            });
            $('.check-all').on('change', function () {
                var target_elem = $(this).attr('data-target');

                $(target_elem).prop('checked', this.checked);
            });
        });
    </script>
@endsection