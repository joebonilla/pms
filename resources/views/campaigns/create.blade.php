@extends('layouts.master')

@section('styles')
    {!! Html::style('css/plugins/auto-complete/auto-complete.css') !!}
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>New Campaign</h2>
            <ol class="breadcrumb">
                <li>
                    <p>Campaign</p>
                </li>
                <li class="active">
                    <strong>Add New Campaign</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Campaign Ticket</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="/campaign" method="POST" class="form-horizontal" id="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Campaign name:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="campaign_name"
                                           placeholder="Sample campaign">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Team:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="team_id" id="team-select">
                                        <option value="">---Select---</option>
                                        @foreach ($teams as $team)
                                            <option value="{{ $team->id }}">{{ $team->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Partner:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="partner_id" id="partner-select">
                                        <option value="">---Select---</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Sub Partner Name:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="sub_partner_name" disabled
                                           id="sub-partner-name">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Advertiser:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="advertiser_name" id="advertiser-name"
                                           disabled>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Campaign Type:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="campaign_type" id="campaign-type">
                                        <option value="">---Select---</option>
                                        <option value="1">HTML5</option>
                                        <option value="2">Flash</option>
                                        <option value="3">PHP</option>
                                    </select>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Creative Brief Link:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="creative_brief">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Add Units</button>
                                    <a href="/" class="btn btn-white">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-3">--}}
            {{--<div class="ibox">--}}
            {{--<div class="ibox-title">--}}
            {{--<h5>Instructions</h5>--}}
            {{--</div>--}}
            {{--<div class="ibox-content">--}}
            {{--<p>Under Construction</p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection

@section('scripts')
    {!! Html::script('js/plugins/auto-complete/auto-complete.js') !!}
    <!-- Jquery Validate -->
    {!! Html::script('js/plugins/validate/jquery.validate.min.js') !!}

    <script type="text/javascript">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}
            });

            $("#form").validate({
                rules: {
                    campaign_name: {
                        required: true
                    },
                    creative_brief: {
                        url: true
                    },
                    team_id: {
                        required: true
                    },
                    partner_id: {
                        required: true
                    },
                    sub_partner_name: {
                        required: true
                    },
                    advertiser_name: {
                        required: true
                    },
                    sub_advertiser_name: {
                        required: true
                    },
                    campaign_type: {
                        required: true
                    }
                }
            });

            $("#team-select").change(function (e) {
                $("#partner-select option").each(function (e) {
                    if ($(this).val() > 0) {
                        $(this).remove();
                    }
                });

                if ($(this).val() > 0) {
                    var team_id = $(this).val();
                    var team_url = "{{ url('team') }}";
                    team_url = team_url + "/" + team_id + "/accounts"

                    console.log(team_url);

                    $.ajax({
                        type: "GET",
                        url: team_url,
                        cache: false,
                        success: function (data) {
                            var isOther = false;
                            for (var i = 0; i < data.length; i++) {
                                //if "Others" was found, skip the loop and add to the last element of data
                                if(data[i].name === "Others" && !isOther){
                                    isOther = true;
                                    data.push(data[i]);
                                    continue;
                                }

                                $("#partner-select").append($("<option></option>").attr("value", data[i].id).text(data[i].name));
                            }
                        }
                    });


                }
            });

            var advertiser_list = new Array();

            $("#partner-select").change(function (e) {
                if ($(this).val() > 0) {
                    $("#advertiser-name").attr('disabled', false);
                    advertiser_list = [];

                    var partner_id = $(this).val();
                    var partner_url = "{{ url('partner') }}";
                    partner_url = partner_url + "/" + partner_id + "/advertisers"

                    $.ajax({
                        type: "GET",
                        url: partner_url,
                        cache: false,
                        success: function (data) {
                            for (var j = 0; j < data.length; j++) {
                                advertiser_list.push(data[j]);
                            }
                        }
                    });
                } else {
                    $("#advertiser-name").attr('disabled', true);
                }
            });

            $("#advertiser-name").keyup(function (e) {
                $("#advertiser-name").autoComplete({
                    minChars: 2,
                    source: function (term, suggest) {
                        term = term.toLowerCase();
                        var choices = advertiser_list;
                        var matches = [];
                        for (i = 0; i < choices.length; i++)
                            if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i]);
                        suggest(matches);
                    }
                });
            });

            $("#partner-select").change(function (e) {
                var partner = $(this).find("option:selected").text();

                if (partner == "Others") {
                    $("#sub-partner-name").attr('disabled', false);
                } else {
                    $("#sub-partner-name").attr('disabled', true);
                }
            });
        });
    </script>
@endsection