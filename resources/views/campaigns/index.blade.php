@extends('layouts.master')

@section('styles')
    <!-- Morris -->
    {!! Html::style('css/plugins/morris/morris-0.4.3.min.css') !!}
@endsection

@section('overrides')
    <style type="text/css">
        .center-text {
            text-align: center;
        }

        .ibox-tools a {
            color: #FFFFFF;
        }
    </style>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Campaigns List</h2>
            <ol class="breadcrumb">
                <li>
                    <p>Campaign</p>
                </li>
                <li class="active">
                    <strong>Campaign List</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-primary pull-right">Today</span>
                        <h5>Open</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><a href="/campaign?status=1">{{ $campaigns_stats->open }}</a></h1>
                        <small>Campaigns</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 hidden">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-warning pull-right">Today</span>
                        <h5>At Risk</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins"><a href="/campaign?due=8">{{ $campaigns_stats->eight_hours_due }}</a>
                        </h1>
                        <div class="font-bold text-warning"><i class="fa fa-clock-o"></i>
                            <small>In <strong>8</strong> Hours</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-success pull-right">Today</span>
                        <h5>Closed</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="no-margins"><a href="/campaign?status=2">{{ $campaigns_stats->closed }}</a>
                                </h1>
                                <div class="font-bold text-success"><i class="fa fa-thumbs-o-up"></i>
                                    <small>Closed</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">Today</span>
                        <h5>Late</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <h1 class="no-margins"><a href="/campaign?status=3">{{ $campaigns_stats->late }}</a>
                                </h1>
                                <div class="font-bold text-danger"><i class="fa fa-thumbs-o-down"></i>
                                    <small> Campaigns</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <span class="label label-danger pull-right">Today</span>
                        <h5>At Risk Due</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-4">
                                <h1 class="no-margins"><a
                                            href="/campaign?due=3">{{ $campaigns_stats->three_hours_due }}</a></h1>
                                <div class="font-bold text-danger"><i class="fa fa-clock-o"></i>
                                    <small>In <strong>3</strong> Hours</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h1 class="no-margins"><a
                                            href="/campaign?due=8">{{ $campaigns_stats->eight_hours_due }}</a></h1>
                                <div class="font-bold text-danger"><i class="fa fa-clock-o"></i>
                                    <small>In <strong>8</strong> Hours</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h1 class="no-margins"><a
                                            href="/campaign?due=24">{{ $campaigns_stats->twentyfour_hours_due }}</a>
                                </h1>
                                <div class="font-bold text-danger"><i class="fa fa-clock-o"></i>
                                    <small>In <strong>24</strong> Hours</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--<div class="row">--}}
        {{--<div class="col-lg-12">--}}
        {{--<div class="ibox float-e-margins">--}}
        {{--<div class="ibox-content">--}}
        {{--<div>--}}
        {{--<span class="pull-right text-right">--}}
        {{--<small>Average value of campaigns in this month.</small>--}}
        {{--<br/>--}}
        {{--Total campaigns: {{ $campaigns->total() }}--}}
        {{--</span>--}}
        {{--<h3 class="font-bold no-margins">--}}
        {{--Campaign margin--}}
        {{--</h3>--}}
        {{--<small>Created campaigns.</small>--}}
        {{--</div>--}}
        {{--<div class="m-t-sm">--}}
        {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
        {{--<div>--}}
        {{--<canvas id="lineChart" height="60"></canvas>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--<div class="m-t-md">--}}
        {{--<small class="pull-right">--}}
        {{--<i class="fa fa-clock-o"> </i>--}}
        {{--Update on 16.07.2015--}}
        {{--</small>--}}
        {{--<small>--}}
        {{--<strong>Analysis of campaigns:</strong> The value has been changed over time, and last month reached a level over {{ $campaigns_created_last_month }}.--}}
        {{--</small>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Campaign List</h5>
                        <div class="ibox-tools">
                            <a href="/campaign/create" class="btn btn-success btn-xs pull-right">New</a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form action="/campaign" method="GET" id="form">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-5 m-b-xs">
                                    <div class="btn-group">
                                        <a href="/campaign?range=2" class="btn btn-sm btn-white">Week</a>
                                        <a href="/campaign?range=3" class="btn btn-sm btn-white">Month</a>
                                        <a href="/campaign" class="btn btn-sm btn-white active">All</a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <input type="text" placeholder="Campaign name" class="input-sm form-control"
                                               name="search">
                                        <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> Go!</button>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>
                                        <a href="javscript:void(0)"
                                           class="header-btn @if(app('request')->input('sort') === 'id') active {{ app('request')->input('order') }} @endif"
                                           data-sort="id">Id</a>
                                    </th>
                                    <th>
                                        <a href="javscript:void(0)"
                                           class="header-btn @if(app('request')->input('sort') === 'name') active {{ app('request')->input('order') }} @endif"
                                           data-sort="name">Name</a>
                                    </th>
                                    <th>
                                        <a href="javscript:void(0)"
                                           class="header-btn @if(app('request')->input('sort') === 'team') active {{ app('request')->input('order') }} @endif"
                                           data-sort="team">Team</a>
                                    </th>
                                    <th>
                                        <a href="javscript:void(0)"
                                           class="header-btn @if(app('request')->input('sort') === 'account') active {{ app('request')->input('order') }} @endif"
                                           data-sort="account">Account</a>
                                    </th>
                                    <th>Units</th>
                                    {{--<th>Initial</th>--}}
                                    {{--<th>Iteration</th>--}}
                                    {{--<th>Revision</th>--}}
                                    <th>
                                        <a href="javscript:void(0)"
                                           class="header-btn @if(app('request')->input('sort') === 'percentage') active {{ app('request')->input('order') }} @endif"
                                           data-sort="percentage">Percentage</a>
                                    </th>
                                    <th>Status</th>
                                    <th>
                                        <a href="javscript:void(0)"
                                           class="header-btn @if(app('request')->input('sort') === 'commit') active {{ app('request')->input('order') }} @endif"
                                           data-sort="commit">Due date</a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($campaigns as $campaign)
                                    <tr>
                                        <td>{{ $campaign->id }}</td>
                                        <td><a href="/campaign/{{ $campaign->id }}">{{ $campaign->name }}</a></td>
                                        <td><a href="/team/{{ $campaign->team->id }}">{{ $campaign->team->name }}</a>
                                        </td>
                                        <td>{{ $campaign->partner->name }}</td>
                                        <td>{{ $campaign->units->count() }}</td>
                                        {{--<td>{{ $campaign->units()->taskType(1)->count() }}</td>--}}
                                        {{--<td>{{ $campaign->units()->taskType(2)->count() }}</td>--}}
                                        {{--<td>{{ $campaign->units()->taskType(3)->count() }}</td>--}}
                                        <td>{{ $campaign->progress }}%</td>
                                        <td>{{ $campaign->status }}</td>
                                        @if(isset($campaign->committed_at))
                                            <td>{{ $campaign->committed_at->toDayDateTimeString() }}</td>
                                        @endif
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="11">No result(s) found.</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                        <div style="text-align:center;">
                            {!! $campaigns->appends(Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            function removeParam(key, sourceURL) {
                var rtn = sourceURL.split("?")[0],
                        param,
                        params_arr = [],
                        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
                if (queryString !== "") {
                    params_arr = queryString.split("&");
                    for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                        param = params_arr[i].split("=")[0];
                        if (param === key) {
                            params_arr.splice(i, 1);
                        }
                    }
                    rtn = rtn + "?" + params_arr.join("&");
                }
                return rtn;
            }

            function updateQueryStringParameter(uri, key, value) {
                var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
                var separator = uri.indexOf('?') !== -1 ? "&" : "?";
                if (uri.match(re)) {
                    return uri.replace(re, '$1' + key + "=" + value + '$2');
                }
                else {
                    return uri + separator + key + "=" + value;
                }
            }

            var lineData = {
                labels: new Array({{ implode(', ', $days) }}),
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: new Array({{ implode(', ', $graph) }})
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
            };

            $('.header-btn').each(function () {
                var elem = $(this);
                var value = elem.attr('data-sort');
                var url = removeParam('order', window.location.href);

                if (elem.hasClass('active')) {
                    if (elem.hasClass('asc')) {
                        value += '&order=desc';
                        elem.append('<span class="dropup"> <span class="caret"></span> </span>');
                    } else {
                        value += '&order=asc';
                        elem.append('<span class="caret"></span>');
                    }
                } else {
                    value += '&order=desc';
                    elem.append('<span class="caret hidden"></span>');
                }

                if (url.indexOf("?") > 0) {
                    url = removeParam('sort', url);
                    url = updateQueryStringParameter(url, 'sort', value);
                } else {
                    url += '?sort=' + value;
                }

                elem.attr('href', url);
            }).on('mouseenter', function () {
                $(this).not('.active').children('.caret').removeClass('hidden');
            }).on('mouseleave', function () {
                $(this).not('.active').children('.caret').addClass('hidden');
            });

            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
        });
    </script>
@endsection
