@extends('layouts.master')

@section('styles')
    {!! Html::style('css/plugins/auto-complete/auto-complete.css') !!}
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Edit Campaign</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/campaign/{{ $campaign->id }}">{{ $campaign->name }}</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Campaign Ticket</h5>
                </div>
                <div class="ibox-content">
                    <form action="/campaign/{{ $campaign->id }}" method="POST" class="form-horizontal" id="form">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campaign name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="campaign_name" value="{{ $campaign->name }}">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Team:</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="team_id" id="team-select">
                                    @foreach ($teams as $team)
                                        @if ($campaign->team_id == $team->id)
                                            <option value="{{ $team->id }}" selected>{{ $team->name }}</option>
                                        @else
                                            <option value="{{ $team->id }}">{{ $team->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Partner:</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="partner_id" id="partner-select">
                                    <option value="">---Select---</option>
                                    @foreach ($partners as $partner)
                                        @if ($campaign->partner_id == $partner->id)
                                            <option value="{{ $partner->id }}" selected>{{ $partner->name }}</option>
                                        @else
                                            <option value="{{ $partner->id }}">{{ $partner->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sub Partner Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="sub_partner_name" disabled id="sub-partner-name">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Creative Brief:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="creative_brief" value="{{ $campaign->creative_brief }}">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group"><label class="col-sm-2 control-label">Advertiser:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="advertiser_name" id="advertiser-name" value="{{ $campaign->advertiser->name }}">
                                {{--<span class="help-block m-b-none">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</span>--}}
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campaign Type:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="campaign_type" id="campaign-type-select">
                                    @foreach ($campaign_types as $ct => $campaign_type)
                                        @if ($campaign->type_tag == $ct)
                                            <option value="{{ $ct }}" selected>{{ $campaign_type }}</option>
                                        @else
                                            <option value="{{ $ct }}">{{ $campaign_type }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-success" type="submit">Save</button>
                                <a href="/campaign/{{ $campaign->id }}" class="btn btn-white">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{--<div class="col-lg-3">--}}
            {{--<div class="ibox">--}}
                {{--<div class="ibox-title">--}}
                    {{--<h5>Instructions</h5>--}}
                {{--</div>--}}
                {{--<div class="ibox-content">--}}
                    {{--<p>Under Construction</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
@endsection

@section('scripts')
    {!! Html::script('js/plugins/auto-complete/auto-complete.js') !!}
    {!! Html::script('js/plugins/validate/jquery.validate.min.js') !!}
<script type="text/javascript">
    var advertiser_list = new Array();

    $(document).ready(function () {
        $.ajaxSetup({
           headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });

        $("#form").validate({
            rules: {
                campaign: {
                    required: true
                }
            }
        });

        $("#team-select").change(function (e) {
            $("#partner-select option").each(function (e) {
                if ($(this).val() > 0) {
                    $(this).remove();
                }
            });

            if ($(this).val() > 0) {
                var team_id = $(this).val();
                var team_url = "{{ url('team') }}";
                team_url = team_url + "/" + team_id + "/accounts"

                $.ajax({
                    type: "GET",
                    url: team_url,
                    cache: false,
                    success: function (data) {
                        for (var i = 0; i < data.length; i++) {
                            $("#partner-select").append($("<option></option>").attr("value", data[i].id).text(data[i].name));
                        }
                    }
                });
            }
        });

        $("#partner-select").change(function (e) {
            $("#advertiser-name").attr('disabled', false);
            advertiser_list = [];

            if ($(this).val() > 0) {
                var partner_id = $(this).val();
                var partner_url = "{{ url('partner') }}";
                partner_url = partner_url + "/" + partner_id + "/advertisers"

                $.ajax({
                    type: "GET",
                    url: partner_url,
                    cache: false,
                    success: function (data) {
                        for (var j = 0; j < data.length; j++) {
                            advertiser_list.push(data[j]);
                        }
                    }
                });
            } else {
                $("#advertiser-name").attr('disabled', true);
            }
        });

        $("#advertiser-name").keyup(function (e) {
            if (advertiser_list.length == 0) {
                advertiser_list = {!! json_encode($advertisers) !!};
            }

            $("#advertiser-name").autoComplete({
                minChars: 2,
                source: function(term, suggest){
                    term = term.toLowerCase();
                    var choices = advertiser_list;
                    var matches = [];
                    for (i=0; i<choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i]);
                    suggest(matches);
                }
            });
        });

        $("#partner-select").change(function (e) {
            var partner = $(this).find("option:selected").text();

            if (partner == "Others") {
                $("#sub-partner-name").attr('disabled', false);
            } else {
                $("#sub-partner-name").attr('disabled', true);
            }
        });
    });
</script>
@endsection