@extends('layouts.master')

@section('styles')
    {!! Html::style('css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}
    {!! Html::style('css/plugins/auto-complete/auto-complete.css') !!}
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Edit Unit</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/campaign">Campaigns</a>
            </li>
            <li>
                <a href="/campaign/{{ $unit->campaign->id }}">{{ $unit->campaign->name }}</a>
            </li>
            <li>
                <a href="/unit/{{ $unit->id }}">{{ $unit->name }}</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Edit Unit</h5>
                </div>
                <div class="ibox-content">
                    <form action="/unit/{{ $unit->id }}" method="POST" class="form-horizontal" id="form">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-lg-2 control-label">Campaign:</label>
                            <div class="col-lg-10">
                                <input type="text" disabled="" value="{{ $unit->campaign->name }}" class="form-control">
                                <input type="hidden" name="campaign_id" value="{{ $unit->campaign->id }}">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div id="units">
                            <div class="unit" id="unit-0">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Unit name:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control unit-name" name="unit_name" value="{{ $unit->name }}">
                                        {{--<span class="help-block m-b-none">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</span>--}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Complexity:</label>
                                                <select class="form-control" name="complexity">
                                                    @foreach ($complexities as $complexity)
                                                        @if ($unit->complexity == $complexity)
                                                            <option value="{{ $complexity }}" selected>@if($complexity == 11) Custom @else {{ $complexity }} @endif</option>
                                                        @else
                                                            <option value="{{ $complexity }}">@if($complexity == 11) Custom @else {{ $complexity }} @endif</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>QA Complexity:</label>
                                                <select class="form-control" name="qa_complexity">
                                                    @foreach ($qa_complexities as $qa_complexity)
                                                        @if ($unit->qa_complexity == $qa_complexity)
                                                            <option value="{{ $qa_complexity }}" selected>{{ $qa_complexity }}</option>
                                                        @else
                                                            <option value="{{ $qa_complexity }}">{{ $qa_complexity }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-4 date-field">
                                                <label>Commit Date:</label>
                                                <div class="input-group date datetimepicker">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                    <input type="text" class="form-control committed_at" name="committed_at">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Task Type:</label>
                                                <select class="form-control" name="unit_type">
                                                    @foreach ($task_types as $key => $task_type)
                                                        @if ($unit->type == $task_type)
                                                            <option value="{{ $key }}" selected>{{ $task_type }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $task_type }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Ad Type:</label>
                                                <select class="form-control" name="ad_type">
                                                    @foreach ($ad_types as $key => $ad_type)
                                                        @if ($unit->ad_type == $ad_type)
                                                            <option value="{{ $key }}" selected>{{ $ad_type }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $ad_type }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Priority:</label>
                                                <select class="form-control" name="priority">
                                                    @foreach ($priorities as $key => $priority)
                                                        @if ($unit->priority == $priority)
                                                            <option value="{{ $key }}" selected>{{ $priority }}</option>
                                                        @else
                                                            <option value="{{ $key }}">{{ $priority }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-10 col-sm-offset-2">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Billable:</label>
                                                <div class="checkbox checkbox-primary">
                                                    @if ($unit->billable)
                                                        <input type="checkbox" name="billable" checked>
                                                    @else
                                                        <input type="checkbox" name="billable">
                                                    @endif
                                                    <label>
                                                        Billable
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Client Contact:</label>
                                    <div class="col-sm-10">
                                        {{-- <select class="form-control" name="client_contact_id">
                                            @foreach ($client_contacts as $client_contact)
                                                <option value="{{ $client_contact->id }}">{{ $client_contact->name }}</option>
                                            @endforeach
                                        </select> --}}
                                        <input type="text" class="form-control client-contact" name="client_contact" value="{{ $unit->clientContact->name }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Creative Brief:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control client-contact" name="creative_brief" value="{{  $unit->creative }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Preview link:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="preview_link" value="{{ $unit->preview }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">QA report link:</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="qa_report_link" value="{{ $unit->qa_report }}">
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <input type="submit" class="btn btn-info" name="edit" value="Save">
                                <a href="/unit/{{ $unit->id }}" class="btn btn-white">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{--<div class="col-lg-3">--}}
            {{--<div class="ibox">--}}
                {{--<div class="ibox-title">--}}
                    {{--<h5>Instructions</h5>--}}
                {{--</div>--}}
                {{--<div class="ibox-content">--}}
                    {{--<p>Under Construction</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
@endsection

@section('scripts')
    {!! Html::script('js/plugins/auto-complete/auto-complete.js') !!}
    {!! Html::script('js/plugins/moment/moment.min.js') !!}
    {!! Html::script('js/plugins/validate/jquery.validate.min.js') !!}
    {!! Html::script('js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') !!}
<script>
    $(document).ready(function () {
        $('.datetimepicker').datetimepicker({
            defaultDate: new Date("{{ $unit->committed_at }}")
        });

        $("#form").validate({
            rules: {
                "name": {
                    required: true
                },
                "committed_at": {
                    required: true,
                    date: true
                },
                "preview": {
                    url: true
                },
                "qa_report": {
                    url: true
                }
            },
            errorPlacement: function (error, element) {
                if (element.hasClass('committed_at')) {
                    error.appendTo(element.parents(".date-field"));
                } else {
                    error.insertAfter(element);
                }
            }
        });

        var client_contact_list = new Array();

        var partner_id = "{{ $unit->partner->id }}";
        var partner_url = "{{ url('partner') }}";
        partner_url = partner_url + "/" + partner_id + "/client-contacts"

        $.ajax({
            type: "GET",
            url: partner_url,
            cache: false,
            success: function (data) {
                for (var j = 0; j < data.length; j++) {
                    client_contact_list.push(data[j]);
                }
            }
        });

        $(".client-contact").keyup(function (e) {
            $(".client-contact").autoComplete({
                minChars: 2,
                source: function(term, suggest) {
                    term = term.toLowerCase();
                    var choices = client_contact_list;
                    var matches = [];
                    for (i=0; i<choices.length; i++)
                        if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i]);
                    suggest(matches);
                }
            });
        });
    });
</script>
@endsection