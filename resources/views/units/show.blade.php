@extends('layouts.master')

@section('styles')
    <!-- Sweet Alert -->
    {!! Html::style('css/plugins/sweetalert/sweetalert.css') !!}
    {!! Html::style('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}
@endsection

@section('overrides')
    <style type="text/css">
        .table > tbody > tr > td {
            border-top: none;
            color: #666666;
            padding: 8px;
        }

        tbody > tr > td > i {
            margin-right: 8px;
            color: #3d4d5d;
        }

        tbody > tr > td.special-label {
            width: 32%;
            padding-left: 0px;
        }

        .phases {
            opacity: 0.3;
            -webkit-border-radius:4px;
            -moz-border-radius:4px;
            border-radius:4px;
        }

        #review {
            color: #3c763d;
            background-color: #dff0d8;
        }

        #development {
            color: #31708f;
            background-color: #d9edf7;
        }

        #qa {
            color: #8a6d3b;
            background-color: #fcf8e3;
            border-color: #faebcc;
        }

        #revision {
            background-color: #5bc0de;
            border-color: #46b8da;
        }
#norevision{

}
        #close {
            color: #a94442;
            background-color: #f2dede;
        }
    </style>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-8">
            <h2>Unit Detail</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/campaign">Campaigns</a>
                </li>
                <li>
                    <a href="/campaign/{{ $unit->campaign->id }}">{{ str_limit($unit->campaign->name, $limit = 32) }}</a>
                </li>
                <li class="active">
                    <strong>{{ str_limit($unit->name, $limit = 32) }}</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-4">
            <div class="title-action">
                <form action="/unit/{{ $unit->id }}" method="POST" class="form-horizontal" id="unit-show-form">
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <a href="/unit/{{ $unit->id }}/edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit </a>
                    <button type="button" class="btn btn-danger" id="unit-delete"><i class="fa fa-times "></i> Delete
                    </button>
                </form>
            </div>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        {{--JM TODO--}}
        <div class="row padding-bottom-20px">

            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div id="review" class="ibox-title phases">
                        <h5>Review</h5>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div id="development" class="ibox-title phases">
                        <h5>Development</h5>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div id="qa" class="ibox-title phases">
                        <h5>QA</h5>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div id="revision" class="ibox-title phases">
                        <h5>Revision</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div id="norevision" class="ibox-title phases">
                        <h5>No Revision</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="ibox float-e-margins">
                    <div id="close" class="ibox-title phases">
                        <h5>Close</h5>
                    </div>
                </div>
            </div>

        </div>
        {{--JM--}}
        <div class="row">
            <div class="col-md-4">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>{{ str_limit($unit->name, $limit = 32) }}</h5>
                        <span class="label label-primary pull-right">{{ $unit->id }}</span>
                    </div>
                    <div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h4>Unit Details</h4>
                                    <table class="table">
                                        <tbody>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-user"></i> Assignee</td>
                                            <td>:</td>
                                            @if ($unit->assignee)
                                                <td>
                                                    <a href="/employee/{{ $unit->assignee->id }}">{{ $unit->assignee->name }}</a>
                                                </td>
                                            @else
                                                <td>No one is assigned</td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-picture-o"></i> Ad-Type</td>
                                            <td>:</td>
                                            <td>{{ $unit->ad_type }}</td>
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-cog"></i> Status</td>
                                            <td>:</td>
                                            <td>{{ $unit->status }}</td>
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-level-up"></i> Priority</td>
                                            <td>:</td>
                                            <td>{{ $unit->priority }}</td>
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-caret-square-o-up"></i> Task Type
                                            </td>
                                            <td>:</td>
                                            <td>{{ $unit->type }}</td>
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-sort-numeric-asc"></i> Complexity
                                            </td>
                                            <td>:</td>
                                            <td>@if($unit->complexity == 11){{'Custom'}}@else{{ $unit->complexity }}@endif</td>
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-sort-numeric-asc"></i>QA
                                                Complexity
                                            </td>
                                            <td>:</td>
                                            <td>{{ $unit->qa_complexity }}</td>
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-external-link"></i> Creative Brief
                                            </td>
                                            <td>:</td>
                                            @if ($unit->campaign->creative_brief)
                                                <td><a href="{{ $unit->campaign->creative_brief}}" target="_blank">Creative
                                                        Brief</a></td>
                                            @else
                                                <td><span>No Creative Brief</span>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-link"></i> Preview link</td>
                                            <td>:</td>
                                            @if ($unit->preview)
                                                <td><a href="{{ $unit->preview_link }}" target="_blank">Unit Preview</a>
                                                </td>
                                            @else
                                                <td><a data-toggle="modal" href="#preview-form"> Add preview link</a>
                                                </td>
                                            @endif
                                        </tr>
                                        <tr>
                                            <td class="special-label"><i class="fa fa-file-text-o"></i> Report link</td>
                                            <td>:</td>
                                            @if ($unit->qa_report)
                                                <td><a href="{{ $unit->qa_report_link }}" target="_blank">QA Report</a>
                                                </td>
                                            @else
                                                <td><a data-toggle="modal" href="#report-form"> Add report link</a></td>
                                            @endif
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div id="creative-form" class="modal fade" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-12"><h3 class="m-t-none m-b">Add Unit
                                                            Preview</h3>
                                                        <p>Copy the URL from "creative brief" from campaign.</p>
                                                        <form action="/unit/{{ $unit->id }}" method="POST" id="action"
                                                              role="form">
                                                            <input type="hidden" name="_token"
                                                                   value="{{ csrf_token() }}">
                                                            <input type="hidden" name="_method" value="PUT">
                                                            <div class="form-group">
                                                                <label>Creative brief</label>
                                                                <input type="text" name="creative"
                                                                       placeholder="http://www.something.com/"
                                                                       class="form-control">
                                                            </div>
                                                            <div>
                                                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs"
                                                                        type="submit"><strong>Submit</strong></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="preview-form" class="modal fade" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-12"><h3 class="m-t-none m-b">Add Unit
                                                            Preview</h3>
                                                        <p>Copy the URL from "creative preview" to save the url.</p>
                                                        <form action="/unit/{{ $unit->id }}" method="POST" id="action"
                                                              role="form">
                                                            <input type="hidden" name="_token"
                                                                   value="{{ csrf_token() }}">
                                                            <input type="hidden" name="_method" value="PUT">
                                                            <div class="form-group">
                                                                <label>Preview Link</label>
                                                                <input type="text" name="preview"
                                                                       placeholder="http://www.something.com/"
                                                                       class="form-control">
                                                            </div>
                                                            <div>
                                                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs"
                                                                        type="submit"><strong>Submit</strong></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="report-form" class="modal fade" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-sm-12"><h3 class="m-t-none m-b">Add QA Report</h3>
                                                        <p>Copy the URL from Google Doc Sheet to save the url.</p>
                                                        <form action="/unit/{{ $unit->id }}" method="POST" id="action"
                                                              role="form">
                                                            <input type="hidden" name="_token"
                                                                   value="{{ csrf_token() }}">
                                                            <input type="hidden" name="_method" value="PUT">
                                                            <div class="form-group">
                                                                <label>QA Report Link</label>
                                                                <input type="text" name="qa_report"
                                                                       placeholder="http://www.something.com/"
                                                                       class="form-control">
                                                            </div>
                                                            <div>
                                                                <button class="btn btn-sm btn-primary pull-right m-t-n-xs"
                                                                        type="submit"><strong>Submit</strong></button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <span>Unit progress:</span>
                                    <div class="stat-percent">{{ $unit->progress }}%</div>
                                    <div class="progress progress-mini">
                                        <div style="width: {{ $unit->progress }}%;" class="progress-bar"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="user-button">
                                <form action="/unit/{{ $unit->id }}" method="POST" id="action" role="form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_method" value="PUT">
                                    <label>Assign Ticket:</label>
                                    <div class="row m-b">
                                        <div class="col-lg-9">
                                            <select {{ (($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) OR $unit->queueNotes()->count() < 1) ? 'disabled' : '' }} class="form-control input-sm"
                                                    name="assignee">
                                                <option>---Select---</option>
                                                @foreach ($team_members as $member)
                                                    <option value="{{ $member->id }}">{{ $member->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-3">
                                            <button {{ (($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) OR $unit->queueNotes()->count() < 1) ? 'disabled' : '' }} type="submit"
                                                    name="assign" value="queue_note"
                                                    class="btn btn-primary btn-sm btn-block">Assign
                                            </button>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    @if ($unit->assignee)
                                        @if ($unit->assignee->id == $employee->id)
                                            @if (($employee->ic_tag > $employee::IC_QA) OR ($employee->role_tag > $employee::ROLE_IC))
                                                <label>Work Type:</label>
                                                <div class="row m-b">
                                                    <div class="col-md-12">
                                                        <select class="form-control input-sm" name="work">
                                                            <option value="4">Review</option>
                                                            <option value="3">Qa</option>
                                                            <option value="2">Dev</option>
                                                            <option value="1">Creative</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            @endif
                                        @endif
                                    @endif
                                    <label>Action Type:</label>
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if ($unit->assignee)
                                                @if ((($employee->ic_tag > $employee::IC_DEV) OR ($employee->role_tag > $employee::ROLE_IC)) OR $unit->assignee->id == $employee->id)
                                                    <button type="submit" name="action" value="3"
                                                            class="btn btn-primary btn-sm btn-block"><i
                                                                class="fa fa-envelope"></i> Drop
                                                    </button>
                                                @else
                                                    <button type="submit" name="action" value="3"
                                                            class="btn btn-primary btn-sm btn-outline" disabled><i
                                                                class="fa fa-envelope"></i> Drop
                                                    </button>
                                                @endif
                                            @else
                                                <button type="submit" name="action" value="2"
                                                        class="btn btn-primary btn-sm btn-block" @if(!in_array('Grab', $employee->getWorkTypes($phase))) disabled @endif>
                                                    <i
                                                            class="fa fa-envelope"></i> Grab
                                                </button>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if ($unit->status == "In Progress")
                                                <button type="submit" name="action" value="5"
                                                        class="btn btn-warning btn-sm btn-block" disabled><i
                                                            class="fa fa-envelope"></i> In Progress
                                                </button>
                                            @else
                                                @if ($unit->assignee and ($unit->assignee->id == $employee->id or ($employee->ic_tag != $employee::IC_DEV and $employee->ic_tag != $employee::IC_QA)))
                                                    <button type="submit" name="action" value="5"
                                                            class="btn btn-primary btn-sm btn-block" @if(!in_array('In Progress', $employee->getWorkTypes($phase))) disabled {{ $phase }} @endif><i
                                                                class="fa fa-envelope"></i> In Progress
                                                    </button>
                                                @else
                                                    <button type="submit" name="action" value="5"
                                                            class="btn btn-warning btn-sm btn-block" disabled><i
                                                                class="fa fa-envelope"></i> In Progress
                                                    </button>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            @if ($unit->status == "On Hold")
                                                <a data-toggle="modal" data-value="6"
                                                   class="btn btn-warning btn-sm btn-block" href="#force-notes-form"
                                                   disabled> <i class="fa fa-envelope"></i> Hold</a>
                                            @else
                                                @if ($unit->assignee and ($unit->assignee->id == $employee->id or ($employee->ic_tag != $employee::IC_DEV and $employee->ic_tag != $employee::IC_QA)))
                                                    <a data-toggle="modal" data-value="6"
                                                       class="btn btn-primary btn-sm btn-block"
                                                       href="#force-notes-form" @if(!in_array('On Hold', $employee->getWorkTypes($phase))) disabled @endif> <i class="fa fa-envelope"></i> Hold</a>
                                                @else
                                                    <a data-toggle="modal" data-value="6"
                                                       class="btn btn-warning btn-sm btn-block" href="#force-notes-form"
                                                       disabled> <i class="fa fa-envelope"></i> Hold</a>
                                                @endif
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            @if ($unit->status == "Info Request")
                                                <a data-toggle="modal" data-value="4"
                                                   class="btn btn-warning btn-sm btn-block" href="#force-notes-form"
                                                   disabled> <i class="fa fa-envelope"></i> Info Request</a>
                                            @else
                                                @if ($unit->assignee and ($unit->assignee->id == $employee->id or ($employee->ic_tag != $employee::IC_DEV and $employee->ic_tag != $employee::IC_QA)))
                                                    <a data-toggle="modal" data-value="4"
                                                       class="btn btn-primary btn-sm btn-block"
                                                       href="#force-notes-form" @if(!in_array('Info Requested', $employee->getWorkTypes($phase))) disabled @endif> <i class="fa fa-envelope"></i> Info
                                                        Request</a>
                                                @else
                                                    <a data-toggle="modal" data-value="4"
                                                       class="btn btn-warning btn-sm btn-block" href="#force-notes-form"
                                                       disabled> <i class="fa fa-envelope"></i> Info Request</a>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="force-notes-form" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <form action="/unit/{{ $unit->id }}" method="POST" id="action"
                                                          role="form">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <div class="form-group">
                                                            <label>Please leave a note</label>
                                                            <textarea name="note" class="form-control" rows="5"
                                                                      placeholder="Leave note here..."
                                                                      required></textarea>
                                                        </div>
                                                        <div>
                                                            <input type="hidden" id="status_submit" name="action">
                                                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs"
                                                                    name="queue_note" value="queue_note" type="submit">
                                                                <strong>Submit</strong></button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div class="dev-note">
                                <form action="/unit/{{ $unit->id }}" method="POST" id="queue_note" role="form">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="_method" value="PUT">
                                    <div class="row m-b">
                                        <div class="col-lg-6">
                                            <label>Phase:</label>
                                            <select class="form-control input-sm" name="queue">
                                                <option value="0">--Phase--</option>

                                                @if ($employee->ic_tag != $employee::IC_DEV && $employee->ic_tag != $employee::IC_QA)
                                                    <option value="1">Development</option>
                                                    <option value="2">QA</option>
                                                    <option value="3">Revision</option>
                                                    <option value="4">No Revision</option>
                                                    <option value="5">Close</option>
                                                @elseif ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE)
                                                    <option value="2">QA</option>
                                                @elseif ($employee->ic_tag == $employee::IC_QA)
                                                    <option value="3">Revision</option>
                                                    <option value="4">No Revision</option>
                                                    <option value="5">Close</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label>Rev Count:</label>
                                            @if ($employee->ic_tag > $employee::IC_DEV OR $employee->ic_tag == $employee::IC_QA)
                                                <input type="text" class="form-control input-sm" name="revision_count">
                                            @else
                                                <input type="text" class="form-control input-sm" name="revision_count"
                                                       disabled>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label>Note:</label>
                                            <textarea name="note" class="form-control" rows="5"
                                                      placeholder="Leave note here..."></textarea>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button type="submit" id="queue_note_submit" name="queue_note"
                                                    value="queue_note" class="btn btn-success btn-sm btn-block">Submit
                                            </button>
                                        </div>
                                        <div class="col-lg-12">
                                            <a data-toggle="modal" class="btn btn-sm btn-danger btn-block"
                                               href="#unit-issue">Add issue</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="unit-issue" class="modal fade" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-12"><h3 class="m-t-none m-b">Add Unit Issue</h3>
                                                    <form role="form" method="POST"
                                                          action="/unit/{{ $unit->id }}/issue">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <div class="row">
                                                            <div class="col-lg-9">
                                                                <div class="form-group">
                                                                    <label>Issue Name:</label>
                                                                    <input type="text" placeholder="Issue name"
                                                                           class="form-control" name="issue_name">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <div class="form-group">
                                                                    <label>Flag:</label>
                                                                    <select class="form-control" name="issue_flag">
                                                                        <option value="1">Valid</option>
                                                                        <option value="2">Invalid</option>
                                                                        <option value="3">Investigation</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row m-b">
                                                            <div class="col-lg-12">
                                                                <label>Note:</label>
                                                                <textarea rows="5" class="form-control"
                                                                          placeholder="Notes.."
                                                                          name="issue_note"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <button class="btn btn-sm btn-success pull-right m-t-n-xs"
                                                                        type="submit"><strong>Submit</strong></button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-laptop"></i> Queues and
                                Notes</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-2"><i class="fa fa-desktop"></i> Activity</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-3"><i class="fa fa-desktop"></i> Issues</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-4"><i class="fa fa-desktop"></i> Revision</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-5"><i class="fa fa-desktop"></i> Audit</a></li>
                        <li class=""><a data-toggle="tab" href="#tab-6"><i class="fa fa-desktop"></i> Audit Activity</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane active">
                            <div class="panel-body">
                                <div class="feed-activity-list">
                                    @forelse ($queue_notes as $queue_note)
                                        <div class="feed-element">
                                            <a href="#" class="pull-left">
                                                <img alt="image" class="img-circle"
                                                     src="{{ $queue_note->employee->avatar }}">
                                            </a>
                                            <div class="media-body ">
                                                <small class="pull-right">{{$queue_note->created_at->diffForHumans()}}</small>
                                                <strong>{{ $queue_note->employee->name }}</strong> {!! ($queue_note->queue) ? "changed queue to <strong>".$queue_note->queue."</strong>" : "" !!}
                                                <br>
                                                <small class="text-muted">{{$queue_note->created_at->toDayDateTimeString()}}</small>
                                                @if ($queue_note->note)
                                                    <div class="well">
                                                        {{ $queue_note->note }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    @empty
                                        <div>
                                            No queues or notes yet.
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Work</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Employee</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($unit_times as $unit_time)
                                        <tr>
                                            <td>
                                                <span class="label label-primary"><i
                                                            class="fa fa-check"></i> {{ $unit_time->action }}</span>
                                            </td>
                                            <td>
                                                {{ $unit_time->work }}
                                            </td>
                                            <td>
                                                {{ $unit_time->start_at }}
                                            </td>
                                            <td>
                                                {{ $unit_time->end_at }}
                                            </td>
                                            <td>
                                                <p class="small">
                                                    {{ $unit_time->employee->name }}
                                                </p>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td rowspan="5">No records yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane">
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Flag</th>
                                        <th>Created By</th>
                                        <th>Created At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($unit_issues as $unit_issue)
                                        <tr>
                                            <td>{{ $unit_issue->name }}</td>
                                            <td>{{ $unit_issue->flag }}</td>
                                            <td>{{ $unit_issue->creator->name }}</td>
                                            <td>{{ $unit_issue->created_at }}</td>
                                            <td></td>
                                        </tr>
                                    @empty
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div id="tab-4" class="tab-pane">
                            <div class="panel-body">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Flag</th>
                                        <th>Created By</th>
                                        <th>Created At</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div id="tab-5" class="tab-pane">
                            <div class="panel-body">
                                @if ($unit_audit)
                                    <p>Last Audit by: <a
                                                href="/employee/{{ $unit_audit->employee->id }}">{{ $unit_audit->employee->name or '' }}</a>
                                    </p>
                                    <div class="hr-line-dashed"></div>
                                @else
                                    <p>Unit has not been audited yet.</p>
                                @endif
                                <form action="/unit/{{ $unit->id }}/audit" method="POST" class="form-horizontal">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <div class="row">
                                        <label class="col-sm-2 control-label">Work Estimate:</label>
                                        <div class="col-sm-3">
                                            <select {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} name="work_estimate_tag"
                                                    class="form-control m-b">
                                                @foreach ($audit_flags as $f => $flag)
                                                    @if ($unit_audit && $unit_audit->work_estimate_tag == $f)
                                                        <option value="{{ $f }}" selected>{{ $flag }}</option>
                                                    @else
                                                        <option value="{{ $f }}">{{ $flag }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <textarea
                                                    {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} name="work_estimate_notes"
                                                    class="form-control" rows="3"
                                                    placeholder="Notes...">{{ $unit_audit->work_estimate_notes or '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="row">
                                        <label class="col-sm-2 control-label">Overall Quality:</label>
                                        <div class="col-sm-3">
                                            <select {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} name="overall_quality_tag"
                                                    class="form-control m-b">
                                                @foreach ($audit_flags as $f => $flag)
                                                    @if ($unit_audit && $unit_audit->overall_quality_tag == $f)
                                                        <option value="{{ $f }}" selected>{{ $flag }}</option>
                                                    @else
                                                        <option value="{{ $f }}">{{ $flag }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <textarea
                                                    {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} name="overall_quality_notes"
                                                    class="form-control" rows="3"
                                                    placeholder="Notes...">{{ $unit_audit->overall_quality_notes or '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="row">
                                        <label class="col-sm-2 control-label">Following Instructions:</label>
                                        <div class="col-sm-3">
                                            <select {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} name="following_instructions_tag"
                                                    class="form-control m-b">
                                                @foreach ($audit_flags as $f => $flag)
                                                    @if ($unit_audit && $unit_audit->following_instructions_tag == $f)
                                                        <option value="{{ $f }}" selected>{{ $flag }}</option>
                                                    @else
                                                        <option value="{{ $f }}">{{ $flag }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <textarea
                                                    {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} name="following_instructions_notes"
                                                    class="form-control" rows="3"
                                                    placeholder="Notes...">{{ $unit_audit->following_instructions_notes or '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="row">
                                        <label class="col-sm-2 control-label">Process Adherence:</label>
                                        <div class="col-sm-3">
                                            <select {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} name="process_adherence_tag"
                                                    class="form-control m-b">
                                                @foreach ($audit_flags as $f => $flag)
                                                    @if ($unit_audit && $unit_audit->process_adherence_tag == $f)
                                                        <option value="{{ $f }}" selected>{{ $flag }}</option>
                                                    @else
                                                        <option value="{{ $f }}">{{ $flag }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <textarea
                                                    {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} name="process_adherence_notes"
                                                    class="form-control" rows="3"
                                                    placeholder="Notes...">{{ $unit_audit->process_adherence_notes or '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-primary"
                                                    type="submit" {{ ($employee->ic_tag == $employee::IC_DEV OR $employee->ic_tag == $employee::IC_CREATIVE) ? 'disabled' : '' }} >
                                                Save
                                                changes
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        {{-- JM --}}
                        <div id="tab-6" class="tab-pane">
                            <div class="panel-body">

                                @forelse ($unit_audit_notes as $unit_audit_note)
                                    <div class="feed-element">
                                        <a href="#" class="pull-left">
                                            <img alt="image" class="img-circle"
                                                 src="{{ $unit_audit_note->employee->avatar }}">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right">{{$unit_audit_note->created_at->diffForHumans()}}</small>
                                            <strong>{{ $unit_audit_note->employee->name }}</strong>
                                            {{--{{$unit_audit_note}}--}}
                                            <br>
                                            @if($unit_audit_note->work_estimate_tag)
                                                {!! "changed Work Estimate to "!!}
                                                <strong>
                                                    @if($unit_audit_note->work_estimate_tag == 1)
                                                        {!! 'Not Done' !!}
                                                    @elseif($unit_audit_note->work_estimate_tag == 2)
                                                        {!! 'Pass' !!}
                                                    @elseif($unit_audit_note->work_estimate_tag == 3)
                                                        {!! 'Fail' !!}
                                                    @endif
                                                </strong>
                                                <br>
                                                @if ($unit_audit_note->work_estimate_notes)
                                                    <div class="well">
                                                        <span>Work Estimate Note:</span><br><strong>
                                                            {{ $unit_audit_note->work_estimate_notes }}
                                                        </strong>
                                                    </div>
                                                @endif

                                            @endif

                                            @if($unit_audit_note->overall_quality_tag)
                                                {!! "changed Overall Quality to "!!}
                                                <strong>
                                                    @if($unit_audit_note->overall_quality_tag == 1)
                                                        {!! 'Not Done' !!}
                                                    @elseif($unit_audit_note->overall_quality_tag == 2)
                                                        {!! 'Pass' !!}
                                                    @elseif($unit_audit_note->overall_quality_tag == 3)
                                                        {!! 'Fail' !!}
                                                    @endif

                                                </strong>
                                                <br>
                                                @if ($unit_audit_note->overall_quality_notes)
                                                    <div class="well">
                                                        <span>Overall Quality Note:</span><br><strong>
                                                            {{ $unit_audit_note->overall_quality_notes }}
                                                        </strong>
                                                    </div>
                                                @endif

                                            @endif

                                            @if($unit_audit_note->following_instructions_tag)
                                                {!! "changed Following Instructions to "!!}
                                                <strong>
                                                    @if($unit_audit_note->following_instructions_tag == 1)
                                                        {!! 'Not Done' !!}
                                                    @elseif($unit_audit_note->following_instructions_tag == 2)
                                                        {!! 'Pass' !!}
                                                    @elseif($unit_audit_note->following_instructions_tag == 3)
                                                        {!! 'Fail' !!}
                                                    @endif
                                                </strong>
                                                <br>
                                                @if ($unit_audit_note->following_instructions_note)
                                                    <div class="well">
                                                        <span>Following Instructions Note:</span><br><strong>
                                                            {{ $unit_audit_note->following_instructions_note }}
                                                        </strong>
                                                    </div>
                                                @endif

                                            @endif

                                            @if($unit_audit_note->process_adherence_tag)
                                                {!! "changed Process Adherence to "!!}
                                                <strong>
                                                    @if($unit_audit_note->process_adherence_tag == 1)
                                                        {!! 'Not Done' !!}
                                                    @elseif($unit_audit_note->process_adherence_tag == 2)
                                                        {!! 'Pass' !!}
                                                    @elseif($unit_audit_note->process_adherence_tag == 3)
                                                        {!! 'Fail' !!}
                                                    @endif
                                                </strong>
                                                <br>
                                                @if ($unit_audit_note->process_adherence_notes)
                                                    <div class="well">
                                                        <span>Process Adherence Note:</span><br><strong>
                                                            {{ $unit_audit_note->process_adherence_notes }}
                                                        </strong>
                                                    </div>
                                                @endif

                                            @endif

                                        </div>
                                    </div>
                                @empty
                                    <div>
                                        Unit has not been audited yet.
                                    </div>
                                @endforelse

                            </div>
                        </div>
                        {{-- JM --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Sweet alert -->
    {!! Html::script('js/plugins/sweetalert/sweetalert.min.js') !!}
    <script type="text/javascript">
        var phases = "@if ($unit->queueNotes()->count()) {{ $unit->queueNotes()->orderBy('created_at', 'desc')->first()->queue }} @else Review @endif";
        $(document).ready(function () {
            phases = phases.replace(/\s+/g, '');
            console.log(phases);
            var selector = $('#' + phases.toLowerCase());

            selector.css({'opacity': '1', 'border': '2px solid #ed5565'});
            $("#unit-delete").on("click", function (event) {
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this unit!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false
                }, function () {
                    $("#unit-show-form").submit();
                });
            });

            if (phases == 'NoRevision') {
                phases = phases.replace(phases, 'No Revision');
            }
            $('select[name=queue] option').filter(function () {
                return ($(this).text() == phases);
            }).prop('selected', true);

            $("#queue_note_submit").on("click", function (event) {
                var queue = $("select[name=queue]").val();
                var note = $("textarea[name=note]").val();

                if (queue == 0 && note.length == 0) {
                    event.preventDefault();

                    swal({
                        title: "Notice!",
                        text: "Please choose a phase or add a note!",
                        type: "warning",
                    });
                }
            });

            $('#force-notes-form').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var val = button.data('value');
                var modal = $(this);
                modal.find('.modal-body #status_submit').val(val);
            })
        });
    </script>
@endsection
