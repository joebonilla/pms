@extends('layouts.master')

@section('styles')
    {!! Html::style('css/plugins/datetimepicker/bootstrap-datetimepicker.min.css') !!}
    {!! Html::style('css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') !!}
    {!! Html::style('css/plugins/auto-complete/auto-complete.css') !!}
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Add/New Unit(s)</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="/campaign/{{ $campaign->id }}">{{ $campaign->name }}</a>
                </li>
                <li class="active">
                    <strong>Unit(s)</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Create Units</h5>
                    </div>
                    <div class="ibox-content">
                        <form action="/unit" method="POST" class="form-horizontal" id="form">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Campaign:</label>
                                <div class="col-lg-9">
                                    <input type="text" disabled="" value="{{ $campaign->name }}" class="form-control">
                                    <input type="hidden" name="campaign_id" value="{{ $campaign->id }}">
                                </div>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <div id="units">
                                <div class="unit" id="unit-0">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Unit name:</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control unit-name" name="units[0][name]">
                                        </div>
                                        <div class="col-sm-2 text-right">
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-rounded add-unit" type="button"><i
                                                            class="fa fa-plus"></i></button>
                                                <button class="btn btn-danger btn-rounded remove-unit" type="button"
                                                        disabled><i class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-10 col-sm-offset-2">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Complexity:</label>
                                                    <select class="form-control complexity" name="units[0][complexity]">
                                                        <option value="0.03">0.03</option>
                                                        <option value="0.05">0.05</option>
                                                        <option value="0.07">0.07</option>
                                                        <option value="0.1">0.1</option>
                                                        <option value="0.13">0.13</option>
                                                        <option value="0.5">0.5</option>
                                                        <option value="0.25">0.25</option>
                                                        <option value="0.50">0.50</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">Custom</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>QA Complexity:</label>
                                                    <select class="form-control qa_complexity"
                                                            name="units[0][qa_complexity]">
                                                        <option value="0.2">0.2</option>
                                                        <option value="0.5">0.5</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                    </select>
                                                </div>

                                                <div class="col-sm-4 date-field">
                                                    <label>Commit Date:</label>
                                                    <div class="input-group date datetimepicker">
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                        <input type="text" class="form-control committed-at"
                                                               name="units[0][committed_at]">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-10 col-sm-offset-2">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Task Type:</label>
                                                    <select class="form-control task-type" name="units[0][type]">
                                                        <option value="1">Initial Build</option>
                                                        <option value="2">Creative Iteration</option>
                                                        <option value="3">Client Revision</option>
                                                        <option value="4">Client Issue</option>
                                                        <option value="5">File Path Update</option>
                                                        <option value="6">Tracking Update</option>
                                                        <option value="7">IPD Build</option>
                                                        <option value="8">Prod QA</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Ad Type:</label>
                                                    <select class="form-control ad-type" name="units[0][ad_type]">
                                                        <option value="1">Inpage</option>
                                                        <option value="2">Expanding</option>
                                                        <option value="3">Pushdown</option>
                                                        <option value="4">MDE</option>
                                                        <option value="5">Floating</option>
                                                        <option value="6">Inpage + Float</option>
                                                        <option value="7">IM Expand</option>
                                                        <option value="8">Tandem</option>
                                                        <option value="9">Super EVA</option>
                                                        <option value="10">Multi-floating</option>
                                                        <option value="11">Peelback</option>
                                                        <option value="12">Wallpaper/Reskin</option>
                                                        <option value="13">Catfish</option>
                                                        <option value="14">In-stream</option>
                                                        <option value="15">VPAID Linear</option>
                                                        <option value="16">VPAID Non-Linear</option>
                                                        <option value="17">Lightbox</option>
                                                        <option value="18">IHB</option>
                                                        <option value="19">Mastbox</option>
                                                        <option value="20">Masthead</option>
                                                        <option value="21">Custom</option>
                                                        <option value="22">Web</option>
                                                        <option value="23">App</option>
                                                        <option value="24">Static Banner</option>
                                                        <option value="25">Animated Gif Banner</option>
                                                        <option value="26">Desktop Landing Page</option>
                                                        <option value="27">Mobile Landing Page</option>
                                                        <option value="28">Standard Flash</option>
                                                        <option value="29">Ad Mail</option>
                                                        <option value="30">iOpener [DBG]</option>
                                                        <option value="31">Scene Stealer [DBG]</option>
                                                        <option value="32">Video Edit [TVG]</option>
                                                        <option value="33">Mock Expand [InnoDes]</option>
                                                        <option value="34">Mock Ad Extender [InnoDes]</option>
                                                        <option value="35">Mock Ad Selector [InnoDes]</option>
                                                        <option value="36">Mock Canvas [InnoDes]</option>
                                                        <option value="37">Live Expand [InnoDes]</option>
                                                        <option value="38">Live Ad Extender [InnoDes]</option>
                                                        <option value="39">Live Ad Selector [InnoDes]</option>
                                                        <option value="40">Live Canvas [InnoDes]</option>
                                                        <option value="41">Hover</option>
                                                        <option value="42">Statics : Designer [TVG]</option>
                                                        <option value="43">Statics : Developer [TVG]</option>
                                                        <option value="44">Progressive/Billboard : Native H5 [TVG]</option>
                                                        <option value="45">Progressive/Billboard : Flash Talking [TVG]</option>
                                                        <option value="46">Progressive/Billboard : GWD [TVG]</option>
                                                        <option value="47">Progressive/Billboard : Addroid [TVG]</option>
                                                        <option value="48">Endplate Animation : Native H5  [TVG]</option>
                                                        <option value="49">Endplate Animation : Sizmek  [TVG]</option>
                                                        <option value="50">Endplate Animation : Flash Talking [TVG]</option>
                                                        <option value="51">Endplate Animation : GWD [TVG]</option>
                                                        <option value="52">Endplate Animation : Addroid [TVG]</option>
                                                        <option value="53">Pushdown/Expanding : Native H5 [TVG]</option>
                                                        <option value="54">Pushdown/Expanding : Sizmek [TVG]</option>
                                                        <option value="55">Pushdown/Expanding : GWD [TVG]</option>
                                                        <option value="56">Custom : Overlay : Native H5 [TVG]</option>
                                                        <option value="57">Custom : Overlay : Sizmek [TVG]</option>
                                                        <option value="58">Custom : Overlay : GWD [TVG]</option>
                                                        <option value="59">Custom : Responsive Ad : GWD [TVG]</option>
                                                        <option value="60">Custom : Transparent Video Ad : Native H5 [TVG]</option>
                                                        <option value="61">Custom : Transparent Video Ad : Sizmek [TVG]</option>
                                                        <option value="62">Custom : Transparent Video Ad : Flash Talking [TVG]</option>
                                                        <option value="63">Custom : Transparent Video Ad : GWD [TVG]</option>
                                                        <option value="64">Custom : Video Wall : Native H5 [TVG]</option>
                                                        <option value="65">Custom : Video Wall : Sizmek [TVG]</option>
                                                        <option value="66">Custom : Video Wall : GWD [TVG]</option>
                                                        <option value="67">Marketing [TVG]</option>
                                                        <option value="68">Expand</option>
                                                        <option value="69">Overlay</option>
                                                        <option value="70">AD Canvas</option>
                                                        <option value="71">AD Extender</option>
                                                        <option value="72">AD Selector</option>
                                                        <option value="73">End Slate</option>
                                                        <option value="74">SWF Only</option>
                                                        <option value="75">Pre-roll</option>
                                                        <option value="76">Advance Creative</option>
                                                        <option value="77">VAST</option>
                                                    </select>
                                                </div>

                                                <div class="col-sm-4">
                                                    <label>Priority:</label>
                                                    <select class="form-control priority" name="units[0][priority]">
                                                        <option value="1">Low</option>
                                                        <option value="2">Normal</option>
                                                        <option value="3">High</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-10 col-sm-offset-2">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Client Contact:</label>
                                                    <input type="text" class="form-control client-contact"
                                                           name="units[0][client_contact_name]">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Quantity:</label>
                                                    <input type="text" class="form-control quantity"
                                                           name="units[0][quantity]" value="1">
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Billable:</label>
                                                    <div class="checkbox checkbox-primary">
                                                        <input type="checkbox" name="units[0][billable]" checked>
                                                        <label>
                                                            Billable
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <button class="btn btn-primary" type="submit">Create</button>
                                    <a href="/campaign/{{ $campaign->id }}" class="btn btn-white">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            {{--<div class="col-lg-3">--}}
            {{--<div class="ibox">--}}
            {{--<div class="ibox-title">--}}
            {{--<h5>Instructions</h5>--}}
            {{--</div>--}}
            {{--<div class="ibox-content">--}}
            {{--<p>Under Construction</p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
@endsection

@section('scripts')
    {!! Html::script('js/plugins/auto-complete/auto-complete.js') !!}
    {!! Html::script('js/plugins/moment/moment.min.js') !!}
    {!! Html::script('js/plugins/validate/jquery.validate.min.js') !!}
    {!! Html::script('js/plugins/datetimepicker/bootstrap-datetimepicker.min.js') !!}
    <script>
        $(document).ready(function () {
            $('.datetimepicker').datetimepicker({
                minDate: new Date(),
                useCurrent : true
            });

            $("#form").validate({
                rules: {
                    "units[0][name]": {
                        required: true
                    },
                    "units[0][committed_at]": {
                        required: true,
                        date: true
                    },
                    "units[0][client_contact_name]": {
                        required: true
                    }
                },
                errorPlacement: function (error, element) {
                    if (element.hasClass('committed-at')) {
                        error.appendTo(element.parents(".date-field"));
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $(document).on('click', '.add-unit', function (e) {
                if ($(this).siblings('.remove-unit').prop('disabled')) {
                    $(this).siblings('.remove-unit').prop('disabled', false);
                }

                // get last unit
                var last_unit = $('.unit').last();
                var last_unit_index = last_unit.attr('id').replace(/\D/g, '');
                var next_unit_index = parseInt(last_unit_index) + 1;

                // set new unit
                var new_unit = $("#unit-" + last_unit_index).clone();
                new_unit.attr('id', "unit-" + next_unit_index);

                // change unit names
                // increment the index from last unit and set it to all select and input fields names
                new_unit.find('select').each(function () {
                    this.name = this.name.replace('[' + last_unit_index + ']', '[' + next_unit_index + ']');
                });
                new_unit.find('input').each(function () {
                    this.name = this.name.replace('[' + last_unit_index + ']', '[' + next_unit_index + ']');
                });

                // append cloned unit
                $("#units").append(new_unit);

                // refresh plugins to be bind to newly cloned objects
                $('.datetimepicker').datetimepicker({
                    minDate: new Date(),
                    useCurrent : true
                });

                $('.unit-name').each(function (e) {
                    $(this).rules("add", {
                        required: true
                    });
                });

                $('.committed-at').each(function (e) {
                    $(this).rules("add", {
                        required: true,
                        date: true
                    });

                    $(this).validate({
                        errorPlacement: function (error, element) {
                            if (element.hasClass('committed-at')) {
                                error.appendTo(element.parents(".date-field"));
                            } else {
                                error.insertAfter(element);
                            }
                        }
                    });
                });

                $('.client-contact').each(function (e) {
                    $(this).rules("add", {
                        required: true
                    });
                });

                // Duplicating last values
                // unit-name
                var last_unit_name = last_unit.find('.unit-name').val();
                var new_unit_name = next_unit_index + " - " + last_unit_name;
                new_unit.find('.unit-name').val(new_unit_name);

                // unit-complexity
                var last_unit_complexity = last_unit.find('.complexity').val();
                new_unit.find('.complexity').val(last_unit_complexity);

                // unit-priority
                var last_unit_priority = last_unit.find('.priority').val();
                new_unit.find('.priority').val(last_unit_priority);

                // unit-committed-at
                var last_unit_committed_at = last_unit.find('.committed-at').val();
                new_unit.find('.committed-at').val(last_unit_committed_at);

                // unit-task-type
                var last_unit_task_type = last_unit.find('.task-type').val();
                new_unit.find('.task-type').val(last_unit_task_type);

                // unit-ad-type
                var last_unit_ad_type = last_unit.find('.ad-type').val();
                new_unit.find('.ad-type').val(last_unit_ad_type);

                // unit-quantity
                var last_unit_quantity = last_unit.find('.quantity').val();
                new_unit.find('.quantity').val(last_unit_quantity);

                // unit-client-contact
                var last_unit_client_contact = last_unit.find('.client-contact').val();
                new_unit.find('.client-contact').val(last_unit_client_contact);

                //qa-complexity
                var last_unit_qa_complexity = last_unit.find('.qa_complexity').val();
                new_unit.find('.qa_complexity').val(last_unit_qa_complexity);
            });

            var client_contact_list = new Array();

            var partner_id = {{ $partner->id }};
            var partner_url = "{{ url('partner') }}";
            partner_url = partner_url + "/" + partner_id + "/client-contacts";

            $.ajax({
                type: "GET",
                url: partner_url,
                cache: false,
                success: function (data) {
                    for (var j = 0; j < data.length; j++) {
                        client_contact_list.push(data[j]);
                    }
                }
            });

            $(".client-contact").keyup(function (e) {
                $(".client-contact").autoComplete({
                    minChars: 2,
                    source: function (term, suggest) {
                        term = term.toLowerCase();
                        var choices = client_contact_list;
                        var matches = [];
                        for (i = 0; i < choices.length; i++)
                            if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i]);
                        suggest(matches);
                    }
                });
            });

            $(document).on('click', '.remove-unit', function (e) {
                $(this).closest('.unit').remove();

                if ($('.unit').length == 1) {
                    $('.unit .remove-unit').prop('disabled', true);
                }

                $('.unit-name').each(function (e) {
                    $(this).rules("add", {
                        required: true
                    });
                });
            });
        });
    </script>
@endsection