@extends('layouts.master')

@section('content')
<!-- <div class="row">
    <h1>{{ $ticket->name }}</h1>
    <ol class="breadcrumb">
        <li><a href="/ticket">Tickets</a></li>
        <li><a href="/ticket/{{ $ticket->id }}">{{ $ticket->name }}</a></li>
        <li class="active">Edit</li>
    </ol>
</div>
<div class="row">
    <div class="col-lg-12">
        <form action="/ticket/{{ $ticket->id }}" method="post">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <input type="text" name="name" value="{{ $ticket->name }}">
            <textarea name="description">{{ $ticket->description }}</textarea>
            <input type="text" name="unit" value="{{ $ticket->unit }}">
            
            <input type="text" name="assigned_to" value="{{ $ticket->assigned_to }}">


            <input type="submit" name="submit" value="submit">
        </form>
    </div>
</div> -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
        <h2>Ticket detail</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/ticket">Tickets</a>
            </li>
            <li>
                <a href="/ticket/{{ $ticket->id }}">{{ $ticket->title }}</a>
            </li>
            <li class="active">
                <strong>Edit</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>New Ticket</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-4">
                            <form action="/ticket/{{ $ticket->id }}" method="POST" id="form" role="form">
                                <input type="hidden" name="_method" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter a Title..." value="{{ $ticket->title }}">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" class="form-control" rows="4" placeholder="Enter a description...">{{ $ticket->description }}</textarea>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <div class="radio">
                                                <label class="radio">
                                                    <input type="radio" name="type" value="external" checked>External
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label class="radio">
                                                    <input type="radio" name="type" value="internal">Internal
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div>
                                                <select class="form-control m-b" name="subtype" id="subtype" disabled>
                                                    <option value="nothing">--select--</option>
                                                    <option value="unassigned">Meeting</option>
                                                </select>
                                            </div>
                                            <div>
                                                <input type="text" name="unit" id="unit" placeholder="Unit" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="account">Account</label>
                                    <input type="text" name="account" id="account" class="form-control" placeholder="Enter an account...">
                                </div>
                                <div class="form-group">
                                    <label for="campaign">Campaign</label>
                                    <input type="text" name="campaign" id="campaign" class="form-control" placeholder="Enter a campaign...">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="start_at">Date Started</label>
                                            <input type="datetime-local" name="start_at" class="form-control">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="committed_at">Date Committed</label>
                                            <input type="datetime-local" name="committed_at" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="text-align: right">
                                    <button class="btn btn-sm btn-primary m-t-n-xs" type="submit">
                                        <strong>Submit</strong>
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="m-b-md">
                                        <h2>TICKET NAME</h2>
                                    </div>
                                    <dl class="dl-horizontal">
                                        <dt>Status:</dt> <dd><span class="label label-primary">TICKET STATUS</span></dd>
                                    </dl>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-5">
                                    <dl class="dl-horizontal">

                                        <dt>Created by:</dt> <dd>Alex Smith</dd>
                                        <dt>Messages:</dt> <dd>  162</dd>
                                        <dt>Client:</dt> <dd><a href="#" class="text-navy"> Zender Company</a> </dd>
                                        <dt>Version:</dt> <dd>  v1.4.2 </dd>
                                    </dl>
                                </div>
                                <div class="col-lg-7" id="cluster_info">
                                    <dl class="dl-horizontal" >

                                        <dt>Last Updated:</dt> <dd>16.08.2014 12:15:57</dd>
                                        <dt>Created:</dt> <dd>  10.07.2014 23:36:57 </dd>
                                        <dt>Participants:</dt>
                                        <dd class="project-people">
                                        <a href=""><img alt="image" class="img-circle" src="http://placehold.it/42x42"></a>
                                        <a href=""><img alt="image" class="img-circle" src="http://placehold.it/42x42"></a>
                                        <a href=""><img alt="image" class="img-circle" src="http://placehold.it/42x42"></a>
                                        <a href=""><img alt="image" class="img-circle" src="http://placehold.it/42x42"></a>
                                        <a href=""><img alt="image" class="img-circle" src="http://placehold.it/42x42"></a>
                                        </dd>
                                    </dl>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection