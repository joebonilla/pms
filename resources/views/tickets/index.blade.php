@extends('layouts.master')

@section('styles')
    {!! Html::style('css/plugins/iCheck/custom.css') !!}
<style type="text/css">
    .issue-info {
        width: 30%;
    }
</style>
@endsection

@section('content')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-success pull-right">Monthly</span>
                    <h5>Income</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">40 886,200</h1>
                    <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div>
                    <small>Total income</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-info pull-right">Annual</span>
                    <h5>Orders</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">275,800</h1>
                    <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div>
                    <small>New orders</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-primary pull-right">Today</span>
                    <h5>Vistits</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">106,120</h1>
                    <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div>
                    <small>New visits</small>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <span class="label label-danger pull-right">Low value</span>
                    <h5>User activity</h5>
                </div>
                <div class="ibox-content">
                    <h1 class="no-margins">80,600</h1>
                    <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div>
                    <small>In first month</small>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Orders</h5>
                    <div class="pull-right">
                        <div class="btn-group">
                            <button type="button" class="btn btn-xs btn-white active">Today</button>
                            <button type="button" class="btn btn-xs btn-white">Monthly</button>
                            <button type="button" class="btn btn-xs btn-white">Annual</button>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                    <div class="col-lg-9">
                        <div class="flot-chart">
                            <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <ul class="stat-list">
                            <li>
                                <h2 class="no-margins">2,346</h2>
                                <small>Total orders in period</small>
                                <div class="stat-percent">48% <i class="fa fa-level-up text-navy"></i></div>
                                <div class="progress progress-mini">
                                    <div style="width: 48%;" class="progress-bar"></div>
                                </div>
                            </li>
                            <li>
                                <h2 class="no-margins ">4,422</h2>
                                <small>Orders in last month</small>
                                <div class="stat-percent">60% <i class="fa fa-level-down text-navy"></i></div>
                                <div class="progress progress-mini">
                                    <div style="width: 60%;" class="progress-bar"></div>
                                </div>
                            </li>
                            <li>
                                <h2 class="no-margins ">9,180</h2>
                                <small>Monthly income from orders</small>
                                <div class="stat-percent">22% <i class="fa fa-bolt text-navy"></i></div>
                                <div class="progress progress-mini">
                                    <div style="width: 22%;" class="progress-bar"></div>
                                </div>
                            </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Custom responsive table </h5>
                    <div class="ibox-tools">
                        <a href="" class="btn btn-primary btn-xs">Add new issue</a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="m-b-lg">
                        <form action="/ticket" method="GET" id="form">
                            <div class="input-group">
                                <input type="text" placeholder="Search issue by name..." name="search" class=" form-control">
                                <span class="input-group-btn">
                                    <button type="submit" class="btn btn-white"> Search</button>
                                </span>
                            </div>
                        </form>
                        <div class="m-t-md">
                            <div class="pull-right">
                                <button type="button" class="btn btn-sm btn-white"> <i class="fa fa-comments"></i> </button>
                                <button type="button" class="btn btn-sm btn-white"> <i class="fa fa-user"></i> </button>
                                <button type="button" class="btn btn-sm btn-white"> <i class="fa fa-list"></i> </button>
                                <button type="button" class="btn btn-sm btn-white"> <i class="fa fa-pencil"></i> </button>
                                <button type="button" class="btn btn-sm btn-white"> <i class="fa fa-print"></i> </button>
                                <button type="button" class="btn btn-sm btn-white"> <i class="fa fa-cogs"></i> </button>
                            </div>
                            <strong>Found {{ $tickets->total() }} tickets.</strong>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover issue-tracker">
                            <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Name</th>
                                    <th>Created by</th>
                                    <th>Date Created</th>
                                    <th>Date End</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($tickets as $ticket)
                                    @if ($ticket->type_tag == $ticket::TYPE_EXTERNAL)
                                        <tr>
                                            <td><span class="label label-info">Campaign</span></td>
                                            <td class="issue-info">
                                                <a href="/campaign/{{ $ticket->campaign->id }}">{{ $ticket->campaign->name }}</a>
                                                <small>Team: <a href="#">{{ $ticket->campaign->team->name }}</a></small>
                                                <small>Account: <a href="#">{{ $ticket->campaign->account->name }}</a></small>
                                            </td>
                                            <td><a href="#">{{ $ticket->creator->name }}</a></td>
                                            <td>12.02.2015 10:00 am</td>
                                            <td>12.02.2015 10:00 am</td>
                                            <td><span class="label label-default">{{ $ticket->campaign->status }}</span></td>
                                        </tr>
                                    @else
                                        <tr>
                                            <td><span class="label label-warning">In-House</span></td>
                                            <td class="issue-info">
                                                <a href="/inhouse/{{ $ticket->inhouse->id }}">{{ $ticket->inhouse->title }}</a>
                                                <small>Purpose: {{ $ticket->inhouse->purpose }}</small>
                                                <small>Type: {{ $ticket->inhouse->type }}</small>
                                            </td>
                                            <td><a href="#">{{ $ticket->creator->name }}</a></td>
                                            <td>12.02.2015 10:00 am</td>
                                            <td>12.02.2015 10:00 am</td>
                                            <td><span class="label label-default">{{ $ticket->inhouse->status }}</span></td>
                                        </tr>
                                    @endif
                                @empty
                                    <tr>
                                        <td colspan="6">No result.</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div style="text-align:center;">
                        {!! $tickets->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('scripts')
<!-- Peity -->
{!! Html::script('js/plugins/peity/jquery.peity.min.js') !!}
{!! Html::script('js/demo/peity-demo.js') !!}

<!-- EayPIE -->
{!! Html::script('js/plugins/easypiechart/jquery.easypiechart.js') !!}

<!-- Sparkline demo data  -->
{!! Html::script('js/demo/sparkline-demo.js') !!}

<script type="text/javascript">
    $(document).ready(function() {
        $('.chart').easyPieChart({
            barColor: '#f8ac59',
            scaleColor: false,
            scaleLength: 5,
            lineWidth: 4,
            size: 80
        });

        $('.chart2').easyPieChart({
            barColor: '#1c84c6',
           scaleColor: false,
            scaleLength: 5,
            lineWidth: 4,
            size: 80
        });

        var data2 = [
            [gd(2012, 1, 1), 1000],
            [gd(2012, 1, 2), 6],
            [gd(2012, 1, 3), 4],
            [gd(2012, 1, 4), 8],
            [gd(2012, 1, 5), 9],
            [gd(2012, 1, 6), 7], [gd(2012, 1, 7), 5], [gd(2012, 1, 8), 4],
            [gd(2012, 1, 9), 7], [gd(2012, 1, 10), 8], [gd(2012, 1, 11), 9], [gd(2012, 1, 12), 6],
            [gd(2012, 1, 13), 4], [gd(2012, 1, 14), 5], [gd(2012, 1, 15), 11], [gd(2012, 1, 16), 8],
            [gd(2012, 1, 17), 8], [gd(2012, 1, 18), 11], [gd(2012, 1, 19), 11], [gd(2012, 1, 20), 6],
            [gd(2012, 1, 21), 6], [gd(2012, 1, 22), 8], [gd(2012, 1, 23), 11], [gd(2012, 1, 24), 13],
            [gd(2012, 1, 25), 7], [gd(2012, 1, 26), 9], [gd(2012, 1, 27), 9], [gd(2012, 1, 28), 8],
            [gd(2012, 1, 29), 5], [gd(2012, 1, 30), 8], [gd(2012, 1, 31), 25]
        ];

        var data3 = [
            [gd(2012, 1, 1), 1000], [gd(2012, 1, 2), 500], [gd(2012, 1, 3), 600], [gd(2012, 1, 4), 700],
            [gd(2012, 1, 5), 500], [gd(2012, 1, 6), 456], [gd(2012, 1, 7), 800], [gd(2012, 1, 8), 589],
            [gd(2012, 1, 9), 467], [gd(2012, 1, 10), 876], [gd(2012, 1, 11), 689], [gd(2012, 1, 12), 700],
            [gd(2012, 1, 13), 500], [gd(2012, 1, 14), 600], [gd(2012, 1, 15), 700], [gd(2012, 1, 16), 786],
            [gd(2012, 1, 17), 345], [gd(2012, 1, 18), 888], [gd(2012, 1, 19), 888], [gd(2012, 1, 20), 888],
            [gd(2012, 1, 21), 987], [gd(2012, 1, 22), 444], [gd(2012, 1, 23), 999], [gd(2012, 1, 24), 567],
            [gd(2012, 1, 25), 786], [gd(2012, 1, 26), 666], [gd(2012, 1, 27), 888], [gd(2012, 1, 28), 900],
            [gd(2012, 1, 29), 178], [gd(2012, 1, 30), 555], [gd(2012, 1, 31), 993]
        ];


        var dataset = [
            {
                label: "Campaigns",
                data: data3,
                color: "#1ab394",
                bars: {
                    show: true,
                    align: "center",
                    barWidth: 24 * 60 * 60 * 600,
                    lineWidth:0
                }

            },
            {
                label: "In-House",
                data: data2,
                yaxis: 2,
                color: "#1C84C6",
                lines: {
                    lineWidth:1,
                        show: true,
                        fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.2
                        }, {
                            opacity: 0.4
                        }]
                    }
                },
                splines: {
                    show: false,
                    tension: 0.6,
                    lineWidth: 1,
                    fill: 0.1
                },
            }
        ];

        var options = {
            xaxis: {
                mode: "time",
                tickSize: [3, "day"],
                tickLength: 0,
                axisLabel: "Date",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Arial',
                axisLabelPadding: 10,
                color: "#d5d5d5"
            },
            yaxes: [{
                position: "left",
                max: 1070,
                color: "#d5d5d5",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Arial',
                axisLabelPadding: 3
            }, {
                position: "right",
                clolor: "#d5d5d5",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: ' Arial',
                axisLabelPadding: 67
            }
            ],
            legend: {
                noColumns: 1,
                labelBoxBorderColor: "#000000",
                position: "nw"
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            tooltip: true,
            tooltipOpts: {
                content: "Day: %x, Count: %y"
            }
        };

        function gd(year, month, day) {
            console.log('year: '+year);
            console.log('month: '+month);
            console.log('day: '+day);

            console.log(new Date(year, month, day).getMonth());

            date = new Date(year, month - 1, day + 1).getTime();

            return date;
        }

        var previousPoint = null, previousLabel = null;

        $.plot($("#flot-dashboard-chart"), dataset, options);
    });
</script>
@endsection