@extends('layouts.master')

@section('styles')
    {!! Html::style('css/plugins/datapicker/datepicker3.css') !!}
<style type="text/css">
    .type-fix {
        margin-bottom: 18px;
    }

    .form-group {
        margin-bottom: 20px;
    }

    .ibox-content {
        min-height: 500px;
    }

    .row-unit {
        margin-bottom: 15px;
    }

    #units {
        margin-bottom: 20px;
    }

    #units > div {
        margin-bottom: 20px;
    }

    #units > div > div {
        
    }
</style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Creating Ticket</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/ticket">Tickets</a>
            </li>
            <li class="active">
                <strong>Create</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>New Ticket</h5>
                    <!-- <div class="ibox-tools">
                        <button class="btn btn-xs btn-success">CREATE</button>
                    </div> -->
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <form action="/ticket" method="POST" id="form" role="form">
                            <div class="col-md-6">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type="text" name="title" class="form-control" placeholder="Enter a Title...">
                                    <small class="title-error" style="display: block;">* Ticket title should correlate the account.</small>
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <textarea name="description" class="form-control" rows="5" placeholder="Enter a description..."></textarea>
                                    <small class="description-error" style="display: block;">* Kindly be precise on ticket description.</small>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <div class="radio">
                                                <div class="type-fix">
                                                    <label>
                                                        <input type="radio" name="type" id="external" value="1" checked>External
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="type" id="internal" value="2">Internal
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Subtype</label>
                                            <select class="form-control m-b" name="subtype" id="subtype" disabled>
                                                <option value="1">Meeting</option>
                                                <option value="2">Trainig</option>
                                                <option value="3">Certification</option>
                                                <option value="4">Others</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label for="account">Account</label>
                                            <select class="form-control" name="account" id="subtype">
                                                @foreach ($accounts as $account)
                                                <option value="{{ $account->id }}">{{ $account->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="campaign">Campaign</label>
                                            <input type="text" name="campaign" id="campaign" class="form-control" placeholder="Campaign name">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="creative_brief">Creative Brief Link</label>
                                    <input type="text" name="creative_brief" id="creative_brief" class="form-control" placeholder="http://google.doc.url">
                                </div>
                                <div class="form-group">
                                    <label for="campaign">Client</label>
                                    <select class="form-control" name="campaign_client" id="campaign_client">
                                        @foreach ($campaign_clients as $campaign_client)
                                        <option value="{{ $campaign_client->id }}">{{ $campaign_client->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>Initial Build</label>
                                            <input type="text" name="initial_build" id="build" class="form-control" placeholder="0">   
                                        </div>
                                        <div class="col-md-6">
                                            <label>Creative Iteration</label>
                                            <input type="text" name="creative_iteration" id="iteration" class="form-control" placeholder="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div style="margin-bottom: 10px;">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <label>UNIT CONTROLS:</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <button class="btn btn-sm btn-success" id="add-unit"><strong>+ Unit</strong></button>        
                                        </div>
                                        <div class="col-lg-5">
                                            <label><strong>Complexity [All]:</strong> </label>
                                            <div class="btn-group">
                                                <select class="form-control" id="complexity-all">
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-5">
                                            <label><strong>Priority [All]:</strong> </label>
                                            <div class="btn-group">
                                                <select class="form-control" id="priority-all">
                                                    <option value="1">Low</option>
                                                    <option value="2">Normal</option>
                                                    <option value="3">High</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div id="units">
                                    <div class="row" class="unit-row">
                                        <div class="col-lg-1">
                                            <h4>1.</h4>
                                        </div>
                                        <div class="col-lg-10">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control unit-name" name="units[0][name]" placeholder="300x250">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <label>Complexity:</label>
                                                    <select class="form-control unit-complexity" name="units[0][complexity]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label>Priority:</label>
                                                    <select class="form-control unit-priority" name="units[0][priority]">
                                                        <option value="1">Low</option>
                                                        <option value="2">Normal</option>
                                                        <option value="3">High</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group" class="committed_at">
                                                        <label>Commit Date:</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" name="units[0][committed_at]" class="form-control" value="03/04/2014">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-default remove-unit" type="button" disabled><span>x</span></button>
                                        </div>
                                    </div>
                                    <div class="row" class="unit-row">
                                        <div class="col-lg-1">
                                            <h4>2.</h4>
                                        </div>
                                        <div class="col-lg-10">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control unit-name" name="units[1][name]" placeholder="300x250">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <label>Complexity:</label>
                                                    <select class="form-control unit-complexity" name="units[1][complexity]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label>Priority:</label>
                                                    <select class="form-control unit-priority" name="units[1][priority]">
                                                        <option value="1">Low</option>
                                                        <option value="2">Normal</option>
                                                        <option value="3">High</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group" class="committed_at">
                                                        <label>Commit Date:</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" name="units[1][committed_at]" class="form-control" value="03/04/2014">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-default remove-unit" type="button" disabled><span>x</span></button>
                                        </div>
                                    </div>
                                    <div class="row" class="unit-row">
                                        <div class="col-lg-1">
                                            <h4>3.</h4>
                                        </div>
                                        <div class="col-lg-10">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control unit-name" name="units[2][name]" placeholder="300x250">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <label>Complexity:</label>
                                                    <select class="form-control unit-complexity" name="units[2][complexity]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label>Priority:</label>
                                                    <select class="form-control unit-priority" name="units[2][priority]">
                                                        <option value="1">Low</option>
                                                        <option value="2">Normal</option>
                                                        <option value="3">High</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group" class="committed_at">
                                                        <label>Commit Date:</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" name="units[2][committed_at]" class="form-control" value="03/04/2014">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-default remove-unit" type="button" disabled><span>x</span></button>
                                        </div>
                                    </div>
                                    <div class="row" class="unit-row">
                                        <div class="col-lg-1">
                                            <h4>4.</h4>
                                        </div>
                                        <div class="col-lg-10">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control unit-name" name="units[3][name]" placeholder="300x250">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <label>Complexity:</label>
                                                    <select class="form-control unit-complexity" name="units[3][complexity]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label>Priority:</label>
                                                    <select class="form-control unit-priority" name="units[3][priority]">
                                                        <option value="1">Low</option>
                                                        <option value="2">Normal</option>
                                                        <option value="3">High</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group" class="committed_at">
                                                        <label>Commit Date:</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" name="units[3][committed_at]" class="form-control" value="03/04/2014">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-default remove-unit" type="button" disabled><span>x</span></button>
                                        </div>
                                    </div>
                                    <div class="row" class="unit-row">
                                        <div class="col-lg-1">
                                            <h4>5.</h4>
                                        </div>
                                        <div class="col-lg-10">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <label>Name:</label>
                                                    <input type="text" class="form-control unit-name" name="units[4][name]" placeholder="300x250">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <label>Complexity:</label>
                                                    <select class="form-control unit-complexity" name="units[4][complexity]">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-3">
                                                    <label>Priority:</label>
                                                    <select class="form-control unit-priority" name="units[4][priority]">
                                                        <option value="1">Low</option>
                                                        <option value="2">Normal</option>
                                                        <option value="3">High</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group" class="committed_at">
                                                        <label>Commit Date:</label>
                                                        <div class="input-group date">
                                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                            <input type="text" name="units[4][committed_at]" class="form-control" value="03/04/2014">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-1">
                                            <button class="btn btn-default remove-unit" type="button" disabled><span>x</span></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="text-align: right">
                                    <button class="btn btn-primary m-t-n-xs" type="submit">
                                        <strong>Create</strong>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<!-- Jquery Validate -->
{!! Html::script('js/plugins/validate/jquery.validate.min.js') !!}
{!! Html::script('js/plugins/datapicker/bootstrap-datepicker.js') !!}
<script type="text/javascript">
    // $(document).ready(function () {
    //     // add unit
    //     $('#add-unit').on('click', function (e) {
    //         e.preventDefault();

    //         // change button style
    //         if ($('.row-unit button').first().hasClass('btn-default')) {
    //             $('.row-unit button').first().removeClass('btn-default');
    //             $('.row-unit button').first().addClass('btn-danger');
    //             $('.row-unit button').first().prop('disabled', false);
    //         }

    //         last_unit_name = $('.row-unit .units').last().prop('name');
    //         last_unit_value = parseInt(last_unit_name.replace(/[units\[\]']+/g,'')) + 1;

    //         // append unit
    //         $('#units').append('<div class="row row-unit"><div class="col-lg-8"><input type="text" class="form-control units" name="units['+last_unit_value+']" placeholder="ex. 300x250"></div><div class="col-lg-3"><select class="form-control complexities" name="complexities['+last_unit_value+']"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option></select></div><div><button class="btn btn-danger remove-unit" type="button"><span>x</span></button></div></div>');

    //         // make complexity same from all
    //         $('.row-unit select[name="complexities['+last_unit_value+']"]').last().val($('#complexity-all').val());

    //         // add jquery validation rules to dynamic elements
    //         $('input[name="units['+last_unit_value+']"]').rules("add", {
    //             required: true
    //         });
    //     });

    //     // remove unit
    //     $(document).on('click', '.remove-unit', function (e) {
    //         e.preventDefault();

    //         $(this).closest('.row-unit').remove();

    //         unit_count = $('.row-unit').length;

    //         if (unit_count == 1) {
    //             $('.row-unit button').removeClass('btn-danger');
    //             $('.row-unit button').addClass('btn-default');
    //             $('.row-unit button').prop('disabled', true);
    //         }
    //     });

    //     // all complexity
    //     $('#complexity-all').change(function (e) {
    //         val = $(this).val();

    //         $('select.complexities').each(function () {
    //             $(this).val(val);
    //         });
    //     });

    //     // radio button changed
    //     $('input[type=radio][name=type]').change(function () {
    //         if (this.value == 'internal') {
    //             $('#subtype').removeAttr('disabled');
    //             $('#unit').val('1');
    //             $('#unit').prop('readonly', true);
    //             $('#complexity').prop('disabled', true);
    //             $('#account').prop('disabled', true);
    //             $('#campaign').prop('disabled', true);
    //             $('#build').prop('disabled', true);
    //             $('#iteration').prop('disabled', true);
    //         } else if (this.value == 'external') {
    //             $('#subtype').prop('disabled', true);
    //             $('#subtype').val('nothing');
    //             $('#unit').val('');
    //             $('#unit').prop('disabled', false);
    //             $('#complexity').prop('disabled', false);
    //             $('#account').prop('disabled', false);
    //             $('#campaign').prop('disabled', false);
    //             $('#build').prop('disabled', false);
    //             $('#iteration').prop('disabled', false);
    //         }
    //     });

    //     $('.committed_at .input-group.date').datepicker({
    //         todayBtn: "linked",
    //         keyboardNavigation: false,
    //         forceParse: false,
    //         calendarWeeks: true,
    //         autoclose: true
    //     });

    //     // creating ticket validation
    //     $("#form").validate({
    //         rules: {
    //             title: "required",
    //             description: "required",
    //             subtype: {
    //                 required: "#internal:checked"
    //             },
    //             "units[0][name]": {
    //                 required: "#external:checked",
    //                 required: true
    //             },
    //             campaign: {
    //                 required: "#external:checked",
    //                 required: true
    //             },
    //             creative_brief: {
    //                 required: "#external:checked",
    //                 required: true
    //             },
    //             initial_build: {
    //                 required: "#external:checked",
    //                 required: true
    //             },
    //             creative_iteration: {
    //                 required: "#external:checked",
    //                 required: true
    //             },
    //             committed_at: {
    //                 required: true,
    //                 date: true
    //             }
    //         },
    //         messages: {
    //             title: "This field is required.",
    //             description: "This field is required."
    //         },
    //         errorPlacement: function(error, element) {
    //             if (element.attr("name") == "description") {
    //                 error.insertAfter(".description-error");
    //             } else if (element.attr("name") == "title") {
    //                 error.insertAfter(".title-error");
    //             } else {
    //                 error.insertAfter(element);
    //             }
    //         }
    //     }); 
    // });
</script>
@endsection
