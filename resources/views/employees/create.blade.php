@extends('layouts.master')

@section('styles')
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>New User</h2>
        <ol class="breadcrumb">
            <li>
                <p>User</p>
            </li>
            <li class="active">
                <strong>New</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>New User</h5>
                </div>
                <div class="ibox-content">
                    <form action="/employee" method="POST" class="form-horizontal" id="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" placeholder="Juan Dela Cruz">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="email" placeholder="juandelacruz@mail.com">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Team:</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="team" id="team-select">
                                    <option value="">---Select---</option>
                                    @foreach ($teams as $team)
                                    <option value="{{ $team->id }}">{{ $team->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Role:</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="role" id="team-select">
                                    <option value="">---Select---</option>
                                    @foreach ($roles as $r => $role)
                                    <option value="{{ $r }}">{{ $role }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">IC Type:</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="ic" id="team-select">
                                    <option value="">---Select---</option>
                                    @foreach ($ics as $i => $ic)
                                    <option value="{{ $i }}">{{ $ic }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Schedule:</label>
                            <div class="col-sm-10">
                                <select class="form-control m-b" name="schedule" id="team-select">
                                    <option value="">---Select---</option>
                                    @foreach ($schedules as $s => $schedule)
                                    <option value="{{ $s }}">{{ $schedule }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">Create</button>
                                <a href="/employee" class="btn btn-white">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{--<div class="col-lg-3">--}}
            {{--<div class="ibox">--}}
                {{--<div class="ibox-title">--}}
                    {{--<h5>Instructions</h5>--}}
                {{--</div>--}}
                {{--<div class="ibox-content">--}}
                    {{--<strong><p style="color: red;"> Section Under Construction</p></strong>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
@endsection

@section('scripts')
@endsection
