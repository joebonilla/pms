@extends('layouts.master')

@section('styles')
<style type="text/css">
    .emojione {
        text-align: center;
    }

    .emojione-active {
        border: 2px solid #1ab394;
        border-radius: 7px;
    }

    .emojione-happy {
        background: url("{{ asset('img/emojione.png') }}") no-repeat 0 0;
    }

    .emojione-sad {
        background: url("{{ asset('img/emojione.png') }}") no-repeat -520px 0;
    }

    .emojione-angry {
        background: url("{{ asset('img/emojione.png') }}") no-repeat -585px -130px;
    }

    .emojione-dontcare {
        background: url("{{ asset('img/emojione.png') }}") no-repeat -455px 0;
    }

    .emojione-inspired {
        background: url("{{ asset('img/emojione.png') }}") no-repeat -195px 0;
    }

    .emojione-afraid {
        background: url("{{ asset('img/emojione.png') }}") no-repeat -455px -260px;
    }

    .emojione-amused {
        background: url("{{ asset('img/emojione.png') }}") no-repeat -260px -260px;
    }

    .emojione-annoyed {
        background: url("{{ asset('img/emojione.png') }}") no-repeat -650px 0;
    }
</style>
@endsection

@section('overrides')
<style type="text/css">
    .ibox-tools a {
        color: #ffffff;
    }
</style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Profile</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/employee">Employees</a>
            </li>
            <li class="active">
                <strong>{{ $employee->name }}</strong>
            </li>
        </ol>
    </div>
    @if (Auth::user()->id == $employee->id)
        <div class="col-lg-4">
            <div class="title-action">
                <a href="/employee/{{ $employee->id }}/edit" class="btn btn-info"><i class="fa fa-pencil"></i> Edit </a>
            </div>
        </div>
    @endif
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Profile Detail</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-5">
                            <img src="{{ $employee->avatar }}" class="img-circle circle-border m-b-md full-width" alt="profile">
                        </div>
                        <div class="col-lg-7">
                            <h2>{{ $employee->name }}</h2>
                            <h5>{{ $employee->email }}</h5>
                            <address>
                                <strong>Team: </strong>{{ $employee->team->name }}<br>
                                <strong>IC: </strong>{{ $employee->ic }}<br>
                                <strong>Role: </strong>{{ $employee->role }}<br>
                            </address>
                        </div>
                    </div>
                    {{--<h5>About me</h5>--}}
                    {{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat.</p>--}}
                    <hr>
                    <h5>Emotion</h5>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-md-3 emojione emojione-active">
                                <button class="btn btn-circle btn-lg emojione-happy" type="button"></button>
                                <h5>Happy</h5>
                            </div>
                            <div class="col-md-3 emojione">
                                <button class="btn btn-circle btn-lg emojione-sad" type="button"></button>
                                <h5>Sad</h5>
                            </div>
                            <div class="col-md-3 emojione">
                                <button class="btn btn-circle btn-lg emojione-angry" type="button"></button>
                                <h5>Angry</h5>
                            </div>
                            <div class="col-md-3 emojione">
                                <button class="btn btn-circle btn-lg emojione-dontcare" type="button"></button>
                                <h5>Dont Care</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-md-3 emojione">
                                <button class="btn btn-circle btn-lg emojione-inspired" type="button"></button>
                                <h5>Inspired</h5>
                            </div>
                            <div class="col-md-3 emojione">
                                <button class="btn btn-circle btn-lg emojione-afraid" type="button"></button>
                                <h5>Afraid</h5>
                            </div>
                            <div class="col-md-3 emojione">
                                <button class="btn btn-circle btn-lg emojione-amused" type="button"></button>
                                <h5>Amused</h5>
                            </div>
                            <div class="col-md-3 emojione">
                                <button class="btn btn-circle btn-lg emojione-annoyed" type="button"></button>
                                <h5>Annoyed</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Assigned units</h5>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Unit Name</th>
                                    <th>Status</th>
                                    <th>Priority</th>
                                    <th>Complexity</th>
                                    <th>Due</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($assigned_units as $unit)
                                    <tr>
                                        <th>{{ $unit->id }}</th>
                                        <td><a href="/unit/{{ $unit->id }}">{{ $unit->name }}</a></td>
                                        <td>{{ $unit->status }}</td>
                                        <td>{{ $unit->priority }}</td>
                                        <td>{{ $unit->complexity }}</td>
                                        <td>{{ $unit->committed_at }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<!-- Peity -->
{!! Html::script('js/plugins/peity/jquery.peity.min.js') !!}
{!! Html::script('js/demo/peity-demo.js') !!}
@endsection