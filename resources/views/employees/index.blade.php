@extends('layouts.master')

@section('styles')
    <style type="text/css">
        .client-detail {
            position: relative;
            height: 100%;
        }

        .hoverBtn {
            margin-left: 15px;
        }
    </style>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>User List</h2>
            <ol class="breadcrumb">
                <li>
                    <strong>User</strong>
                </li>
                <li class="active">
                    <strong>User List</strong>
                </li>
            </ol>
        </div>
        {{--<div class="col-lg-2">--}}
        {{--<strong><p style="color: red;">Page Under Construction</p></strong>--}}
        {{--</div>--}}
    </div>

    <div class="row wrapper wrapper-content">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Users List</h5>
                </div>
                <div class="ibox-content">
                    <form action="/employee" method="GET" id="form">
                        <div class="row">
                            <div class="col-sm-3 pull-right">
                                <div class="input-group">
                                    <input type="text" placeholder="Sample Name" class="input-sm form-control"
                                           name="search" value="@if(!empty(app('request')->input('search'))){{ app('request')->input('search') }}@endif">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> Go!</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                            {{--<tr>--}}
                            {{--<th></th>--}}
                            {{--<th>--}}
                            {{--<div class="form-group">--}}
                            {{--<label class="control-label" for="product_name">Name</label>--}}
                            {{--<input type="text" id="product_name" name="product_name" value="" placeholder="Employee Name" class="form-control">--}}
                            {{--</div>--}}
                            {{--</th>--}}
                            {{--<th>--}}
                            {{--<div class="form-group">--}}
                            {{--<label class="control-label" for="price">Email</label>--}}
                            {{--<input type="text" id="price" name="price" value="" placeholder="Email" class="form-control">--}}
                            {{--</div>--}}
                            {{--</th>--}}
                            {{--<th>--}}
                            {{--<div class="form-group">--}}
                            {{--<label class="control-label">Team</label>--}}
                            {{--<select class="form-control">--}}
                            {{--<option>1</option>--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            {{--</th>--}}
                            {{--<th>--}}
                            {{--<div class="form-group">--}}
                            {{--<label class="control-label">Role</label>--}}
                            {{--<select class="form-control">--}}
                            {{--<option>SA</option>--}}
                            {{--<option>SV</option>--}}
                            {{--<option>IC</option>--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            {{--</th>--}}
                            {{--<th>--}}
                            {{--<div class="form-group">--}}
                            {{--<label class="control-label">Role</label>--}}
                            {{--<select class="form-control">--}}
                            {{--<option>POC</option>--}}
                            {{--<option>SME</option>--}}
                            {{--<option>QA</option>--}}
                            {{--<option>DEV</option>--}}
                            {{--<option>CRE</option>--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            {{--</th>--}}
                            {{--<th>--}}
                            {{--<div class="form-group">--}}
                            {{--<label class="control-label">Role</label>--}}
                            {{--<select class="form-control">--}}
                            {{--<option>DAY</option>--}}
                            {{--<option>MID</option>--}}
                            {{--<option>NIG</option>--}}
                            {{--</select>--}}
                            {{--</div>--}}
                            {{--</th>--}}
                            {{--</tr>--}}
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Team</th>
                                <th>Role</th>
                                <th>IC Type</th>
                                <th>Schedule</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($employees as $employee)
                                <tr>
                                    <th>{{ $employee->id }}</th>
                                    <td><a href="/employee/{{ $employee->id }}">{{ $employee->name }}</a></td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->team->name }}</td>
                                    <td>{{ $employee->role }}</td>
                                    <td>{{ $employee->ic }}</td>
                                    <td>{{ $employee->schedule }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div style="text-align: center;">
                        {!! $employees->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @if(Auth::user()->getIcAttribute() != 'Developer' AND Auth::user()->getIcAttribute() != 'Creative' )
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('table tr');
                var editBtn = '<i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
                table.mouseenter(function () {
                    $(this).find('td:last-child').append('<a class="hoverBtn" href="/employee/' + $(this).find('th').text() + '/edit">' + editBtn + '</a>');
                });
                table.mouseleave(function () {
                    $(this).find('td:last-child').find('a').remove();
                });
            });
        </script>
    @endif
@endsection

