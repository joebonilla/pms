<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>WO | PMS</title>

{!! Html::style('css/bootstrap.min.css') !!}
{!! Html::style('font-awesome/css/font-awesome.min.css') !!}

<!-- Toastr style -->
{!! Html::style('css/plugins/toastr/toastr.min.css') !!}

<!-- Gritter -->
    {!! Html::style('js/plugins/gritter/jquery.gritter.css') !!}
    {!! Html::style('css/plugins/morris/morris-0.4.3.min.css') !!}
    @yield('styles')
    {!! Html::style('css/animate.css') !!}
    {!! Html::style('css/style.css') !!}
    {!! Html::script('js/modernizr.js') !!}
    <style type="text/css">
        #page-wrapper {
            min-height: 920px;
        }
    </style>

    @yield('overrides')
</head>
<body>
<div id="wrapper">
    @include('layouts.sidebar')

    <div id="page-wrapper" class="gray-bg">
        @include('layouts.navbar')

        @yield('content')

        @include('layouts.footer')
    </div>
</div>
{!! Html::script('js/jquery-2.2.4.min.js') !!}
{!! Html::script('js/jquery-migrate-1.4.1.min.js') !!}
{!! Html::script('js/bootstrap.min.js') !!}
{!! Html::script('js/plugins/metisMenu/metisMenu.min.js') !!}
{!! Html::script('js/plugins/slimscroll/jquery.slimscroll.min.js') !!}
{!! Html::script('js/plugins/flot/jquery.flot.js') !!}
{!! Html::script('js/plugins/flot/jquery.flot.tooltip.min.js') !!}
{!! Html::script('js/plugins/flot/jquery.flot.spline.js') !!}
{!! Html::script('js/plugins/flot/jquery.flot.resize.js') !!}
{!! Html::script('js/plugins/flot/jquery.flot.pie.js') !!}
{!! Html::script('js/plugins/flot/jquery.flot.symbol.js') !!}
{!! Html::script('js/plugins/flot/curvedLines.js') !!}
{!! Html::script('js/plugins/peity/jquery.peity.min.js') !!}
{!! Html::script('js/inspinia.js') !!}
{!! Html::script('js/plugins/pace/pace.min.js') !!}
{!! Html::script('js/plugins/jquery-ui/jquery-ui.min.js') !!}
{!! Html::script('js/plugins/gritter/jquery.gritter.min.js') !!}
{!! Html::script('js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js') !!}
{!! Html::script('js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') !!}
{!! Html::script('js/plugins/sparkline/jquery.sparkline.min.js') !!}
{!! Html::script('js/plugins/chartJs/Chart.min.js') !!}
{!! Html::script('js/plugins/toastr/toastr.min.js') !!}

@yield('scripts')
</body>
</html>
