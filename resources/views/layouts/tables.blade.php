<div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Ticket List</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a></li>
                            <li><a href="#">Config option 2</a></li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Unit</th>
                                <th>Subtype</th>
                                <th>Status</th>
                                <th>Team</th>
                                <th>Account</th>
                                <th>Comitted Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($tickets as $ticket)
                            <tr>
                                <td><a href="/ticket/{{ $ticket->id }}">{{ $ticket->name }}</a></td>
                                <td>{{ $ticket->unit }}</td>
                                <td>{{ $ticket->subtype }}</td>
                                <td>{{ $ticket->status }}</td>
                                <td>{{ $ticket->team }}</td>
                                <td>{{ $ticket->account }}</td>
                                <td>{{ $ticket->committed_at }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Unit</th>
                            <th>Subtype</th>
                            <th>Status</th>
                            <th>Team</th>
                            <th>Account</th>
                            <th>Comitted Date</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>