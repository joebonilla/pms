<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content=""> 

    <title>PMS | Login</title>

    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('font-awesome/css/font-awesome.min.css') !!}

    {!! Html::style('css/animate.css') !!}
    {!! Html::style('css/style.css') !!}
</head>
<body class="gray-bg">
    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div>
            <div>
                <h1 class="logo-name">WO</h1>
            </div>
            <!-- <h3>Welcome to PMS</h3> -->
            {{--<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</p>--}}
            <p>Login in. To see it in action.</p>
            
            <form method="POST" class="m-t" role="form" action="/employee">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <a href="/auth/google" class="btn btn-primary block full-width m-b">Google+ Login</a>
                <!-- <p class="text-muted text-center"><small>Do not have an account?</small></p>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Email" required="required">
                </div>
                <button type="submit" class="btn btn-sm btn-white btn-block">Request an account</button> -->
            </form>
            
            <p class="m-t"> <small>PMS is based on Bootstrap 3 &copy; 2015</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    {!! Html::script('js/jquery-2.2.4.min.js') !!}
    {!! Html::script('js/jquery-migrate-1.4.1.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
</body>
</html>
