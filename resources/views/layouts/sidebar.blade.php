<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" height="48" width="48" src="{{ Auth::user()->avatar }}"/>
                         </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> <span class="block m-t-xs"> <strong
                                        class="font-bold">{{ Auth::user()->name }}</strong>
                         </span> <span class="text-muted text-xs block">{{Auth::user()->getIcAttribute() . ' - ' . Auth::user()->team->name}}&nbsp; <b
                                        class="caret"></b></span> </span>
                    </a>
                    <ul class="dropdown-menu m-t-xs">
                        <li><a href="{{ url('/employee/'.Auth::user()->id) }}">Profile</a></li>
                        <!-- <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li> -->
                        <li class="divider"></li>
                        <li><a href="/auth/logout">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    <img alt="image" class="img-circle" height="48" width="48" src="{{ Auth::user()->avatar }}"/>
                </div>
            </li>
            @if (Route::currentRouteName() == 'main')
                <li class="active">
            @else
                <li>
                    @endif
                    <a href="/"><i class="fa fa-dashboard"></i> <span class="nav-label">Main</span></a>
                </li>
            <!-- @if (Route::currentRouteName() == 'line-of-business' || Route::currentRouteName() == 'terms-and-accounts')
                <li class="active">
                @else
                <li>
                @endif
                    <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        @if (Route::currentRouteName() == 'line-of-business')
                <li class="active">
                @else
                <li>
                @endif
                    <a href="/dashboard/line-of-business">Line of Business</a>
                </li>
                @if (Route::currentRouteName() == 'terms-and-accounts')
                <li class="active">
                @else
                <li>
                @endif
                    <a href="/dashboard/terms-and-accounts">Teams and Accounts</a>
                </li>
            </ul>
        </li> -->
                <li class="{{ (str_contains(URL::current(), 'metric')) ? 'active' : '' }}">
                    <a href="#">
                        <i class="fa fa-line-chart"></i> <span class="nav-label">Metrics </span><span
                                class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li class="{{ (str_contains(URL::current(), 'volume')) ? 'active' : '' }}">
                            <a href="/metric/volume">Volume</a>
                        </li>
                        <li class="{{ (str_contains(URL::current(), 'utilization')) ? 'active' : '' }}">
                            <a href="/metric/utilization">Utilization</a>
                        </li>
                        <li class="{{ (str_contains(URL::current(), 'efficiency')) ? 'active' : '' }}">
                            <a href="/metric/efficiency">Efficiency</a>
                        </li>
                    </ul>
                </li>
            <!-- <li class="{{ (str_contains(URL::current(), 'ticket')) ? 'active' : '' }}">
                <a href="#"><i class="fa fa-ticket"></i> <span class="nav-label">Tickets </span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li class="{{ (str_contains(URL::current(), 'ticket')) ? 'active' : '' }}">
                        <a href="/ticket">List</a>
                    </li>
                    <li>
                        <a href="#">Create<span class="fa arrow"></span></a>
                        <ul class="nav nav-third-level">
                            <li>
                                <a href="/campaign/create">External</a>
                            </li>
                            <li>
                                <a href="/inhouse/create">Internal</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li> -->
                <li class="{{ (str_contains(URL::current(), 'campaign')) ? 'active' : '' }}">
                    <a href="#"><i class="fa fa-sitemap"></i> <span class="nav-label">Campaign</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="{{ (!str_contains(URL::current(), 'create')) ? 'active' : '' }}"><a href="/campaign">Campaign
                                List</a>
                        </li>
                        <li class="{{ (str_contains(URL::current(), 'create')) ? 'active' : '' }}"><a
                                    href="/campaign/create">Add New Campaign</a></li>
                    </ul>
                </li>
                <li class="{{ (str_contains(URL::current(), 'inhouse')) ? 'active' : '' }} hidden">
                    <a href="#"><i class="fa fa-home"></i> <span class="nav-label">In-House</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="{{ (!str_contains(URL::current(), 'create')) ? 'active' : '' }}"><a href="/inhouse">All</a>
                        </li>
                        <li class="{{ (str_contains(URL::current(), 'create')) ? 'active' : '' }}"><a
                                    href="/inhouse/create">New</a></li>
                    </ul>
                </li>
                <li class="{{ (str_contains(URL::current(), 'report')) ? 'active' : '' }}">
                    <a href="#"><i class="fa fa-file-text"></i> <span class="nav-label">Reports</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li>
                            <a href="#">Production Report<span class="fa arrow"></span></a>
                            <ul class="nav nav-third-level">
                                <li>
                                    <a href="/report">External</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="/billing">
                                {{--<i class="fa fa-users"></i>--}}
                                <span class="nav-label">Billing</span></a>
                        </li>
                        <li>
                            <a href="/report/arrival_pattern">
                                {{--<i class="fa fa-users"></i>--}}
                                <span class="nav-label">Arrival Patterns</span></a>
                        </li>
                        <li>
                            <a href="/report/audit_unit">
                                {{--<i class="fa fa-users"></i>--}}
                                <span class="nav-label">Audit Unit</span></a>
                        </li>
                    </ul>
                </li>
                <!-- MAIL -->
                <li class="{{ (str_contains(URL::current(), 'mail')) ? 'active' : '' }} hidden">
                    <a href="/mail"><i class="fa fa-envelope"></i> <span class="nav-label">Mail</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="{{ (!str_contains(URL::current(), 'TWC')) ? 'active' : '' }}"><a
                                    href="/mail/twc">TWC</a>
                        </li>

                    </ul>

                </li>

                <li>
                    <a href="/team"><i class="fa fa-users"></i> <span class="nav-label">Teams</span></a>

                </li>
                <li class="{{ (str_contains(URL::current(), 'employee')) ? 'active' : '' }}">
                    <a href="#"><i class="fa fa-user"></i> <span class="nav-label">User</span><span
                                class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li class="{{ (!str_contains(URL::current(), 'create')) ? 'active' : '' }}"><a href="/employee">User
                                List</a>
                        </li>
                        @if(Auth::user()->getIcAttribute() != 'Developer' AND Auth::user()->getIcAttribute() != 'Creative' )
                            <li class="{{ (str_contains(URL::current(), 'create')) ? 'active' : '' }}"><a
                                        href="/employee/create">Add New User</a></li>
                        @endif
                    </ul>
                </li>
                <li class="special_link">
                    <a href="https://docs.google.com/a/wideout.com/forms/d/1CDVNLMAhwXmKKbAyuepirmxCp19DPQJInrIVyDO8gVk/viewform"
                       target='_blank'>
                        <i class="fa fa-file-text-o"></i> <span class="nav-label">Suggestion Form</span>
                    </a>
                </li>
        </ul>

    </div>
</nav>
