@extends('layouts.master')

@section('styles')
    {!! Html::style('css/plugins/iCheck/custom.css') !!}
@endsection

@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12 animated fadeInRight">
                @if(!empty($print))
                    <div class="mail-box-header">
                        <h2>
                            New Mail ({{count($print)}})
                        </h2>
                    </div>
                    <div class="mail-box">

                        <table class="table table-hover table-mail">
                            <tbody>
                            @foreach ($print as $printed)
                                <tr>
                                    <td class="check-mail">
                                        <input type="checkbox" class="i-checks">
                                    </td>
                                    <td class="mail-ontact"><a href="mail_detail.html">{{$printed->from}}</a></td>
                                    <td class="mail-subject"><a href="mail_detail.html">{{$printed->subject}}</a>
                                    </td>
                                    <td class="text-right mail-date">{{$printed->date}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
    @endsection
    @section('scripts')
            <!-- iCheck -->
            {!! Html::script('js/plugins/iCheck/icheck.min.js') !!}
    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>
@endsection