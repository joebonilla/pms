<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Alerts e.g. approaching your limit</title>
</head>
<body style="margin: 0; padding: 0; font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; box-sizing: border-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6; background-color: #f6f6f6;">
    <table style="background-color: #f6f6f6; width: 100%;">
        <tr>
            <td style="vertical-align: top;"></td>
            <td style="vertical-align: top; display: block !important; max-width: 600px !important; margin: 0 auto !important; clear: both !important;" width="600">
                <div style="max-width: 600px; margin: 0 auto; display: block; padding: 20px;">
                    <table style="background: #fff; border: 1px solid #e9e9e9; border-radius: 3px;" width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="vertical-align: top; font-size: 16px; color: #fff; font-weight: 500; padding: 20px; text-align: center; border-radius: 3px 3px 0 0; background: #1ab394;" class="alert alert-good">
                                Info: A unit has been assigned to you
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding: 20px;">
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="vertical-align: top; padding: 0 0 20px;">
                                            <strong>{{ $unit->name }}</strong> has been assigned to you.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; padding: 0 0 20px;">
                                            It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top; padding: 0 0 20px;">
                                            <a href="{{ url('unit', [$unit->id]) }}" style="color: #1ab394; text-decoration: underline; text-decoration: none; color: #FFF; background-color: #1ab394; border: solid #1ab394; border-width: 5px 10px; line-height: 2; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize;">Go to unit</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: top;">
                                            PMS Wideout (Beta)
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top;"></td>
        </tr>
    </table>
</body>
</html>
