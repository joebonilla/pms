@extends('layouts.master')


@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{ $team->name }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/team">Teams</a>
            </li>
            <li class="">
                <a href="/team/{{ $team->id }}">{{ $team->name }}</a>
            </li>
            <li class="active">
                <strong>Productivity</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Productivity</h5>
                    <div class="ibox-tools">
                    <form action="/team/{{ $team->id }}/productivity" method="GET" id="form">
                        <div class="btn-group">
                            <button class="btn btn-xs {{ ($timespan == 'days') ? 'btn-primary' : 'btn-white' }}" type="submit" name="timespan" value="days">Day</button>
                            <button class="btn btn-xs {{ ($timespan == 'weeks') ? 'btn-primary' : 'btn-white' }}" type="submit" name="timespan" value="weeks">Week</button>
                            <button class="btn btn-xs {{ ($timespan == 'months') ? 'btn-primary' : 'btn-white' }}" type="submit" name="timespan" value="months">Month</button>
                        </div>
                    </form>
                    </div>
                </div>
                        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="text-center">
                        <tr>
                            <td></td>
                            <td>Sum of Build Unit Quantity</td>
                            <td>Sum of Volume X Quantity</td>
                            <td>Sum of Task Time Against Benchmarks</td>
                            <td>Productivity Against Benchmarks (counting idle time)</td>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    foreach ($team_prod as $prod) { ?>
                        <tr>
                        <td>{{ $prod->member}}</td>
                        <td class="text-center">{{ $prod->total_unit}}</td>
                        <td class="text-center">{{ $prod->total_volume}}</td>
                        <td class="text-center">{{ $prod->total_benchmark}}</td>
                        <td class="text-center">{{ $prod->prod_benchmark}}</td>
                    </tr>
                    <?php } ?> 
                    </tbody>
                </table>
            </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection





