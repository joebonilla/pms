@extends('layouts.master')

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Create Team</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/team">Teams</a>
            </li>
            <li class="active">
                <a>New</a>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>New Team</h5>
                </div>
                <div class="ibox-content">
                    <form method="POST" action="/ticket" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label for="name">Team Name:</label>
                            <input type="text" name="name" placeholder="Awesome Team.." class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="description">Description:</label>
                            <textarea name="description" rows="4" placeholder="Describe your team.." class="form-control"></textarea>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="poc">POC:</label>
                                    <input type="text" name="poc" placeholder="POC.." class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="sub_poc">Sub POC:</label>
                                    <input type="text" name="sub_poc" placeholder="Sub POC.." class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-sm btn-primary pull-right m-t-n-xs" type="submit"><strong>Create</strong></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
