@extends('layouts.master')


@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>{{ $team->name }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/team">Teams</a>
            </li>
            <li class="">
                <a href="/team/{{ $team->id }}">{{ $team->name }}</a>
            </li>
            <li class="active">
                <strong>Benchmark</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
               
                    <h5>Benchmark</h5>
            </div>
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead class="text-center">
                        <tr>
                            <td>Complexity</td>
                            <td>Benchmark</td>
                        </tr>
                    </thead>
                    <tbody> 
                        @foreach ($team_benchmark as $key => $benchmark)
                        <tr class="text-center">
                            <td>{{ $benchmark->complexity}}</td>
                            <td>{{ $benchmark->benchmark}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
           
            </div>
        </div>
    </div>
</div>
@endsection





