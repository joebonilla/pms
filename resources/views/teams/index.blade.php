@extends('layouts.master')

@section('styles')
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Teams board</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>Teams</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        @foreach ($groups as $teams)
        <div class="col-lg-4">
            @foreach ($teams as $team)
                <div class="ibox">
                    <div class="ibox-title">
                        <a href="/team/{{ $team->id }}"><h5>{{ $team->name }}</h5></a>
                    </div>
                    <div class="ibox-content">
                        <div class="team-members">
                            @foreach ($team->member_display as $member)
                                <a href="/employee/{{ $member->id }}"><img alt="member" class="img-circle" src="{{ $member->avatar }}"></a>
                            @endforeach
                        </div>
                        <!-- <h4>Info about {{ $team->name }}</h4> -->

                        {{--<p>--}}
                            {{--It is a long established fact that a reader will be distracted by the readable content--}}
                            {{--of a page when looking at its layout. The point of using Lorem Ipsum is that it has.--}}
                        {{--</p>--}}

                        <div class="row  m-t-sm">
                            <div class="col-sm-6">
                                <div class="font-bold">MEMBERS</div>
                                {{ $team->members->count() }}
                            </div>
                            <div class="col-sm-6">
                                <div class="font-bold">CAMPAIGNS</div>
                                {{ $team->campaigns()->count() }}
                            </div>
                            <div class="col-sm-6">
                                <div class="font-bold">UNITS</div>
                                {{ $team->units()->count() }}
                            </div>
                            <div class="col-sm-6 ">
                                <div class="font-bold">HEADCOUNT</div>
                                <a href="#" class="headcount-edit" data-team_id="{{ $team->id }}">
                                @if($team->teamHeadcount()->latestHc()->first() !== null)
                                    {{ $team->teamHeadcount()->latestHc()->first()->headcount }}
                                @else
                                    {{ $team->members->count() }}
                                @endif
                                </a>
                            </div>
                        </div>
                        <div>
                            <hr>
                            <a href="/team/{{ $team->id }}/productivity?timespan=months">Productivity</a><br>
                            <a href="/team/{{ $team->id }}/benchmark">Benchmark</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        var csrf_token = "{{ csrf_token() }}";
        $(window).ready(function(){
            $('a.headcount-edit').click(function(e){
                console.log('test');
                var parent = $(this).parent();
                var value = $(this).text();
                var team_id = $(this).attr('data-team_id');
                var html = "";

                html += "<form action='/team/" + team_id + "' method='post'>";
                html += "<input type='hidden' name='_token' value=" + csrf_token + ">";
                html += "<input type='text' size='10' name='headcount' value=" + value + ">";
                html += "<input type='submit' value='Ok'>";
                html += "</form>";

                parent.html(html);
            });
        })
    </script>
@endsection
