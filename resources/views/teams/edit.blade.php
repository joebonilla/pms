@extends('layouts.master')

@section('content')
<div class="row">
    <h1>{{ $team->name }}</h1>
    <ol class="breadcrumb">
        <li><a href="/team">Teams</a></li>
        <li><a href="/team/{{ $team->id }}">{{ $team->name }}</a></li>
        <li class="active">Edit</li>
    </ol>
</div>
<div class="row">
    <div class="col-lg-12">
        <form action="/team/{{ $team->id }}" method="post">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <input type="text" name="poc_id" placeholder="POC" value="{{ $poc->poc_id or '' }}">
            <input type="text" name="sub_poc_id" placeholder="Sub POC" value="{{ $poc->sub_poc_id or '' }}">

            <input type="text" name="name" value="{{ $team->name }}">
            <input type="submit" name="submit" value="submit">
        </form>
    </div>
</div>
@endsection