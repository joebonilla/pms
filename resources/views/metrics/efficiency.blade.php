@extends('layouts.master')

@section('styles')

@endsection

@section('overrides')
    <style type="text/css">
        th {
            text-align: center;
        }

        .table > thead > tr > th {
            vertical-align: middle;
        }

        .table > tbody > tr > td {
            vertical-align: middle;
            text-align: center;
        }

        .table > tbody > tr > td.metrics {
            text-align: left;
        }
    </style>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Metrics</h2>
            <ol class="breadcrumb">
                <li>
                    <p>Metrics</p>
                </li>
                <li class="active">
                    <strong>Efficiency</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <form action="/metric/efficiency" method="GET" id="form" style="display: none;">
                                <div class="btn-group">
                                    <button class="btn btn-xs {{ ($timespan == 'days') ? 'btn-primary' : 'btn-white' }}"
                                            type="submit" name="timespan" value="days">Day
                                    </button>
                                    <button class="btn btn-xs {{ ($timespan == 'weeks') ? 'btn-primary' : 'btn-white' }}"
                                            type="submit" name="timespan" value="weeks">Week
                                    </button>
                                    <button class="btn btn-xs {{ ($timespan == 'months') ? 'btn-primary' : 'btn-white' }}"
                                            type="submit" name="timespan" value="months">Month
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2">Team</th>
                                    <th rowspan="2">Account</th>
                                    <th rowspan="2">Metrics</th>
                                    <th rowspan="2">{{$now}}</th>
                                    <th>Variance %</th>
                                    <th rowspan="2">{{$now1}}</th>
                                    <th>Variance %</th>
                                    <th rowspan="2">{{$now2}}</th>
                                    <th>Variance %</th>
                                    <th rowspan="2">{{$now3}}</th>
                                    <th rowspan="2" colspan="2">Graph</th>
                                </tr>
                                <tr>
                                    {{--<th>Value</th>--}}
                                    {{--<th>%</th>--}}
                                    {{--<th>Value</th>--}}
                                    {{--<th>%</th>--}}
                                    {{--<th>Value</th>--}}
                                    {{--<th>%</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($efficiency as $team)
                                    @for ($i = 0; $i < count($team['accounts']); $i++)
                                        @for ($j = 0; $j < count($team['accounts'][$i]['metrics']); $j++)
                                            <tr>
                                                @if ($i == 0 && $j == 0)
                                                    <td rowspan="{{ $team['rowspan'] }}">{{ $team['name'] }}</td>
                                                @endif
                                                @if ($j == 0)
                                                    <td class="accounts"
                                                        rowspan="{{ $team['accounts'][$i]['rowspan'] }}">{{ $team['accounts'][$i]['name'] }}</td>
                                                @endif
                                                <td class="metrics">{{ $team['accounts'][$i]['metrics'][$j]['name'] }}</td>
                                                @for ($k = 0; $k < count($team['accounts'][$i]['metrics'][$j]['cols']); $k++)
                                                    <td>{{ $team['accounts'][$i]['metrics'][$j]['cols'][$k] }}</td>
                                                @endfor
                                                <td class="modal-shows"
                                                    data-values="{{ $team['accounts'][$i]['name'] }}">
                                                    <a data-toggle="modal" class="btn btn-default" href="#modal-form"
                                                       data-graph="{{ $team['accounts'][$i]['metrics'][$j]['graph'] }}">
                                                        <span class="sparkline">{{ $team['accounts'][$i]['metrics'][$j]['graph'] }}</span>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endfor
                                    @endfor
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="modal-form" class="modal fade" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div id="graph-container">
                                                        <div class="ibox-title modal-titles">
                                                            {{--<h5>Line Chart Example--}}
                                                            {{--<small>With custom colors.</small>--}}
                                                            {{--</h5>--}}
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div>
                                                                <canvas id="lineChart" height="140"></canvas>
                                                            </div>
                                                            <p style="transform: rotate(-90deg);transform-origin: left top 0;position: absolute;left: 16px;top: 65%;width: 160px;text-align: center;font-size: 10px;" class="graph-title"></p>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-footer text-center">
                                                        <button class="btn btn-primary" id="to-image">Download Graph
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- html2canvas -->
    {!! Html::script('js/plugins/html2canvas/html2canvas.min.js') !!}
    <!-- canvas2image -->
    {!! Html::script('js/plugins/canvas2image/canvas2image.js') !!}

    <script type="text/javascript">
        $(document).ready(function () {
            $(".sparkline").sparkline('html', {
                type: 'line',
                lineColor: '#17997f',
                fillColor: '#ffffff',
            });
            $('.modal-shows').on('click', function () {

                var modaltitle = $('.modal-titles');
                var graphtitle = $('.graph-title');

                modaltitle.html($(this).data('values') + " - " + $(this).parent().find('.metrics').html());
                modaltitle.css('font-weight', 'bold');

                graphtitle.html($(this).parent().find('.metrics').html());
                graphtitle.css('font-weight', '');

            });
            $("#modal-form").on('shown.bs.modal', function (e) {
                var graph = $(e.relatedTarget).data('graph');

                var lineData = {
                    labels: ["{{$now}}", "{{$now1}}", "{{$now2}}", "{{$now3}}"],
                    datasets: [
                        {
                            label: "Example dataset",
                            fillColor: "rgba(26,179,148,0.5)",
                            strokeColor: "rgba(26,179,148,0.7)",
                            pointColor: "rgba(26,179,148,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(26,179,148,1)",
                            data: JSON.parse("[" + graph + "]")
                        }
                    ]
                };

                var lineOptions = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    bezierCurve: true,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    responsive: true,
                    scaleBeginAtZero: true,
                };

                var ctx = document.getElementById("lineChart").getContext("2d");
                var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

                $("#to-image").off().on('click', function () {
                    html2canvas($("#graph-container"), {
                        useCORS: true,
                        background: "#fff",
                        onrendered: function (canvas) {
//                            Canvas2Image.saveAsJPEG(canvas);
                            var a = document.createElement('a');
                            a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
                            a.download = 'graph.png';
                            a.click();
                        }
                    });
                });
            });

            $("#modal-form").on('hidden.bs.modal', function (e) {
                $(this).find('canvas').remove();
                $(this).find('.ibox-content div').html(
                        '<canvas id="lineChart" height="140">'
                );
            });
        });
    </script>
@endsection
