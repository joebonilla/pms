@extends('layouts.master')

@section('styles')

@endsection

@section('overrides')
    <style type="text/css">
        th {
            text-align: center;
        }

        .table > thead > tr > th {
            vertical-align: middle;
        }

        .table > tbody > tr > td {
            vertical-align: middle;
            text-align: center;
        }

        .table > tbody > tr > td.metrics {
            text-align: left;
        }
    </style>
@endsection

@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Metrics</h2>
            <ol class="breadcrumb">
                <li>
                    <p>Metrics</p>
                </li>
                <li class="active">
                    <strong>Utilization</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <form action="/metric/utilization" method="GET" id="form">
                                <div class="btn-group">
                                    <button class="btn btn-xs {{ ($timespan == 'days') ? 'btn-primary' : 'btn-white' }}"
                                            type="submit" name="timespan" value="days">Day
                                    </button>
                                    <button class="btn btn-xs {{ ($timespan == 'weeks') ? 'btn-primary' : 'btn-white' }}"
                                            type="submit" name="timespan" value="weeks">Week
                                    </button>
                                    <button class="btn btn-xs {{ ($timespan == 'months') ? 'btn-primary' : 'btn-white' }}"
                                            type="submit" name="timespan" value="months">Month
                                    </button>
                                </div>
                            </form>
                            <form action="/metric/utilization" method="GET" class="form-inline">
                                <div class="date-range-container">
                                    <div class="form-group">
                                        <div class="input-group date">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control" name="from" placeholder="From" required value="{{ app('request')->input('from') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group date">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control" name="to" placeholder="To" required value="{{ app('request')->input('to') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    </div>
                                    <button class="btn btn-primary" name="timespan_range" value="weeks" type="submit" style="margin-bottom: 0">Week to week</button>
                                    <button class="btn btn-primary" name="timespan_range" value="months" type="submit" style="margin-bottom: 0">Month to month</button>
                                    <a class="btn btn-danger" name="csv_export" style="margin-bottom: 0">Export .csv</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th rowspan="2">Team</th>
                                    <th rowspan="2">Account</th>
                                    <th rowspan="2">Metrics</th>
                                    @if ($timespan == "days")
                                        @foreach($dates as $date)
                                            <th rowspan="2">{{$date[0]->format('F j')}}</th>
                                            @if($date !== $dates[count($dates) - 1])
                                                <th>Variance %</th>
                                            @endif
                                        @endforeach
                                    @elseif ($timespan == "weeks")
                                        @foreach($dates as $date)
                                            <th rowspan="2">{{$date[0]->format('F j')}} - {{$date[1]->format('F j')}}</th>
                                            @if($date !== $dates[count($dates) - 1])
                                                <th>Variance %</th>
                                            @endif
                                        @endforeach
                                    @else
                                        @foreach($dates as $date)
                                            <th rowspan="2">{{$date[0]->format('F')}}</th>
                                            @if($date !== $dates[count($dates) - 1])
                                                <th>Variance %</th>
                                            @endif
                                        @endforeach
                                    @endif
                                    <th rowspan="2" colspan="2">Graph</th>
                                </tr>
                                <tr>
                                    {{--<th>Value</th>--}}
                                    {{--<th>%</th>--}}
                                    {{--<th>Value</th>--}}
                                    {{--<th>%</th>--}}
                                    {{--<th>Value</th>--}}
                                    {{--<th>%</th>--}}
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($utilization as $team)
                                    @for ($i = 0; $i < count($team['overall']['metrics']); $i++)
                                        <tr class="table-content">
                                            @if ($i == 0)
                                                <td class="csv-exclude" rowspan="{{ $team['rowspan'] }}">{{ $team['name'] }}</td>
                                                <td class="account csv-exclude" rowspan="{{ $team['overall']['rowspan'] }}">{{ $team['overall']['text'] }}</td>
                                            @endif
                                            <td data-team="{{ $team['name'] }}" data-account="{{ $team['overall']['text'] }}" class="metrics">{{ $team['overall']['metrics'][$i]['name'] }}</td>
                                            @for ($j = 0; $j < count($team['overall']['metrics'][$i]['cols']); $j++)
                                                <td>{{ $team['overall']['metrics'][$i]['cols'][$j] }}</td>
                                            @endfor
                                            <td class="modal-shows csv-exclude" data-values="{{ $team['overall']['text'] }}">
                                                <a data-toggle="modal" class="btn btn-default" href="#modal-form"
                                                   data-graph="{{ $team['overall']['metrics'][$i]['graph'] }}">
                                                    <span class="sparkline">{{ $team['overall']['metrics'][$i]['graph'] }}</span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endfor
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div id="modal-form" class="modal fade" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox float-e-margins">
                                                    <div id="graph-container">
                                                        <div class="ibox-title modal-titles">
                                                            {{--<h5>Line Chart Example--}}
                                                            {{--<small>With custom colors.</small>--}}
                                                            {{--</h5>--}}
                                                        </div>
                                                        <div class="ibox-content">
                                                            <div>
                                                                <canvas id="lineChart" height="140"></canvas>
                                                            </div>
                                                            <p style="transform: rotate(-90deg);transform-origin: left top 0;position: absolute;left: 16px;top: 65%;width: 160px;text-align: center;font-size: 10px;" class="graph-title"></p>
                                                        </div>
                                                    </div>
                                                    <div class="ibox-footer text-center">
                                                        <button class="btn btn-primary" id="to-image">Download Graph</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- html2canvas -->
    {!! Html::script('js/plugins/html2canvas/html2canvas.min.js') !!}
    <!-- canvas2image -->
    {!! Html::script('js/plugins/canvas2image/canvas2image.js') !!}
    <!-- Data picker -->
    {!! Html::script('js/plugins/datapicker/bootstrap-datepicker.js') !!}

    <script type="text/javascript">
        $(document).ready(function () {
            $(".sparkline").sparkline('html', {
                type: 'line',
                lineColor: '#17997f',
                fillColor: '#ffffff',
            });
            $('.modal-shows').on('click', function () {

                var modaltitle = $('.modal-titles');
                var graphtitle = $('.graph-title');

                modaltitle.html($(this).data('values') + " - " + $(this).parent().find('.metrics').html());
                modaltitle.css('font-weight', 'bold');

                graphtitle.html($(this).parent().find('.metrics').html());
                graphtitle.css('font-weight', '');

            });
            $("#modal-form").on('shown.bs.modal', function (e) {
                var graph = $(e.relatedTarget).data('graph');

                var lineData = {
                    @if ($timespan == "days")
                    labels: [
                        @foreach($dates as $date)
                                "{{$date[0]->format('F j')}}",
                        @endforeach
                    ],
                    @elseif ($timespan == "weeks")
                    labels: [
                        @foreach($dates as $date)
                                "{{$date[0]->format('F j')}} - {{$date[1]->format('F j')}}",
                        @endforeach
                    ],
                    @else
                    labels: [
                        @foreach($dates as $date)
                                "{{$date[0]->format('F')}}",
                        @endforeach
                    ],
                    @endif
                    datasets: [
                        {
                            label: "Example dataset",
                            fillColor: "rgba(26,179,148,0.5)",
                            strokeColor: "rgba(26,179,148,0.7)",
                            pointColor: "rgba(26,179,148,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(26,179,148,1)",
                            data: JSON.parse("[" + graph + "]")
                        }
                    ]
                };

                var lineOptions = {
                    scaleShowGridLines: true,
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleGridLineWidth: 1,
                    bezierCurve: true,
                    bezierCurveTension: 0.4,
                    pointDot: true,
                    pointDotRadius: 4,
                    pointDotStrokeWidth: 1,
                    pointHitDetectionRadius: 20,
                    datasetStroke: true,
                    datasetStrokeWidth: 2,
                    datasetFill: true,
                    responsive: true,
                    scaleBeginAtZero: true,
                };

                var ctx = document.getElementById("lineChart").getContext("2d");
                var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

                $("#to-image").off().on('click', function () {
                    html2canvas($("#graph-container"), {
                        useCORS: true,
                        background: "#fff",
                        onrendered: function (canvas) {
//                            Canvas2Image.saveAsJPEG(canvas);
                            var a = document.createElement('a');
                            a.href = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
                            a.download = 'graph.png';
                            a.click();
                        }
                    });
                });
            });

            $("#modal-form").on('hidden.bs.modal', function (e) {
                $(this).find('canvas').remove();
                $(this).find('.ibox-content div').html(
                        '<canvas id="lineChart" height="140">'
                );
            });

            $('.input-group.date').datepicker({
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true,
                orientation: "bottom auto"
            });

            var tableContent = $('.table-content');

            var csv_fields = [];
            tableContent.each(function(){
                var csv_field = [];
                $(this).find('td:not(.csv-exclude)').each(function(){
                    var me = $(this);
                    if(me.hasClass('metrics')){
                        csv_field.push(me.attr('data-team'));
                        csv_field.push(me.attr('data-account'));
                    }
                    csv_field.push($(this).text());
                });
                csv_fields.push(csv_field);
            });

            var content = [
                ['Team', 'Account', 'Metrics',
                    @if ($timespan == "days")
                        @foreach($dates as $date)
                                "{{$date[0]->format('F j')}}",
                            @if($date !== $dates[count($dates) - 1])
                                "Variance %",
                            @endif
                        @endforeach
                    @elseif ($timespan == "weeks")
                        @foreach($dates as $date)
                                "{{$date[0]->format('F j')}} - {{$date[1]->format('F j')}}",
                            @if($date !== $dates[count($dates) - 1])
                                "Variance %",
                            @endif
                        @endforeach
                    @else
                        @foreach($dates as $date)
                            "{{$date[0]->format('F')}}",
                            @if($date !== $dates[count($dates) - 1])
                                "Variance %",
                            @endif
                        @endforeach
                    @endif
                ]
            ];

            Array.prototype.push.apply(content, csv_fields);

            var finalVal = '';

            for (var i = 0; i < content.length; i++) {
                var value = content[i];

                for (var j = 0; j < value.length; j++) {
                    var innerValue =  value[j]===null?'':value[j].toString();
                    var result = innerValue.replace(/"/g, '""');
                    if (result.search(/("|,|\n)/g) >= 0)
                        result = '"' + result + '"';
                    if (j > 0)
                        finalVal += ',';
                    finalVal += result;
                }

                finalVal += '\n';
            }

            Date.prototype.yyyymmdd = function() {
                var mm = this.getMonth() + 1; // getMonth() is zero-based
                var dd = this.getDate();

                return [this.getFullYear(), !mm[1] && '0', mm, !dd[1] && '0', dd].join(''); // padding
            };

            var date = new Date();

            var download = $('a[name="csv_export"]');
            download.attr('href', 'data:text/csv;charset=utf-8,' + encodeURIComponent(finalVal));
            download.attr('download', 'Utilization_' + date.yyyymmdd() +'.csv');
        });
    </script>
@endsection
