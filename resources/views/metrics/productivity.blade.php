@extends('layouts.master')

@section('styles')

@endsection

@section('overrides')
<style type="text/css">
    th {
        text-align: center;
    }

    .table > thead > tr > th {
        vertical-align: middle;
    }

    .table > tbody > tr > td {
        vertical-align: middle;
        text-align: center;
    }

    .table > tbody > tr > td.metrics {
        text-align: left;
    }
</style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Metrics</h2>
        <ol class="breadcrumb">
            <li>
                <a href="/metrics">Metrics</a>
            </li>
            <li class="active">
                <strong>Volume</strong>
            </li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Volume</h5>
                    <div class="ibox-tools">
                        <form action="/metric/volume" method="GET" id="form">
                            <div class="btn-group">
                                <button class="btn btn-xs {{ ($timespan == 'days') ? 'btn-primary' : 'btn-white' }}" type="submit" name="timespan" value="days">Day</button>
                                <button class="btn btn-xs {{ ($timespan == 'weeks') ? 'btn-primary' : 'btn-white' }}" type="submit" name="timespan" value="weeks">Week</button>
                                <button class="btn btn-xs {{ ($timespan == 'months') ? 'btn-primary' : 'btn-white' }}" type="submit" name="timespan" value="months">Month</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">Team</th>
                                    <th rowspan="2">Accout</th>
                                    <th rowspan="2">Metrics</th>
                                    @if ($timespan == "days")
                                    <th rowspan="2">D-3</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">D-2</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">D-1</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">D</th>
                                    @elseif ($timespan == "weeks")
                                    <th rowspan="2">W-3</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">W-2</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">W-1</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">W</th>
                                    @else
                                    <th rowspan="2">M-3</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">M-2</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">M-1</th>
                                    <th colspan="2">Variance</th>
                                    <th rowspan="2">M</th>
                                    @endif
                                    <th rowspan="2" colspan="2">Graph</th>
                                </tr>
                                <tr>
                                    <th>Value</th>
                                    <th>%</th>
                                    <th>Value</th>
                                    <th>%</th>
                                    <th>Value</th>
                                    <th>%</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($volume as $team)
                                    @for ($i = 0; $i < count($team['accounts']); $i++)
                                        @for ($j = 0; $j < count($team['accounts'][$i]['metrics']); $j++)
                                        <tr>
                                            @if ($i == 0 && $j == 0)
                                            <td rowspan="{{ $team['rowspan'] }}">{{ $team['name'] }}</td>
                                            @endif
                                            @if ($j == 0)
                                            <td rowspan="{{ $team['accounts'][$i]['rowspan'] }}">{{ $team['accounts'][$i]['name'] }}</td>
                                            @endif
                                            <td>{{ $team['accounts'][$i]['metrics'][$j]['name'] }}</td>
                                            @for ($k = 0; $k < count($team['accounts'][$i]['metrics'][$j]['cols']); $k++)
                                            <td>{{ $team['accounts'][$i]['metrics'][$j]['cols'][$k] }}</td>
                                            @endfor
                                            <td>
                                                <a data-toggle="modal" class="btn btn-default" href="#modal-form" data-graph="{{ $team['accounts'][$i]['metrics'][$j]['graph'] }}">
                                                    <span class="sparkline">{{ $team['accounts'][$i]['metrics'][$j]['graph'] }}</span>
                                                </a>
                                            </td>
                                        </tr>
                                        @endfor
                                    @endfor
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="modal-form" class="modal fade" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="ibox float-e-margins">
                                                <div class="ibox-title">
                                                    <h5>Line Chart Example
                                                        <small>With custom colors.</small>
                                                    </h5>
                                                </div>
                                                <div class="ibox-content">
                                                    <div>
                                                        <canvas id="lineChart" height="140"></canvas>
                                                    </div>
                                                </div>
                                                <div class="ibox-footer">
                                                    <button class="btn btn-default" id="to-image">to image</button>
                                                </div>
                                            </div>
                                        </div>        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <!-- html2canvas -->
    {!! Html::script('js/plugins/html2canvas/html2canvas.min.js') !!}
    <!-- canvas2image -->
    {!! Html::script('js/plugins/canvas2image/canvas2image.js') !!}

<script type="text/javascript">
    $(document).ready(function () {
        $(".sparkline").sparkline('html', {
            type: 'line',
            lineColor: '#17997f',
            fillColor: '#ffffff',
        });

        $("#modal-form").on('shown.bs.modal', function (e) {
            var graph = $(e.relatedTarget).data('graph');

            var lineData = {
                @if ($timespan == "days")
                labels: ["D-3", "D-2", "D-1", "D"],
                @elseif ($timespan == "weeks")
                labels: ["W-3", "W-2", "W-1", "W"],
                @else
                labels: ["M-3", "M-2", "M-1", "M"],
                @endif
                datasets: [
                    {
                        label: "Example dataset",
                        fillColor: "rgba(26,179,148,0.5)",
                        strokeColor: "rgba(26,179,148,0.7)",
                        pointColor: "rgba(26,179,148,1)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(26,179,148,1)",
                        data: JSON.parse("[" + graph + "]")
                    }
                ]
            };

            var lineOptions = {
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                bezierCurve: true,
                bezierCurveTension: 0.4,
                pointDot: true,
                pointDotRadius: 4,
                pointDotStrokeWidth: 1,
                pointHitDetectionRadius: 20,
                datasetStroke: true,
                datasetStrokeWidth: 2,
                datasetFill: true,
                responsive: true,
                scaleBeginAtZero: true,
            };

            var ctx = document.getElementById("lineChart").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

            $("#to-image").click(function () {
                html2canvas($("#lineChart"), {
                    useCORS: true,
                    background: "#fff",
                    onrendered: function(canvas) {
                        Canvas2Image.saveAsJPEG(canvas);
                    }
                });
            });
        });
    });
</script>
@endsection
