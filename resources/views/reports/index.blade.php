@extends('layouts.master')

@section('styles')
{!! Html::style('css/plugins/datapicker/datepicker3.css') !!}
@endsection

@section('overrides')
<style type="text/css">
    .client-detail {
        position: relative;
        height: auto;
    }
</style>
@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Generate Report</h2>
        <ol class="breadcrumb">
            <li>
                <p>Report</p>
            </li>
            <li class="active">
                <strong>External</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Filters</h5>
                </div>
                <div class="ibox-content">
                    <form action="/report" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campaign ID:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="campaign_id">
                                <p>Note: Please enter real numbers only</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Campaign name:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="campaign_name">
                                <p>eg: Wide-Out Workforces Inc.</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Team:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="team_id">
                                    <option value="0">-</option>
                                    @foreach ($teams as $team)
                                    <option value="{{ $team->id }}">{{ $team->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Partner:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="partner_id">
                                    <option value="0">-</option>
                                    @foreach ($partners as $partner)
                                    <option value="{{ $partner->id }}">{{ $partner->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Advertiser:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="advertiser_id">
                                    <option value="0">-</option>
                                    @foreach ($advertisers as $advertiser)
                                    <option value="{{ $advertiser->id }}">{{ $advertiser->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Client Contact:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="client_contact_id">
                                    <option value="0">-</option>
                                    @foreach ($client_contacts as $client_contact)
                                    <option value="{{ $client_contact->id }}">{{ $client_contact->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Assigned To:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="assigned_to">
                                <p>eg: John Doe</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Type:</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="type">
                                    <option value="0">-</option>
                                    <option value="1">Initial Build</option>
                                    <option value="2">Creative Iteration</option>
                                    <option value="3">Client Revision</option>
                                    <option value="4">Client Issue</option>
                                    <option value="5">File Path Update</option>
                                    <option value="6">Tracking Update</option>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Ad Type:</label>
                            <div class="col-sm-4">
                                <select class="form-control" name="ad_type">
                                    <option value="0">-</option>
                                    <option value="1">Inpage</option>
                                    <option value="2">Expanding</option>
                                    <option value="3">Pushdown</option>
                                    <option value="4">MDE</option>
                                    <option value="5">Floating</option>
                                    <option value="6">Inpage + Float</option>
                                    <option value="7">IM Expand</option>
                                    <option value="8">Tandem</option>
                                    <option value="9">Super EVA</option>
                                    <option value="10">Multi-floating</option>
                                    <option value="11">Peelback</option>
                                    <option value="12">Wallpaper/Reskin</option>
                                    <option value="13">Catfish</option>
                                    <option value="14">In-stream</option>
                                    <option value="15">VPAID Linear</option>
                                    <option value="16">VPAID Non-Linear</option>
                                    <option value="17">Lightbox</option>
                                    <option value="18">IHB</option>
                                    <option value="19">Mastbox</option>
                                    <option value="20">Masthead</option>
                                    <option value="21">Custom</option>
                                    <option value="22">Web</option>
                                    <option value="23">App</option>
                                    <option value="24">Static Banner</option>
                                    <option value="25">Animated Gif Banner</option>
                                    <option value="26">Desktop Landing Page</option>
                                    <option value="27">Mobile Landing Page</option>
                                    <option value="28">Standard Flash</option>
                                    <option value="29">Ad Mail</option>
                                    <option value="30">iOpener [DBG]</option>
                                    <option value="31">Scene Stealer [DBG]</option>
                                    <option value="32">Video Edit [TVG]</option>
                                    <option value="33">Mock Expand [InnoDes]</option>
                                    <option value="34">Mock Ad Extender [InnoDes]</option>
                                    <option value="35">Mock Ad Selector [InnoDes]</option>
                                    <option value="36">Mock Canvas [InnoDes]</option>
                                    <option value="37">Live Expand [InnoDes]</option>
                                    <option value="38">Live Ad Extender [InnoDes]</option>
                                    <option value="39">Live Ad Selector [InnoDes]</option>
                                    <option value="40">Live Canvas [InnoDes]</option>
                                    <option value="41">Hover</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Complexity:</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="complexity">
                                    <option value="0">-</option>
                                    <option value="0.13">0.13</option>
                                    <option value="0.50">0.50</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Priority:</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="priority">
                                    <option value="0">-</option>
                                    <option value="1">Low</option>
                                    <option value="2">Normal</option>
                                    <option value="3">High</option>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Status:</label>
                            <div class="col-sm-2">
                                <select class="form-control" name="status">
                                    <option value="0">-</option>
                                    <option value="1">New</option>
                                    <option value="2">Waiting</option>
                                    <option value="3">Unassigned</option>
                                    <option value="4">Info Request</option>
                                    <option value="5">In Progress</option>
                                    <option value="6">Hold</option>
                                    <option value="7">Solved</option>
                                    <option value="8">Closed</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="created-at">
                            <label class="col-sm-2 control-label">Created At:</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" name="created_at_start">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" name="created_at_end">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Committed At:</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" name="committed_at_start">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" name="committed_at_end">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Closed At:</label>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" name="closed_at_start">
                                </div>
                            </div>
                            <div class="col-sm-5">
                                <div class="input-group date">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control" name="closed_at_end">
                                </div>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit" name="consolidated" value="consolidated"><i class="fa fa-th-large"></i> Consolidated</button>
                                <button class="btn btn-info" type="submit" name="granular" value="granular"><i class="fa fa-th"></i> Granular</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{--<div class="col-lg-3">--}}
            {{--<div class="ibox">--}}
                {{--<div class="ibox-title">--}}
                    {{--<h5>Instructions</h5>--}}
                {{--</div>--}}
                {{--<div class="ibox-content">--}}
                    {{--<p>Under Construction</p>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
</div>
@endsection

@section('scripts')
<!-- Data picker -->
{!! Html::script('js/plugins/datapicker/bootstrap-datepicker.js') !!}
<script type="text/javascript">
    $(document).ready(function () {
        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true
        });
    });
</script>
@endsection
