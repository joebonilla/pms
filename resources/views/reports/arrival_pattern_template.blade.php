<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Billing - Invoice</title>

    <style type="text/css">
        .main-title{
            text-align: center;
        }

        td {
            width: 20px;
        }

        .hours-tbl td {
            width: 5px;
        }

        .title{
            font-weight: bold;
        }
    </style>
</head>
<body>
    <table border="1">
        <tr>
            <td colspan="8" class="main-title">ARRIVAL PATTERN</td>
        </tr>
        <tr>
            <td colspan="4">Team: </td>
            <td colspan="4">{{ $partner->name }}</td>
        </tr>
        <tr>
            <td colspan="4">From: </td>
            <td colspan="4">{{ $from }}</td>
        </tr>
        <tr>
            <td colspan="4">To: </td>
            <td colspan="4">{{ $to }}</td>
        </tr>
    </table>

    <h2>Days: </h2>
    <table border="1">
        <thead>
            <tr>
                @foreach($day as $key => $value)
                    <td class="title" colspan="2">{{ $key }}</td>
                @endforeach
                    <td class="title">Grand Total</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                @foreach($day as $key => $value)
                    <td colspan="2">{{ $value }}</td>
                @endforeach
                    <td colspan="2">{{ $days_total }}</td>
            </tr>
            <tr>
                <td> </td>
            </tr>
        </tbody>
    </table>

    <h2>Hours: </h2>
    <table class="hours-tbl" border="1">
        <thead>
            <tr>
                @foreach($hours as $key => $value)
                    <td class="title">{{ $key }}</td>
                @endforeach
                    <td class="title">Grand Total</td>
            </tr>
        </thead>
        <tbody>
        <tr>
            @foreach($hours as $key => $value)
                <td>{{ $value }}</td>
            @endforeach
                <td colspan="2">{{ $hours_total }}</td>
        </tr>
        <tr>
            <td> </td>
        </tr>
        </tbody>
    </table>
</body>
</html>