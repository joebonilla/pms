@extends('layouts.master')

@section('styles')

@endsection

@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Basic Form</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li>
                <a>Forms</a>
            </li>
            <li class="active">
                <strong>Basic Form</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-9">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>In-House Ticket</h5>
                </div>
                <div class="ibox-content">
                    <form action="/inhouse" method="POST" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Title:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="title">
                                {{--<span class="help-block m-b-none">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</span>--}}
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Description:</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" rows="10" name="description"></textarea>
                                {{--<span class="help-block m-b-none">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.</span>--}}
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Type:</label>
                            <div class="col-sm-4">
                                <select class="form-control m-b" name="type">
                                    <option value="1">Audit</option>
                                    <option value="2">Meeting / Huddles</option>
                                    <option value="3">Reports Generation</option>
                                    <option value="4">Training / Upskilling</option>
                                    <option value="5">Others</option>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Purpose:</label>
                            <div class="col-sm-4">
                                <select class="form-control m-b" name="purpose">
                                    <option value="1">Account Management / Sales</option>
                                    <option value="2">Activations / Projects</option>
                                    <option value="3">Facilities / IT</option>
                                    <option value="4">Finance</option>
                                    <option value="5">Human Resources</option>
                                    <option value="6">Service Delivery</option>
                                    <option value="7">Marketing</option>
                                    <option value="8">Business Development</option>
                                    <option value="9">Others</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Date Start:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="start_at">
                            </div>
                            <label class="col-sm-2 control-label">Date End:</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="end_at">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-primary" type="submit">Create</button>
                                <button class="btn btn-white" type="submit">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="ibox">
                <div class="ibox-title">
                    <h5>Instruction</h5>
                </div>
                <div class="ibox-content">
                    <p>Under Construction</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

@endsection