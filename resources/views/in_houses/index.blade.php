@extends('layouts.master')

@section('styles')
@endsection()

@section('content')
<div class="wrapper wrapper-content">
    <div class="row animated fadeInRight">
        <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Basic left float timeline</h5>
            </div>
            <div class="ibox-content inspinia-timeline">
                @foreach ($inhouses as $inhouse)
                <div class="timeline-item">
                    <div class="row">
                        <div class="col-xs-3 date">
                            <i class="fa fa-briefcase"></i>
                            6:00 am
                            <br/>
                            <small class="text-navy">2 hour ago</small>
                        </div>
                        <div class="col-xs-9 content no-top-border">
                            <a href="/inhouse/{{ $inhouse->id }}">
                                <p class="m-b-xs">
                                    <strong>{{ $inhouse->title }}</strong>
                                </p>
                            </a>
                            <p>{{ $inhouse->description }}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div style="text-align:center;">
                        {!! $inhouses->appends(Request::except('page'))->render() !!}
                    </div>
        </div>
    </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection
