<?php

return [
    'status' => [
        'open' => 'Open',
        'closed' => 'Closed'
    ],
    'type' => [
        'html5' => 'HTML5',
        'flash' => 'Flash',
        'php' => 'PHP'
    ]
];
