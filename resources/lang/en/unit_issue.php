<?php

return [
    'flag' => [
        'valid' => "Valid",
        'invalid' => "Invalid",
        'investigation' => "Investigation"
    ]
];