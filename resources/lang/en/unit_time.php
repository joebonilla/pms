<?php

return [
	'work' => [
		'creative' => 'Creative',
		'dev' => 'Development',
		'qa' => 'Quality Assurance',
		'review' => 'Review'
	],
	'action' => [
		'wait' => 'Wait',
		'hold' => 'Hold',
		'info_req' => 'Info Request',
		'in_prog' => 'In Progress'
	]
];