<?php

return [
    'label' => [
    	'pending' => 'Review',
        'dev_ready' => 'Development',
        'qa_ready' => 'QA',
        'rev' => 'Revision',
        'no_rev' => 'No Revision',
        'close' => 'Close',
    ]
];
