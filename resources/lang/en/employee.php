<?php

return [
	'ic' => [
		'creative' => 'Creative',
		'dev' => 'Developer',
		'qa' => 'QA',
		'poc' => 'POC',
		'sme' => 'SME',
		'mgmt' => 'Management'
	],
	'role' => [
		'ic' => 'IC',
		'sv' => 'Supervisor',
		'sa' => 'Super Admin'
	],
	'schedule' => [
		'day' => 'Day',
		'mid' => 'Mid',
		'night' => 'Night'
	]
];
