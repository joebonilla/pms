<?php

return [
    'flag' => [
        'not_done' => "Not Done",
        'pass' => "Pass",
        'fail' => "Fail"
    ]
];