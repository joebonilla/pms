<?php

return [
    'type' => [
        'audit' => 'Audit',
        'meeting_huddles' => 'Meeting / Huddles',
        'reports_generation' => 'Reports Generation',
        'training_upskilling' => 'Training / Upskilling',
        'others' => 'Others'
    ],
    'purpose' => [
        'account_management_sales' => 'Account Management / Sales',
        'activations_projects' => 'Activations / Projects',
        'facilities_it' => 'Facilities / IT',
        'finance' => 'Finance',
        'human_resources' => 'Human Resources',
        'service_delivery' => 'Service Delivery',
        'marketing' => 'Marketing',
        'business_development' => 'Business Development',
        'others' => 'Others'
    ],
    'status' => [
        'new' => 'New',
        'in_prog' => 'In Progress',
        'hold' => 'On Hold',
        'info_req' => 'Info Request',
        'closed' => 'Closed'
    ]
];