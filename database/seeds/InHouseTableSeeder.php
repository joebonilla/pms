<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class InHouseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 11; $i <= 20; $i++) { 
            $faker = Faker::create();

            DB::table('tickets')->insert([
                'type_tag' => 2,
                'status_tag' => 1,
                'employee_id' => 1,
                'created_at' => Carbon::now()
            ]);

            DB::table('in_houses')->insert([
                'ticket_id' => $i,
                'title' => $faker->catchPhrase,
                'description' => $faker->text,
                'purpose_tag' => 1,
                'type_tag' => 1,
                'status_tag' => 1,
                'start_at' => Carbon::now(),
                'end_at' => Carbon::tomorrow(),
                'created_at' => Carbon::now()
            ]);
        }
    }
}
