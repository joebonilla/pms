<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class CampaignsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10 ; $i++) {
            $faker = Faker::create();

            DB::table('tickets')->insert([
                'type_tag' => 1,
                'status_tag' => 1,
                'employee_id' => 1,
                'created_at' => Carbon::now()
            ]);

            DB::table('campaigns')->insert([
                'name' => $faker->company,
                'ticket_id' => $i,
                'team_id' => 1,
                'account_id' => 1,
                'campaign_client_id' => 1,
                'creative_brief_link' => $faker->url,
                'status_tag' => 1,
                'created_at' => Carbon::now()
            ]);

            $unit_count = rand(1, 10);

            for ($j = 1; $j <= $unit_count; $j++) {
                DB::table('units')->insert([
                    'campaign_id' => $i,
                    'name' => $faker->bs,
                    'complexity' => rand(1, 10),
                    'priority_tag' => rand(1, 3),
                    'employee_id' => null,
                    'status_tag' => 1,
                    'last_touch_tag' => null,
                    'created_at' => Carbon::now()
                ]);
            }
        }
    }
}
