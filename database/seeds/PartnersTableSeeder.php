<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PartnersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partners = [
            'AARP',
            'Goodway Group',
            'Spongecell',
            'TWC',
            'Innovid',
            'TVG',
            'Others'
        ];

        foreach ($partners as $partner) {
            DB::table('partners')->insert([
                'name' => $partner,
                'created_at' => Carbon::now()
            ]);
        }
    }
}
