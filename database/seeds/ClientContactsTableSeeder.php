<?php

use Illuminate\Database\Seeder;

class ClientContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contacts = [
            'Contact 1',
            'Contact 2',
            'Contact 3',
            'Contact 4',
            'Contact 5'
        ];

        foreach ($contacts as $c => $contact) {
            $partner_id = $c + 1;

            DB::table('client_contacts')->insert([
                'name' => $contact,
                'partner_id' => $partner_id
            ]);
        }
    }
}
