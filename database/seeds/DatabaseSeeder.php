<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(TeamsTableSeeder::class);
        $this->call(PartnersTableSeeder::class);
        $this->call(AccountsTableSeeder::class);
        $this->call(AdvertiserTableSeeder::class);
        $this->call(ClientContactsTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
        $this->call(BenchmarkTableSeeder::class);
        
        // $this->call(CampaignClientsTableSeeder::class);
        // $this->call(TicketsTableSeeder::class);
        // $this->call(CampaignsTableSeeder::class);
        // $this->call(UnitsTableSeeder::class);
        // $this->call(InHouseTableSeeder::class);

        Model::reguard();
    }
}
