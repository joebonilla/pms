<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
            [
                'name' => 'Unit 1',
                'complexity' => 10,
                'priority_tag' => 3,
            ],
            [
                'name' => 'Unit 2',
                'complexity' => 10,
                'priority_tag' => 3,
            ],
            [
                'name' => 'Unit 3',
                'complexity' => 5,
                'priority_tag' => 2,
            ],
            [
                'name' => 'Unit 4',
                'complexity' => 5,
                'priority_tag' => 2,
            ],
            [
                'name' => 'Unit 5',
                'complexity' => 1,
                'priority_tag' => 1,
            ]
        ];

        foreach ($units as $unit) {
            DB::table('units')->insert([
                'campaign_id' => 1,
                'name' => $unit['name'],
                'complexity' => $unit['complexity'],
                'priority_tag' => $unit['priority_tag'],
                'employee_id' => null,
                'status_tag' => 1,
                'last_touch_tag' => null,
                'created_at' => Carbon::now()
            ]);
        }
    }
}
