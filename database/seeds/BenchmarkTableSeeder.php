<?php


use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class BenchmarkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $benchmarks = [
            1 => [
                'team_id' => '1',
                'complexity' => '1',
                'benchmark' => '1'
            ],
            2 => [
                'team_id' => '1',
                'complexity' => '2',
                'benchmark' => '2'
            ],
            3 => [
                'team_id' => '1',
                'complexity' => '3',
                'benchmark' => '3'
            ],
            4 => [
                'team_id' => '1',
                'complexity' => '4',
                'benchmark' => '4'
            ],
            5 => [
                'team_id' => '1',
                'complexity' => '5',
                'benchmark' => '5'
            ],
            6 => [
                'team_id' => '1',
                'complexity' => '6',
                'benchmark' => '6'
            ],
            7 => [
                'team_id' => '1',
                'complexity' => '7',
                'benchmark' => '7'

            ],
            8 => [
                'team_id' => '1',
                'complexity' => '8',
                'benchmark' => '8'

            ],
            9 => [
                'team_id' => '1',
                'complexity' => '9',
                'benchmark' => '9'

            ],
            10 => [
                'team_id' => '1',
                'complexity' => '10',
                'benchmark' => '10'

            ]
        ];

        foreach ($benchmarks as $key => $benchmark) {
            DB::table('team_benchmark')->insert([
                'team_id' => $benchmark['team_id'],
                'complexity' => $benchmark['complexity'],
                'benchmark' => $benchmark['benchmark'],
                'created_at' => Carbon::now()
            ]);
        }
    }
}
