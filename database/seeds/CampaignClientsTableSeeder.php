<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CampaignClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clients = [
            'AARP',
            'Agency5 Media',
            'Dentsu Mobius',
            'Evite',
            'Goodway Group',
            'iFlix',
            'Innovid',
            'Medialets',
            'Philo',
            'Sea Cocoon Hotel',
            'Spongecell',
            'StubHub',
            'The Weather Channel',
            'The Visionaire Group',
            'Victorious',
            'Others'
        ];

        foreach ($clients as $c => $client) {
            DB::table('campaign_clients')->insert([
                'name' => $client,
                'created_at' => Carbon::now()
            ]);   
        }
    }
}
