<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts = [
            2 => [1, 2, 3, 4, 7],
            3 => [5, 7],
            4 => [5, 7],
            5 => [5, 7],
            6 => [6, 7],
        ];

        for ($i = 2; $i <= 6; $i++) {
            $account = $accounts[$i];

            foreach ($account as $a) {
                DB::table('accounts')->insert([
                    'team_id' => $i,
                    'partner_id' => $a,
                    'created_at' => Carbon::now()
                ]);
            }
        }
    }
}
