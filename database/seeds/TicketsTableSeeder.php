<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class TicketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $campaign_id = 1;

        for ($i = 1; $i <= 200 ; $i++) {
            $faker = Faker::create();

            $type = rand(1, 2);

            if ($type == 1) {
                $subweek = rand(0, 3);
                $completed_week = $subweek + 1;

                DB::table('tickets')->insert([
                    'type_tag' => 1,
                    'status_tag' => 1,
                    'employee_id' => 1,
                    'created_at' => Carbon::now()->subWeeks($subweek)->addDay()
                ]);

                $team = rand(1, 5);

                if ($team == 1) {
                    $account = rand(1, 5);
                } else if ($team == 2) {
                    $account = 6;
                } else if ($team == 3) {
                    $account = 7;
                } else if ($team == 4) {
                    $account = 8;
                } else if ($team == 5) {
                    $account = rand(9, 10);
                }

                DB::table('campaigns')->insert([
                    'name' => $faker->company,
                    'ticket_id' => $i,
                    'team_id' => $team,
                    'account_id' => $account,
                    'campaign_client_id' => 1,
                    'creative_brief_link' => $faker->url,
                    'status_tag' => 2,
                    'created_at' => Carbon::now()->subWeeks($subweek)->addDay(),
                    'closed_at' => Carbon::now()->subWeeks($completed_week)->addDay()
                ]);

                $unit_count = rand(1, 10);

                $initial_build = 0;
                $creative_iteration = 0;

                for ($j = 1; $j <= $unit_count; $j++) {
                    $type2 = rand(1, 2);

                    // force the first unit to be initial build
                    // if ($j == 1 && $type2 == 2) {
                    //     $type2 = 1;
                    // }

                    DB::table('units')->insert([
                        'campaign_id' => $campaign_id,
                        'name' => $faker->bs,
                        'complexity' => rand(1, 10),
                        'priority_tag' => rand(1, 3),
                        'type_tag' => $type2,
                        'employee_id' => null,
                        'status_tag' => 8,
                        'last_touch_tag' => null,
                        'committed_at' => Carbon::now()->addWeek(),
                        'created_at' => Carbon::now()->subWeeks($subweek)->addDay(),
                        'closed_at' => Carbon::now()->subWeeks($completed_week)->addDay()
                    ]);

                    if ($type2 == 1) {
                        $initial_build++;
                    } else {
                        $creative_iteration++;
                    }
                }

                DB::table('campaign_quantities')->insert([
                    'campaign_id' => $campaign_id,
                    'initial_build' => $initial_build,
                    'creative_iteration' => $creative_iteration,
                    'client_revision' => 0,
                    'created_at' => Carbon::now()->subWeeks($subweek)->addDay()
                ]);

                $campaign_id++;
            } else {
                DB::table('tickets')->insert([
                    'type_tag' => 2,
                    'status_tag' => 1,
                    'employee_id' => 1,
                    'created_at' => Carbon::now()
                ]);

                DB::table('in_houses')->insert([
                    'ticket_id' => $i,
                    'title' => $faker->catchPhrase,
                    'description' => $faker->text,
                    'purpose_tag' => 1,
                    'type_tag' => 1,
                    'status_tag' => 1,
                    'start_at' => Carbon::now(),
                    'end_at' => Carbon::tomorrow(),
                    'created_at' => Carbon::now()
                ]);
            }
        }
    }
}
