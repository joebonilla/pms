<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = [
            1 => [
                'name' => 'Management',
                'type' => 1
            ],
            2 => [
                'name' => 'Dev Pool',
                'type' => 2
            ],
            3 => [
                'name' => 'Innovid Post-Prod Mid',
                'type' => 2
            ],
            4 => [
                'name' => 'Innovid Post-Prod Night',
                'type' => 2
            ],
            5 => [
                'name' => 'Innovid Design',
                'type' => 2
            ],
            6 => [
                'name' => 'TVG',
                'type' => 2
            ],
            7 => [
                'name' => 'QA Pool',
                'type' => 3
            ],
        ];

        foreach ($teams as $key => $team) {
            DB::table('teams')->insert([
                'name' => $team['name'],
                'type_tag' => $team['type'],
                'created_at' => Carbon::now()
            ]);
        }
    }
}
