<?php

use Illuminate\Database\Seeder;

class AdvertiserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $advertisers = [
            'AARP - Ford',
            'Goodway Group - Ford',
            'Spongecell - Ford',
            'TWC - Ford',
            'TVG - Ford',
            'Innovid - Ford',
            'Others'
        ];

        foreach ($advertisers as $a => $advertiser) {
            $partner_id = $a + 1;

            DB::table('advertisers')->insert([
                'name' => $advertiser,
                'partner_id' => $partner_id
            ]);
        }
    }
}
