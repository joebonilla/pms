<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employees = [
            [
                'name' => 'PMS System',
                'email' => 'prod3@wideout.com',
                'nickname' => 'PMS',
                'team_id' => 1,
                'ic_tag' => 6,
                'role_tag' => 3,
                'schedule_tag' => 1
            ],
            [
                'name' => 'Edgar Ryan Samson',
                'email' => 'er.samson@wideout.com',
                'nickname' => 'ER',
                'team_id' => 1,
                'ic_tag' => 6,
                'role_tag' => 3,
                'schedule_tag' => 1
            ],
            [
                'name' => 'Christian Estrito',
                'email' => 'christian.estrito@wideout.com',
                'nickname' => 'Bryan',
                'team_id' => 2,
                'ic_tag' => 2,
                'role_tag' => 1,
                'schedule_tag' => 1
            ],
            [
                'name' => 'Joe Marie Bonilla',
                'email' => 'joe.bonilla@wideout.com',
                'nickname' => 'JM',
                'team_id' => 2,
                'ic_tag' => 2,
                'role_tag' => 1,
                'schedule_tag' => 1
            ],
            [
                'name' => 'Kristine Iris Salvador',
                'email' => 'kristine.salvador@wideout.com',
                'nickname' => 'Iris',
                'team_id' => 2,
                'ic_tag' => 2,
                'role_tag' => 1,
                'schedule_tag' => 1
            ],

            [
                'name' => 'Keane Almo',
                'email' => 'keane.almo@wideout.com',
                'nickname' => 'Keane',
                'team_id' => 2,
                'ic_tag' => 2,
                'role_tag' => 1,
                'schedule_tag' => 1
            ],
            [
                'name' => 'Gabby Manaloto',
                'email' => 'gabby.manaloto@wideout.com',
                'nickname' => 'Gabby',
                'team_id' => 2,
                'ic_tag' => 6,
                'role_tag' => 2,
                'schedule_tag' => 3
            ],
            [
                'name' => 'John Christopher Pena',
                'email' => 'johncristopher.pena@wideout.com',
                'nickname' => 'Topher',
                'team_id' => 2,
                'ic_tag' => 4,
                'role_tag' => 2,
                'schedule_tag' => 3
            ],
            [
                'name' => 'Raiza Del Mundo',
                'email' => 'raiza.delmundo@wideout.com',
                'nickname' => 'Raiza',
                'team_id' => 2,
                'ic_tag' => 4,
                'role_tag' => 2,
                'schedule_tag' => 3
            ],
            [
                'name' => 'Danil Hementiza',
                'email' => 'danil.hementiza@wideout.com',
                'nickname' => 'Danil',
                'team_id' => 3,
                'ic_tag' => 4,
                'role_tag' => 2,
                'schedule_tag' => 2
            ],
            [
                'name' => 'Jenice Pulga',
                'email' => 'jenice.pulga@wideout.com',
                'nickname' => 'Jenice',
                'team_id' => 4,
                'ic_tag' => 4,
                'role_tag' => 2,
                'schedule_tag' => 3
            ],
            [
                'name' => 'Angelo Bagsit',
                'email' => 'angelo.bagsit@wideout.com',
                'nickname' => 'Don',
                'team_id' => 5,
                'ic_tag' => 4,
                'role_tag' => 2,
                'schedule_tag' => 1
            ],
            [
                'name' => 'Mark Gabriel Misa',
                'email' => 'mark.misa@wideout.com',
                'nickname' => 'Misa',
                'team_id' => 6,
                'ic_tag' => 4,
                'role_tag' => 2,
                'schedule_tag' => 3
            ]
        ];

        foreach ($employees as $employee) {
            DB::table('employees')->insert([
                'name' => $employee['name'],
                'email' => $employee['email'],
                'nickname' => $employee['nickname'],
                'ic_tag' => $employee['ic_tag'],
                'team_id' => $employee['team_id'],
                'role_tag' => $employee['role_tag'],
                'schedule_tag' => $employee['schedule_tag'],
                'created_at' => Carbon::now()
            ]);
        }
    }
}
