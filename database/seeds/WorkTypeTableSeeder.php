<?php

use Illuminate\Database\Seeder;

class WorkTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $workTypes = [
            'Grab', 'Assign', 'Drop', 'In Progress', 'Info Requested', 'On Hold', 'Close'
        ];

        foreach ($workTypes as $workType) {
            DB::table('work_type')->insert([
                'name' => $workType,
            ]);
        }
    }
}
