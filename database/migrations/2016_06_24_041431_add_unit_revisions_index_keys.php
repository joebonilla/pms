<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitRevisionsIndexKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unit_revisions', function (Blueprint $table) {
            $table->foreign('unit_id')
                ->references('id')
                ->on('units');
            $table->foreign('employee_dev_id')
                ->references('id')
                ->on('employees');
            $table->foreign('employee_qa_id')
                ->references('id')
                ->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unit_revisions', function (Blueprint $table) {
            $table->dropForeign('unit_revisions_unit_id_foreign');
            $table->dropForeign('unit_revisions_employee_dev_id_foreign');
            $table->dropForeign('unit_revisions_employee_qa_id_foreign');
        });
    }
}
