<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 70);
            $table->integer('unit_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->smallInteger('flag_tag');
            $table->text('note');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unit_issues');
    }
}
