<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unit_id')->unsigned();
            $table->integer('employee_id')->unsigned();
            $table->smallInteger('work_estimate_tag')->unsigned();
            $table->text('work_estimate_notes')->nullable();
            $table->smallInteger('overall_quality_tag')->unsigned();
            $table->text('overall_quality_notes')->nullable();
            $table->smallInteger('following_instructions_tag')->unsigned();
            $table->text('following_instructions_notes')->nullable();
            $table->smallInteger('process_adherence_tag')->unsigned();
            $table->text('process_adherence_notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('unit_audits');
    }
}
