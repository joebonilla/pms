<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitIssuesIndexKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unit_issues', function (Blueprint $table) {
            $table->foreign('unit_id')
                ->references('id')
                ->on('units');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unit_issues', function (Blueprint $table) {
            $table->dropForeign('unit_issues_unit_id_foreign');
            $table->dropForeign('unit_issues_employee_id_foreign');
        });
    }
}
