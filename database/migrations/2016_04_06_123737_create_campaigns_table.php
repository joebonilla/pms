<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 70);
            $table->integer('ticket_id')->unsigned();
            $table->integer('team_id')->unsigned();
            $table->integer('partner_id')->unsigned();
            $table->string('sub_partner_name')->nullable();
            $table->integer('advertiser_id')->unsigned();
            $table->string('sub_advertiser_name', 70)->nullable();
            $table->smallInteger('type_tag')->unsigned();
            $table->smallInteger('status_tag')->unsigned();
            $table->string('creative_brief_link', 2083)->nullable();
            $table->dateTime('closed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaigns');
    }
}
