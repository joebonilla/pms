<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('units', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 70);
            $table->integer('team_id')->unsigned();
            $table->integer('partner_id')->unsigned();
            $table->integer('advertiser_id')->unsigned();
            $table->integer('campaign_id')->unsigned();
            $table->integer('client_contact_id')->unsigned();
            $table->integer('employee_id')->unsigned()->nullable();
            $table->float('complexity')->unsigned();
            $table->float('qa_complexity')->unsigned();
            $table->smallInteger('priority_tag')->unsigned();
            $table->smallInteger('type_tag')->unsigned();
            $table->smallInteger('status_tag')->unsigned();
            $table->smallInteger('ad_type_tag')->unsigned();
            $table->boolean('billable');
            $table->string('preview_link', 2083)->nullable();
            $table->string('qa_report_link', 2083)->nullable();
            $table->dateTime('started_at')->nullable(); // first grab
            $table->dateTime('completed_at')->nullable(); // dev-qa ticket complete - auto
            $table->dateTime('closed_at')->nullable(); // poc ticket complete - auto
            $table->dateTime('committed_at')->nullable(); // client deadline - fill-up
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('units');
    }
}
