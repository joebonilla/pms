<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql')->create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email', 255)->unique();
            $table->string('name', 70);
            $table->string('nickname', 70)->nullable();
            $table->string('avatar_file', 2083)->nullable();
            $table->integer('team_id')->unsigned();
            $table->smallInteger('ic_tag')->unsigned();
            $table->smallInteger('role_tag')->unsigned();
            $table->smallInteger('schedule_tag')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
