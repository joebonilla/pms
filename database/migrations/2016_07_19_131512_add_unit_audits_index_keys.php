<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitAuditsIndexKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unit_audits', function (Blueprint $table) {
            $table->foreign('unit_id')
                ->references('id')
                ->on('units');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unit_audits', function (Blueprint $table) {
            $table->dropForeign('unit_audits_unit_id_foreign');
            $table->dropForeign('unit_audits_employee_id_foreign');
        });
    }
}
