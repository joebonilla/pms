<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitsIndexKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('units', function (Blueprint $table) {
            $table->foreign('team_id')
                ->references('id')
                ->on('teams');
            $table->foreign('partner_id')
                ->references('id')
                ->on('partners');
            $table->foreign('advertiser_id')
                ->references('id')
                ->on('advertisers');
            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns');
            $table->foreign('client_contact_id')
                ->references('id')
                ->on('client_contacts');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('units', function (Blueprint $table) {
            $table->dropForeign('units_team_id_foreign');
            $table->dropForeign('units_partner_id_foreign');
            $table->dropForeign('units_advertiser_id_foreign');
            $table->dropForeign('units_campaign_id_foreign');
            $table->dropForeign('units_client_contact_id_foreign');
            $table->dropForeign('units_employee_id_foreign');
        });
    }
}
