<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitTimesIndexKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('unit_times', function (Blueprint $table) {
            $table->foreign('team_id')
                ->references('id')
                ->on('teams');
            $table->foreign('partner_id')
                ->references('id')
                ->on('partners');
            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns');
            $table->foreign('unit_id')
                ->references('id')
                ->on('units');
            $table->foreign('employee_id')
                ->references('id')
                ->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('unit_times', function (Blueprint $table) {
            $table->dropForeign('unit_times_team_id_foreign');
            $table->dropForeign('unit_times_partner_id_foreign');
            $table->dropForeign('unit_times_campaign_id_foreign');
            $table->dropForeign('unit_times_unit_id_foreign');
            $table->dropForeign('unit_times_employee_id_foreign');
        });
    }
}
