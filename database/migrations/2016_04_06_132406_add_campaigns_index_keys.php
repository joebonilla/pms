<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampaignsIndexKeys extends Migration
{
        /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->foreign('ticket_id')
                ->references('id')
                ->on('tickets');
            $table->foreign('team_id')
                ->references('id')
                ->on('teams');
            $table->foreign('partner_id')
                ->references('id')
                ->on('partners');
            $table->foreign('advertiser_id')
                ->references('id')
                ->on('advertisers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropForeign('campaigns_ticket_id_foreign');
            $table->dropForeign('campaigns_team_id_foreign');
            $table->dropForeign('campaigns_partner_id_foreign');
            $table->dropForeign('campaigns_advertiser_id_foreign');
        });
    }
}
