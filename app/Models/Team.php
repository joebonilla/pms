<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Team extends Model
{
    use SoftDeletes;

    const TEAM_MANAGEMENT = 1;
    const TEAM_DEVPOOL = 2;
    const TEAM_INNOVID_PP_MID = 3;
    const TEAM_INNOVID_PP_NIGHT = 4;
    const TEAM_INNOVID_DESIGN = 5;
    const TEAM_TVG = 6;
    const TEAM_QA = 7;

    const TYPE_NON_BUNDY_BASED = 1;
    const TYPE_ACCOUNT_BASED = 2;
    const TYPE_SCHEDULE_BASED = 3;

    const DISPLAY_LIMIT = 21;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'teams';

    /**
     * Appended table attributes
     *
     * @var Array
     */
    protected $appends = [
        'poc',
        'member_display'
    ];

    /**
     * Account model relationship
     *
     * @return Object Has many accounts
     */
    public function accounts()
    {
        return $this->belongsToMany('App\Models\Partner', 'accounts', 'team_id', 'partner_id');
    }

    /**
     * Campaign model relationship
     *
     * @return Object Has many campaigns
     */
    public function campaigns()
    {
        return $this->hasMany('App\Models\Campaign', 'team_id', 'id');
    }

    /**
     * Unit model relationship
     *
     * @return Object Has many units
     */
    public function units()
    {
        return $this->hasMany('App\Models\Unit', 'team_id', 'id');
    }

    /**
     * UnitTime model relationship
     *
     * @return Object Has many unit times
     */
    public function unitTimes()
    {
        return $this->hasMany('App\Models\UnitTime', 'team_id', 'id');
    }

    /**
     * Employee model relationship
     *
     * @return Object Has many members
     */
    public function members()
    {
        return $this->hasMany('App\Models\Employee', 'team_id', 'id');
    }

    /**
     * Team Headcount model relationship
     *
     * @return Object Has many members
     */
    public function teamHeadcount()
    {
        return $this->hasMany('App\Models\TeamHeadcount', 'team_id', 'id');
    }

    /**
     * [getPocAttribute description]
     * @return [type] [description]
     */
    public function getPocAttribute()
    {
        return $this->members()->where('ic_tag', Employee::IC_POC)
            ->where('role_tag', Employee::ROLE_SV)
            ->first();
    }

    /**
     * [getMemberDisplayAttribute description]
     * @return [type] [description]
     */
    public function getMemberDisplayAttribute()
    {
        $members = $this->members()
            ->where('ic_tag', '!=', Employee::IC_MGMT)
            ->where('role_tag', '!=', Employee::ROLE_SA)
            ->orderBy('role_tag', 'desc')
            ->orderBy('ic_tag', 'asc')
            ->get();

        $limit = $members->take(self::DISPLAY_LIMIT);

        return $limit;
    }

    /**
     * [scopeTypeBased description]
     * @param  [type] $query [description]
     * @param  [type] $type  [description]
     * @return [type]        [description]
     */
    public function scopeTypeBased($query, $type)
    {
        return $query->where('type_tag', $type);
    }

    /**
     * [scopeDisplayable description]
     * @param  [type] $query [description]
     * @return [type]        [description]
     */
    public function scopeDisplayable($query)
    {
        return $query->where('type_tag', self::TYPE_ACCOUNT_BASED)
            ->orWhere('type_tag', self::TYPE_SCHEDULE_BASED);
    }
}
