<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class UnitTimeDev extends Model
{
    use SoftDeletes;

    const WORK_TYPE = 'Dev';

    const ACTION_WAIT = 1;
    const ACTION_INFO_REQ = 2;
    const ACTION_IN_PROG = 3;
    const ACTION_HOLD = 4;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_time_devs';

    protected $dates = ['start_at', 'end_at', 'created_at', 'updated_at', 'deleted_at'];

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function getActionAttribute()
    {
        switch ($this->action_tag) {
            case UnitTime::ACTION_WAIT:
                return trans('unit_time.action.wait');
            case UnitTime::ACTION_HOLD:
                return trans('unit_time.action.hold');
            case UnitTime::ACTION_INFO_REQ:
                return trans('unit_time.action.info_req');
            case UnitTime::ACTION_IN_PROG:
                return trans('unit_time.action.in_prog');
        }
    }

    public function getWorkAttribute()
    {
        return self::WORK_TYPE;
    }

    public function scopeInProgress($query)
    {
        $query->where('action_tag', self::ACTION_IN_PROG);
    }

    public function scopeActionAt($query, Carbon $initial_date, $event, $date_group, $length = 0, $onwards = true)
    {
        $start = $initial_date;
        $end = $start->copy();

        if ($onwards) {
            if ($date_group == "days") {
                $start->addDays($length)->startOfDay();
                $end->addDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }
                
                $start->addWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->addWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->addMonths($length)->startOfMonth()->startOfDay();
                $end->addMonths($length)->endOfMonth()->endOfDay();
            }
        } else {
            if ($date_group == "days") {
                $start->subDays($length)->startOfDay();
                $end->subDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }

                $start->subWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->subWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->subMonths($length)->startOfMonth()->startOfDay();
                $end->subMonths($length)->endOfMonth()->endOfDay();
            }
        }

        if ($event == "in_prog") {
            return $query->where('action_tag', self::ACTION_IN_PROG)
                ->whereNotNull('start_at')
                ->whereNotNull('end_at')
                ->where('start_at', '>=', $start)
                ->where('end_at', '<=', $end);
        }
    }
}
