<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class UnitIssue extends Model
{
    use SoftDeletes;

    const FLAG_VALID = 1;
    const FLAG_INVALID = 2;
    const FLAG_INVESTIGATION = 3;

    protected $appends = ['flag'];

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }

    public function creator()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function getFlagAttribute()
    {
        switch ($this->flag_tag) {
            case self::FLAG_VALID:
                return trans('unit_issue.flag.valid');
            case self::FLAG_INVALID:
                return trans('unit_issue.flag.invalid');
            case self::FLAG_INVESTIGATION:
                return trans('unit_issue.flag.investigation');
        }
    }
}
