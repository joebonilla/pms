<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

/**
 * Campaign model
 */
class Campaign extends Model
{
    use SoftDeletes;

    const TYPE_HTML5 = 1;
    const TYPE_FLASH = 2;
    const TYPE_PHP = 3;

    const STATUS_OPEN = 1;
    const STATUS_CLOSED = 2;
    const EXTRA_STATUS_LATE = 3;

    const SUB_STATUS_LATE = 1;
    const SUB_STATUS_DUE_3 = 2;
    const SUB_STATUS_DUE_9 = 3;
    const SUB_STATUS_DUE_24 = 4;
    const SUB_STATUS_NORMAL = 5;
    const SUB_STATUS_CLOSED = 6;

    const PARTICIPANTS_LIMIT = 4;

    // Misc constants
    const SECONDS_IN_HOUR = 3600;
    const TWO_DECIMAL_PLACES = 2;

    /**
     * The database table used by the model.
     *
     * @var String
     */
    protected $table = 'campaigns';

    /**
     * Appended table attributes
     *
     * @var Array
     */
    protected $appends = [
        'creator',
        'status',
        'creative_brief',
        'progress',
        'participants',
        'sub_status_tag',
        'tat',
        'committed_at'
    ];

    /**
     * Ticket model relationship
     *     
     * @return Object Belongs to one ticket
     */
    public function ticket()
    {
        return $this->belongsTo('App\Models\Ticket', 'ticket_id', 'id');
    }

    /**
     * Team model relationship
     *
     * @return Object Belongs to one team
     */
    public function team()
    {
        return $this->belongsTo('App\Models\Team', 'team_id', 'id');
    }

    /**
     * Partner model relationship
     *
     * @return Object Belongs to one partner
     */
    public function partner()
    {
        return $this->belongsTo('App\Models\Partner', 'partner_id', 'id');
    }

    public function advertiser()
    {
        return $this->belongsTo('App\Models\Advertiser', 'advertiser_id', 'id');
    }

    /**
     * Unit model relationship
     *
     * @return Object Has many units
     */
    public function units()
    {
        return $this->hasMany('App\Models\Unit', 'campaign_id', 'id');
    }

    /**
     * UnitTimes model relationship
     *
     * @return Object Has many unit times
     */
    public function unitTimes()
    {
        return $this->hasMany('App\Models\UnitTime', 'campaign_id', 'id');
    }

    public function getCreatorAttribute()
    {
        return $this->ticket->creator;
    }

    /**
     * Status appended attribute
     *
     * @return Object Translated status value
     */
    public function getStatusAttribute()
    {
        switch ($this->status_tag) {
            case self::STATUS_OPEN:
                return trans('campaign.status.open');
            case self::STATUS_CLOSED:
                return trans('campaign.status.closed');
        }
    }

    /**
     * Creative Brief appended attribute
     * Link can modified before fetching
     *
     * @return String Creative brief url
     */
    public function getCreativeBriefAttribute()
    {
        return $this->creative_brief_link;
    }

    /**
     * Participants appended attribute
     *
     * @return Array Collection of splice participants, limit 4
     */
    public function getParticipantsAttribute()
    {
        $collection = collect([$this->ticket->creator]);

        $assigned_units = [];

        foreach ($this->units as $unit) {
            if ($unit->assignee) {
                $assigned_units[] = $unit->assignee;
            }
        }

        $merged = $collection->merge($assigned_units);
        $uniqued = $merged->unique();

        if ($uniqued->count() > self::PARTICIPANTS_LIMIT) {
            $participants = $uniqued->splice(self::PARTICIPANTS_LIMIT);
        } else {
            $participants = $uniqued;
        }

        return $participants;
    }

    /**
     * Progress appended attribute
     *
     * @return Integer Progress percentage, real number
     */
    public function getProgressAttribute()
    {
        $total_units = $this->units->count();
        $progress = 0;

        if ($total_units) {
            foreach ($this->units as $unit) {
                if ($unit) {
                    $progress += $unit->progress;
                }
            }

            $percentage = $progress / $total_units;
            $rounded = round($percentage, self::TWO_DECIMAL_PLACES);

            return $rounded;
        }

        return $progress;
    }

    /**
     * Tat appended attribute
     *
     * @return Float Computed tat of campaign
     */
    public function getTatAttribute()
    {
        $first_created_unit = $this->units()->orderBy('created_at', 'asc')->first();
        $last_closed_unit = $this->units()->orderBy('closed_at', 'desc')->first();

        $tat_time = $last_closed_unit->closed_at->diffInSeconds($first_created_unit->created_at);

        return round($tat_time / self::SECONDS_IN_HOUR, self::TWO_DECIMAL_PLACES);
    }

    /**
     * CommittedAt appended attribute
     * Campaign's units earliest committed date
     *
     * @return Carbon Date time
     */
    public function getCommittedAtAttribute()
    {
        return $this->units->max('committed_at');
    }

    public function getSubStatusTagAttribute()
    {
        $min_commit_date = $this->units->min('committed_at');
        
        $now = Carbon::now();
        $three_hours = Carbon::now()->addHours(3);
        $nine_hours = Carbon::now()->addHours(9);
        $twentyfour_hours = Carbon::now()->addHours(24);

        if ($this->status_tag == self::STATUS_OPEN) {
            if ($twentyfour_hours->gt($min_commit_date) && $nine_hours->lt($min_commit_date)) {
                $sub_status = self::SUB_STATUS_DUE_24;
            } else if ($nine_hours->gt($min_commit_date) && $three_hours->lt($min_commit_date)) {
                $sub_status = self::SUB_STATUS_DUE_9;
            } else if ($three_hours->gt($min_commit_date) && $now->lt($min_commit_date)) {
                $sub_status = self::SUB_STATUS_DUE_3;
            } else if ($now->gt($min_commit_date)) {
                $sub_status = self::SUB_STATUS_LATE;
            } else {
                $sub_status = self::SUB_STATUS_NORMAL;
            }
        } else {
            $sub_status = self::SUB_STATUS_CLOSED;
        }

        return $sub_status;
    }

    // SCOPES
    public function scopeInAccount($query, $account_id)
    {
        return $query->where('account_id', $account_id);
    }

    public function scopeInTeam($query, $team_id)
    {
        return $query->where('team_id', $team_id);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('campaigns.status_tag', $status);
    }

    public function scopeClosed($query)
    {
        return $query->where('status_tag', self::STATUS_CLOSED);
    }

    public function scopeClosedFrom($query, $date)
    {
        return $query->where('closed_at', '>=', $date);
    }

    public function scopeClosedTo($query, $date)
    {
        return $query->where('closed_at', '<=', $date);
    }

    public function scopeCreatedFrom($query, $date)
    {
        return $query->where('created_at', '>=', $date);
    }

    public function scopeCreatedTo($query, $date)
    {
        return $query->where('created_at', '<=', $date);
    }

    public function scopeLate($query, $late = true)
    {
        return $query->where('campaigns.status_tag', self::STATUS_OPEN)
            ->whereHas('units', function ($query2) {
                $query2->where('committed_at', '<', Carbon::now());
            }
        );
    }

    public function scopeDueIn($query, $date)
    {
        return $query->where('status_tag', self::STATUS_OPEN)
            ->whereHas('units', function ($query2) use ($date) {
                $query2->where('status_tag', '!=', Unit::STATUS_CLOSED)
                    ->where('committed_at', '>', Carbon::now())
                    ->where('committed_at', '<=', $date);
            });
    }

    public function scopeDueBetween($query, $from, $to)
    {
        return $query->where('status_tag', self::STATUS_OPEN)
            ->whereHas('units', function ($query2) use ($from, $to) {
                $query2->where('status_tag', '!=', Unit::STATUS_CLOSED)
                    ->where('committed_at', '>', $from)
                    ->where('committed_at', '<=', $to);
            }
        );
    }

    public function scopeLike($query, $search, $column = "name")
    {
        return $query->where($column, 'like', '%'.$search.'%');
    }


////////////////////////////////////////////////////////////////////////////////


    public function scopeCreatedThisMonth($query)
    {
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();

        $query->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end);
    }

    public function scopeCreatedThisDay($query, $date)
    {
        $start = Carbon::now();
        $start->month = $month;
        $start_of_month = $start->startOfMonth();

        $end = Carbon::now();
        $end->month = $month;
        $end_of_month = $end->endOfMonth();

        $query->where('created_at', '>=', $start_of_month)
            ->where('created_at', '<=', $end_of_month);
    }

    public function scopeCreatedThisWeek($query, $weeks = 0, $onwards = true)
    {
        if ($onwards) {
            $start = Carbon::now()->addWeeks($weeks)->startOfWeek()->subDay();
            $end = Carbon::now()->addWeeks($weeks)->endOfWeek()->subDay();
        } else {
            $start = Carbon::now()->subWeeks($weeks)->startOfWeek()->subDay();
            $end = Carbon::now()->subWeeks($weeks)->endOfWeek()->subDay();
        }

        return $query->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end);
    }
    
    /**
     * Filter query by date group
     * @param  Object  $query      Eloquent query
     * @param  String  $event      created|updated|closed|deleted
     * @param  String  $date_group days|weeks|months|years
     * @param  Integer $length     Span of date group
     * @param  boolean $onwards    Onwards or backwards
     * @return Object              Eloquent query
     */
    public function scopeDateGroup($query, Carbon $initial_date, $event, $date_group, $length = 0, $onwards = true)
    {
        $start = $initial_date;
        $end = $start->copy();

        if ($onwards) {
            if ($date_group == "days") {
                $start->addDays($length)->startOfDay();
                $end->addDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                $start->addWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->addWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->addMonths($length)->startOfMonth()->startOfDay();
                $end->addMonths($length)->endOfMonth()->endOfDay();
            }
        } else {
            if ($date_group == "days") {
                $start->subDays($length)->startOfDay();
                $end->subDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                $start->subWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->subWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->subMonths($length)->startOfMonth()->startOfDay();
                $end->subMonths($length)->endOfMonth()->endOfDay();
            }
        }

        if ($event == "created") {
            return $query->where('created_at', '>=', $start)->where('created_at', '<=', $end);    
        } else if ($event == "closed") {
            return $query->where('closed_at', '>=', $start)->where('closed_at', '<=', $end);    
        }
    }

    public function scopeEventAt($query, Carbon $initial_date, $event, $date_group, $length = 0, $onwards = true)
    {
        $start = $initial_date;
        $end = $start->copy();

        if ($onwards) {
            if ($date_group == "days") {
                $start->addDays($length)->startOfDay();
                $end->addDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }
                
                $start->addWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->addWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->addMonths($length)->startOfMonth()->startOfDay();
                $end->addMonths($length)->endOfMonth()->endOfDay();
            }
        } else {
            if ($date_group == "days") {
                $start->subDays($length)->startOfDay();
                $end->subDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }

                $start->subWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->subWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->subMonths($length)->startOfMonth()->startOfDay();
                $end->subMonths($length)->endOfMonth()->endOfDay();
            }
        }

        if ($event == "closed") {
            return $query->where('status_tag', self::STATUS_CLOSED)
                ->where('closed_at', '>=', $start)
                ->where('closed_at', '<=', $end);
        } else if ($event == "created") {
            return $query->where('created_at', '>=', $start)
                ->where('created_at', '<=', $end);
        }
    }

    public function scopeFilterThisWeek($query){
        $last_sunday = Carbon::now()->subWeek()->endOfWeek()->subDay()->toDateTimeString();
        return $query->where('created_at', '>' , $last_sunday);
    }

    public function scopeStatusClose($query)
    {
        return $query->where('status_tag', self::STATUS_CLOSED);
    }

    public function scopeStatusOpen($query)
    {
        return $query->where('status_tag', self::STATUS_OPEN);
    }

    public function scopeLateCampaigns($query)
    {
        return $query->where('status_tag', self::STATUS_OPEN)
            ->whereHas('units', function ($query2) {
                $query2->where('committed_at', '<', Carbon::now());
            }
        );
    }

    public function scopeDueInHours($query, $hour = 1)
    {
        switch ($hour) {
            case 3:
                $from = Carbon::now();
                $to = Carbon::now()->addHours(3);
                break;
            case 8:
                $from = Carbon::now()->addHours(3);
                $to = Carbon::now()->addHours(8);
                break;
            case 24:
                $from = Carbon::now()->addHours(8);
                $to = Carbon::now()->addHours(24);
                break;
            default:
                $from = Carbon::now();
                $to = Carbon::now()->addHours($hour);
                break;
        }

        return $query->where('status_tag', self::STATUS_OPEN)
            ->whereHas('units', function ($query2) use ($from, $to) {
                $query2->where('committed_at', '>', $from)
                    ->where('committed_at', '<=', $to);
                }
            );
    }
}
