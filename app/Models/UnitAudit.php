<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class UnitAudit extends Model
{
    use SoftDeletes;

    const FLAG_NOT_DONE = 1;
    const FLAG_PASS = 2;
    const FLAG_FAIL = 3;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_audits';

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function getFlagname($flag){
        switch ($flag){
            case self::FLAG_NOT_DONE:
                return 'Not Done';
            case self::FLAG_PASS:
                return 'Pass';
            case self::FLAG_FAIL:
                return 'Fail';
        }

        return '';
    }
}
