<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Ticket extends Model
{
    use SoftDeletes;

    const TYPE_EXTERNAL = 1;
    const TYPE_INTERNAL = 2;

    const STATUS_OPEN = 1;
    const STATUS_CLOSED = 2;

    const SUBTYPE_MEETING = 1;
    const SUBTYPE_TRAINING = 2;
    const SUBTYPE_CERTIFICATION = 3;
    const SUBTYPE_OTHERS = 4;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tickets';

    protected $appends = ['type', 'subtype', 'status', 'participants'];

    public function creator()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function campaign()
    {
        return $this->hasOne('App\Models\Campaign', 'ticket_id', 'id');
    }

    public function inhouse()
    {
        return $this->hasOne('App\Models\InHouse', 'ticket_id', 'id');
    }

    public function getTypeAttribute()
    {
        switch ($this->type_tag) {
            case self::TYPE_EXTERNAL:
                return trans('ticket.type.external');
            case self::TYPE_INTERNAL:
                return trans('ticket.type.internal');
        }
    }

    public function getStatusAttribute()
    {
        switch ($this->status_tag) {
            case self::STATUS_OPEN:
                return trans('ticket.status.open');
            case self::STATUS_CLOSED:
                return trans('ticket.status.closed');
        }
    }

    public function getSubtypeAttribute()
    {
        switch ($this->subtype_tag) {
            case self::SUBTYPE_MEETING:
                return trans('ticket.subtype.meeting');
            case self::SUBTYPE_TRAINING:
                return trans('ticket.subtype.training');
            case self::SUBTYPE_CERTIFICATION:
                return trans('ticket.subtype.certification');
            case self::SUBTYPE_OTHERS:
                return trans('ticket.subtype.others');
        }
    }
}
