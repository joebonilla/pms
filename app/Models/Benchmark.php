<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Benchmark extends Model
{

    protected $table = 'team_benchmark';

    protected $fillable = [ 'complexity', 
                            'benchmark'
                        ];

    public function team(){
        $this->belongsTo('App\Models\Team', 'team_id', 'id');
    }



}
