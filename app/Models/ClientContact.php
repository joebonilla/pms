<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ClientContact extends Model
{
    use SoftDeletes;
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'client_contacts';

    public function partner()
    {
    	return $this->belongsTo('App\Models\Partner', 'partner_id', 'id');
    }

    public function units()
    {
        return $this->hasMany('App\Models\Unit', 'client_contact_id', 'id');
    }
}
