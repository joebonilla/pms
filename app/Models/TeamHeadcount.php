<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeamHeadcount extends Model
{
    public function team(){
        return $this->belongsTo('App\Models\Team', 'team_id', 'id');
    }

    public function scopeTo($query, $date){
        return $query->where('created_at', '<=', $date);
    }

    public function scopeFrom($query, $date){
        return $query->where('created_at', '>=', $date);
    }

    public function scopeLatestHc($query){
        return $query->orderBy('created_at', 'desc');
    }
}
