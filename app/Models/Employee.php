<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

use App\Models\Team;

class Employee extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;

    const IC_CREATIVE = 1;
    const IC_DEV = 2;
    const IC_QA = 3;
    const IC_POC = 4;
    const IC_SME = 5;
    const IC_MGMT = 6;

    const ROLE_IC = 1;
    const ROLE_SV = 2;
    const ROLE_SA = 3;

    const SCHEDULE_DAY = 1;
    const SCHEDULE_MID = 2;
    const SCHEDULE_NIGHT = 3;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['remember_token'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $appends = ['avatar', 'ic', 'role', 'schedule'];

    public function team()
    {
        return $this->belongsTo('App\Models\Team', 'team_id', 'id');
    }

    public function units()
    {
        return $this->hasMany('App\Models\Unit', 'employee_id', 'id');
    }

    public function issues()
    {
        return $this->hasMany('App\Models\UnitIssue', 'employee_id', 'id');
    }

    public function unitTimes()
    {
        return $this->hasMany('App\Models\UnitTime', 'employee_id', 'id');
    }

    public function queueNotes()
    {
        return $this->hasMany('App\Models\QueueNote', 'employee_id', 'id');
    }

    public function userMatrix(){
        return $this->hasMany('App\Models\UserMatrix', 'ic_tag', 'ic_tag');
    }

    public function getAvatarAttribute()
    {
        if ($this->avatar_file) {
            if (file_exists(public_path('img/employees/'.$this->avatar_file))) {
                return asset('img/employees/'.$this->avatar_file);
            } else {
                return asset('img/no-image.jpg');
            }
        } else {
            return asset('img/no-image.jpg');
        }
    }

    public function getNicknameAttribute($nickname)
    {
        if (!$nickname) {
            $nickname = explode(" ", trim($this->name));
            return $nickname[0];
        } else {
            return $nickname;
        }
    }

    public function getIcAttribute()
    {
        switch ($this->ic_tag) {
            case self::IC_CREATIVE:
                return trans('employee.ic.creative');
            case self::IC_DEV:
                return trans('employee.ic.dev');
            case self::IC_QA:
                return trans('employee.ic.qa');
            case self::IC_POC:
                return trans('employee.ic.poc');
            case self::IC_SME:
                return trans('employee.ic.sme');
            case self::IC_MGMT:
                return trans('employee.ic.mgmt');
        }
    }

    public function getRoleAttribute()
    {
        switch ($this->role_tag) {
            case self::ROLE_IC:
                return trans('employee.role.ic');
            case self::ROLE_SV:
                return trans('employee.role.sv');
            case self::ROLE_SA:
                return trans('employee.role.sa');
        }
    }

    public function getScheduleAttribute()
    {
        switch ($this->schedule_tag) {
            case self::SCHEDULE_DAY:
                return trans('employee.schedule.day');
            case self::SCHEDULE_MID:
                return trans('employee.schedule.mid');
            case self::SCHEDULE_NIGHT:
                return trans('employee.schedule.night');
        }
    }

    public function getIsPocAttribute()
    {
        return ($this->ic_tag == self::IC_POC);
    }

    public function getIsSubPocAttribute()
    {
        return ($this->ic_tag > self::IC_POC) || ($this->role_tag >= self::ROLE_SV);
    }

    public function scopeAdmin($query)
    {
        return $query->where('name', 'like', '%pms%')
            ->where('team_id', Team::TEAM_MANAGEMENT)
            ->where('ic_tag', self::IC_MGMT)
            ->where('role_tag', self::ROLE_SA)
            ->first();
    }

    public function scopeSchedule($query, $schedule)
    {
        switch ($schedule) {
            case self::SCHEDULE_DAY:
                return $query->where('schedule_tag', self::SCHEDULE_DAY);
            case self::SCHEDULE_MID:
                return $query->where('schedule_tag', self::SCHEDULE_MID);
            case self::SCHEDULE_NIGHT:
                return $query->where('schedule_tag', self::SCHEDULE_NIGHT);
        }
    }

    public function getWorkTypes($phase = 0){
        $workTypes = [];
        $userWorkType = isset($this->userMatrix[$phase]) ? $this->userMatrix[$phase]->userWorkType : false;

        if ($userWorkType){
            foreach($userWorkType as $workType){
                $workTypes[] = $workType->workType->name;
            }
        }

        return $workTypes;
    }
}
