<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Unit extends Model
{
    use SoftDeletes;

    const STATUS_NEW = 1;
    const STATUS_WAITING = 2;
    const STATUS_UNASSIGNED = 3;
    const STATUS_INFO_REQ = 4;
    const STATUS_IN_PROG = 5;
    const STATUS_HOLD = 6;
    const STATUS_SOLVED = 7;
    const STATUS_CLOSED = 8;

    const PRIORITY_LOW = 1;
    const PRIORITY_NORMAL = 2;
    const PRIORITY_HIGH = 3;

    const TYPE_INITIAL_BUILD = 1;
    const TYPE_CREATIVE_ITERATION = 2;
    const TYPE_CLIENT_REVISION = 3;
    const TYPE_CLIENT_ISSUE = 4;
    const TYPE_FILE_PATH_UPDATE = 5;
    const TYPE_TRACKING_UPDATE = 6;
    const TYPE_IPD_BUILD = 7;
    const TYPE_PROD_QA = 8;

    const AD_TYPE_INPAGE = 1;
    const AD_TYPE_EXPANDING = 2;
    const AD_TYPE_PUSHDOWN = 3;
    const AD_TYPE_MDE = 4;
    const AD_TYPE_FLOATING = 5;
    const AD_TYPE_INPAGE_FLOAT = 6;
    const AD_TYPE_IM_EXPAND = 7;
    const AD_TYPE_TANDEM = 8;
    const AD_TYPE_SUPER_EVA = 9;
    const AD_TYPE_MULTI_FLOATING = 10;
    const AD_TYPE_PEELBACK = 11;
    const AD_TYPE_WALLPAPER_RESKIN = 12;
    const AD_TYPE_CATFISH = 13;
    const AD_TYPE_IN_STREAM = 14;
    const AD_TYPE_VPAID_LINEAR = 15;
    const AD_TYPE_VPAID_NON_LINEAR = 16;
    const AD_TYPE_LIGHTBOX = 17;
    const AD_TYPE_IHB = 18;
    const AD_TYPE_MASTBOX = 19;
    const AD_TYPE_MASTHEAD = 20;
    const AD_TYPE_CUSTOM = 21;
    const AD_TYPE_WEB = 22;
    const AD_TYPE_APP = 23;
    const AD_TYPE_STATIC_BANNER = 24;
    const AD_TYPE_ANIMATED_GIF_BANNER = 25;
    const AD_TYPE_DESKTOP_LANDING_PAGE = 26;
    const AD_TYPE_MOBILE_LANDING_PAGE = 27;
    const AD_TYPE_STANDARD_FLASH = 28;
    const AD_TYPE_AD_MAIL = 29;
    const AD_TYPE_IOPENER_DBG = 30;
    const AD_TYPE_SCENE_STEALER_DBG = 31;
    const AD_TYPE_VIDEO_EDIT_TVG = 32;
    const AD_TYPE_MOCK_EXPAND_INNODES = 33;
    const AD_TYPE_MOCK_AD_EXTENDER_INNODES = 34;
    const AD_TYPE_MOCK_AD_SELECTOR_INNODES = 35;
    const AD_TYPE_MOCK_CANVAS_INNODES = 36;
    const AD_TYPE_LIVE_EXPAND_INNODES = 37;
    const AD_TYPE_LIVE_AD_EXTENDER_INNODES = 38;
    const AD_TYPE_LIVE_AD_SELECTOR_INNODES = 39;
    const AD_TYPE_LIVE_CANVAS_INNODES = 40;
    const AD_TYPE_HOVER = 41;

    const AD_TYPE_STATICS_DESIGNER = 42;
    const AD_TYPE_STATICS_DEVELOPER = 43;
    const AD_TYPE_PROGRESSIVE_BILLBOARD_NATIVE_H5 = 44;
    const AD_TYPE_PROGRESSIVE_BILLBOARD_FLASH_TALKING = 45;
    const AD_TYPE_PROGRESSIVE_BILLBOARD_GWD = 46;
    const AD_TYPE_PROGRESSIVE_BILLBOARD_ADDROID = 47;
    const AD_TYPE_ENDPLATE_ANIMATION_NATIVE_H5 = 48;
    const AD_TYPE_ENDPLATE_ANIMATION_SIZMEK = 49;
    const AD_TYPE_ENDPLATE_ANIMATION_FLASH_TALKING = 50;
    const AD_TYPE_ENDPLATE_ANIMATION_GWD = 51;
    const AD_TYPE_ENDPLATE_ANIMATION_ADDROID = 52;
    const AD_TYPE_PUSHDOWN_EXPANDING_NATIVE_H5 = 53;
    const AD_TYPE_PUSHDOWN_EXPANDING_SIZMEK = 54;
    const AD_TYPE_PUSHDOWN_EXPANDING_GWD = 55;
    const AD_TYPE_CUSTOM_OVERLAY_NATIVE_H5 = 56;
    const AD_TYPE_CUSTOM_OVERLAY_SIZMEK = 57;
    const AD_TYPE_CUSTOM_OVERLAY_GWD = 58;
    const AD_TYPE_CUSTOM_RESPONSIVE_AD_GWD = 59;
    const AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_NATIVE_H5 = 60;
    const AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_SIZMEK = 61;
    const AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_FLASH_TALKING = 62;
    const AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_GWD = 63;
    const AD_TYPE_CUSTOM_VIDEO_WALL_NATIVE_H5 = 64;
    const AD_TYPE_CUSTOM_VIDEO_WALL_SIZMEK = 65;
    const AD_TYPE_CUSTOM_VIDEO_WALL_GWD = 66;
    const AD_TYPE_MARKETING = 67;

    const AD_TYPE_EXPAND = 68;
    const AD_TYPE_OVERLAY = 69;
    const AD_TYPE_AD_CANVAS = 70;
    const AD_TYPE_AD_EXTENDER = 71;
    const AD_TYPE_AD_SELECTOR = 72;
    const AD_TYPE_END_SLATE = 73;
    const AD_TYPE_SWF_ONLY = 74;
    const AD_TYPE_PRE_ROLL = 75;
    const AD_TYPE_ADVANCE_CREATIVE = 76;
    const AD_TYPE_VAST = 77;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'units';

    protected $appends = [
        'type',
        'status',
        'priority',
        'ad_type',
        'preview',
        'qa_report',
        'progress',
        'tat',
        'is_late',
        'queue'
    ];

    protected $dates = [
        'started_at',
        'completed_at',
        'committed_at',
        'closed_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'billable' => 'boolean'
    ];

    public function team()
    {
        $this->belongsTo('App\Models\Team', 'team_id', 'id');
    }

    public function partner()
    {
        return $this->belongsTo('App\Models\Partner', 'partner_id', 'id');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign', 'campaign_id', 'id');
    }

    public function clientContact()
    {
        return $this->belongsTo('App\Models\ClientContact', 'client_contact_id', 'id');
    }

    public function unitTimes()
    {
        return $this->hasMany('App\Models\UnitTime', 'unit_id', 'id');
    }

    public function assignee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function issues()
    {
        return $this->hasMany('App\Models\UnitIssue', 'unit_id', 'id');
    }

    public function queueNotes()
    {
        return $this->hasMany('App\Models\UnitQueueNote', 'unit_id', 'id');
    }

    public function revisions()
    {
        return $this->hasMany('App\Models\UnitRevision', 'unit_id', 'id');
    }

    public function audits()
    {
        return $this->hasMany('App\Models\UnitAudit', 'unit_id', 'id');
    }

    public function getTypeAttribute()
    {
        switch ($this->type_tag) {
            case self::TYPE_INITIAL_BUILD:
                return trans('unit.type.initial_build');
            case self::TYPE_CREATIVE_ITERATION:
                return trans('unit.type.creative_iteration');
            case self::TYPE_CLIENT_REVISION:
                return trans('unit.type.client_revision');
            case self::TYPE_CLIENT_ISSUE:
                return trans('unit.type.client_issue');
            case self::TYPE_FILE_PATH_UPDATE:
                return trans('unit.type.file_path_update');
            case self::TYPE_TRACKING_UPDATE:
                return trans('unit.type.tracking_update');
            case self::TYPE_IPD_BUILD:
                return trans('unit.type.ipd_build');
            case self::TYPE_PROD_QA:
                return trans('unit.type.prod_qa');
        }
    }

    public function getStatusAttribute()
    {
        switch ($this->status_tag) {
            case self::STATUS_NEW:
                return trans('unit.status.new');
            case self::STATUS_WAITING:
                return trans('unit.status.waiting');
            case self::STATUS_UNASSIGNED:
                return trans('unit.status.unassigned');
            case self::STATUS_IN_PROG:
                return trans('unit.status.in_prog');
            case self::STATUS_INFO_REQ:
                return trans('unit.status.info_req');
            case self::STATUS_HOLD:
                return trans('unit.status.hold');
            case self::STATUS_SOLVED:
                return trans('unit.status.solved');
            case self::STATUS_CLOSED:
                return trans('unit.status.closed');
        }
    }

    public function getPriorityAttribute()
    {
        switch ($this->priority_tag) {
            case self::PRIORITY_LOW:
                return trans('unit.priority.low');
            case self::PRIORITY_NORMAL:
                return trans('unit.priority.normal');
            case self::PRIORITY_HIGH:
                return trans('unit.priority.high');
        }
    }

    public function getAdTypeAttribute()
    {
        switch ($this->ad_type_tag) {
            case self::AD_TYPE_INPAGE:
                return trans('unit.ad_type.inpage');
            case self::AD_TYPE_EXPANDING:
                return trans('unit.ad_type.expanding');
            case self::AD_TYPE_PUSHDOWN:
                return trans('unit.ad_type.pushdown');
            case self::AD_TYPE_MDE:
                return trans('unit.ad_type.mde');
            case self::AD_TYPE_FLOATING:
                return trans('unit.ad_type.floating');
            case self::AD_TYPE_INPAGE_FLOAT:
                return trans('unit.ad_type.inpage_float');
            case self::AD_TYPE_IM_EXPAND:
                return trans('unit.ad_type.im_expand');
            case self::AD_TYPE_TANDEM:
                return trans('unit.ad_type.tandem');
            case self::AD_TYPE_SUPER_EVA:
                return trans('unit.ad_type.super_eva');
            case self::AD_TYPE_MULTI_FLOATING:
                return trans('unit.ad_type.multi_floating');
            case self::AD_TYPE_PEELBACK:
                return trans('unit.ad_type.peelback');
            case self::AD_TYPE_WALLPAPER_RESKIN:
                return trans('unit.ad_type.wallpaper_reskin');
            case self::AD_TYPE_CATFISH:
                return trans('unit.ad_type.catfish');
            case self::AD_TYPE_IN_STREAM:
                return trans('unit.ad_type.in_stream');
            case self::AD_TYPE_VPAID_LINEAR:
                return trans('unit.ad_type.vpaid_linear');
            case self::AD_TYPE_VPAID_NON_LINEAR:
                return trans('unit.ad_type.vpaid_non_linear');
            case self::AD_TYPE_LIGHTBOX:
                return trans('unit.ad_type.lightbox');
            case self::AD_TYPE_IHB:
                return trans('unit.ad_type.ihb');
            case self::AD_TYPE_MASTBOX:
                return trans('unit.ad_type.mastbox');
            case self::AD_TYPE_MASTHEAD:
                return trans('unit.ad_type.masthead');
            case self::AD_TYPE_CUSTOM:
                return trans('unit.ad_type.custom');
            case self::AD_TYPE_WEB:
                return trans('unit.ad_type.web');
            case self::AD_TYPE_APP:
                return trans('unit.ad_type.app');
            case self::AD_TYPE_STATIC_BANNER:
                return trans('unit.ad_type.static_banner');
            case self::AD_TYPE_ANIMATED_GIF_BANNER:
                return trans('unit.ad_type.animated_gif_banner');
            case self::AD_TYPE_DESKTOP_LANDING_PAGE:
                return trans('unit.ad_type.desktop_landing_page');
            case self::AD_TYPE_MOBILE_LANDING_PAGE:
                return trans('unit.ad_type.mobile_landing_page');
            case self::AD_TYPE_STANDARD_FLASH:
                return trans('unit.ad_type.standard_flash');
            case self::AD_TYPE_AD_MAIL:
                return trans('unit.ad_type.ad_mail');
            case self::AD_TYPE_IOPENER_DBG:
                return trans('unit.ad_type.iopener_dbg');
            case self::AD_TYPE_SCENE_STEALER_DBG:
                return trans('unit.ad_type.scene_stealer_dbg');
            case self::AD_TYPE_VIDEO_EDIT_TVG:
                return trans('unit.ad_type.video_edit_tvg');
            case self::AD_TYPE_MOCK_EXPAND_INNODES:
                return trans('unit.ad_type.mock_expand_innodes');
            case self::AD_TYPE_MOCK_AD_EXTENDER_INNODES:
                return trans('unit.ad_type.mock_ad_extender_innodes');
            case self::AD_TYPE_MOCK_AD_SELECTOR_INNODES:
                return trans('unit.ad_type.mock_ad_selector_innodes');
            case self::AD_TYPE_MOCK_CANVAS_INNODES:
                return trans('unit.ad_type.mock_canvas_innodes');
            case self::AD_TYPE_LIVE_EXPAND_INNODES:
                return trans('unit.ad_type.live_expand_innodes');
            case self::AD_TYPE_LIVE_AD_EXTENDER_INNODES:
                return trans('unit.ad_type.live_ad_extender_innodes');
            case self::AD_TYPE_LIVE_AD_SELECTOR_INNODES:
                return trans('unit.ad_type.live_ad_selector_innodes');
            case self::AD_TYPE_LIVE_CANVAS_INNODES:
                return trans('unit.ad_type.live_canvas_innodes');
            case self::AD_TYPE_HOVER:
                return trans('unit.ad_type.hover');

            case self::AD_TYPE_STATICS_DESIGNER:
                return trans('unit.ad_type.statics_designer');
            case self::AD_TYPE_STATICS_DEVELOPER:
                return trans('unit.ad_type.statics_developer');
            case self::AD_TYPE_PROGRESSIVE_BILLBOARD_NATIVE_H5:
                return trans('unit.ad_type.progressive_billboard_native_h5');
            case self::AD_TYPE_PROGRESSIVE_BILLBOARD_FLASH_TALKING:
                return trans('unit.ad_type.progressive_billboard_flash_talking');
            case self::AD_TYPE_PROGRESSIVE_BILLBOARD_GWD:
                return trans('unit.ad_type.progressive_billboard_gwd');
            case self::AD_TYPE_PROGRESSIVE_BILLBOARD_ADDROID:
                return trans('unit.ad_type.progressive_billboard_addroid');
            case self::AD_TYPE_ENDPLATE_ANIMATION_NATIVE_H5:
                return trans('unit.ad_type.endplate_animation_native_h5');
            case self::AD_TYPE_ENDPLATE_ANIMATION_SIZMEK:
                return trans('unit.ad_type.endplate_animation_sizmek');
            case self::AD_TYPE_ENDPLATE_ANIMATION_FLASH_TALKING:
                return trans('unit.ad_type.endplate_animation_flash_talking');
            case self::AD_TYPE_ENDPLATE_ANIMATION_GWD:
                return trans('unit.ad_type.endplate_animation_gwd');
            case self::AD_TYPE_ENDPLATE_ANIMATION_ADDROID:
                return trans('unit.ad_type.endplate_animation_addroid');
            case self::AD_TYPE_PUSHDOWN_EXPANDING_NATIVE_H5:
                return trans('unit.ad_type.pushdown_expanding_native_h5');
            case self::AD_TYPE_PUSHDOWN_EXPANDING_SIZMEK:
                return trans('unit.ad_type.pushdown_expanding_sizmek');
            case self::AD_TYPE_PUSHDOWN_EXPANDING_GWD:
                return trans('unit.ad_type.pushdown_expanding_gwd');
            case self::AD_TYPE_CUSTOM_OVERLAY_NATIVE_H5:
                return trans('unit.ad_type.custom_overlay_native_h5');
            case self::AD_TYPE_CUSTOM_OVERLAY_SIZMEK:
                return trans('unit.ad_type.custom_overlay_sizmek');
            case self::AD_TYPE_CUSTOM_OVERLAY_GWD:
                return trans('unit.ad_type.custom_overlay_gwd');
            case self::AD_TYPE_CUSTOM_RESPONSIVE_AD_GWD:
                return trans('unit.ad_type.custom_responsive_ad_gwd');
            case self::AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_NATIVE_H5:
                return trans('unit.ad_type.custom_transparent_video_ad_native_h5');
            case self::AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_SIZMEK:
                return trans('unit.ad_type.custom_transparent_video_ad_sizmek');
            case self::AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_FLASH_TALKING:
                return trans('unit.ad_type.custom_transparent_video_ad_flash_talking');
            case self::AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_GWD:
                return trans('unit.ad_type.custom_transparent_video_ad_gwd');
            case self::AD_TYPE_CUSTOM_VIDEO_WALL_NATIVE_H5:
                return trans('unit.ad_type.custom_video_wall_native_h5');
            case self::AD_TYPE_CUSTOM_VIDEO_WALL_SIZMEK:
                return trans('unit.ad_type.custom_video_wall_sizmek');
            case self::AD_TYPE_CUSTOM_VIDEO_WALL_GWD:
                return trans('unit.ad_type.custom_video_wall_gwd');
            case self::AD_TYPE_MARKETING:
                return trans('unit.ad_type.marketing');

            case self::AD_TYPE_EXPAND:
                return trans('unit.ad_type.expand');
            case self::AD_TYPE_OVERLAY:
                return trans('unit.ad_type.overlay');
            case self::AD_TYPE_AD_CANVAS:
                return trans('unit.ad_type.ad_canvas');
            case self::AD_TYPE_AD_EXTENDER:
                return trans('unit.ad_type.ad_extender');
            case self::AD_TYPE_AD_SELECTOR:
                return trans('unit.ad_type.ad_selector');
            case self::AD_TYPE_END_SLATE:
                return trans('unit.ad_type.end_slate');
            case self::AD_TYPE_SWF_ONLY:
                return trans('unit.ad_type.swf_only');
            case self::AD_TYPE_PRE_ROLL:
                return trans('unit.ad_type.pre_roll');
            case self::AD_TYPE_ADVANCE_CREATIVE:
                return trans('unit.ad_type.advance_creative');
            case self::AD_TYPE_VAST:
                return trans('unit.ad_type.vast');
        }
    }

    public function getCreativeAttribute()
    {
        if (!$this->creative_brief) {
            return null;
        }

        return $this->creative_brief;
    }

    public function getPreviewAttribute()
    {
        if (!$this->preview_link) {
            return null;
        }

        return $this->preview_link;
    }

    public function getQaReportAttribute()
    {
        return $this->qa_report_link;
    }

    public function getProgressAttribute()
    {
        $queue_notes = $this->queueNotes;
        $progress = 0;

        if ($queue_notes->count() == 0) {
            return $progress;
        }

        foreach ($queue_notes as $queue_note) {
            $queues[] = $queue_note->queue_tag;
        }

        if (in_array(UnitQueueNote::QUEUE_DEV_READY, $queues)) {
            $progress = 25;
        }

        if (in_array(UnitQueueNote::QUEUE_QA_READY, $queues)) {
            $progress = 50;
        }

        if (in_array(UnitQueueNote::QUEUE_NO_REVISION, $queues)) {
            $progress = 75;
        }

        if (in_array(UnitQueueNote::QUEUE_CLOSE, $queues)) {
            $progress = 100;
        }

        return $progress;
    }

    public function getTatAttribute()
    {
        return $this->closed_at->diffInSeconds($this->created_at) / 3600;
    }

    public function getQueueAttribute()
    {
        if ($this->queueNotes()->count()) {
            $queue = $this->queueNotes()
                ->whereNotNull('queue_tag')
                ->orderBy('created_at', 'desc')
                ->first();

            if ($queue) {
                return $queue->queue;
            } else {
                return trans('unit_queue_note.label.pending');
            }
        } else {
            return trans('unit_queue_note.label.pending');
        }
    }

    public function getIsLateAttribute()
    {
        if ($this->closed_at) {
            if ($this->closed_at->gt($this->committed_at)) {
                return true;
            } else {
                return false;
            }
        } else {
            return null;
        }
    }

    public function scopeTaskType($query, $type)
    {
        return $query->where('type_tag', $type);
    }

    public function scopeStatus($query, $status, $equal = true)
    {
        if ($equal) {
            return $query->where('status_tag', $status);
        } else {
            return $query->where('status_tag', '!=', $status);
        }
    }

    public function scopeClosedFrom($query, $date)
    {
        return $query->where('closed_at', '>=', $date);
    }

    public function scopeClosedTo($query, $date)
    {
        return $query->where('closed_at', '<=', $date);
    }

    public function scopeLate($query)
    {
        return $query->whereRaw('closed_at > committed_at');
    }

    public function scopeDueIn($query, $date)
    {

    }

////////////////////////////////////////////////////////////////////////////////


    public function scopeFilterCreated($query, $created_at, $created_end)
    {
        if (!empty($created_at) && !empty($created_end)) {
            return $query->whereBetween('created_at', [$created_at . ' 00:00:00', $created_end . ' 23:59:59']);
        } else if (!empty($created_at)) {
            return $query->where('created_at', '>', $created_at . ' 00:00:00');
        } else if (!empty($created_end)) {
            return $query->where('created_at', '<', $created_end . ' 23:59:59');
        }

        return $query;
    }

    public function scopeFilterCommitted($query, $committed_at, $committed_end)
    {
        if (!empty($committed_at) && !empty($committed_end)) {
            return $query->whereBetween('committed_at', [$committed_at . ' 00:00:00', $committed_end . ' 23:59:59']);
        } else if (!empty($committed_at)) {
            return $query->where('committed_at', '>', $committed_at . ' 00:00:00');
        } else if (!empty($committed_end)) {
            return $query->where('committed_at', '<', $committed_end . ' 23:59:59');
        }

        return $query;
    }

    public function scopeFilterCompleted($query, $completed_at, $completed_end)
    {
        if (!empty($completed_at) && !empty($completed_end)) {
            return $query->whereBetween('completed_at', [$completed_at . ' 00:00:00', $completed_end . ' 23:59:59']);
        } else if (!empty($completed_at)) {
            return $query->where('completed_at', '>', $completed_at . ' 00:00:00');
        } else if (!empty($completed_end)) {
            return $query->where('completed_at', '<', $completed_end . ' 23:59:59');
        }

        return $query;
    }

    public function scopeFilterAssignedto($query, $assinged_to)
    {
        if ($assinged_to) {
            $ids = [0];
            $employees = Employee::where('name', 'like', '%' . $assinged_to . '%')->get(['id']);
            foreach ($employees as $employee) {
                $ids[] = $employee->id;
            }
            return $query->whereIn('employee_id', $ids);
        } else {
            return $query->whereNotNull('employee_id');
        }
    }

    public function scopeFilterStatus($query, $status)
    {
        if ($status > 0) {
            return $query->where('status_tag', $status);
        }
        return $query;
    }

    public function scopeCreatedThisWeek($query, $weeks = 0, $onwards = true)
    {
        if ($onwards) {
            $start = Carbon::now()->addWeeks($weeks)->startOfWeek()->subDay();
            $end = Carbon::now()->addWeeks($weeks)->endOfWeek()->subDay();
        } else {
            $start = Carbon::now()->subWeeks($weeks)->startOfWeek()->subDay();
            $end = Carbon::now()->subWeeks($weeks)->endOfWeek()->subDay();
        }

        return $query->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end);
    }

    public function scopeClosed($query)
    {
        return $query->where('status_tag', self::STATUS_CLOSED);
    }

    public function scopeEventAt($query, Carbon $initial_date, $event, $date_group, $length = 0, $onwards = true)
    {
        $start = $initial_date;
        $end = $start->copy();

        if ($onwards) {
            if ($date_group == "days") {
                $start->addDays($length)->startOfDay();
                $end->addDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }

                $start->addWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->addWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->addMonths($length)->startOfMonth()->startOfDay();
                $end->addMonths($length)->endOfMonth()->endOfDay();
            }
        } else {
            if ($date_group == "days") {
                $start->subDays($length)->startOfDay();
                $end->subDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }

                $start->subWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->subWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->subMonths($length)->startOfMonth()->startOfDay();
                $end->subMonths($length)->endOfMonth()->endOfDay();
            }
        }

        if ($event == "closed") {
            return $query->where('status_tag', self::STATUS_CLOSED)
                ->where('closed_at', '>=', $start)
                ->where('closed_at', '<=', $end);
        } else if ($event == "lates") {
            return $query->whereRaw('closed_at > committed_at')
                ->where('closed_at', '>=', $start)
                ->where('closed_at', '<=', $end);
        } else if ($event == "on_time") {
            return $query->whereRaw('committed_at > closed_at')
                ->where('closed_at', '>=', $start)
                ->where('closed_at', '<=', $end);
        }
    }

    public function scopeFilterThisWeek($query)
    {
        $last_sunday = Carbon::now()->subWeek()->endOfWeek()->subDay()->toDateTimeString();
        return $query->where('created_at', '>', $last_sunday);
    }

    public function scopeFilterLastWeek($query)
    {
        $date = Carbon::now();
        $last_sunday = $date->subWeek()->endOfWeek()->subDay()->toDateTimeString();
        $last_last_sunday = $date->subWeek()->toDateTimeString();
        return $query->whereBetween('created_at', [$last_last_sunday, $last_sunday]);
    }

    public function scopeIntialBuildsThisMonth($query)
    {
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();

        return $query->where('type_tag', self::TYPE_INITIAL_BUILD)
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end);
    }

    public function scopeCreativeIterationsThisMonth($query)
    {
        $start = Carbon::now()->startOfMonth();
        $end = Carbon::now()->endOfMonth();

        return $query->where('type_tag', self::TYPE_CREATIVE_ITERATION)
            ->where('created_at', '>=', $start)
            ->where('created_at', '<=', $end);
    }
}
