<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Partner extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'partners';

    public function advertisers()
    {
        return $this->hasMany('App\Models\Advertiser', 'partner_id', 'id');
    }

    public function clientContacts()
    {
        return $this->hasMany('App\Models\ClientContact', 'partner_id', 'id');
    }

    public function campaigns()
    {
        return $this->hasMany('App\Models\Campaign', 'partner_id', 'id');
    }
}
