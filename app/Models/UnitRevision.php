<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class UnitRevision extends Model
{
    use SoftDeletes;

    protected $table = 'unit_revisions';

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }

    public function employeeDev()
    {
    	return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function employeeQa()
    {
    	return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }
}
