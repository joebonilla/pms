<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserWorktype extends Model
{
    public $table = 'user_worktype';

    public function workType(){
        return $this->belongsTo('App\Models\WorkType', 'work_type_id', 'id');
    }

    public function user_matrix(){
        return $this->belongsTo('App\Models\UserMatrix', 'id', 'user_matrix_id');
    }
}
