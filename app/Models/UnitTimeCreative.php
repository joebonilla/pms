<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class UnitTimeCreative extends Model
{
    use SoftDeletes;

    const WORK_TYPE = 'Creative';

    const ACTION_WAIT = 1;
    const ACTION_INFO_REQ = 2;
    const ACTION_IN_PROG = 3;
    const ACTION_HOLD = 4;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_time_creatives';
    
    protected $dates = ['start_at', 'end_at', 'created_at', 'updated_at', 'deleted_at'];

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function getActionAttribute()
    {
        switch ($this->action_tag) {
            case UnitTime::ACTION_WAIT:
                return trans('unit_time.action.wait');
            case UnitTime::ACTION_HOLD:
                return trans('unit_time.action.hold');
            case UnitTime::ACTION_INFO_REQ:
                return trans('unit_time.action.info_req');
            case UnitTime::ACTION_IN_PROG:
                return trans('unit_time.action.in_prog');
        }
    }

    public function getWorkAttribute()
    {
        return self::WORK_TYPE;
    }
}
