<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class InHouse extends Model
{
    use SoftDeletes;

    const TYPE_AUDIT = 1;
    const TYPE_MEETING_HUDDLES = 2;
    const TYPE_REPORTS_GENERATION = 3;
    const TYPE_TRAINING_UPSKILLING = 4;
    const TYPE_TROUBLESHOOTING = 5;
    const TYPE_OTHERS = 6;

    const PURPOSE_ACCOUNT_MANAGEMENT_SALES = 1;
    const PURPOSE_ACTIVATIONS_PROJECTS = 2;
    const PURPOSE_FACILITIES_IT = 3;
    const PURPOSE_FINANCE = 4;
    const PURPOSE_HUMAN_RESOURCES = 5;
    const PURPOSE_SERVICE_DELIVERY = 6;
    const PURPOSE_MARKETING = 7;
    const PURPOSE_BUSINESS_DEVELOPMENT = 8;
    const PURPOSE_OTHERS = 9;

    const STATUS_NEW = 1;
    const STATUS_IN_PROG = 2;
    const STATUS_HOLD = 3;
    const STATUS_INFO_REQ = 4;
    const STATUS_CLOSED = 5;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'in_houses';

    protected $appends = ['type', 'purpose'];

    public function ticket()
    {
        return $this->belongsTo('App\Models\Ticket', 'ticket_id', 'id');
    }

    public function getTypeAttribute()
    {
        switch ($this->type_tag) {
            case self::TYPE_AUDIT:
                return trans('inhouse.type.audit');
            case self::TYPE_MEETING_HUDDLES:
                return trans('inhouse.type.meeting_huddles');
            case self::TYPE_REPORTS_GENERATION:
                return trans('inhouse.type.reports_generation');
            case self::TYPE_TRAINING_UPSKILLING:
                return trans('inhouse.type.training_upskilling');
            case self::TYPE_OTHERS:
                return trans('inhouse.type.others');
        }
    }

    public function getPurposeAttribute()
    {
        switch ($this->purpose_tag) {
            case self::PURPOSE_ACCOUNT_MANAGEMENT_SALES:
                return trans('inhouse.purpose.account_management_sales');
            case self::PURPOSE_ACTIVATIONS_PROJECTS:
                return trans('inhouse.purpose.activations_projects');
            case self::PURPOSE_FACILITIES_IT:
                return trans('inhouse.purpose.facilities_it');
            case self::PURPOSE_FINANCE:
                return trans('inhouse.purpose.finance');
            case self::PURPOSE_HUMAN_RESOURCES:
                return trans('inhouse.purpose.human_resources');
            case self::PURPOSE_SERVICE_DELIVERY:
                return trans('inhouse.purpose.service_delivery');
            case self::PURPOSE_MARKETING:
                return trans('inhouse.purpose.marketing');
            case self::PURPOSE_BUSINESS_DEVELOPMENT:
                return trans('inhouse.purpose.business_development');
            case self::PURPOSE_OTHERS:
                return trans('inhouse.purpose.others');
        }
    }

    public function getStatusAttribute()
    {
        switch ($this->status_tag) {
            case self::STATUS_NEW:
                return trans('inhouse.status.new');
            case self::STATUS_IN_PROG:
                return trans('inhouse.status.in_prog');
            case self::STATUS_HOLD:
                return trans('inhouse.status.hold');
            case self::STATUS_INFO_REQ:
                return trans('inhouse.status.info_req');
            case self::STATUS_CLOSED:
                return trans('inhouse.status.closed');
        }
    }
}
