<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkType extends Model
{
    public $table = 'work_type';

    public function user_worktype(){
        return $this->hasMany('App\Models\UserWorktype', 'work_type_id', 'id');
    }
}
