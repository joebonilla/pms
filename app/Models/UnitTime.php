<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use App\Models\Employee;
use App\Models\Team;

class UnitTime extends Model
{
    use SoftDeletes;

    const WORK_CREATIVE = 1;
    const WORK_DEV = 2;
    const WORK_QA = 3;
    const WORK_REVIEW = 4;

    const ACTION_WAIT = 1;
    const ACTION_INFO_REQ = 2;
    const ACTION_IN_PROG = 3;
    const ACTION_HOLD = 4;

    // Misc constants
    const SECONDS_IN_HOUR = 3600;

    protected $table = 'unit_times';

    protected $appends = ['work', 'action', 'utilization', 'util', 'task_time'];

    protected $dates = ['start_at', 'end_at', 'created_at', 'updated_at', 'deleted_at'];

    public function team()
    {
        $this->belongsTo('App\Models\Team', 'team_id', 'id');
    }

    public function parnter()
    {
        $this->belongsTo('App\Models\Parnter', 'parnter_id', 'id');
    }

    public function campaign()
    {
        return $this->belongsTo('App\Models\Campaign', 'campaign_id', 'id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function getWorkAttribute()
    {
        switch ($this->work_tag) {
            case self::WORK_CREATIVE:
                return trans('unit_time.work.creative');
            case self::WORK_DEV:
                return trans('unit_time.work.dev');
            case self::WORK_QA:
                return trans('unit_time.work.qa');
            case self::WORK_REVIEW:
                return trans('unit_time.work.review');
        }
    }

    public function getActionAttribute()
    {
        switch ($this->action_tag) {
            case self::ACTION_WAIT:
                return trans('unit_time.action.wait');
            case self::ACTION_HOLD:
                return trans('unit_time.action.hold');
            case self::ACTION_INFO_REQ:
                return trans('unit_time.action.info_req');
            case self::ACTION_IN_PROG:
                return trans('unit_time.action.in_prog');
        }
    }

    // pending to remove / duplicate / nonsense / tasktime is better name
    public function getUtilizationAttribute()
    {
        return $this->start_at->diffInSeconds($this->end_at) / self::SECONDS_IN_HOUR;
    }

    public function getUtilAttribute()
    {
        return $this->start_at->diffInSeconds($this->end_at);
    }

    public function getTaskTimeAttribute()
    {
        return $this->start_at->diffInSeconds($this->end_at) / self::SECONDS_IN_HOUR;
    }

    public function scopeWorkType($query, $work, $only = true)
    {
        if ($only) {
            return $query->where('work_tag', $work);
        }

        return $query->where('work_tag', '!=', $work);
    }

    public function scopeActionType($query, $action, $only = true)
    {
        if ($only) {
            return $query->where('action_tag', $action);
        }

        return $query->where('action_tag', '!=', $action);
    }

    public function scopeEndedFrom($query, $date)
    {
        return $query->where('end_at', '>=', $date);
    }

    public function scopeEndedTo($query, $date)
    {
        return $query->where('end_at', '<=', $date);
    }

    public function scopeEnded($query)
    {
        return $query->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeSchedule($query, $schedule)
    {
        switch ($schedule) {
            case Employee::SCHEDULE_DAY:
                $employees = Employee::schedule(Employee::SCHEDULE_DAY)->get();
                break;
            case Employee::SCHEDULE_MID:
                $employees = Employee::schedule(Employee::SCHEDULE_MID)->get();
                break;
            case Employee::SCHEDULE_NIGHT:
                $employees = Employee::schedule(Employee::SCHEDULE_NIGHT)->get();
                break;
        }

        $employee_ids = $employees->pluck('id')->toArray();

        return $query->whereIn('employee_id', $employee_ids);
    }

    public function scopeTeamTimes($query, $team)
    {
        if ($team == Team::TEAM_QA) {
            $employees = Employee::where('team_id', $team)->get();

            $employee_ids = $employees->pluck('id')->toArray();

            return $query->whereIn('employee_id', $employee_ids);
        }

        return $query->where('team_id', $team);
    }


////////////////////////////////////////////////////////////////////////////////


    public function scopeReviewWait($query)
    {
        return $query->where('work_tag', self::WORK_REVIEW)
            ->where('action_tag', self::ACTION_WAIT)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeReviewInfoReq($query)
    {
        return $query->where('work_tag', self::WORK_REVIEW)
            ->where('action_tag', self::ACTION_INFO_REQ)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeReviewInProg($query)
    {
        return $query->where('work_tag', self::WORK_REVIEW)
            ->where('action_tag', self::ACTION_IN_PROG)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeReviewHold($query)
    {
        return $query->where('work_tag', self::WORK_REVIEW)
            ->where('action_tag', self::ACTION_HOLD)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeDevWait($query)
    {
        return $query->where('work_tag', self::WORK_DEV)
            ->where('action_tag', self::ACTION_WAIT)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeDevInfoReq($query)
    {
        return $query->where('work_tag', self::WORK_DEV)
            ->where('action_tag', self::ACTION_INFO_REQ)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeDevInProg($query)
    {
        return $query->where('work_tag', self::WORK_DEV)
            ->where('action_tag', self::ACTION_IN_PROG)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeDevHold($query)
    {
        return $query->where('work_tag', self::WORK_DEV)
            ->where('action_tag', self::ACTION_HOLD)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeCreativeWait($query)
    {
        return $query->where('work_tag', self::WORK_CREATIVE)
            ->where('action_tag', self::ACTION_WAIT)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeCreativeInfoReq($query)
    {
        return $query->where('work_tag', self::WORK_CREATIVE)
            ->where('action_tag', self::ACTION_INFO_REQ)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeCreativeInProg($query)
    {
        return $query->where('work_tag', self::WORK_CREATIVE)
            ->where('action_tag', self::ACTION_IN_PROG)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeCreativeHold($query)
    {
        return $query->where('work_tag', self::WORK_CREATIVE)
            ->where('action_tag', self::ACTION_HOLD)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeQaWait($query)
    {
        return $query->where('work_tag', self::WORK_QA)
            ->where('action_tag', self::ACTION_WAIT)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeQaInfoReq($query)
    {
        return $query->where('work_tag', self::WORK_QA)
            ->where('action_tag', self::ACTION_INFO_REQ)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeQaInProg($query)
    {
        return $query->where('work_tag', self::WORK_QA)
            ->where('action_tag', self::ACTION_IN_PROG)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }

    public function scopeQaHold($query)
    {
        return $query->where('work_tag', self::WORK_QA)
            ->where('action_tag', self::ACTION_HOLD)
            ->whereNotNull('start_at')
            ->whereNotNull('end_at');
    }








    public function scopeUnendedUnitTimes($query, $unit)
    {
        return $query->where('unit_id', $unit->id)->whereNull('end_at');
    }


    public function scopeActivityAt($query, $date_group, $length = 0, $onwards = true)
    {
        $start = Carbon::now();
        $end = $start->copy();

        if ($onwards) {
            if ($date_group == "days") {
                $start->addDays($length)->startOfDay();
                $end->addDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }
                
                $start->addWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->addWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->addMonths($length)->startOfMonth()->startOfDay();
                $end->addMonths($length)->endOfMonth()->endOfDay();
            }
        } else {
            if ($date_group == "days") {
                $start->subDays($length)->startOfDay();
                $end->subDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }

                $start->subWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->subWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->subMonths($length)->startOfMonth()->startOfDay();
                $end->subMonths($length)->endOfMonth()->endOfDay();
            }
        }

        return $query->whereNotNull('start_at')
            ->whereNotNull('end_at')
            ->where('start_at', '>=', $start)
            ->where('end_at', '<=', $end);
    }

    public function scopeWorkAt($query, $work_type, $date_group, $length = 0, $onwards = true)
    {
        $start = Carbon::now();
        $end = $start->copy();

        if ($onwards) {
            if ($date_group == "days") {
                $start->addDays($length)->startOfDay();
                $end->addDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }
                
                $start->addWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->addWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->addMonths($length)->startOfMonth()->startOfDay();
                $end->addMonths($length)->endOfMonth()->endOfDay();
            }
        } else {
            if ($date_group == "days") {
                $start->subDays($length)->startOfDay();
                $end->subDays($length)->endOfDay();
            } else if ($date_group == "weeks") {
                if ($start->dayOfWeek == 0) {
                    $start->addDay();
                    $end->addDay();
                }

                $start->subWeeks($length)->startOfWeek()->subDay()->startOfDay();
                $end->subWeeks($length)->endOfWeek()->subDay()->endOfDay();
            } else if ($date_group == "months") {
                $start->subMonths($length)->startOfMonth()->startOfDay();
                $end->subMonths($length)->endOfMonth()->endOfDay();
            }
        }

        if ($work_type == "qa") {
            $qa_members = Employee::where('team_id', 6)->get();
            $plucked = $qa_members->pluck('id');
            $qa_ids = $plucked->toArray();

            return $query->whereIn('employee_id', $qa_ids)
                ->where('work_tag', self::WORK_QA)
                ->where('action_tag', self::ACTION_IN_PROG)
                ->where('end_at', '>=', $start)
                ->where('end_at', '<=', $end);
        }
    }
}
