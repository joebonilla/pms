<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMatrix extends Model
{
    public $table = "user_matrix";

    public function employee(){
        return $this->belongsTo('App\Models\Employee', 'ic_tag', 'ic_tag');
    }

    public function userWorkType(){
        return $this->hasMany('App\Models\UserWorktype', 'user_matrix_id', 'id');
    }
}
