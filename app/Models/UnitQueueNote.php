<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class UnitQueueNote extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unit_queue_notes';

    protected $appends = ['queue'];

    const QUEUE_DEV_READY = 1;
    const QUEUE_QA_READY = 2;
    const QUEUE_REVISION = 3;
    const QUEUE_NO_REVISION = 4;
    const QUEUE_CLOSE = 5;

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit', 'unit_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id', 'id');
    }

    public function getQueueAttribute()
    {
        switch ($this->queue_tag) {
            case self::QUEUE_DEV_READY:
                return trans('unit_queue_note.label.dev_ready');
            case self::QUEUE_QA_READY:
                return trans('unit_queue_note.label.qa_ready');
            case self::QUEUE_REVISION:
                return trans('unit_queue_note.label.rev');
            case self::QUEUE_NO_REVISION:
                return trans('unit_queue_note.label.no_rev');
            case self::QUEUE_CLOSE:
                return trans('unit_queue_note.label.close');
        }
    }

    public function getCurrentQueueAttribute()
    {
        
    }

    public function scopeRevisionRound($query)
    {
        return $query->where('queue_tag', self::QUEUE_REVISION);
    }

    public function scopeNotes($query)
    {
        return $query->whereNotNull('note');
    }
}
