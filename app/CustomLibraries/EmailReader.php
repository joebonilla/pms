<?php

namespace App\CustomLibraries;

/**
* IMAP Email Reader
*/
class EmailReader
{
    public $inbox;
    public $emails;

    const UNSEEN_EMAILS = 'UNSEEN';
    const IMAP_HOSTNAME = '{imap.gmail.com:993/imap/ssl}INBOX';

    public function __construct()
    {
        $hostname = self::IMAP_HOSTNAME;
        $username = env('MAIL_USERNAME');
        $password = env('MAIL_PASSWORD');

        $inbox = imap_open($hostname, $username, $password);

        if ($inbox) {
            $this->inbox = $inbox;
            $this->emails = imap_search($inbox, self::UNSEEN_EMAILS);
        } else {
            die('Cannot connect: ' . imap_last_error());
        }
    }

    public function getEmails()
    {
        $emails = $this->emails;
        $inbox = $this->inbox;
        $data = [];

        if ($emails) {
            foreach ($emails as $email_number) {
                $overview = imap_fetch_overview($inbox, $email_number, 0);

                $data[] = $this->getEmailData($inbox, $email_number, $overview);
            }
        }

        return $data;
    }

    public function getEmailData($inbox, $email_number, $overview)
    {
        $message = trim(utf8_encode(quoted_printable_decode(imap_fetchbody($inbox, (int)$email_number, (int)1.2))));
        $message = str_replace("=", "", $message);

        var_dump($overview);
        exit();

        return $data = [
            'subject' => $overview[0]->subject,
            'from' => $overview[0]->from,
            'to' => $overview[0]->to,
            'date' => $overview[0]->date,
            'message' => $message
        ];
    }

    public function cleanCharacters($data)
    {
        var_dump($overview);
        exit();
    }
}