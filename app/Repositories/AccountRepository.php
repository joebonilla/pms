<?php

namespace App\Repositories;

use App\Models\Team;
use App\Models\Account;

/**
* 
*/
class AccountRepository
{
    /**
     * [getAccounts description]
     * @return [type] [description]
     */
    public function getTeamAccounts(Team $team)
    {
        return $team->accounts;
    }

    public function getAllAccounts()
    {
        return Account::all();
    }
}