<?php

namespace App\Repositories;

use App\Models\UnitIssue;

class UnitIssueRepository
{
    public function getLatestUnitIssues($unit)
    {
        return $unit->issues()->orderBy('created_at', 'desc')->take(10)->get();
    }
}