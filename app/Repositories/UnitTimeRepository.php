<?php

namespace App\Repositories;

use App\Models\UnitTime;

use Carbon\Carbon;

/**
* 
*/
class UnitTimeRepository
{
    public function getUnitTimes($unit, $limit = 30)
    {
        $campaign = $unit->campaign;
        $team = $campaign->team;
        $partner = $campaign->partner;

        $unit_times = UnitTime::where('unit_id', $unit->id)
            ->where('team_id', $team->id)
            ->where('partner_id', $partner->id)
            ->where('campaign_id', $campaign->id)
            ->orderBy('created_at', 'desc')
            ->take($limit)
            ->get();

        return $unit_times;
    }
}
