<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Unit;
use App\Models\UnitQueue;

/**
 *
 */
class UnitRepository
{
    public function getUnitStats()
    {
        $new = Unit::status(Unit::STATUS_NEW)->count();
        $waiting = Unit::status(Unit::STATUS_WAITING)->count();
        $unassigned = Unit::status(Unit::STATUS_UNASSIGNED)->count();
        $in_prog = Unit::status(Unit::STATUS_INFO_REQ)->count();
        $info_req = Unit::status(Unit::STATUS_IN_PROG)->count();
        $on_hold = Unit::status(Unit::STATUS_HOLD)->count();
        $solved = Unit::status(Unit::STATUS_SOLVED)->count();
        $closed = Unit::status(Unit::STATUS_CLOSED)->count();
        $late = Unit::late()->count();

        $stats = (object)[
            'new' => $new,
            'waiting' => $waiting,
            'unassigned' => $unassigned,
            'in_prog' => $in_prog,
            'info_req' => $info_req,
            'on_hold' => $on_hold,
            'solved' => $solved,
            'closed' => $closed,
            'late' => $late
        ];

        return $stats;
    }

    public function searchUnit(Request $request)
    {
        $default_column = "id";
        $default_order = "desc";
        $default_per_page = 10;
        $default_search = 'name';

        $sort = $request->input('sort', $default_column);
        if ($sort == 'commit_due') {
            $sort = 'committed_at';
        }
        $order = $request->input('order', $default_order);
        $per_page = $request->input('per_page', $default_per_page);
        $search = $request->input('search_for', $default_search);

        $units = Unit::where($search, 'LIKE', '%' . $request->search . '%')->orderBy($sort, $order)->paginate($per_page);

        return $units;

    }

    public function unitComplexities()
    {
        return [
            "0.03",
            "0.05",
            "0.07",
            "0.1",
            "0.13",
            "0.5",
            "0.25",
            "0.50",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11"
        ];
    }

    public function unitQaComplexities()
    {
        return ["0.2", "0.5", "1", "2", "3", "4", "5", "6", "7"];
    }

    public function unitPriorities()
    {
        return [
            Unit::PRIORITY_LOW => trans('unit.priority.low'),
            Unit::PRIORITY_NORMAL => trans('unit.priority.normal'),
            Unit::PRIORITY_HIGH => trans('unit.priority.high')
        ];
    }

    public function unitTaskTypes()
    {
        return [
            Unit::TYPE_INITIAL_BUILD => trans('unit.type.initial_build'),
            Unit::TYPE_CREATIVE_ITERATION => trans('unit.type.creative_iteration'),
            Unit::TYPE_CLIENT_REVISION => trans('unit.type.client_revision'),
            Unit::TYPE_CLIENT_ISSUE => trans('unit.type.client_issue'),
            Unit::TYPE_FILE_PATH_UPDATE => trans('unit.type.file_path_update'),
            Unit::TYPE_TRACKING_UPDATE => trans('unit.type.tracking_update'),
            Unit::TYPE_IPD_BUILD => trans('unit.type.ipd_build'),
            Unit::TYPE_PROD_QA => trans('unit.type.prod_qa')
        ];
    }

    public function unitAdTypes()
    {
        return [
            Unit::AD_TYPE_INPAGE => trans('unit.ad_type.inpage'),
            Unit::AD_TYPE_EXPANDING => trans('unit.ad_type.expanding'),
            Unit::AD_TYPE_PUSHDOWN => trans('unit.ad_type.pushdown'),
            Unit::AD_TYPE_MDE => trans('unit.ad_type.mde'),
            Unit::AD_TYPE_FLOATING => trans('unit.ad_type.floating'),
            Unit::AD_TYPE_INPAGE_FLOAT => trans('unit.ad_type.inpage_float'),
            Unit::AD_TYPE_IM_EXPAND => trans('unit.ad_type.im_expand'),
            Unit::AD_TYPE_TANDEM => trans('unit.ad_type.tandem'),
            Unit::AD_TYPE_SUPER_EVA => trans('unit.ad_type.super_eva'),
            Unit::AD_TYPE_MULTI_FLOATING => trans('unit.ad_type.multi_floating'),
            Unit::AD_TYPE_PEELBACK => trans('unit.ad_type.peelback'),
            Unit::AD_TYPE_WALLPAPER_RESKIN => trans('unit.ad_type.wallpaper_reskin'),
            Unit::AD_TYPE_CATFISH => trans('unit.ad_type.catfish'),
            Unit::AD_TYPE_IN_STREAM => trans('unit.ad_type.in_stream'),
            Unit::AD_TYPE_VPAID_LINEAR => trans('unit.ad_type.vpaid_linear'),
            Unit::AD_TYPE_VPAID_NON_LINEAR => trans('unit.ad_type.vpaid_non_linear'),
            Unit::AD_TYPE_LIGHTBOX => trans('unit.ad_type.lightbox'),
            Unit::AD_TYPE_IHB => trans('unit.ad_type.ihb'),
            Unit::AD_TYPE_MASTBOX => trans('unit.ad_type.mastbox'),
            Unit::AD_TYPE_MASTHEAD => trans('unit.ad_type.masthead'),
            Unit::AD_TYPE_CUSTOM => trans('unit.ad_type.custom'),
            Unit::AD_TYPE_WEB => trans('unit.ad_type.web'),
            Unit::AD_TYPE_APP => trans('unit.ad_type.app'),
            Unit::AD_TYPE_STATIC_BANNER => trans('unit.ad_type.static_banner'),
            Unit::AD_TYPE_ANIMATED_GIF_BANNER => trans('unit.ad_type.animated_gif_banner'),
            Unit::AD_TYPE_DESKTOP_LANDING_PAGE => trans('unit.ad_type.desktop_landing_page'),
            Unit::AD_TYPE_MOBILE_LANDING_PAGE => trans('unit.ad_type.mobile_landing_page'),
            Unit::AD_TYPE_STANDARD_FLASH => trans('unit.ad_type.standard_flash'),
            Unit::AD_TYPE_AD_MAIL => trans('unit.ad_type.ad_mail'),
            Unit::AD_TYPE_IOPENER_DBG => trans('unit.ad_type.iopener_dbg'),
            Unit::AD_TYPE_SCENE_STEALER_DBG => trans('unit.ad_type.scene_stealer_dbg'),
            Unit::AD_TYPE_VIDEO_EDIT_TVG => trans('unit.ad_type.video_edit_tvg'),
            Unit::AD_TYPE_MOCK_EXPAND_INNODES => trans('unit.ad_type.mock_expand_innodes'),
            Unit::AD_TYPE_MOCK_AD_EXTENDER_INNODES => trans('unit.ad_type.mock_ad_extender_innodes'),
            Unit::AD_TYPE_MOCK_AD_SELECTOR_INNODES => trans('unit.ad_type.mock_ad_selector_innodes'),
            Unit::AD_TYPE_MOCK_CANVAS_INNODES => trans('unit.ad_type.mock_canvas_innodes'),
            Unit::AD_TYPE_LIVE_EXPAND_INNODES => trans('unit.ad_type.live_expand_innodes'),
            Unit::AD_TYPE_LIVE_AD_EXTENDER_INNODES => trans('unit.ad_type.live_ad_extender_innodes'),
            Unit::AD_TYPE_LIVE_AD_SELECTOR_INNODES => trans('unit.ad_type.live_ad_selector_innodes'),
            Unit::AD_TYPE_LIVE_CANVAS_INNODES => trans('unit.ad_type.live_canvas_innodes'),
            Unit::AD_TYPE_HOVER => trans('unit.ad_type.hover'),

            Unit::AD_TYPE_STATICS_DESIGNER => trans('unit.ad_type.statics_designer'),
            Unit::AD_TYPE_STATICS_DEVELOPER => trans('unit.ad_type.statics_developer'),
            Unit::AD_TYPE_PROGRESSIVE_BILLBOARD_NATIVE_H5 => trans('unit.ad_type.progressive_billboard_native_h5'),
            Unit::AD_TYPE_PROGRESSIVE_BILLBOARD_FLASH_TALKING => trans('unit.ad_type.progressive_billboard_flash_talking'),
            Unit::AD_TYPE_PROGRESSIVE_BILLBOARD_GWD => trans('unit.ad_type.progressive_billboard_gwd'),
            Unit::AD_TYPE_PROGRESSIVE_BILLBOARD_ADDROID => trans('unit.ad_type.progressive_billboard_addroid'),
            Unit::AD_TYPE_ENDPLATE_ANIMATION_NATIVE_H5 => trans('unit.ad_type.endplate_animation_native_h5'),
            Unit::AD_TYPE_ENDPLATE_ANIMATION_SIZMEK => trans('unit.ad_type.endplate_animation_sizmek'),
            Unit::AD_TYPE_ENDPLATE_ANIMATION_FLASH_TALKING => trans('unit.ad_type.endplate_animation_flash_talking'),
            Unit::AD_TYPE_ENDPLATE_ANIMATION_GWD => trans('unit.ad_type.endplate_animation_gwd'),
            Unit::AD_TYPE_ENDPLATE_ANIMATION_ADDROID => trans('unit.ad_type.endplate_animation_addroid'),
            Unit::AD_TYPE_PUSHDOWN_EXPANDING_NATIVE_H5 => trans('unit.ad_type.pushdown_expanding_native_h5'),
            Unit::AD_TYPE_PUSHDOWN_EXPANDING_SIZMEK => trans('unit.ad_type.pushdown_expanding_sizmek'),
            Unit::AD_TYPE_PUSHDOWN_EXPANDING_GWD => trans('unit.ad_type.pushdown_expanding_gwd'),
            Unit::AD_TYPE_CUSTOM_OVERLAY_NATIVE_H5 => trans('unit.ad_type.custom_overlay_native_h5'),
            Unit::AD_TYPE_CUSTOM_OVERLAY_SIZMEK => trans('unit.ad_type.custom_overlay_sizmek'),
            Unit::AD_TYPE_CUSTOM_OVERLAY_GWD => trans('unit.ad_type.custom_overlay_gwd'),
            Unit::AD_TYPE_CUSTOM_RESPONSIVE_AD_GWD => trans('unit.ad_type.custom_responsive_ad_gwd'),
            Unit::AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_NATIVE_H5 => trans('unit.ad_type.custom_transparent_video_ad_native_h5'),
            Unit::AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_SIZMEK => trans('unit.ad_type.custom_transparent_video_ad_sizmek'),
            Unit::AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_FLASH_TALKING => trans('unit.ad_type.custom_transparent_video_ad_flash_talking'),
            Unit::AD_TYPE_CUSTOM_TRANSPARENT_VIDEO_AD_GWD => trans('unit.ad_type.custom_transparent_video_ad_gwd'),
            Unit::AD_TYPE_CUSTOM_VIDEO_WALL_NATIVE_H5 => trans('unit.ad_type.custom_video_wall_native_h5'),
            Unit::AD_TYPE_CUSTOM_VIDEO_WALL_SIZMEK => trans('unit.ad_type.custom_video_wall_sizmek'),
            Unit::AD_TYPE_CUSTOM_VIDEO_WALL_GWD => trans('unit.ad_type.custom_video_wall_gwd'),
            Unit::AD_TYPE_MARKETING => trans('unit.ad_type.marketing'),

            Unit::AD_TYPE_EXPAND => trans('unit.ad_type.expand'),
            Unit::AD_TYPE_OVERLAY => trans('unit.ad_type.overlay'),
            Unit::AD_TYPE_AD_CANVAS => trans('unit.ad_type.ad_canvas'),
            Unit::AD_TYPE_AD_EXTENDER => trans('unit.ad_type.ad_extender'),
            Unit::AD_TYPE_AD_SELECTOR => trans('unit.ad_type.ad_selector'),
            Unit::AD_TYPE_END_SLATE => trans('unit.ad_type.end_slate'),
            Unit::AD_TYPE_SWF_ONLY => trans('unit.ad_type.swf_only'),
            Unit::AD_TYPE_PRE_ROLL => trans('unit.ad_type.pre_roll'),
            Unit::AD_TYPE_ADVANCE_CREATIVE => trans('unit.ad_type.advance_creative'),
            Unit::AD_TYPE_VAST => trans('unit.ad_type.vast')
        ];
    }
}