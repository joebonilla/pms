<?php

namespace App\Repositories;

use Carbon\Carbon;

use App\Models\UnitIssue;
use App\Models\UnitTime;
use App\Models\Campaign;
use App\Models\Unit;

class DashboardRepository
{
    const PERCENTAGE_MULTIPLIER = 100;
    const DEFAULT_VALUE = 0;
    const TWO_DECIMAL_PLACES = 2;

    public function getVolume()
    {
        $graph = [];

        for ($week = 7; $week >= 0; $week--) {
            $graph[] = $units = Unit::eventAt(Carbon::now(), "closed", "weeks", $week, false)->count();
        }

        $volume = Unit::eventAt(Carbon::now(), "closed", "weeks")->count();
        $volume_graph = $graph;
        $this_week_units = $volume;
        $last_week_units = Unit::eventAt(Carbon::now(), "closed", "weeks", 1, false)->count();
        $variance = $this_week_units - $last_week_units;
        $percentage = ($last_week_units) ? ($variance / $last_week_units) * self::PERCENTAGE_MULTIPLIER : 0;
        $campaigns = Campaign::eventAt(Carbon::now(), "closed", "weeks")->count();

        $initial_builds = Unit::eventAt(Carbon::now(), "closed", "weeks")
            ->where('type_tag', Unit::TYPE_INITIAL_BUILD)->count();

        $creative_iterations = Unit::eventAt(Carbon::now(), "closed", "weeks")
            ->where('type_tag', Unit::TYPE_CREATIVE_ITERATION)->count();

        return (object) [
            'volume' => $volume,
            'volume_graph' => $volume_graph,
            'this_week_units' => $this_week_units,
            'last_week_units' => $last_week_units,
            'variance' => $variance,
            'percentage' => round($percentage, self::TWO_DECIMAL_PLACES),
            'campaigns' => $campaigns,
            'initial_builds' => $initial_builds,
            'creative_iterations' => $creative_iterations
        ];
    }

    public function getUtilization()
    {
        $graph = [];

        for ($week = 7; $week >= 0; $week--) {
            $unit_times = UnitTime::activityAt("weeks", $week, false)
                ->where('work_tag', '!=', UnitTime::WORK_QA)->get();

            $graph[] = round($unit_times->sum('utilization'), self::TWO_DECIMAL_PLACES);
        }

        $this_week = UnitTime::activityAt("weeks")->where('work_tag', '!=', UnitTime::WORK_QA)->get();
        $last_week = UnitTime::activityAt("weeks", 1, false)->where('work_tag', '!=', UnitTime::WORK_QA)->get();
        $reviews = UnitTime::activityAt("weeks")->where('work_tag', UnitTime::WORK_REVIEW)->get();
        $devs = UnitTime::activityAt("weeks")->where('work_tag', UnitTime::WORK_DEV)->get();
        $qas = UnitTime::activityAt("weeks")->where('work_tag', UnitTime::WORK_QA)->get();

        $utilization = round($this_week->sum('utilization'), self::TWO_DECIMAL_PLACES);
        $utilization_graph = $graph;
        $this_week_hours = $utilization;
        $last_week_hours = round($last_week->sum('utilization'), self::TWO_DECIMAL_PLACES);
        $variance = $this_week_hours - $last_week_hours;
        $percentage = ($last_week_hours) ? ($variance / $last_week_hours) * self::PERCENTAGE_MULTIPLIER : 100;
        $review_hours = round($reviews->sum('utilization'), self::TWO_DECIMAL_PLACES);
        $dev_hours = round($devs->sum('utilization'), self::TWO_DECIMAL_PLACES);
        $qa_hours = round($qas->sum('utilization'), self::TWO_DECIMAL_PLACES);

        return (object) [
            'utilization' => $utilization,
            'utilization_graph' => $utilization_graph,
            'this_week_hours' => $utilization,
            'last_week_hours' => $last_week_hours,
            'variance' => $variance,
            'percentage' => round($percentage, self::TWO_DECIMAL_PLACES),
            'review_hours' => $review_hours,
            'dev_hours' => $dev_hours,
            'qa_hours' => $qa_hours
        ];
    }

    public function getQuality()
    {
        $closed_units = Unit::eventAt(Carbon::now(), "closed", "weeks")->count();

        $this_week_issues = UnitIssue::whereHas('unit.campaign', function ($query) {
            $query->eventAt(Carbon::now(), "closed", "weeks");
        })->count();

        $last_week_issues = UnitIssue::whereHas('unit.campaign', function ($query) {
            $query->eventAt(Carbon::now(), "closed", "weeks", 1, false);
        })->count();

        $variance = $this_week_issues - $last_week_issues;
        $percentage = ($last_week_issues) ? ($variance / $last_week_issues) * self::PERCENTAGE_MULTIPLIER : 0;

        $client_issues = $this_week_issues;

        $units_with_issues = Unit::whereHas('campaign', function ($query) {
            $query->eventAt(Carbon::now(), "closed", "weeks");
        })->has('issues')->count();

        $campaigns_with_issues = Campaign::eventAt(Carbon::now(), "closed", "weeks")
            ->whereHas('units', function ($query) {
                $query->has('issues');
            })->count();

        if ($closed_units) {
            $quality_value = ($closed_units - $units_with_issues) / $closed_units;
        } else {
            $quality_value = 0;
        }

        $quality = round($quality_value * self::PERCENTAGE_MULTIPLIER, self::TWO_DECIMAL_PLACES);

        $graph = [];

        for ($week = 7; $week >= 0; $week--) {
            $closed_units_week = Unit::eventAt(Carbon::now(), "closed", "weeks", $week, false)->count();

            $units_with_issues_week = Unit::whereHas('campaign', function ($query) use ($week) {
                $query->eventAt(Carbon::now(), "closed", "weeks", $week, false);
            })->has('issues')->count();

            if ($closed_units_week) {
                $graph_value = ($closed_units_week - $units_with_issues_week) / $closed_units_week;
            } else {
                $graph_value = 0;
            }

            $graph[] = round($graph_value * self::PERCENTAGE_MULTIPLIER, self::TWO_DECIMAL_PLACES);
        }

        return (object) [
            'quality' => $quality,
            'quality_graph' => $graph,
            'this_week_issues' => $this_week_issues,
            'last_week_issues' => $last_week_issues,
            'variance' => $variance,
            'percentage' => $percentage,
            'client_issues' => $client_issues,
            'units_with_issues' => $units_with_issues,
            'campaigns_with_issues' => $campaigns_with_issues,
        ];
    }

    public function getEfficiency()
    {
        $this_week_lates_count = Unit::eventAt(Carbon::now(), "lates", "weeks")->count();
        $last_week_lates_count = Unit::eventAt(Carbon::now(), "lates", "weeks", 1, false)->count();
        $on_time_units_count = Unit::eventAt(Carbon::now(), "on_time", "weeks")->count();
        $closed_units = Unit::eventAt(Carbon::now(), "closed", "weeks")->get();

        $closed_units_count = $closed_units->count();
        $variance = $this_week_lates_count - $last_week_lates_count;
        $percentage = ($last_week_lates_count) ? ($variance / $last_week_lates_count) * self::PERCENTAGE_MULTIPLIER : 0;
        $percentage = round($percentage, self::TWO_DECIMAL_PLACES);
        $otd_level = ($closed_units_count) ? $this_week_lates_count / $closed_units_count * self::PERCENTAGE_MULTIPLIER : 0;
        $otd_level = round($percentage, self::TWO_DECIMAL_PLACES);

        $avg_tat_hour = round($closed_units->avg('tat'));

        if ($this_week_lates_count) {
            $efficiency = round(($this_week_lates_count - $closed_units_count) / $this_week_lates_count, self::TWO_DECIMAL_PLACES);
        } else {
            $efficiency = 0;
        }

        $graph = [];

        for ($week = 7; $week >= 0; $week--) {
            $week_lates_count = Unit::eventAt(Carbon::now(), "lates", "weeks", $week, false)->count();
            $week_closed_count = Unit::eventAt(Carbon::now(), "closed", "weeks", $week, false)->count();

            if ($week_lates_count) {
                $graph[] = round(($week_lates_count - $week_closed_count) / $week_lates_count, self::TWO_DECIMAL_PLACES);
            } else {
                $graph[] = 0;
            }
        }

        return (object) [
            'efficiency' => $efficiency,
            'efficiency_graph' => $graph,
            'this_week_lates' => $this_week_lates_count,
            'last_week_lates' => $last_week_lates_count,
            'variance' => $variance,
            'percentage' => $percentage,
            'on_time_units' => $on_time_units_count,
            'otd_level' => $otd_level,
            'avg_tat_hour' => $avg_tat_hour
        ];
    }
}

























