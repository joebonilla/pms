<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Account;
use App\Models\Campaign;
use App\Models\Team;
use Illuminate\Support\Facades\Auth as log_user;

/**
 *
 */
class CampaignRepository
{
    const THIRTY_HOURS = 30;
    const TWENTYFOUR_HOURS = 24;
    const EIGHT_HOURS = 8;
    const THREE_HOURS = 3;

    const RANGE_DAY = 1;
    const RANGE_WEEK = 2;
    const RANGE_MONTH = 3;

    public $thirty_hours;
    public $twentyfour_hours;
    public $eight_hours;
    public $three_hours;

    public $sort_columns = [
        'id' => 'id',
        'name' => 'name',
        'team' => 'team_id',
        'account' => 'partner_id',
        'created' => 'created_at',
        'commit' => 'committed_at',
        'percentage' => 'queue_tag',
    ];

    public function __construct()
    {
        $this->thirty_hours = Carbon::now()->addHours(self::THIRTY_HOURS);
        $this->twentyfour_hours = Carbon::now()->addHours(self::TWENTYFOUR_HOURS);
        $this->eight_hours = Carbon::now()->addHours(self::EIGHT_HOURS);
        $this->three_hours = Carbon::now()->addHours(self::THREE_HOURS);
    }

    public function campaign_types()
    {
        return [
            Campaign::TYPE_HTML5 => trans('campaign.type.html5'),
            Campaign::TYPE_FLASH => trans('campaign.type.flash'),
            Campaign::TYPE_PHP => trans('campaign.type.php')
        ];
    }

    /**
     * Campaigns Paginated
     * @param  Request $request Request parameters
     * @return Campaign         Campaign collection
     */
    public function getCampaignsPaginated(Request $request)
    {
        $default_column = "created";
        $default_order = "desc";
        $default_per_page = 10;
        $default_search = 'name';
        $from_team = false;
        $from_partner = false;
        $whereOr = false;
        $sort = $request->input('sort', $default_column);
        $order = $request->input('order', $default_order);
        $per_page = $request->input('per_page', $default_per_page);
        $search_for = $request->input('search_for', $default_search);


        if ($sort && array_key_exists($sort, $this->sort_columns)) {
            if ($sort == "commit") {
                $campaigns = Campaign::join('units', 'units.campaign_id', '=', 'campaigns.id', 'left outer')
                    ->orderBy('units.committed_at', $order)
                    ->groupBy('campaigns.id')
                    ->select('campaigns.*');
            } elseif ($sort == "team") {
                $campaigns = Campaign::join('teams', 'teams.id', '=', 'campaigns.team_id')
                    ->orderBy('teams.name', $order)
                    ->groupBy('campaigns.id')
                    ->select('campaigns.*');
                $from_team = true;
            } elseif ($sort == "account") {
                $campaigns = Campaign::join('partners', 'partners.id', '=', 'campaigns.partner_id')
                    ->orderBy('partners.name', $order)
                    ->groupBy('campaigns.id')
                    ->select('campaigns.*');
                $from_partner = true;
            } elseif ($sort == "percentage") {
                $campaigns = Campaign::join('units', 'units.campaign_id', '=', 'campaigns.id', 'left outer')
                    ->join('unit_queue_notes', 'units.id', '=', 'unit_queue_notes.unit_id', 'left outer')
                    ->orderBy('percentage', $order)
                    ->groupBy('campaigns.id')
                    ->selectRaw('campaigns.*, SUM(unit_queue_notes.queue_tag) / COUNT(units.id) as percentage');
            } else {
                $campaigns = Campaign::join('teams', 'teams.id', '=', 'campaigns.team_id')
                    ->orderBy($this->sort_columns[$sort], $order)
                    ->groupBy('campaigns.id')
                    ->select('campaigns.*');
                $from_team = true;
            }
        } else {

            $campaigns = Campaign::all();
        }

        if (!empty($request->search)) {
            if ($sort == 'commit_due') {

                $campaigns = Campaign::leftJoin('units', 'units.campaign_id', '=', 'campaigns.id')
                    ->groupBy('units.campaign_id')
                    ->orderBy('units.id', $order)
                    ->orderBy('units.committed_at', $order)
                    ->where('campaigns.name', 'LIKE', '%' . $request->search . '%')
                    ->select('campaigns.*');
            } else {
                if ($from_partner) {

                    $campaigns->where(function($query) use ($request) {
                        $query->where('partners.name', 'LIKE', '%' . $request->search . '%')
                            ->orwhere('campaigns.name', 'LIKE', '%' . $request->search . '%');
                    });

                } else {
                    $campaigns->join('partners', 'partners.id', '=', 'campaigns.partner_id')
                        ->where(function($query) use ($request) {
                            $query->where('partners.name', 'LIKE', '%' . $request->search . '%')
                                ->orwhere('campaigns.name', 'LIKE', '%' . $request->search . '%');
                        })
                        ->groupBy('campaigns.id')
                        ->select('campaigns.*');
                    $whereOr = true;
                }
            }
        }


        if ($request->team) {
            $campaigns->inTeam($request->team);
        }

        if ($request->account) {
            $campaigns->inAccount($request->account);
        }

        if ($request->status) {
            if ($request->status == Campaign::EXTRA_STATUS_LATE) {
                $campaigns->late();
            } else {
                $campaigns->status($request->status);
            }
        }

        if ($request->due) {
            $hours = (int)$request->due;

            $campaigns->dueIn(Carbon::now()->addHours($hours));
        }

        if ($request->created_from) {
            $created_from = Carbon::parse($request->created_from);

            $campaigns->createdFrom($created_from);
        }

        if ($request->created_to) {
            $created_to = Carbon::parse($request->created_to);

            $campaigns->createdTo($created_to);
        }

        if ($request->committed_from) {
            $committed_from = Carbon::parse($request->committed_from);

            $campaigns->whereHas('units', function ($query) use ($committed_from) {
                $query->where('committed_at', '>=', $committed_from);
            });
        }

        if ($request->committed_to) {
            $committed_to = Carbon::parse($request->committed_to);

            $campaigns->whereHas('units', function ($query) use ($committed_to) {
                $query->where('committed_at', '<=', $committed_to);
            });
        }

        if ($request->range) {
            if ($request->range == self::RANGE_MONTH) {
                $campaigns->whereHas('units', function ($query) {
                    $query->where('committed_at', '>=', Carbon::now()->startOfMonth()->startOfDay())
                        ->where('committed_at', '<=', Carbon::now()->endOfMonth()->endOfDay());
                });
            } else if ($request->range == self::RANGE_WEEK) {
                $campaigns->whereHas('units', function ($query) {
                    $query->where('committed_at', '>=', Carbon::now()->startOfWeek()->startOfDay())
                        ->where('committed_at', '<=', Carbon::now()->endOfWeek()->endOfDay());
                });
            }
        }

        if (log_user::user()->team->name != 'Management' and log_user::user()->team->name != 'QA Pool') {
            if ($from_team) {
                $campaigns->where('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%');
            } else {
                if (log_user::user()->team->name) {
                    if ($whereOr) {
                        $campaigns->join('teams', 'teams.id', '=', 'campaigns.team_id')->orwhere('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%');
                    } else {
                        $campaigns->join('teams', 'teams.id', '=', 'campaigns.team_id')->where('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%');
                    }
                }
            }
        }

        $paginated = $campaigns->paginate($per_page);

        return $paginated;
    }

    /**
     * Campaign Stats
     *
     * @return Object Campaign basic stats
     */
    public function getCampaignStats()
    {
        if (log_user::user()->team->name != 'Management' && log_user::user()->team->name != 'QA Pool') {
            $closed = Campaign::join('teams', 'teams.id', '=', 'campaigns.team_id')->where('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%')->status(Campaign::STATUS_CLOSED)->count();
            $open = Campaign::join('teams', 'teams.id', '=', 'campaigns.team_id')->where('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%')->status(Campaign::STATUS_OPEN)->count();
            $late = Campaign::join('teams', 'teams.id', '=', 'campaigns.team_id')->where('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%')->late()->count();

            $twentyfour_hours_due = Campaign::join('teams', 'teams.id', '=', 'campaigns.team_id')->where('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%')->dueBetween($this->eight_hours, $this->twentyfour_hours)->count();
            // $twentyfour_hours_due = max($twentyfour_hours_due - $late, 0);

            $eight_hours_due = Campaign::join('teams', 'teams.id', '=', 'campaigns.team_id')->where('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%')->dueBetween($this->three_hours, $this->eight_hours)->count();
            // $eight_hours_due = max($eight_hours_due - $late, 0);

            $three_hours_due = Campaign::join('teams', 'teams.id', '=', 'campaigns.team_id')->where('teams.name', 'LIKE', '%' . log_user::user()->team->name . '%')->dueBetween(Carbon::now(), $this->three_hours)->count();
            // $three_hours_due = max($three_hours_due - $late, 0);

        } else {
            $closed = Campaign::status(Campaign::STATUS_CLOSED)->count();
            $open = Campaign::status(Campaign::STATUS_OPEN)->count();
            $late = Campaign::late()->count();

            $twentyfour_hours_due = Campaign::dueBetween($this->eight_hours, $this->twentyfour_hours)->count();
            // $twentyfour_hours_due = max($twentyfour_hours_due - $late, 0);

            $eight_hours_due = Campaign::dueBetween($this->three_hours, $this->eight_hours)->count();
            // $eight_hours_due = max($eight_hours_due - $late, 0);

            $three_hours_due = Campaign::dueBetween(Carbon::now(), $this->three_hours)->count();
            // $three_hours_due = max($three_hours_due - $late, 0);
        }
        $stats = (object)[
            'twentyfour_hours_due' => $twentyfour_hours_due,
            'eight_hours_due' => $eight_hours_due,
            'three_hours_due' => $three_hours_due,
            'closed' => $closed,
            'late' => $late,
            'open' => $open
        ];

        return $stats;
    }
}
