<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\Ticket;
use Carbon\Carbon;

/**
* 
*/
class TicketRepository
{
    public function getTicket($id)
    {
        return Ticket::find($id);
    }

    /**
     * [getLatestTickets description]
     * @return [type] [description]
     */
    public function getAllTickets($search = null)
    {
        if ($search) {
            $tickets = Ticket::whereHas('campaign', function ($query) use ($search) {
                $query->where('name', 'like', '%'.$search.'%');
            })->orWhereHas('inhouse', function ($query) use ($search) {
                $query->where('title', 'like', '%'.$search.'%');
            })->orderBy('created_at', 'desc')->paginate(15);
        } else {
            $tickets = Ticket::orderBy('created_at', 'desc')->paginate(15);
        }

        return $tickets;
    }

    public function getTicketStats()
    {
        Ticket::with('campaigns');
    }
}