<?php

namespace App\Repositories;

use App\Models\UnitQueueNote;
use Carbon\Carbon;

class UnitQueueNoteRepository
{
    public function getLatestQueueNotes($unit, $limit = 10)
    {
        return $unit->queueNotes()->orderBy('created_at', 'desc')->take(10)->get();
    }
}