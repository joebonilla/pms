<?php

namespace App\Repositories;

use DB;
use Carbon\Carbon;
use App\Models\Employee;
use App\Models\Team;
use App\Models\Unit;
use App\Models\UnitQueueNote;
use App\Models\UnitTime;
use App\Models\Benchmark;

/**
*
*/
class TeamRepository
{
    /**
     * [getTeamWeek description]
     * @param  [type] $team [description]
     * @return [type]       [description]
     */
    public function getTeamWeek($team)
    {
        $units = Unit::with(['assignee.team' => function ($query) use ($team) {
            $query->where('id', $team->id);
        }])->get();

        $units = Unit::where('team_id', $team->id)->get();

        $tasks = [];

        foreach ($units as $unit) {
            if ($unit->assignee) {
                $tasks[] = [
                    'title' => $unit->assignee->name . ' [' .$unit->id. '] '.$unit->name,
                    'start' => $unit->started_at->toIso8601String(),
                    'end' => $unit->committed_at->toIso8601String(),
                    'url' => url('unit', [$unit->id]),
                    // 'color' => 'yellow'
                ];
            }
        }

        return $tasks;
    }

    /**
     * Unassigned units that belongs to the team
     *
     * @param  Integer $team_id Team id
     * @return Unit             Unit collection
     */
    public function getTeamUnassignedUnits($team_id)
    {
        if ($team_id == Team::TEAM_QA) {
            $units = Unit::whereBetween('team_id', [Team::TEAM_DEVPOOL, Team::TEAM_INNOVID_PP_NIGHT])
                ->where('current_queue_tag', UnitQueueNote::QUEUE_QA_READY)
                ->where('status_tag', '!=', Unit::STATUS_CLOSED)
                ->whereNull('employee_id')
                ->orderBy('committed_at', 'asc')
                ->get();
        } else {
            $units = Unit::where('team_id', $team_id)
                ->where('status_tag', '!=', Unit::STATUS_CLOSED)
                ->whereNull('employee_id')
                ->orderBy('committed_at', 'asc')
                ->get();
        }

        return $units;
    }

    /**
     * Team members with task
     *
     * @param  Integer $team_id Team id
     * @return Unit             Unit collection
     */
    public function getTeamMembersWithTask($team_id)
    {
        if ($team_id == Team::TEAM_QA) {
            $qa_members = Employee::where('team_id', Team::TEAM_QA)->get();
            $qa_members_id = $qa_members->pluck('id');

            $units = Unit::whereBetween('team_id', [Team::TEAM_DEVPOOL, Team::TEAM_INNOVID_PP_NIGHT])
                ->where('current_queue_tag', UnitQueueNote::QUEUE_QA_READY)
                ->where('status_tag', '!=', Unit::STATUS_CLOSED)
                ->whereNotNull('employee_id')
                ->whereIn('employee_id', $qa_members_id)
                ->orderBy('priority_tag', 'desc')
                ->get();
        } else {
            $units = Unit::where('team_id', $team_id)
                ->where('status_tag', '!=', Unit::STATUS_CLOSED)
                ->whereNotNull('employee_id')
                ->orderBy('priority_tag', 'desc')
                ->get();
        }

        return $units;
    }

    /**
     * Team members witout task
     *
     * @param  Integer  $team_id Team id
     * @return UnitTime          UnitTime collection
     */
    public function getTeamMembersWithoutTask($team_id)
    {
        $assigned_units = $this->getTeamMembersWithTask($team_id);
        $ids = $assigned_units->pluck('employee_id');

        return UnitTime::selectRaw('unit_id, employee_id, action_tag, MAX(updated_at) as updated_at')
            ->where('team_id', $team_id)
            ->whereNotIn('employee_id', $ids)
            ->groupBy('employee_id')
            ->orderBy('updated_at', 'asc')
            ->get();
    }

    public function getTeamProductivity($team_id, $timespan){

        $employee = Employee::where('team_id', $team_id)->get();
        $teambenchmark = Benchmark::where('team_id', $team_id)->get();
        $team_prod = [];

        foreach ($employee as $member) {

            if($timespan == 'months'){
                $days = $this->countDays(date('Y'), date('m'), array(0, 6)) * 7.5;
            }else if($timespan == 'weeks'){
                $days = 5 * 7.5;
            }else{
                $days = 1 * 7.5;
            }
            
            $units = Unit::where('team_id', $team_id)
            ->where('employee_id', $member->id)
            ->eventAt(Carbon::now(), "closed", $timespan, 1, false)
            ->get();

            $total_units = Unit::where('team_id', $team_id)
            ->where('employee_id', $member->id)
            ->eventAt(Carbon::now(), "closed", $timespan, 1, false)->count();

            $total_volume = Unit::where('team_id', $team_id)
            ->where('employee_id', $member->id)
            ->eventAt(Carbon::now(), "closed", $timespan, 1, false)->sum('complexity');

            $total_benchmark = 0;
            foreach ($units as $unit) {
                foreach ($teambenchmark as $benchmark) {
                   if($benchmark->complexity == $unit->complexity){
                        $total_benchmark += $benchmark->benchmark;
                   }
                }
            }
            
            $avg_benchmark = round($total_benchmark / $days, 2);
            $team_prod[] = (object)[
                'member' => $member->name,
                'total_unit' => $total_units,
                'total_volume' => $total_volume,
                'total_benchmark' => $total_benchmark,
                'prod_benchmark' => $avg_benchmark
            ];
        }         
        return $team_prod;
    }


    function countDays($year, $month, $ignore) {
        $count = 0;
        $counter = mktime(0, 0, 0, $month, 1, $year);
        while (date("n", $counter) == $month) {
            if (in_array(date("w", $counter), $ignore) == false) {
                $count++;
            }
            $counter = strtotime("+1 day", $counter);
        }
        return $count;
    }

    function getTeamBenchmark($team_id){
        $teambenchmark = Benchmark::where('team_id', $team_id)->get();   
        return $teambenchmark;
    }

}