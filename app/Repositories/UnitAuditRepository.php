<?php

namespace App\Repositories;

use App\Models\UnitAudit;

class UnitAuditRepository
{
    public function auditFlags()
    {
        return [
            UnitAudit::FLAG_NOT_DONE => trans('unit_audit.flag.not_done'),
            UnitAudit::FLAG_PASS => trans('unit_audit.flag.pass'),
            UnitAudit::FLAG_FAIL => trans('unit_audit.flag.fail')
        ];
    }
}