<?php

namespace App\Repositories;

use Illuminate\Http\Request;
use App\Models\CampaignClient;
use Carbon\Carbon;

class CampaignClientRepository
{
	public function getAllCampaignClients()
	{
		return CampaignClient::all();
	}
}
