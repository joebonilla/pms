<?php

use Carbon\Carbon;

define("TIME_SPAN_DAYS", "days");
define("TIME_SPAN_WEEKS", "weeks");
define("TIME_SPAN_MONTHS", "months");

function dateScope($time_span, $length = 0, $onwards = true, $to = null)
{
    $start = ($to === null) ? Carbon::now() : $to->startOfWeek()->subDay();
    $end = $start->copy();

    if ($onwards) {
        if ($time_span == TIME_SPAN_DAYS) {
            $start->addDays($length)->startOfDay();
            $end->addDays($length)->endOfDay();
        } else if ($time_span == TIME_SPAN_WEEKS) {
            if ($start->dayOfWeek == 0) {
                $start->addDay();
                $end->addDay();
            }
            
            $start->addWeeks($length)->startOfWeek()->subDay()->startOfDay();
            $end->addWeeks($length)->endOfWeek()->subDay()->endOfDay();
        } else if ($time_span == TIME_SPAN_MONTHS) {
            $start->addMonths($length)->startOfMonth()->startOfDay();
            $end->addMonths($length)->endOfMonth()->endOfDay();
        }
    } else {
        if ($time_span == TIME_SPAN_DAYS) {
            $start->subDays($length)->startOfDay();
            $end->subDays($length)->endOfDay();
        } else if ($time_span == TIME_SPAN_WEEKS) {
            if ($start->dayOfWeek == 0) {
                $start->addDay();
                $end->addDay();
            }

            $start->subWeeks($length)->startOfWeek()->subDay()->startOfDay();
            $end->subWeeks($length)->endOfWeek()->subDay()->endOfDay();
        } else if ($time_span == TIME_SPAN_MONTHS) {
            $start->subMonths($length)->startOfMonth()->startOfDay();
            $end->subMonths($length)->endOfMonth()->endOfDay();
        }
    }

    return (object) [
        'start' => $start,
        'end' => $end
    ];
}
