<?php

namespace App\Http\Middleware;

use Closure;

class EmployeeProfileMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->user()->getIcAttribute() == 'Developer' OR $request->user()->getIcAttribute() == 'Creative') {
            if ($request->employee != $request->user()->id) {

                return redirect('/employee');
            }
        }
        return $next($request);
    }
}
