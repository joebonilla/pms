<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('app');
// });

Route::get('/', ['as' => 'main', 'uses' => 'DashboardController@index']);
// Route::get('/dashboard/line-of-business', ['as' => 'line-of-business', 'uses' => 'DashboardController@lineOfBusiness']);
// Route::get('/dashboard/terms-and-accounts', ['as' => 'terms-and-accounts', 'uses' => 'DashboardController@termsAndAccounts']);

Route::get('/metric/volume', ['as' => 'metric.volume', 'uses' => 'MetricController@volume']);
Route::get('/metric/utilization', ['as' => 'metric.utilization', 'uses' => 'MetricController@utilization']);
Route::get('/metric/efficiency', ['as' => 'metric.efficiency', 'uses' => 'MetricController@efficiency']);

Route::post('/unit/{id}/issue', ['as' => 'unit.issue', 'uses' => 'UnitController@issue']);
Route::post('/unit/{id}/audit', ['as' => 'unit.audit', 'uses' => 'UnitController@audit']);

Route::get('/ticket/email', ['as' => 'ticket.email', 'uses' => 'TicketController@email']);

Route::resource('ticket', 'TicketController');
Route::resource('team', 'TeamController');
Route::resource('partner', 'PartnerController');
Route::resource('client-contact', 'ClientContactController');
Route::resource('campaign', 'CampaignController');
Route::resource('unit', 'UnitController');
Route::resource('employee', 'EmployeeController');
Route::resource('benchmark', 'BenchmarkController');

Route::post('/unit/mass-update', ['as' => 'unit.massupdate', 'uses' => 'UnitController@massUpdate']);

Route::post('/team/{id}', 'TeamController@createHeadCount');

Route::get('/team/{id}/accounts', ['as' => 'team.accounts', 'uses' => 'TeamController@accounts']);
Route::get('/team/{id}/benchmark', ['as' => 'team.benchmark', 'uses' => 'TeamController@benchmark']);
Route::get('/team/{id}/productivity', ['as' => 'team.productivity', 'uses' => 'TeamController@productivity']);
Route::get('/partner/{id}/advertisers', ['as' => 'partner.advertisers', 'uses' => 'PartnerController@advertisers']);
Route::get('/partner/{id}/client-contacts', ['as' => 'partner.clientcontacts', 'uses' => 'PartnerController@clientContacts']);

Route::get('/report', ['as' => 'report.index', 'uses' => 'ReportController@index']);
Route::post('/report', ['as' => 'report.generate', 'uses' => 'ReportController@generate']);
Route::get('/report/arrival_pattern', ['as' => 'report.arrival_pattern_index', 'uses' => 'ReportController@arrivalPattern']);
Route::post('/report/arrival_pattern', ['as' => 'report.arrival_pattern_generate', 'uses' => 'ReportController@arrivalPatternGenerate']);
Route::get('/report/audit_unit', ['as' => 'report.audit_unit_index', 'uses' => 'ReportController@auditUnit']);
Route::post('/report/audit_unit', ['as' => 'report.audit_unit_generate', 'uses' => 'ReportController@auditUnitGenerate']);
Route::get('/billing', ['as' => 'billing.index', 'uses' => 'BillingController@index']);
Route::post('/billing/invoice', ['as' => 'billing.invoice', 'uses' => 'BillingController@invoice']);

Route::get('/auth/login', 'Auth\AuthController@getLogin');
Route::post('/auth/register', 'Auth\AuthController@postRegister');
Route::get('/auth/logout', 'Auth\AuthController@getLogout');
Route::get('/auth/google', 'Auth\AuthController@redirectToProvider');
Route::get('/auth/google/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('/mail', 'MailController@index');
Route::get('/search', 'SearchController@index');
