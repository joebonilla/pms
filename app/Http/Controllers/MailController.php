<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CustomLibraries\EmailReader;

use App\Models\Advertiser;
use App\Models\Campaign;
use App\Models\Employee;
use App\Models\Partner;
use App\Models\Team;
use App\Models\Ticket;

class MailController extends Controller
{
    public function index(EmailReader $email_reader)
    {
        $employee = Employee::admin();

        $emails = $email_reader->getEmails();

        foreach ($emails as $data) {
            $team = Team::where('name', 'like', '%'.$data['to'].'%')->first();
            $partner = Partner::where('name', 'like', '%'.$data['from'].'%')->first();
            $advertiser = Advertiser::where('name', 'like', '%'.$data['subject'].'%')->first();


            $partner = Partner::where('name', 'like', '%'.$data['to'].'%')->first();

            $ticket = $this->createTicket($employee);
            $this->createCampaign($data, $ticket, $team, $partner, $advertiser);
        }
    }

    public function createTicket($employee)
    {
        // Create Ticket
        $ticket = new Ticket();
        $ticket->employee_id = $employee->id;
        $ticket->type_tag = Ticket::TYPE_EXTERNAL;
        $ticket->status_tag = Ticket::STATUS_OPEN;
        $ticket->created_at = Carbon::parse($data['date']);
        $ticket->save();

        return $ticket;
    }

    /**
     * Create a campaign
     * @param  Array    $data       Data from email
     * @param  Object   $ticket     Ticket instance object
     * @param  Object   $team       Team instance object
     * @param  Object   $partner    Partner instance object
     * @param  Object   $advertiser Advertiser instance object
     */
    public function createCampaign($data, $ticket, $team, $partner, $advertiser)
    {
        // Create Campaign
        $campaign = new Campaign();
        $campaign->name = $data['message'];
        $campaign->ticket_id = $ticket->id;
        $campaign->team_id = $team->id;
        $campaign->partner_id = $partner->id;
        $campaign->advertiser_id = $advertiser->id;
        $campaign->creative_brief_link = null;
        $campaign->status_tag = Campaign::STATUS_OPEN;
        $campaign->created_at = Carbon::parse($data['date']);
        $campaign->save();
    }
}
