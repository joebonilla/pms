<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TeamHeadcount;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use DB;

use App\Models\Employee;
use App\Models\UnitTime;
use App\Models\Team;
use App\Models\Unit;
use App\Models\Poc;

use App\Repositories\TeamRepository;

class TeamController extends Controller
{
    const TEAM_DIVIDER = 3;

    protected $team;

    public function __construct(TeamRepository $team)
    {
        $this->middleware('auth');

        $this->team = $team;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::displayable()->get();

        $groups = [];

        foreach ($teams as $key => $team) {
            $groups[$key % self::TEAM_DIVIDER][] = $team;
        }

        // temporary until filter
        $data = [
            'user' => Auth::user(),
            'groups' => $groups
        ];

        return view('teams.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'user' => Auth::user()
        ];

        return view('teams.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // TODO: Validation

        $team = new Team;
        $team->name = $request->name;
        $team->save();

        return redirect('/team');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, TeamRepository $team_repo)
    {
        $team = Team::find($id);

        if ($request->ajax()) {
            $tasks = $this->team->getTeamWeek($team);

            return response()->json($tasks);
        }

        $unassigned_units = $team_repo->getTeamUnassignedUnits($team->id);
        $members_with_task = $team_repo->getTeamMembersWithTask($team->id);
        $members_without_task = $team_repo->getTeamMembersWithoutTask($team->id);

        $data = [
            'team' => $team,
            'unassigned_units' => $unassigned_units,
            'members_with_task' => $members_with_task,
            'members_without_task' => $members_without_task
        ];

        return view('teams.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $team = Team::find($id);

        $poc = $team->poc;

        $data = [
            'team' => $team,
            'poc' => $poc
        ];

        return view('teams.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::find($id);
        $team->name = $request->name;

        $poc = new Poc;
        $poc->poc_id = $request->poc_id;
        $poc->sub_poc_id = $request->sub_poc_id;

        $team->poc()->save($poc);

        return redirect('/team');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $team = Team::destroy($id);

        return redirect('/team');
    }

    public function accounts($id)
    {
        $team = Team::find($id);

        if (!$team) {
            return response()->json();
        }

        return response()->json($team->accounts()->orderBy('name', 'asc')->get()->toArray());
    }

    public function createHeadCount(Request $request, $id){
        $headcount = new TeamHeadcount();

        $headcount->headcount = $request->headcount;
        $headcount->created_at = new \DateTime();
        $headcount->updated_at = new \DateTime();
        $headcount->team_id = $id;

        $headcount->save();

        return redirect('/team');
    }

    public function benchmark($id, TeamRepository $team_repo)
    {
        $benchmark = $team_repo->getTeamBenchmark($id); 
        $team = Team::find($id);        
        $data = [
            'team' => $team,
            'team_benchmark' => $benchmark
        ];
        return view('teams.benchmark', $data);
    }

    public function productivity($id, TeamRepository $team_repo)
    {
        $timespan = $_GET['timespan'];
        $team = Team::find($id);        
        $team_prod = $team_repo->getTeamProductivity($id, $timespan);           
        
        $data = [
            'team' => $team,
            'team_prod' => $team_prod,
            'timespan' => $timespan
        ];

        return view('teams.productivity', $data);
    }
}
