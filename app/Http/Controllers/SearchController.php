<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CampaignRepository;
use App\Repositories\UnitRepository;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, CampaignRepository $campaignRepository, UnitRepository $unitRepository)
    {
        return view('search.index', [
            'campaigns' => $campaignRepository->getCampaignsPaginated($request),
            'units' => $unitRepository->searchUnit($request),
            'search_options' => ['id' => 'Id', 'name' => 'Name', 'commit_due' => 'Due Date'],
            'order_options' => ['ASC' => 'Ascending', 'DESC' => 'Descending'],
            'per_page_options' => ['5', '10', '15', '20']
        ]);
    }
}
