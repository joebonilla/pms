<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\Repositories\CampaignRepository;

use App\Models\Account;
use App\Models\Advertiser;
use App\Models\Campaign;
use App\Models\Team;
use App\Models\Ticket;
use App\Models\Unit;


class CampaignController extends Controller
{
    const MONTH_START_DAY = 1;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request $request Http request object
     * @param  CampaignRepository $campaign_repo Campaign repository instance
     *
     * @return Response                          View with data
     */
    public function index(Request $request, CampaignRepository $campaign_repo)
    {

        $campaigns_stats = $campaign_repo->getCampaignStats();
        $days_in_month = Carbon::now()->daysInMonth;
        $graph = [];

        for ($day = self::MONTH_START_DAY; $day <= $days_in_month; $day++) {
            $date = Carbon::now()->startOfMonth();
            $date->day = $day;

            $graph[] = Campaign::createdFrom($date->startOfDay())
                ->createdTo($date->endOfDay())
                ->count();
        }

        $days = range(self::MONTH_START_DAY, $days_in_month);

        $this_month_start = Carbon::now()->startOfMonth();
        $this_moneth_end = Carbon::now()->endOfMonth();
        $campaigns_created_this_month = Campaign::createdFrom($this_month_start)
            ->createdTo($this_moneth_end)
            ->count();

        $last_month_start = Carbon::now()->startOfMonth()->subMonth()->startOfMonth();
        $last_month_end = Carbon::now()->startOfMonth()->subMonth()->endOfMonth();
        $campaigns_created_last_month = Campaign::createdFrom($last_month_start)
            ->createdTo($last_month_end)
            ->count();

        $campaigns = $campaign_repo->getCampaignsPaginated($request);

        $data = [
            'campaigns_stats' => $campaigns_stats,
            'graph' => $graph,
            'days' => $days,
            'campaigns_created_this_month' => $campaigns_created_this_month,
            'campaigns_created_last_month' => $campaigns_created_last_month,
            'campaigns' => $campaigns,
        ];

        return view('campaigns.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $teams = Team::displayable()->get();

        $data = [
            'teams' => $teams,
        ];

        return view('campaigns.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Create Ticket
        $ticket = new Ticket();
        $ticket->employee_id = $request->user()->id;
        $ticket->type_tag = Ticket::TYPE_EXTERNAL;
        $ticket->status_tag = Ticket::STATUS_OPEN;
        $ticket->save();

        $advertiser = Advertiser::where('name', $request->advertiser_name)->first();

        if (!$advertiser) {
            $advertiser = new Advertiser();
            $advertiser->name = $request->advertiser_name;
            $advertiser->partner_id = $request->partner_id;
            $advertiser->created_at = Carbon::now();
            $advertiser->save();
        }

        // Create Campaign
        $campaign = new Campaign();
        $campaign->ticket_id = $ticket->id;
        $campaign->name = $request->campaign_name;
        $campaign->team_id = $request->team_id;
        $campaign->partner_id = $request->partner_id;
        $campaign->advertiser_id = $advertiser->id;
        $campaign->type_tag = $request->campaign_type;
        $campaign->creative_brief_link = $request->creative_brief;
        $campaign->status_tag = Campaign::STATUS_OPEN;

        if ($request->sub_partner_name) {
            $campaign->sub_partner_name = $request->sub_partner_name;
        }

        $campaign->save();

        return redirect('/unit/create?campaign=' . $campaign->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign = Campaign::find($id);
        $employee = \Auth::user();
        $team_members = $campaign->team->members;
        $units = $campaign->units;

        if (!$units->isEmpty()) {
            // $chunks = $units->chunk(3);
            $chunks = $units;
        } else {
            $chunks = null;
        }

        $data = [
            'campaign' => $campaign,
            'team_members' => $team_members,
            'chunks' => $chunks,
            'units' => $units,
            'employee' => $employee,
        ];

        return view('campaigns.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id, CampaignRepository $campaign_repo)
    {
        $campaign = Campaign::find($id);

        if (!$campaign) {
            return redirect('/campaign');
        }

        $teams = Team::displayable()->get();
        $partners = $campaign->team->accounts;
        $advertisers = $campaign->partner->advertisers->pluck('name')->toArray();
        $campaign_types = $campaign_repo->campaign_types();

        $data = [
            'campaign' => $campaign,
            'teams' => $teams,
            'partners' => $partners,
            'advertisers' => $advertisers,
            'campaign_types' => $campaign_types
        ];

        return view('campaigns.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advertiser = Advertiser::where('name', $request->advertiser_name)->first();

        if (!$advertiser) {
            $advertiser = new Advertiser();
            $advertiser->name = $request->advertiser_name;
            $advertiser->partner_id = $request->partner_id;
            $advertiser->created_at = Carbon::NOW();
            $advertiser->save();
        }

        // Update Campaign
        $campaign = Campaign::find($id);
        $campaign->name = $request->campaign_name;
        $campaign->team_id = $request->team_id;
        $campaign->partner_id = $request->partner_id;
        $campaign->advertiser_id = $advertiser->id;
        $campaign->type_tag = $request->campaign_type;
        $campaign->creative_brief_link = $request->creative_brief;

        if ($request->sub_partner_name) {
            $campaign->sub_partner_name = $request->sub_partner_name;
        }

        $campaign->save();

        if ($campaign->units->count()) {
            foreach ($campaign->units as $u => $unit) {
                $unit->team_id = $request->team_id;
                $unit->partner_id = $request->partner_id;
                $unit->advertiser_id = $advertiser->id;

                $unit->save();

                if ($unit->unitTimes->count()) {
                    foreach ($unit->unitTimes as $ut => $unit_time) {
                        $unit_time->team_id = $request->team_id;
                        $unit_time->partner_id = $request->partner_id;

                        $unit_time->save();
                    }
                }
            }
        }

        return redirect('/campaign/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);

        if ($campaign->units->count()) {
            foreach ($campaign->units as $unit) {
                if ($unit->unitTimes->count()) {
                    foreach ($unit->unitTimes as $unit_time) {
                        $unit_time->delete();
                    }
                }

                if ($unit->issues->count()) {
                    foreach ($unit->issues as $unit_issue) {
                        $unit_issue->delete();
                    }
                }

                if ($unit->queueNotes->count()) {
                    foreach ($unit->queueNotes as $unit_queue_note) {
                        $unit_queue_note->delete();
                    }
                }

                $unit->delete();
            }
        }

        $campaign->delete();

        // UnitTimes::where('campaign_id', $id)->delete();
        // Unit::where('campaign_id', $id)->delete();
        // Campaign::find($id)->delete();

        return redirect('/campaign');
    }
}
