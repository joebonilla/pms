<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Models\UnitAudit;
use Illuminate\Http\Request;
use App\Http\Requests;
use Excel;
use Carbon\Carbon;

use App\Models\Advertiser;
use App\Models\Campaign;
use App\Models\ClientContact;
use App\Models\Partner;
use App\Models\Team;
use App\Models\Unit;
use App\Models\UnitTime;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::displayable()->get();
        $partners = Partner::all();
        $advertisers = Advertiser::all();
        $client_contacts = ClientContact::all();

        $data = [
            'teams' => $teams,
            'partners' => $partners,
            'advertisers' => $advertisers,
            'client_contacts' => $client_contacts
        ];

        return view('reports.index', $data);
    }

    public function auditUnit(){
        $closed_dates = [
            'from' => Carbon::now()->subMonth()->format('m/d/Y'),
            'to' => Carbon::now()->format('m/d/Y')
        ];

        $data = [
            'dates' => $closed_dates
        ];

        return view('reports.audit_unit', $data);
    }

    public function auditUnitGenerate(Request $request){
        $from = Carbon::parse($request->created_from);
        $to = Carbon::parse($request->created_to);
        $units = Unit::whereBetween('created_at', array($from->startOfDay(), $to->endOfDay()))->get();
        $data = [];

        foreach($units as $unit){
            $dev_name = null;
            $result = 'Not done';
            $last_touch_dev = $unit->unitTimes()->where(function ($query) {
                $query->where('work_tag', UnitTime::WORK_DEV);
            })->orderBy('created_at', 'desc');

            if ($last_touch_dev->count()) {
                $dev_name = $last_touch_dev->first()->employee->name;
            }

            $poc = $unit->audits()->join('employees', 'employees.id', '=', 'unit_audits.employee_id')->where('employees.ic_tag', Employee::IC_POC)
                    ->orderBy('unit_audits.created_at', 'desc')
                    ->first();

            $sme = $unit->audits()->join('employees', 'employees.id', '=', 'unit_audits.employee_id')->where('employees.ic_tag', Employee::IC_SME)
                    ->orderBy('unit_audits.created_at', 'desc')
                    ->first();

            $qa = $unit->audits()->join('employees', 'employees.id', '=', 'unit_audits.employee_id')->where('employees.ic_tag', Employee::IC_QA)
                    ->orderBy('unit_audits.created_at', 'desc')
                    ->first();

            $unit_audit = $unit->audits()->orderBy('created_at', 'desc')->first();



            if($unit_audit){

                if($unit_audit->work_estimate_tag === UnitAudit::FLAG_PASS &&
                    $unit_audit->overall_quality_tag === UnitAudit::FLAG_PASS &&
                    $unit_audit->following_instructions_tag === UnitAudit::FLAG_PASS &&
                    $unit_audit->process_adherence_tag === UnitAudit::FLAG_PASS){
                    $result = 'Pass';
                }

                $data[] = [
                    'Unit ID' => $unit_audit->unit_id,
                    'Team' => $unit_audit->unit->campaign->team->name,
                    'QA' => isset($qa->name) ? $qa->name : ' ',
                    'POC' => isset($poc->name) ? $poc->name : ' ',
                    'SME' => isset($sme->name) ? $sme->name : ' ',
                    'Work Estimate Audit' => $unit_audit->getFlagname($unit_audit->work_estimate_tag),
                    'Overall Quality Audit' => $unit_audit->getFlagname($unit_audit->overall_quality_tag),
                    'Following Instructions Audit' => $unit_audit->getFlagname($unit_audit->following_instructions_tag),
                    'Process Adherence Audit' => $unit_audit->getFlagname($unit_audit->process_adherence_tag),
                    'Result' => $result,
                    'Work Estimate Note' => $unit_audit->work_estimate_notes,
                    'Overall Quality Note' => $unit_audit->overall_quality_notes,
                    'Following Instructions Note' => $unit_audit->following_instructions_notes,
                    'Process Adherence Note' => $unit_audit->process_adherence_notes,
                    'Developer' => $dev_name,
                ];
            }
        }


        $filename = "audit_unit_report_".Carbon::now()->format('Y-m-d_H-i-s');

        Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('New Sheet', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xls');
    }

    public function arrivalPattern(){
        $partners = Partner::all();
        $closed_dates = [
            'from' => Carbon::now()->subMonth()->format('m/d/Y'),
            'to' => Carbon::now()->format('m/d/Y')
        ];

        $data = [
            'partners' => $partners,
            'dates' => $closed_dates
        ];

        return view('reports.arrival_pattern', $data);
    }

    public function arrivalPatternGenerate(Request $request){
        $partner = Partner::find($request->partner_id);
        $from = Carbon::parse($request->created_from);
        $to = Carbon::parse($request->created_to);
        $units = Unit::where('partner_id', $partner->id)->whereBetween('created_at', array($from->startOfDay(), $to->endOfDay()));

        $days_total = 0;
        $hours_total = 0;
        $headers_hours = [];
        $headers_day = [
            'Sunday' => 0,
            'Monday' => 0,
            'Tuesday' => 0,
            'Wednesday' => 0,
            'Thursday' => 0,
            'Friday' => 0,
            'Saturday' => 0,
        ];

        foreach($headers_day as $day => $value){
            $unit = clone $units;
            $headers_day[$day] = $unit->where(DB::raw('DAYNAME(created_at)'), $day)->count();
            $days_total += $headers_day[$day];
        }

        for ($x = 0; $x <= 23; $x++){
            $unit = clone $units;
            $headers_hours[$x] = $unit->where(DB::raw('HOUR(created_at)'), $x)->count();
            $hours_total += $headers_hours[$x];
        }

        $filename = "arrival_pattern_report_".Carbon::now()->format('Y-m-d_H-i-s');

        $data = [
            'from' => $request->created_from,
            'to' => $request->created_to,
            'partner' => $partner,
            'day' => $headers_day,
            'hours' => $headers_hours,
            'hours_total' => $hours_total,
            'days_total' => $days_total,
        ];

        Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('New sheet', function($sheet) use ($data) {
                $sheet->loadView('reports.arrival_pattern_template', $data);
            });
        })->download('xls');
    }

    public function generate(Request $request)
    {
        $campaign_id = $request->campaign_id;
        $campaign_name = $request->campaign_name;
        $team_id = $request->team_id;
        $partner_id = $request->partner_id;
        $advertiser_id = $request->advertiser_id;
        $client_contact_id = $request->client_contact_id;
        $assigned_to = $request->assigned_to;
        $complexity = $request->complexity;
        $priority = $request->priority;
        $status = $request->status;
        $type = $request->type;
        $ad_type = $request->ad_type;
        $created_at_start = $request->created_at_start;
        $created_at_end = $request->created_at_end;
        $committed_at_start = $request->committed_at_start;
        $committed_at_end = $request->committed_at_end;
        $closed_at_start = $request->closed_at_start;
        $closed_at_end = $request->closed_at_end;

        if ($campaign_id) {
            $units = Unit::with('unitTimes', 'campaign', 'campaign.team', 'campaign.partner')
                ->where('campaign_id', $campaign_id)
                ->get();
        } else {
            $units = Unit::whereHas('campaign', function ($query) use ($campaign_name) {
                    $query->where('name', 'like', '%'.$campaign_name.'%');
                })->where(function ($query) use ($team_id) {
                    if ($team_id) {
                        $query->where('team_id', $team_id);
                    }
                })->where(function ($query) use ($partner_id) {
                    if ($partner_id) {
                        $query->where('partner_id', $partner_id);
                    }
                })->where(function ($query) use ($advertiser_id) {
                    if ($advertiser_id) {
                        $query->where('advertiser_id', $advertiser_id);
                    }
                })->where(function ($query) use ($client_contact_id) {
                    if ($client_contact_id) {
                        $query->where('client_contact_id', $client_contact_id);
                    }
                })->where(function ($query) use ($complexity) {
                    if ($complexity) {
                        $query->where('complexity', $complexity);
                    }
                })->where(function ($query) use ($priority) {
                    if ($priority) {
                        $query->where('priority', $priority);
                    }
                })->where(function ($query) use ($status) {
                    if ($status) {
                        $query->where('status_tag', $status);
                    }
                })->where(function ($query) use ($type) {
                    if ($type) {
                        $query->where('type_tag', $type);
                    }
                })->where(function ($query) use ($ad_type) {
                    if ($ad_type) {
                        $query->where('ad_type_tag', $ad_type);
                    }
                })->where(function ($query) use ($assigned_to) {
                    if ($assigned_to) {
                        $query->whereHas('assignee', function ($query) use ($assigned_to) {
                            $query->where('name', 'like', '%'.$assigned_to.'%');
                        });
                    }
                })->where(function ($query) use ($created_at_start, $created_at_end) {
                    if ($created_at_start) {
                        $created_at_start = Carbon::parse($created_at_start)->startOfDay();
                        $query->where('created_at', '>', $created_at_start);
                    }

                    if ($created_at_end) {
                        $created_at_end = Carbon::parse($created_at_end)->endOfDay();
                        $query->where('created_at', '<', $created_at_end);
                    }
                })->where(function ($query) use ($committed_at_start, $committed_at_end) {
                    if ($committed_at_start) {
                        $committed_at_start = Carbon::parse($committed_at_start)->startOfDay();
                        $query->where('committed_at', '>', $committed_at_start);
                    }

                    if ($committed_at_end) {
                        $committed_at_end = Carbon::parse($committed_at_end)->endOfDay();
                        $query->where('committed_at', '<', $committed_at_end);
                    }
                })->where(function ($query) use ($closed_at_start, $closed_at_end) {
                    if ($closed_at_start) {
                        $closed_at_start = Carbon::parse($closed_at_start)->startOfDay();
                        $query->where('closed_at', '>', $closed_at_start);
                    }

                    if ($closed_at_end) {
                        $closed_at_end = Carbon::parse($closed_at_end)->endOfDay();
                        $query->where('closed_at', '<', $closed_at_end);
                    }
                })->get();
        }

        $data = [];

        if ($request->consolidated) {
            foreach ($units as $unit) {
                if ($unit->unitTimes->count()) {
                    $unit_times = $unit->unitTimes;

                    $report = [];

                    $report['unit_id'] = $unit->id;
                    $report['unit_name'] = $unit->name;
                    $report['campaign_name'] = $unit->campaign->name;
                    $report['campaign_id'] = $unit->campaign->id;
                    $report['team_name'] = $unit->campaign->team->name;
                    $report['partner_name'] = $unit->campaign->partner->name;
                    $report['sub_partner_name'] = $unit->campaign->sub_partner_name;
                    $report['advertiser_name'] = $unit->campaign->advertiser->name;

                    $last_touch_poc = $unit->unitTimes()->where(function ($query) {
                        $query->where('work_tag', UnitTime::WORK_REVIEW);
                    })->orderBy('created_at', 'desc');

                    if ($last_touch_poc->count()) {
                        $report['last_touch_poc'] = $last_touch_poc->first()->employee->name;
                    } else {
                        $report['last_touch_poc'] = "";
                    }

                    $last_touch_dev = $unit->unitTimes()->where(function ($query) {
                        $query->where('work_tag', UnitTime::WORK_DEV);
                    })->orderBy('created_at', 'desc');

                    if ($last_touch_dev->count()) {
                        $report['last_touch_dev'] = $last_touch_dev->first()->employee->name;
                    } else {
                        $report['last_touch_dev'] = "";
                    }

                    $last_touch_qa = $unit->unitTimes()->where(function ($query) {
                        $query->where('work_tag', UnitTime::WORK_QA);
                    })->orderBy('created_at', 'desc');

                    if ($last_touch_qa->count()) {
                        $report['last_touch_qa'] = $last_touch_qa->first()->employee->name;
                    } else {
                        $report['last_touch_qa'] = "";
                    }

                    $report['complexity'] = $unit->complexity;
                    $report['status'] = $unit->status;
                    $report['priority'] = $unit->priority;
                    $report['task_type'] = $unit->type;
                    $report['ad_type'] = $unit->ad_type;
                    $report['created_at'] = $unit->created_at->toDateTimeString();
                    $report['started_at'] = $unit->started_at->toDateTimeString();
                    $report['updated_at'] = $unit->updated_at->toDateTimeString();
                    $report['committed_at'] = $unit->committed_at->toDateTimeString();

                    if ($unit->closed_at) {
                        $report['closed_at'] = $unit->closed_at->toDateTimeString();
                    } else {
                        $report['closed_at'] = "";
                    }

                    $report['late'] = $unit->is_late;

                    if ($unit->closed_at) {
                        $report['tat'] = round($unit->tat, 2);
                    } else {
                        $report['tat'] = "";
                    }

                    $report['revision_rounds'] = $unit->queueNotes()->revisionRound()->count();
                    $report['client_issues'] = $unit->issues()->count();

                    $review_wait_time = $unit->unitTimes()->reviewWait()->get();
                    $report['review_wait_time'] = gmdate('H:i:s', $review_wait_time->sum('util'));

                    $review_info_req_time = $unit->unitTimes()->reviewInfoReq()->get();
                    $report['review_info_req_time'] = gmdate('H:i:s', $review_info_req_time->sum('util'));

                    $review_in_prog_time = $unit->unitTimes()->reviewInProg()->get();
                    $report['review_in_prog_time'] = gmdate('H:i:s', $review_in_prog_time->sum('util'));

                    $review_hold_time = $unit->unitTimes()->reviewHold()->get();
                    $report['review_hold_time'] = gmdate('H:i:s', $review_hold_time->sum('util'));

                    $dev_wait_time = $unit->unitTimes()->devWait()->get();
                    $report['dev_wait_time'] = gmdate('H:i:s', $dev_wait_time->sum('util'));

                    $dev_info_req_time = $unit->unitTimes()->devInfoReq()->get();
                    $report['dev_info_req_time'] = gmdate('H:i:s', $dev_info_req_time->sum('util'));

                    $dev_in_prog_time = $unit->unitTimes()->devInProg()->get();
                    $report['dev_in_prog_time'] = gmdate('H:i:s', $dev_in_prog_time->sum('util'));

                    $dev_hold_time = $unit->unitTimes()->devHold()->get();
                    $report['dev_hold_time'] = gmdate('H:i:s', $dev_hold_time->sum('util'));

                    $creative_wait_time = $unit->unitTimes()->creativeWait()->get();
                    $report['creative_wait_time'] = gmdate('H:i:s', $creative_wait_time->sum('util'));

                    $creative_info_req_time = $unit->unitTimes()->creativeInfoReq()->get();
                    $report['creative_info_req_time'] = gmdate('H:i:s', $creative_info_req_time->sum('util'));

                    $creative_in_prog_time = $unit->unitTimes()->creativeInProg()->get();
                    $report['creative_in_prog_time'] = gmdate('H:i:s', $creative_in_prog_time->sum('util'));

                    $creative_hold_time = $unit->unitTimes()->creativeHold()->get();
                    $report['creative_hold_time'] = gmdate('H:i:s', $creative_hold_time->sum('util'));

                    $qa_wait_time = $unit->unitTimes()->qaWait()->get();
                    $report['qa_wait_time'] = gmdate('H:i:s', $qa_wait_time->sum('util'));

                    $qa_info_req_time = $unit->unitTimes()->qaInfoReq()->get();
                    $report['qa_info_req_time'] = gmdate('H:i:s', $qa_info_req_time->sum('util'));

                    $qa_in_prog_time = $unit->unitTimes()->qaInProg()->get();
                    $report['qa_in_prog_time'] = gmdate('H:i:s', $qa_in_prog_time->sum('util'));

                    $qa_hold_time = $unit->unitTimes()->qaHold()->get();
                    $report['qa_hold_time'] = gmdate('H:i:s', $qa_hold_time->sum('util'));

                    $data[] = $report;
                }
            }
        } else {
            foreach ($units as $unit) {
                if ($unit->unitTimes->count()) {
                    foreach ($unit->unitTimes as $unit_time) {
                        $report = [];

                        $report['unit_name'] = $unit->name;
                        $report['campaign_name'] = $unit->campaign->name;
                        $report['team_name'] = $unit->campaign->team->name;
                        $report['partner_name'] = $unit->campaign->partner->name;
                        $report['sub_partner_name'] = $unit->campaign->sub_partner_name;
                        $report['advertiser'] = $unit->campaign->advertiser->name;
                        $report['client_contact_name'] = $unit->clientContact->name;
                        $report['employee_name'] = $unit_time->employee->name;
                        $report['complexity'] = $unit->complexity;
                        $report['status'] = $unit->status;
                        $report['priority'] = $unit->priority;
                        $report['task_type'] = $unit->type;
                        $report['ad_type'] = $unit->ad_type;
                        $report['unit_created_at'] = $unit->created_at->toDateTimeString();
                        $report['unit_started_at'] = $unit->started_at->toDateTimeString();
                        $report['unit_updated_at'] = $unit->updated_at->toDateTimeString();
                        $report['unit_committed_at'] = $unit->committed_at->toDateTimeString();

                        if ($unit->closed_at) {
                            $report['unit_closed_at'] = $unit->closed_at->toDateTimeString();
                        } else {
                            $report['unit_closed_at'] = "";
                        }

                        $report['unit_time_work'] = $unit_time->work;
                        $report['unit_time_action'] = $unit_time->action;

                        $review_wait_start_at = "";
                        $review_wait_end_at = "";
                        $review_info_req_start_at = "";
                        $review_info_req_end_at = "";
                        $review_in_prog_start_at = "";
                        $review_in_prog_end_at = "";
                        $review_hold_start_at = "";
                        $review_hold_end_at = "";
                        $dev_wait_start_at = "";
                        $dev_wait_end_at = "";
                        $dev_info_req_start_at = "";
                        $dev_info_req_end_at = "";
                        $dev_in_prog_start_at = "";
                        $dev_in_prog_end_at = "";
                        $dev_hold_start_at = "";
                        $dev_hold_end_at = "";
                        $creative_wait_start_at = "";
                        $creative_wait_end_at = "";
                        $creative_info_req_start_at = "";
                        $creative_info_req_end_at = "";
                        $creative_in_prog_start_at = "";
                        $creative_in_prog_end_at = "";
                        $creative_hold_start_at = "";
                        $creative_hold_end_at = "";
                        $qa_wait_start_at = "";
                        $qa_wait_end_at = "";
                        $qa_info_req_start_at = "";
                        $qa_info_req_end_at = "";
                        $qa_in_prog_start_at = "";
                        $qa_in_prog_end_at = "";
                        $qa_hold_start_at = "";
                        $qa_hold_end_at = "";

                        switch ($unit_time->work_tag) {
                            case UnitTime::WORK_REVIEW:
                                switch ($unit_time->action_tag) {
                                    case UnitTime::ACTION_WAIT:
                                        $review_wait_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $review_wait_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $review_wait_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_INFO_REQ:
                                        $review_info_req_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $review_info_req_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $review_info_req_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_IN_PROG:
                                        $review_in_prog_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $review_in_prog_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $review_in_prog_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_HOLD:
                                        $review_hold_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $review_hold_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $review_hold_end_at = "";
                                        }

                                        break;
                                }
                                break;
                            case UnitTime::WORK_DEV:
                                switch ($unit_time->action_tag) {
                                    case UnitTime::ACTION_WAIT:
                                        $dev_wait_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $dev_wait_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $dev_wait_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_INFO_REQ:
                                        $dev_info_req_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $dev_info_req_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $dev_info_req_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_IN_PROG:
                                        $dev_in_prog_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $dev_in_prog_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $dev_in_prog_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_HOLD:
                                        $dev_hold_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $dev_hold_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $dev_hold_end_at = "";
                                        }

                                        break;
                                }
                                break;
                            case UnitTime::WORK_CREATIVE:
                                switch ($unit_time->action_tag) {
                                    case UnitTime::ACTION_WAIT:
                                        $creative_wait_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $creative_wait_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $creative_wait_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_INFO_REQ:
                                        $creative_info_req_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $creative_info_req_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $creative_info_req_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_IN_PROG:
                                        $creative_in_prog_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $creative_in_prog_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $creative_in_prog_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_HOLD:
                                        $creative_hold_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $creative_hold_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $creative_hold_end_at = "";
                                        }

                                        break;
                                }
                                break;
                            case UnitTime::WORK_QA:
                                switch ($unit_time->action_tag) {
                                    case UnitTime::ACTION_WAIT:
                                        $qa_wait_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $qa_wait_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $qa_wait_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_INFO_REQ:
                                        $qa_info_req_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $qa_info_req_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $qa_info_req_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_IN_PROG:
                                        $qa_in_prog_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $qa_in_prog_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $qa_in_prog_end_at = "";
                                        }

                                        break;
                                    case UnitTime::ACTION_HOLD:
                                        $qa_hold_start_at = $unit_time->start_at->toDateTimeString();

                                        if ($unit_time->end_at) {
                                            $qa_hold_end_at = $unit_time->end_at->toDateTimeString();
                                        } else {
                                            $qa_hold_end_at = "";
                                        }

                                        break;
                                }
                                break;
                        }

                        $report['review_wait_start_at'] = $review_wait_start_at;
                        $report['review_wait_end_at'] = $review_wait_end_at;
                        $report['review_info_req_start_at'] = $review_info_req_start_at;
                        $report['review_info_req_end_at'] = $review_info_req_end_at;
                        $report['review_in_prog_start_at'] = $review_in_prog_start_at;
                        $report['review_in_prog_end_at'] = $review_in_prog_end_at;
                        $report['review_hold_start_at'] = $review_hold_start_at;
                        $report['review_hold_end_at'] = $review_hold_end_at;
                        $report['dev_wait_start_at'] = $dev_wait_start_at;
                        $report['dev_wait_end_at'] = $dev_wait_end_at;
                        $report['dev_info_req_start_at'] = $dev_info_req_start_at;
                        $report['dev_info_req_end_at'] = $dev_info_req_end_at;
                        $report['dev_in_prog_start_at'] = $dev_in_prog_start_at;
                        $report['dev_in_prog_end_at'] = $dev_in_prog_end_at;
                        $report['dev_hold_start_at'] = $dev_hold_start_at;
                        $report['dev_hold_end_at'] = $dev_hold_end_at;
                        $report['creative_wait_start_at'] = $creative_wait_start_at;
                        $report['creative_wait_end_at'] = $creative_wait_end_at;
                        $report['creative_info_req_start_at'] = $creative_info_req_start_at;
                        $report['creative_info_req_end_at'] = $creative_info_req_end_at;
                        $report['creative_in_prog_start_at'] = $creative_in_prog_start_at;
                        $report['creative_in_prog_end_at'] = $creative_in_prog_end_at;
                        $report['creative_hold_start_at'] = $creative_hold_start_at;
                        $report['creative_hold_end_at'] = $creative_hold_end_at;
                        $report['qa_wait_start_at'] = $qa_wait_start_at;
                        $report['qa_wait_end_at'] = $qa_wait_end_at;
                        $report['qa_info_req_start_at'] = $qa_info_req_start_at;
                        $report['qa_info_req_end_at'] = $qa_info_req_end_at;
                        $report['qa_in_prog_start_at'] = $qa_in_prog_start_at;
                        $report['qa_in_prog_end_at'] = $qa_in_prog_end_at;
                        $report['qa_hold_start_at'] = $qa_hold_start_at;
                        $report['qa_hold_end_at'] = $qa_hold_end_at;

                        $data[] = $report;
                    }
                }
            }
        }

        if ($request->consolidated) {
            $filename = "consolidated_report_".Carbon::now()->format('Y-m-d_H-i-s');
        } else {
            $filename = "granular_report_".Carbon::now()->format('Y-m-d_H-i-s');
        }

        Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('New Sheet', function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xls');
    }
}
