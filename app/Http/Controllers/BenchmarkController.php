<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use DB;


use App\Models\Benchmark;
use App\Models\Team;

class BenchmarkController extends Controller{

    public function __construct()
    {
        $this->middleware('auth', ['except' => ['email']]);
    }

    public function index()
    {
        $benchmark = Benchmark::all();
        $team = Team::all();
        $data = [
            'benchmark' => $benchmark,
            'team' => $team
        ];
        return view('benchmark.index', $data);
    }

 /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        
        return view('benchmark.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $benchmark = new Benchmark();
        $benchmark->team_id = '1';
        $benchmark->complexity = $request->complexity;
        $benchmark->benchmark = $request->benchmark;
        $benchmark->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, TeamRepository $team_repo)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }


}
