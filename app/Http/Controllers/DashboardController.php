<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\TicketRepository;
use Auth;
use App\Repositories\DashboardRepository;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DashboardRepository $dashboard_repo)
    {
        $volume = $dashboard_repo->getVolume();
        $utilization = $dashboard_repo->getUtilization();
        $quality = $dashboard_repo->getQuality();
        $efficiency = $dashboard_repo->getEfficiency();

        $data = [
            'volume' => $volume,
            'utilization' => $utilization,
            'quality' => $quality,
            'efficiency' => $efficiency
        ];

        return view('dashboards.main', $data);
    }

    /**
     * [lineOfBusiness description]
     * @return [type] [description]
     */
    public function lineOfBusiness()
    {
        return view('dashboards.line-of-business');
    }

    /**
     * [termsAndAccounts description]
     * @return [type] [description]
     */
    public function termsAndAccounts()
    {
        return view('dashboards.terms-and-accounts');
    }
}
