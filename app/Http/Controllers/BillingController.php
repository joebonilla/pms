<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Excel;

use App\Http\Controllers\Controller;
use App\Http\Requests;

use App\Models\Partner;
use App\Models\Unit;

class BillingController extends Controller
{
    public function invoice(Request $request)
    {
        $partner = Partner::find($request->partner_id);
        $client_contacts = $partner->clientContacts;
        $closed_from = Carbon::parse($request->closed_from)->startOfDay();
        $closed_to = Carbon::parse($request->closed_to)->endOfDay();

        $billings = [];

        foreach ($client_contacts as $client_contact) {
            $client_contact_name = $client_contact->name;

            $units = $client_contact->units()
                ->status(Unit::STATUS_CLOSED)
                ->closedFrom($closed_from)
                ->closedTo($closed_to)
                ->get()->toArray();

            $billing = [
                'client_contact' => $client_contact_name,
                'units' => $units
            ];

            $billings[] = $billing;
        }

        $coverage_dates = [
            'coverage_from' => $request->closed_from,
            'coverage_to' => $request->closed_to
        ];

        $data = [
            'partner_name' => $partner->name,
            'coverage_dates' => $coverage_dates,
            'billings' => $billings
        ];

        $filename = "billing_report_".Carbon::now()->format('Y-m-d_H-i-s');

        Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('New sheet', function($sheet) use ($data) {
                $sheet->loadView('billings.invoice', $data);
            });
        })->download('xls');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::all();
        $closed_dates = [
            'closed_from' => Carbon::now()->subMonth()->format('m/d/Y'),
            'closed_to' => Carbon::now()->format('m/d/Y')
        ];

        $data = [
            'partners' => $partners,
            'closed_dates' => $closed_dates
        ];

        return view('billings.index', $data);
    }
}
