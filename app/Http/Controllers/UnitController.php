<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use Auth;
use Mail;

// Models for writing
use App\Models\Campaign;
use App\Models\ClientContact;
use App\Models\DevNote;
use App\Models\Employee;
use App\Models\Ticket;
use App\Models\Unit;
use App\Models\UnitIssue;
use App\Models\UnitQueueNote;
use App\Models\UnitRevision;
use App\Models\UnitTime;
use App\Models\UnitAudit;

// Repositories for reading
use App\Repositories\UnitAuditRepository;
use App\Repositories\UnitIssueRepository;
use App\Repositories\UnitQueueNoteRepository;
use App\Repositories\UnitRepository;
use App\Repositories\UnitTimeRepository;

/**
 *
 */
class UnitController extends Controller
{
    protected $unit;
    protected $time;
    protected $queue_note;

    public $complexities;
    public $priorities;
    public $types;
    public $ad_types;

    public function __construct(UnitRepository $unit, UnitTimeRepository $time, UnitIssueRepository $issue, UnitQueueNoteRepository $queue_note)
    {
        $this->middleware('auth');

        $this->unit = $unit;
        $this->time = $time;
        $this->issue = $issue;
        $this->queue_note = $queue_note;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if (!$request->campaign) {
            return redirect('/campaign');
        }

        $campaign = Campaign::find($request->campaign);
        $partner = $campaign->partner;

        $data = [
            'campaign' => $campaign,
            'partner' => $partner
        ];

        return view('units.create', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, UnitRepository $unit_repo)
    {
        $unit = Unit::find($id);
        $client_contacts = $unit->partner->clientContacts;
        $complexities = $unit_repo->unitComplexities();
        $priorities = $unit_repo->unitPriorities();
        $task_types = $unit_repo->unitTaskTypes();
        $ad_types = $unit_repo->unitAdTypes();
        $qa_complexities = $unit_repo->unitQaComplexities();

        $data = [
            'unit' => $unit,
            'client_contacts' => $client_contacts,
            'complexities' => $complexities,
            'priorities' => $priorities,
            'task_types' => $task_types,
            'ad_types' => $ad_types,
            'qa_complexities' => $qa_complexities
        ];

        return view('units.edit', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(
        $id,
        UnitTimeRepository $time_repo,
        UnitQueueNoteRepository $queue_note_repo,
        UnitIssueRepository $issue_repo,
        UnitAuditRepository $audit_repo
    )
    {
        $employee = Auth::user();

        $unit = Unit::find($id);
        $unit_times = $time_repo->getUnitTimes($unit);
        $queue_notes = $queue_note_repo->getLatestQueueNotes($unit);
        $unit_issues = $issue_repo->getLatestUnitIssues($unit);
        $team_members = $unit->campaign->team->members;
        $unit_audit = $unit->audits->last();
        $unit_audit_notes = $unit->audits()->orderBy('created_at', 'desc')->get()->take(10);
        $audit_flags = $audit_repo->auditFlags();
        $phase = $unit->queueNotes()->count() ? $unit->queueNotes()->orderBy('created_at', 'desc')->first()->queue_tag : 0;

        $data = [
            'employee' => $employee,
            'unit' => $unit,
            'unit_times' => $unit_times,
            'queue_notes' => $queue_notes,
            'unit_issues' => $unit_issues,
            'team_members' => $team_members,
            'unit_audit' => $unit_audit,
            'phase' => $phase,
            'unit_audit_notes' => $unit_audit_notes,
            'audit_flags' => $audit_flags
        ];

        return view('units.show', $data);
    }

    public function store(Request $request)
    {

        $campaign = Campaign::find($request->campaign_id);
        $team = $campaign->team;
        $partner = $campaign->partner;
        $advertiser = $campaign->advertiser;

        // Create units
        foreach ($request->units as $units) {
            $quantity = $units['quantity'];

            $client_contact = ClientContact::where('name', $units['client_contact_name'])->first();

            if (!$client_contact) {
                $client_contact = new ClientContact();
                $client_contact->name = $units['client_contact_name'];
                $client_contact->partner_id = $partner->id;
                $client_contact->created_at = Carbon::now();
                $client_contact->save();
            }

            for ($ctr = 1; $ctr <= $quantity; $ctr++) {
                $unit = new Unit();

                if ($units['name']) {
                    if ($quantity > 1) {
                        $unit->name = $ctr . ' - ' . $units['name'];
                    } else {
                        $unit->name = $units['name'];
                    }
                }
                $unit->team_id = $team->id;
                $unit->partner_id = $partner->id;
                $unit->advertiser_id = $advertiser->id;
                $unit->campaign_id = $campaign->id;
                $unit->client_contact_id = $client_contact->id;
                $unit->committed_at = Carbon::parse($units['committed_at']);
                $unit->complexity = $units['complexity'];
                $unit->qa_complexity = $units['qa_complexity'];
                $unit->priority_tag = $units['priority'];
                $unit->type_tag = $units['type'];
                $unit->ad_type_tag = $units['ad_type'];
                $unit->billable = (array_key_exists('billable', $units)) ? true : false;
                $unit->status_tag = Unit::STATUS_NEW;
                $unit->employee_id = null;

                $unit->save();
            }
        }

        // re-open campaign for every new unit
        $campaign = Campaign::find($request->campaign_id);
        $campaign->status_tag = Campaign::STATUS_OPEN;
        $campaign->save();

        $ticket = Ticket::find($campaign->ticket_id);
        $ticket->status_tag = Ticket::STATUS_OPEN;
        $ticket->save();

        return redirect('/campaign/' . $request->campaign_id);
    }

    public function massUpdate(Request $request)
    {
        $unit_action = $request->unit_action;
        $unit_queue = $request->unit_queue;
        $mass_units = $request->mass_units;
        $employee = $request->user();

        // Get unit work type
        if (!$request->work) {
            $work_tag = ($employee->ic_tag < Employee::IC_POC) ? $employee->ic_tag : UnitTime::WORK_REVIEW;
        } else {
            $work_tag = $request->work;
        }

        $last_unit_time = UnitTime::whereIn('unit_id', $mass_units)
            ->whereNull('end_at')
            ->orderBy('start_at', 'desc')
            ->first();

        if ($last_unit_time) {
            $time_divider = count($mass_units);
            $diff_in_seconds = $last_unit_time->start_at->diffInSeconds(Carbon::now());
            $partial_time = $diff_in_seconds / $time_divider;
        }

        if ($request->unit_action) {
            foreach ($mass_units as $mass_unit) {
                $unit = Unit::find($mass_unit);
                $campaign = $unit->campaign;
                $team = $campaign->team;
                $partner = $campaign->partner;

                if ($request->unit_action_tag != Unit::STATUS_UNASSIGNED) {
                    if (!$unit->unitTimes->count()) {
                        // Set unit initial start
                        $unit->started_at = Carbon::now();
                    } else {
                        // End last unit time
                        $last_unit_time = UnitTime::unendedUnitTimes($unit)->get()->last();

                        if ($last_unit_time) {
                            $last_unit_time->end_at = $last_unit_time->start_at->addSeconds($partial_time);
                            $last_unit_time->save();
                        }
                    }

                    // Create unit time
                    $unit_time = new UnitTime;
                    $unit_time->unit_id = $unit->id;
                    $unit_time->campaign_id = $campaign->id;
                    $unit_time->partner_id = $partner->id;
                    $unit_time->team_id = $team->id;
                    $unit_time->employee_id = $employee->id;

                    $unit_time->start_at = Carbon::now();

                    if ($request->unit_action_tag == Unit::STATUS_WAITING) {
                        $unit->status_tag = Unit::STATUS_WAITING;
                        $unit->employee_id = $employee->id;
                        $unit_time->action_tag = UnitTime::ACTION_WAIT;
                        $unit_time->work_tag = $work_tag;
                    } else if ($request->unit_action_tag == Unit::STATUS_INFO_REQ) {
                        $unit->status_tag = Unit::STATUS_INFO_REQ;
                        $unit->employee_id = $employee->id;
                        $unit_time->action_tag = UnitTime::ACTION_INFO_REQ;
                        $unit_time->work_tag = $work_tag;
                    } else if ($request->unit_action_tag == Unit::STATUS_IN_PROG) {
                        $unit->status_tag = Unit::STATUS_IN_PROG;
                        $unit->employee_id = $employee->id;
                        $unit_time->action_tag = UnitTime::ACTION_IN_PROG;
                        $unit_time->work_tag = $work_tag;
                    } else if ($request->unit_action_tag == Unit::STATUS_HOLD) {
                        $unit->status_tag = Unit::STATUS_HOLD;
                        $unit->employee_id = $employee->id;
                        $unit_time->action_tag = UnitTime::ACTION_HOLD;
                        $unit_time->work_tag = $work_tag;
                    }

                    $unit_time->save();
                } else {
                    if ($employee->role_tag > Employee::ROLE_IC || $employee->ic_tag > Employee::IC_QA || $unit->assignee->id == $employee->id) {
                        $unit->status_tag = Unit::STATUS_UNASSIGNED;
                        $unit->employee_id = null;

                        // End last unit time
                        $last_unit_time = UnitTime::unendedUnitTimes($unit)->get()->last();

                        if ($last_unit_time) {
                            $last_unit_time->end_at = $last_unit_time->start_at->addSeconds($partial_time);
                            $last_unit_time->save();
                        }
                    }
                }

                $unit->save();
            }
        } else if ($request->unit_queue) {
            foreach ($mass_units as $mass_unit) {
                $unit = Unit::find($mass_unit);
                $campaign = $unit->campaign;
                $team = $campaign->team;
                $partner = $campaign->partner;

                if (!$unit->unitTimes->count()) {
                    // Set unit initial start
                    $unit->started_at = Carbon::now();
                }

                $unit_queue_note = new UnitQueueNote;
                $unit_queue_note->unit_id = $unit->id;
                $unit_queue_note->employee_id = $employee->id;

                // End last unit time
                $last_unit_time = UnitTime::unendedUnitTimes($unit)->get()->last();

                if ($last_unit_time) {
                    $last_unit_time->end_at = $last_unit_time->start_at->addSeconds($partial_time);
                    $last_unit_time->save();
                }

                $unit_queue_note->queue_tag = $request->unit_queue_tag;

                if ($request->unit_queue_tag == UnitQueueNote::QUEUE_REVISION) {
                    $last_touch_dev = $unit->unitTimes()->where(function ($query) {
                        $query->where('work_tag', UnitTime::WORK_DEV);
                    })->orderBy('created_at', 'desc');

                    if ($last_touch_dev->count()) {
                        $employee_dev_id = $last_touch_dev->first()->employee->id;
                    } else {
                        $employee_dev_id = null;
                    }

                    if ($request->revision_count) {
                        $unit_revision = new UnitRevision;
                        $unit_revision->unit_id = $unit->id;
                        $unit_revision->revision_count = $request->revision_count;
                        $unit_revision->employee_dev_id = $employee_dev_id;
                        $unit_revision->employee_qa_id = $employee->id;
                        $unit_revision->save();
                    }
                } else if ($request->unit_queue_tag == UnitQueueNote::QUEUE_NO_REVISION) {
                    $unit->status_tag = Unit::STATUS_SOLVED;
                    $unit->completed_at = Carbon::now();
                    $unit->save();
                } else if ($request->unit_queue_tag == UnitQueueNote::QUEUE_CLOSE) {
                    $unit->status_tag = Unit::STATUS_CLOSED;
                    $unit->closed_at = Carbon::now();
                    $unit->save();

                    $closed_units = $unit->campaign->units;
                    $are_all_closed = true;

                    foreach ($closed_units as $closed_unit) {
                        if ($closed_unit->status_tag != Unit::STATUS_CLOSED) {
                            $are_all_closed = false;
                        }
                    }

                    // close the campaign if all units are closed
                    if ($are_all_closed) {
                        $unit->campaign->status_tag = Campaign::STATUS_CLOSED;
                        $unit->campaign->closed_at = Carbon::now();
                        $unit->campaign->save();

                        $unit->campaign->ticket->status_tag == Ticket::STATUS_CLOSED;
                        $unit->campaign->ticket->closed_at = Carbon::now();
                        $unit->campaign->ticket->save();
                    }
                } else {
                    $unit->status_tag = Unit::STATUS_UNASSIGNED;
                }

                $unit_queue_note->save();

                $unit->current_queue_tag = $request->unit_queue_tag;
                $unit->employee_id = null;
                $unit->save();
            }
        } else if ($request->unit_assign) {
            foreach ($mass_units as $mass_unit) {
                $unit = Unit::find($mass_unit);
                $campaign = $unit->campaign;
                $team = $campaign->team;
                $partner = $campaign->partner;

                if (!$unit->unitTimes->count()) {
                    // Set unit initial start
                    $unit->started_at = Carbon::now();
                } else {
                    // End last unit time
                    $last_unit_time = UnitTime::unendedUnitTimes($unit)->get()->last();

                    if ($last_unit_time) {
                        $last_unit_time->end_at = $last_unit_time->start_at->addSeconds($partial_time);
                        $last_unit_time->save();
                    }
                }

                // Find employee
                $employee = Employee::find($request->unit_assignee);

                // Update unit to waiting
                $unit->status_tag = Unit::STATUS_WAITING;
                $unit->employee_id = $employee->id;

                // Create unit time
                $unit_time = new UnitTime;
                $unit_time->unit_id = $unit->id;
                $unit_time->campaign_id = $campaign->id;
                $unit_time->partner_id = $partner->id;
                $unit_time->team_id = $team->id;
                $unit_time->employee_id = $employee->id;
                $unit_time->work_tag = $employee->ic_tag;
                $unit_time->start_at = Carbon::now();
                $unit_time->action_tag = UnitTime::ACTION_WAIT;

                $unit_time->save();
                $unit->save();

                Mail::queue('emails.assign', ['unit' => $unit], function ($message) use ($employee) {
                    $message->from('prod3@wideout.com', 'PMS Wideout');
                    $message->to($employee->email, $employee->name)
                        ->subject('A unit has been assigned to you!');
                });
            }
        }

        return redirect('/campaign/' . $request->campaign_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Get relationship objects
        $unit = Unit::find($id);
        $campaign = $unit->campaign;
        $team = $campaign->team;
        $partner = $campaign->partner;
        $employee = $unit->assignee == null ? $request->user() : $unit->assignee;

        if ($request->edit) {
            $unit->name = $request->unit_name;
            $unit->complexity = $request->complexity;
            $unit->priority_tag = $request->priority;
            $unit->type_tag = $request->unit_type;
            $unit->ad_type_tag = $request->ad_type;
            $unit->preview_link = $request->preview_link;
            $unit->qa_report_link = $request->qa_report_link;
            $unit->creative_brief = $request->creative_brief;
            $unit->billable = ($request->billable) ? true : false;
            $unit->committed_at = Carbon::parse($request->committed_at);
            $unit->save();
        }

        if ($request->action) {
            if ($request->action != Unit::STATUS_UNASSIGNED) {
                // Get unit work type
                if (!$request->work) {
                    $work_tag = ($employee->ic_tag < Employee::IC_POC) ? $employee->ic_tag : UnitTime::WORK_REVIEW;
                } else {
                    $work_tag = $request->work;
                }

                if (!$unit->unitTimes->count()) {
                    // Set unit initial start
                    $unit->started_at = Carbon::now();
                } else {
                    if ($request->action != Unit::STATUS_WAITING) {
                        // End last unit time
                        $last_unit_time = UnitTime::unendedUnitTimes($unit)->get()->last();
                        $last_unit_time->end_at = Carbon::now();
                        $last_unit_time->save();
                    }
                }

                // Create unit time
                $unit_time = new UnitTime;
                $unit_time->unit_id = $unit->id;
                $unit_time->campaign_id = $campaign->id;
                $unit_time->partner_id = $partner->id;
                $unit_time->team_id = $team->id;
                $unit_time->employee_id = $employee->id;

                $unit_time->start_at = Carbon::now();

                if ($request->action == Unit::STATUS_WAITING) {
                    $unit->status_tag = Unit::STATUS_WAITING;
                    $unit->employee_id = $employee->id;
                    $unit_time->action_tag = UnitTime::ACTION_WAIT;
                    $unit_time->work_tag = $work_tag;
                } else if ($request->action == Unit::STATUS_INFO_REQ) {
                    $unit->status_tag = Unit::STATUS_INFO_REQ;
                    $unit_time->action_tag = UnitTime::ACTION_INFO_REQ;
                    $unit_time->work_tag = $work_tag;
                } else if ($request->action == Unit::STATUS_IN_PROG) {
                    $unit->status_tag = Unit::STATUS_IN_PROG;
                    $unit_time->action_tag = UnitTime::ACTION_IN_PROG;
                    $unit_time->work_tag = $work_tag;
                } else if ($request->action == Unit::STATUS_HOLD) {
                    $unit->status_tag = Unit::STATUS_HOLD;
                    $unit_time->action_tag = UnitTime::ACTION_HOLD;
                    $unit_time->work_tag = $work_tag;
                }

                $unit_time->save();
            } else {
                if (
                    $employee->role_tag > Employee::ROLE_IC ||
                    $employee->ic_tag > Employee::IC_QA ||
                    $unit->assignee->id == $employee->id
                ) {
                    $unit->status_tag = Unit::STATUS_UNASSIGNED;
                    $unit->employee_id = null;

                    // End last unit time
                    $last_unit_time = UnitTime::unendedUnitTimes($unit)->get()->last();
                    $last_unit_time->end_at = Carbon::now();
                    $last_unit_time->save();
                }
            }

            $unit->save();
        }

        if ($request->assign) {
            if (!$unit->unitTimes->count()) {
                // Set unit initial start
                $unit->started_at = Carbon::now();
            } else {
                // End last unit time
                $last_unit_time = UnitTime::unendedUnitTimes($unit)->get()->last();

                if ($last_unit_time) {
                    $last_unit_time->end_at = Carbon::now();
                    $last_unit_time->save();
                }
            }

            // Find employee
            $employee = Employee::find($request->assignee);

            // Update unit to waiting
            $unit->status_tag = Unit::STATUS_WAITING;
            $unit->employee_id = $employee->id;

            // Create unit time
            $unit_time = new UnitTime;
            $unit_time->unit_id = $unit->id;
            $unit_time->campaign_id = $campaign->id;
            $unit_time->partner_id = $partner->id;
            $unit_time->team_id = $team->id;
            $unit_time->employee_id = $employee->id;
            $unit_time->work_tag = $employee->ic_tag;
            $unit_time->start_at = Carbon::now();
            $unit_time->action_tag = UnitTime::ACTION_WAIT;

            $unit_time->save();
            $unit->save();

            Mail::queue('emails.assign', ['unit' => $unit], function ($message) use ($employee) {
                $message->from('prod3@wideout.com', 'PMS Wideout');
                $message->to($employee->email, $employee->name)
                    ->subject('A unit has been assigned to you!');
            });
        }
//QUEUE NOTE

        if ($request->queue_note) {
            if (!$request->note && !$request->queue) {
                return redirect('/unit/' . $unit->id);
            }

            if (!$unit->unitTimes->count()) {
                // Set unit initial start
                $unit->started_at = Carbon::now();
            }

            $unit_queue_note = new UnitQueueNote;
            $unit_queue_note->unit_id = $unit->id;
            $unit_queue_note->employee_id = $request->user()->id;

            if ($request->note) {
                $unit_queue_note->note = $request->note;
            } else {
                $unit_queue_note->note = null;
            }

            if ($request->queue) {
                // End last unit time
                $last_unit_time = UnitTime::unendedUnitTimes($unit)->get()->last();

                if ($last_unit_time) {
                    $last_unit_time->end_at = Carbon::now();
                    $last_unit_time->save();
                }

                $unit_queue_note->queue_tag = $request->queue;

                if ($request->queue == UnitQueueNote::QUEUE_REVISION) {
                    $last_touch_dev = $unit->unitTimes()->where(function ($query) {
                        $query->where('work_tag', UnitTime::WORK_DEV);
                    })->orderBy('created_at', 'desc');

                    if ($last_touch_dev->count()) {
                        $employee_dev_id = $last_touch_dev->first()->employee->id;
                    } else {
                        $employee_dev_id = null;
                    }

                    if ($request->revision_count) {
                        $unit_revision = new UnitRevision;
                        $unit_revision->unit_id = $unit->id;
                        $unit_revision->revision_count = $request->revision_count;
                        $unit_revision->employee_dev_id = $employee_dev_id;
                        $unit_revision->employee_qa_id = $employee->id;
                        $unit_revision->save();
                    }
                } else if ($request->queue == UnitQueueNote::QUEUE_NO_REVISION) {
                    $unit->status_tag = Unit::STATUS_SOLVED;
                    $unit->completed_at = Carbon::now();
                    $unit->save();
                } else if ($request->queue == UnitQueueNote::QUEUE_CLOSE) {
                    $unit->status_tag = Unit::STATUS_CLOSED;
                    $unit->closed_at = Carbon::now();
                    $unit->save();

                    $units = $unit->campaign->units;
                    $are_all_closed = true;

                    foreach ($units as $unit) {
                        if ($unit->status_tag != Unit::STATUS_CLOSED) {
                            $are_all_closed = false;
                        }
                    }

                    // close the campaign if all units are closed
                    if ($are_all_closed) {
                        $unit->campaign->status_tag = Campaign::STATUS_CLOSED;
                        $unit->campaign->closed_at = Carbon::now();
                        $unit->campaign->save();

                        $unit->campaign->ticket->status_tag == Ticket::STATUS_CLOSED;
                        $unit->campaign->ticket->closed_at = Carbon::now();
                        $unit->campaign->ticket->save();
                    }
                } else {
                    $unit->status_tag = Unit::STATUS_UNASSIGNED;
                }

                $unit->current_queue_tag = $request->queue;
                $unit->employee_id = null;
                $unit->save();
            } else {
                $unit_queue_note->queue_tag = $unit->queueNotes()->count() ? $unit->queueNotes()->orderBy('created_at', 'desc')->first()->queue_tag : 0;
            }

            $unit_queue_note->save();
        }

        if ($request->creative) {
            $unit->creative_brief = $request->creative;
            $unit->save();
        }

        if ($request->preview) {
            $unit->preview_link = $request->preview;
            $unit->save();
        }

        if ($request->qa_report) {
            $unit->qa_report_link = $request->qa_report;
            $unit->save();
        }

        $redirect = redirect('/unit/' . $id);

        return $redirect;
    }

    public function issue(Request $request, $id)
    {
        $unit = Unit::find($id);

        $unit_issue = new UnitIssue;
        $unit_issue->name = $request->issue_name;
        $unit_issue->unit_id = $unit->id;
        $unit_issue->employee_id = $request->user()->id;
        $unit_issue->flag_tag = $request->issue_flag;
        $unit_issue->note = $request->issue_note;

        $unit_issue->save();

        return redirect('/unit/' . $unit->id);
    }

    public function audit(Request $request, $id)
    {
        $unit = Unit::find($id);

        $unit_audit = new UnitAudit;
        $unit_audit->unit_id = $unit->id;
        $unit_audit->employee_id = $request->user()->id;
        $unit_audit->work_estimate_tag = $request->work_estimate_tag;
        $unit_audit->work_estimate_notes = trim($request->work_estimate_notes);
        $unit_audit->overall_quality_tag = $request->overall_quality_tag;
        $unit_audit->overall_quality_notes = trim($request->overall_quality_notes);
        $unit_audit->following_instructions_tag = $request->following_instructions_tag;
        $unit_audit->following_instructions_notes = trim($request->following_instructions_notes);
        $unit_audit->process_adherence_tag = $request->process_adherence_tag;
        $unit_audit->process_adherence_notes = trim($request->process_adherence_notes);

        $unit_audit->save();

        return redirect('/unit/' . $unit->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $unit = Unit::find($id);
        $unit->delete();

        if ($unit->unitTimes->count()) {
            foreach ($unit->unitTimes as $ut => $unit_time) {
                $unit_time->delete();
            }
        }

        return redirect('/campaign/' . $unit->campaign->id);
    }
}
