<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Employee;
use Validator;
use Socialite;
use Image;
use Auth;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:employees',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return Employee::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin()
    {
        return view('layouts.login');
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Google.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $oauth = Socialite::driver('google')->user();

        $employee = Employee::where('email', $oauth->getEmail())->first();

        if ($employee) {
            if (!$employee->avatar_file) {
                $path = 'img/employees';

                File::exists(public_path($path)) or File::makeDirectory(public_path($path));

                $img = Image::make($oauth->getAvatar());
                $img->resize(128, 128);
                $img->save(public_path($path).'/'.$employee->id.'.jpg');

                $employee->avatar_file = $employee->id.'.jpg';
            }

            if (!$employee->nickname) {
                if ($oauth->getNickname()) {
                    $employee->nickname = $oauth->getNickname();
                } else {
                    $nickname = explode(" ", trim($oauth->getName()));
                    $nickname = $nickname[0];

                    $employee->nickname = $nickname;
                }
            }

            $employee->save();

            Auth::login($employee);

            return redirect('/');
        } else {
            return redirect('/');
        }
    }
}
