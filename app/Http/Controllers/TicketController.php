<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;
use Validator;
use Auth;

// Models for writing
use App\Models\Employee;
use App\Models\Campaign;
use App\Models\UnitTime;
use App\Models\Account;
use App\Models\Ticket;
use App\Models\Team;
use App\Models\Unit;

// Repositories for reading
use App\Repositories\CampaignClientRepository;
use App\Repositories\UnitQueueRepository;
use App\Repositories\UnitTimeRepository;
use App\Repositories\AccountRepository;
use App\Repositories\TicketRepository;
use App\Repositories\UnitRepository;

/**
 *
 */
class TicketController extends Controller
{
    /**
     * [__construct description]
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['email']]);
    }

    public function email(Request $request)
    {
        var_dump($request);
        exit();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, TicketRepository $ticket_repo)
    {
        $search = $request->search;

        $tickets = $ticket_repo->getAllTickets($search);

        $data = [
            'tickets' => $tickets
        ];

        return view('tickets.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, AccountRepository $account, CampaignClientRepository $campaign_client)
    {
        $team = $request->user()->team;

        if ($team) {
            $accounts = $account->getTeamAccounts($team);
        } else {
            $accounts = $account->getAllAccounts();
        }

        $campaign_clients = $campaign_client->getAllCampaignClients();

        $data = [
            'accounts' => $accounts,
            'campaign_clients' => $campaign_clients
        ];

        return view('tickets.create', $data);
    }

    /**
     * [store description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, TicketRepository $ticket_repo)
    {
        $ticket = $ticket_repo->getTicket($id);
        $units = $ticket->campaign->units;

        $units_array = [];

        foreach ($units as $unit) {
            $units_array[] = $unit;
        }

        $chunks = array_chunk($units_array, (int) ceil(count($units_array) / 3));

        // var_dump($ticket);
        // exit();

        $data = [
            'ticket' => $ticket,
            'chunks' => $chunks
        ];

        return view('tickets.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket = Ticket::find($id);

        $data = [
            'ticket' => $ticket
        ];

        return view('tickets.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ticket = Ticket::find($id);
        $user_id = $request->user()->id;
        $ic_type = $request->user()->ic_type;
        $work_type = $this->ticket_time->getWorkType($ic_type);

        if ($request->grab) {
            $last_ticket_time = $this->ticket_time->lastTicketTime($ticket);

            $ticket = $this->ticket->changeAssignedAndStatus(
                $ticket,
                $user_id,
                Ticket::STATUS_WAITING
            );

            $ticket_time = $this->ticket_time->something(
                $user_id,
                TicketTime::getIcWorkType($ic_type),
                TicketTime::ACTION_TYPE_WAIT,
                Carbon::parse($last_ticket_time->end_at)
            );

            $ticket->times()->save($ticket_time);



            $ticket_time = new TicketTime();
            $ticket_time->employee_id = Auth::user()->id;

            $ticket_time->work_type = TicketTime::getIcWorkType(Auth::user()->ic_type);

            $ticket_time->action_type = TicketTime::ACTION_TYPE_WAIT;
            $ticket_time->start_at = Carbon::parse($last_ticket_time->end_at);
            $ticket->times()->save($ticket_time);
        }

        if ($request->drop) {
            $ticket->assigned_to = null;
            $ticket->status = Ticket::STATUS_UNASSIGNED;

            $ticket_time = TicketTime::where('ticket_id', $ticket->id)
                            ->where('employee_id', Auth::user()->id)
                            ->where('end_at', null)
                            ->orderBy('created_at', 'desc')
                            ->first();

            $ticket_time->end_at = Carbon::now();
            $ticket->times()->save($ticket_time);
        }

        if ($request->status) {
            $ticket->status = $request->status;

            $last_ticket_time = TicketTime::where('ticket_id', $ticket->id)
                                ->where('employee_id', Auth::user()->id)
                                ->where('end_at', null)
                                ->orderBy('created_at', 'desc')
                                ->first();

            $last_ticket_time->end_at = Carbon::now();
            $ticket->times()->save($last_ticket_time);

            $ticket_time = new TicketTime();
            $ticket_time->employee_id = Auth::user()->id;

            if ($request->work_type) {
                $ticket_time->work_type = $request->work_type;
            } else {
                if (Auth::user()->ic_type == Employee::IC_TYPE_DEV) {
                    $ticket_time->work_type = TicketTime::WORK_TYPE_DEV;
                } else if (Auth::user()->ic_type == Employee::IC_TYPE_QA) {
                    $ticket_time->work_type = TicketTime::WORK_TYPE_QA;
                } else {
                    $ticket_time->work_type = TicketTime::WORK_TYPE_CREATIVE;
                }
            }

            // temporary, magkaparehas naman kasi
            $ticket_time->action_type = $request->status;
            $ticket_time->start_at = Carbon::now();
            $ticket->times()->save($ticket_time);
        }

        if ($request->assign) {
            $employee = Employee::find($request->assigned_to);

            $ticket->assigned_to = $employee->id;
            $ticket->status = Ticket::STATUS_WAITING;

            $ticket_time = new TicketTime();
            $ticket_time->employee_id = $employee->id;

            if ($employee->ic_type == 1) {
                $ticket_time->work_type = TicketTime::WORK_TYPE_CREATIVE;
            } else if ($employee->ic_type == 2) {
                $ticket_time->work_type = TicketTime::WORK_TYPE_QA;
            } else if ($employee->ic_type == 3) {
                $ticket_time->work_type = TicketTime::WORK_TYPE_DEV;
            } else {
                $ticket_time->work_type = TicketTime::WORK_TYPE_REVIEW;
            }

            $ticket_time->action_type = TicketTime::ACTION_TYPE_WAIT;
            $ticket_time->start_at    = Carbon::now();
            $ticket->times()->save($ticket_time);
        }

        if ($request->queue) {
            $ticket->assigned_to = null;
            $ticket->status = Ticket::STATUS_UNASSIGNED;

            $ticket_time = TicketTime::where('ticket_id', $ticket->id)
                            ->where('end_at', null)
                            ->orderBy('created_at', 'desc')
                            ->first();

            $ticket_time->end_at = Carbon::now();
            $ticket->times()->save($ticket_time);

            if ($request->label != TicketQueue::NOT_LABEL_DROP) {
                $ticket_queue = new TicketQueue();
                // $ticket_queue->from = ewan;
                // $ticket_queue->to = ewan;
                $ticket_queue->label = $request->label;
                $ticket->queues()->save($ticket_queue);
            }
        }

        $ticket->save();

        return redirect('/ticket/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
