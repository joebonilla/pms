<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Ticket;
use App\Models\InHouse;

class InHouseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inhouses = InHouse::paginate(10);

        $data = [
            'inhouses' => $inhouses
        ];

        return view('in_houses.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('in_houses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Create Ticket
        $ticket = new Ticket();
        $ticket->employee_id = $request->user()->id;
        $ticket->type_tag = Ticket::TYPE_INTERNAL;
        $ticket->status_tag = Ticket::STATUS_OPEN;
        $ticket->save();

        $inhouse = new InHouse;
        $inhouse->ticket_id = $ticket->id;
        $inhouse->title = $request->title;
        $inhouse->description = $request->description;
        $inhouse->type_tag = $request->type;
        $inhouse->purpose_tag = $request->purpose;
        $inhouse->status_tag = InHouse::STATUS_NEW;
        $inhouse->save();

        return redirect('/inhouse/'.$inhouse->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $inhouse = InHouse::find($id);

        $data = [
            'inhouse' => $inhouse
        ];

        return view('in_houses.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
