<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Models\Employee;
use App\Models\Team;
use App\Models\Unit;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('profile', ['only' => ['edit', 'create']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (isset($request)) {
            $employees = Employee::where('name', 'LIKE', '%' . $request->search . '%')->paginate(10);

        } else {
            $employees = Employee::paginate(10);

        }
        $data = [
            'employees' => $employees
        ];

        return view('employees.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teams = Team::all();

        $roles = [
            Employee::ROLE_IC => trans('employee.role.ic'),
            Employee::ROLE_SV => trans('employee.role.sv'),
            Employee::ROLE_SA => trans('employee.role.sa')
        ];

        $ics = [
            Employee::IC_CREATIVE => trans('employee.ic.creative'),
            Employee::IC_DEV => trans('employee.ic.dev'),
            Employee::IC_QA => trans('employee.ic.qa'),
            Employee::IC_POC => trans('employee.ic.poc'),
            Employee::IC_SME => trans('employee.ic.sme'),
            Employee::IC_MGMT => trans('employee.ic.mgmt')
        ];

        $schedules = [
            Employee::SCHEDULE_DAY => trans('employee.schedule.day'),
            Employee::SCHEDULE_MID => trans('employee.schedule.mid'),
            Employee::SCHEDULE_NIGHT => trans('employee.schedule.night')
        ];

        $data = [
            'teams' => $teams,
            'roles' => $roles,
            'ics' => $ics,
            'schedules' => $schedules
        ];

        return view('employees.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->name;
        $email = $request->email;
        $team = $request->team;
        $role = $request->role;
        $ic = $request->ic;
        $schedule = $request->schedule;

        $employee = new Employee;
        $employee->name = $name;
        $employee->email = $email;
        $employee->team_id = $team;
        $employee->role_tag = $role;
        $employee->ic_tag = $ic;
        $employee->schedule_tag = $schedule;
        $employee->save();

        return redirect('/employee');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if ($request->ajax()) {
            return response()->json(Employee::find($id)->toArray());
        }

        $employee = Employee::find($id);
        $assigned_units = Unit::where('employee_id', $employee->id)
            ->where('status_tag', '!=', Unit::STATUS_CLOSED)
            ->get();

        $data = [
            'employee' => $employee,
            'assigned_units' => $assigned_units
        ];

        return view('employees.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $employee = Employee::find($id);

        $teams = Team::all();

        $roles = [
            Employee::ROLE_IC => trans('employee.role.ic'),
            Employee::ROLE_SV => trans('employee.role.sv'),
            Employee::ROLE_SA => trans('employee.role.sa')
        ];

        $ics = [
            Employee::IC_CREATIVE => trans('employee.ic.creative'),
            Employee::IC_DEV => trans('employee.ic.dev'),
            Employee::IC_QA => trans('employee.ic.qa'),
            Employee::IC_POC => trans('employee.ic.poc'),
            Employee::IC_SME => trans('employee.ic.sme'),
            Employee::IC_MGMT => trans('employee.ic.mgmt')
        ];

        $schedules = [
            Employee::SCHEDULE_DAY => trans('employee.schedule.day'),
            Employee::SCHEDULE_MID => trans('employee.schedule.mid'),
            Employee::SCHEDULE_NIGHT => trans('employee.schedule.night')
        ];

        $data = [
            'employee' => $employee,
            'teams' => $teams,
            'roles' => $roles,
            'ics' => $ics,
            'schedules' => $schedules
        ];

        return view('employees.edit', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);

        $name = $request->name;
        $email = $request->email;
        $team = $request->team;
        $role = $request->role;
        $ic = $request->ic;
        $schedule = $request->schedule;

        $employee->name = $name;
        $employee->email = $email;
        $employee->team_id = $team;
        $employee->role_tag = $role;
        $employee->ic_tag = $ic;
        $employee->schedule_tag = $schedule;
        $employee->save();

        return redirect('/employee');
    }
}
