<?php

// When I write this code,
// Only God and I know what I was doing,
// Now...
// Only God knows.

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\Models\Employee;
use App\Models\Campaign;
use App\Models\UnitTime;
use App\Models\Team;
use App\Models\Unit;

class MetricController extends Controller
{
    // Misc constants
    const PERCENTAGE_MULTIPLIER = 100;
    const TWO_DECIMAL_PLACES = 2;
    const SECONDS_IN_HOUR = 3600;
    const WEEKS_AGO = 0;
    const DAYS_PER_WEEK = 6;

    public $schedules = [
        [
            'name' => "Day",
            'value' => Employee::SCHEDULE_DAY
        ],
        [
            'name' => "Mid",
            'value' => Employee::SCHEDULE_MID
        ],
        [
            'name' => "Night",
            'value' => Employee::SCHEDULE_NIGHT
        ]
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware('auth');
    }

    public function volume(Request $request)
    {
        $date_length = 3;
        $timespan = $request->input('timespan', 'weeks');
        $dates = [];
        $base_date = Carbon::now();

        if(!empty($request->timespan_range)){
            $timespan = $request->input('timespan_range', 'weeks');
            $first = Carbon::parse($request->from);
            $second = Carbon::parse($request->to);

            switch ($timespan){
                case 'weeks':
                    $date_length = (int) ceil($first->startOfWeek()->diffInDays($second->endOfWeek()) / 7) - 1;
                    break;
                case 'months':
                    $date_length = ($second->month - $first->month);
                    $second = $second->endOfMonth();
                    break;
            }

            $base_date = $second->copy();
        }

        $volume = [];
        $qa_volume = [];

        $metrics = [
            "No. of Campaigns",
            "No. of Units",
            "Average Complexity Level",
            "Volume X Complexity"
        ];

        for ($length = self::WEEKS_AGO; $length <= $date_length; $length++) {
            $scope = dateScope($timespan, $length, false, $base_date->copy());

            $teams = Team::where('type_tag', Team::TYPE_ACCOUNT_BASED)
                ->with(['accounts.campaigns' => function ($query) use ($scope) {
                    $query->status(Campaign::STATUS_CLOSED)
                        ->closedFrom($scope->start)
                        ->closedTo($scope->end);
                }, 'units' => function ($query) use ($scope) {
                    $query->status(Campaign::STATUS_CLOSED)
                        ->closedFrom($scope->start)
                        ->closedTo($scope->end);
                }])->get();

            foreach ($teams as $t => $team) {
                $volume[$t]['name'] = $team->name;

                foreach ($team->accounts as $a => $account) {
                    $no_of_campaigns = $account->campaigns->count();
                    $no_of_units = 0;

                    $complexities = [];

                    foreach ($account->campaigns as $campaign) {
                        foreach ($campaign->units as $unit) {
                            $no_of_units++;
                            $complexities[] = $unit->complexity;
                        }
                    }

                    $average_complexity_level = collect($complexities)->avg();
                    $volume_x_complexity = $no_of_units * $average_complexity_level;

                    $account_metrics = [
                        round($no_of_campaigns, self::TWO_DECIMAL_PLACES),
                        round($no_of_units, self::TWO_DECIMAL_PLACES),
                        round($average_complexity_level, self::TWO_DECIMAL_PLACES),
                        round($volume_x_complexity, self::TWO_DECIMAL_PLACES)
                    ];

                    $volume[$t]['accounts'][$a]['name'] = $account->name;

                    foreach ($metrics as $m => $metric) {
                        $volume[$t]['accounts'][$a]['metrics'][$m]['name'] = $metric;
                        $volume[$t]['accounts'][$a]['metrics'][$m]['cols'][$length] = $account_metrics[$m];
                        $volume[$t]['accounts'][$a]['metrics'][$m]['graph'][] = $account_metrics[$m];
                    }

                    $volume[$t]['accounts'][$a]['rowspan'] = count($volume[$t]['accounts'][$a]['metrics']);
                }

                $volume[$t]['rowspan'] = count($volume[$t]['accounts']) * count($volume[$t]['accounts'][0]['metrics']);
            }

            // Qa Team
            $qa_volume['name'] = "QA Pool";

            foreach ($this->schedules as $s => $schedule) {
                $qa_campaigns = Campaign::with('units')
                    ->whereHas('unitTimes', function ($query) use ($scope, $schedule) {
                        $query->teamTimes(Team::TEAM_QA)
                            ->schedule($schedule['value'])
                            ->workType(UnitTime::WORK_QA)
                            ->actionType(UnitTime::ACTION_IN_PROG)
                            ->endedFrom($scope->start)
                            ->endedTo($scope->end);
                    })->get();

                $qa_no_of_campaigns = $qa_campaigns->count();
                $qa_no_of_units = 0;

                $qa_complexities = [];

                foreach ($qa_campaigns as $qac => $qa_campaign) {
                    foreach ($qa_campaign->units as $qau => $qa_unit) {
                        $qa_no_of_units++;
                        $qa_complexities[] = $qa_unit->complexity;
                    }
                }

                $qa_average_complexity_level = collect($qa_complexities)->avg();
                $qa_volume_x_complexity = $qa_no_of_units * $qa_average_complexity_level;

                $qa_metrics = [
                    round($qa_no_of_campaigns, self::TWO_DECIMAL_PLACES),
                    round($qa_no_of_units, self::TWO_DECIMAL_PLACES),
                    round($qa_average_complexity_level, self::TWO_DECIMAL_PLACES),
                    round($qa_volume_x_complexity, self::TWO_DECIMAL_PLACES)
                ];

                $qa_volume['accounts'][$s]['name'] = $schedule['name'];

                foreach ($qa_metrics as $qm => $qa_metric) {
                    $qa_volume['accounts'][$s]['metrics'][$qm]['name'] = $metrics[$qm];
                    $qa_volume['accounts'][$s]['metrics'][$qm]['cols'][$length] = $qa_metric;
                    $qa_volume['accounts'][$s]['metrics'][$qm]['graph'][] = $qa_metric;
                }

                $qa_volume['accounts'][$s]['rowspan'] = count($qa_volume['accounts'][$s]['metrics']);
            }

            $qa_volume['rowspan'] = count($qa_volume['accounts']) * count($qa_volume['accounts'][0]['metrics']);

            $dates[] = [$scope->start->copy(), $scope->end->copy()];
        }
        array_push($volume, $qa_volume);

        foreach ($volume as $key => $team) {
            $accounts = $team['accounts'];

            foreach ($accounts as $key2 => $account) {
                $metrics = $account['metrics'];

                foreach ($metrics as $key3 => $metric) {
//                    dd($metric['cols']);
                    $cols = array_reverse($metric['cols']);

                    $ctr = 0;
                    $tmp_cols = [];

                    while($ctr < count($cols) - 1){
                        $val = $cols[$ctr] - $cols[$ctr + 1];
                        $percentage = ($cols[$ctr + 1]) ? round(($val / $cols[$ctr + 1]) * 100, 2) . "%" : "-";
                        $tmp_cols[] = $cols[$ctr];
                        $tmp_cols[] = $percentage;
                        $ctr++;
                    }

                    $tmp_cols[] = $cols[count($cols) - 1];

                    $cols = array_reverse($tmp_cols);
                    $graph = $metric['graph'];

                    $volume[$key]['accounts'][$key2]['metrics'][$key3]['cols'] = $cols;
                    $volume[$key]['accounts'][$key2]['metrics'][$key3]['graph'] = implode(",", $graph);
                }
            }
        }

        $data = [
            'dates' => $dates,
            'volume' => $volume,
            'timespan' => $timespan,
        ];

        return view('metrics.volume', $data);
    }

    public function utilization(Request $request)
    {
        $metrics = [
            "Headcount",
            "HC Available Time(Hours)",
            "Actual Task Time(Hours)",
            "Utilization Level"
        ];

        $utilization = [];
        $qa_utilization = [];

        $date_length = 3;
        $timespan = $request->input('timespan', 'weeks');
        $dates = [];
        $base_date = Carbon::now();

        if(!empty($request->timespan_range)){
            $timespan = $request->input('timespan_range', 'weeks');
            $first = Carbon::parse($request->from);
            $second = Carbon::parse($request->to);

            switch ($timespan){
                case 'weeks':
                    $date_length = (int) ceil($first->startOfWeek()->diffInDays($second->endOfWeek()) / 7) - 1;
                    break;
                case 'months':
                    $date_length = ($second->month - $first->month);
                    $second = $second->endOfMonth();
                    break;
            }

            $base_date = $second->copy();
        }

        for ($length = self::WEEKS_AGO; $length <= $date_length; $length++) {
            $scope = dateScope($timespan, $length, false, $base_date->copy());

            $teams = Team::where('type_tag', Team::TYPE_ACCOUNT_BASED)
                ->with(['accounts.campaigns' => function ($query) use ($scope) {
                    $query->status(Campaign::STATUS_CLOSED)
                        ->closedFrom($scope->start)
                        ->closedTo($scope->end);
                }, 'accounts.campaigns.units' => function ($query) use ($scope) {
                    $query->status(Unit::STATUS_CLOSED)
                        ->closedFrom($scope->start)
                        ->closedTo($scope->end);
                }, 'accounts.campaigns.units.unitTimes' => function ($query) {
                    $query->workType(UnitTime::WORK_QA, false)
                        ->actionType(UnitTime::ACTION_IN_PROG)
                        ->ended();
                }])->get();

            foreach ($teams as $t => $team) {
                $actual_task_times = [];

                foreach ($team->accounts as $a => $account) {
                    foreach ($account->campaigns as $c => $campaign) {
                        foreach ($campaign->units as $u => $unit) {
                            foreach ($unit->unitTimes as $ut => $unit_time) {
                                $actual_task_times[] = $unit_time->utilization;
                            }
                        }
                    }
                }

                $headcount = ($team->teamHeadcount()->from($scope->start)->to($scope->end)->latestHc()->first() !== null) ? $team->teamHeadcount()->from($scope->start)->to($scope->end)->latestHc()->first()->headcount : $team->members->count();

                $total_out_of_office = 0; // temporary

                $actual_task_time = collect($actual_task_times)->sum();
                $availlable_time_based_on_headcount = $headcount * 7.5;
                $actual_available_time = $availlable_time_based_on_headcount - $total_out_of_office;

                if ($availlable_time_based_on_headcount) {
                    $utilization_level = $actual_task_time / $availlable_time_based_on_headcount;
                } else {
                    $utilization_level = 0;
                }

                $team_metrics = [
                    $headcount,
                    round($availlable_time_based_on_headcount, 2),
                    round($actual_task_time, 2),
                    round($utilization_level, 2)
                ];


                $utilization[$t]['name'] = $team->name;
                $utilization[$t]['overall']['text'] = "Team Overall";

                foreach ($metrics as $m => $metric) {
                    $utilization[$t]['overall']['metrics'][$m]['name'] = $metric;
                    $utilization[$t]['overall']['metrics'][$m]['cols'][$length] = $team_metrics[$m];
                    $utilization[$t]['overall']['metrics'][$m]['graph'][] = $team_metrics[$m];
                }

                $utilization[$t]['overall']['rowspan'] = count($utilization[$t]['overall']['metrics']);
                $utilization[$t]['rowspan'] = count($utilization[$t]['overall']['metrics']);
            }

            // QA Team (Special)
            $qa_unit_times = UnitTime::actionType(UnitTime::ACTION_IN_PROG)
                ->workType(UnitTime::WORK_QA)
                ->endedFrom($scope->start)
                ->endedTo($scope->end)
                ->teamTimes(Team::TEAM_QA)
                ->get();

            $team = Team::find(Team::TEAM_QA);

            $qa_headcount = ($team->teamHeadcount()->from($scope->start)->to($scope->end)->latestHc()->first() !== null) ? $team->teamHeadcount()->from($scope->start)->to($scope->end)->latestHc()->first()->headcount : $team->members->count();
            $qa_total_out_of_office = 0; // temporary

            $qa_actual_task_time = $qa_unit_times->sum();
            $qa_availlable_time_based_on_headcount = $qa_headcount * 7.5;
            $qa_actual_available_time = $qa_availlable_time_based_on_headcount - $qa_total_out_of_office;

            if ($qa_availlable_time_based_on_headcount) {
                $qa_utilization_level = $qa_actual_task_time / $qa_availlable_time_based_on_headcount;
            } else {
                $qa_utilization_level = 0;
            }

            $qa_metrics = [
                $qa_headcount,
                round($qa_availlable_time_based_on_headcount, 2),
                round($qa_actual_task_time, 2),
                round($qa_utilization_level, 2)
            ];

            $qa_utilization['name'] = "QA Pool";
            $qa_utilization['overall']['text'] = "Team Overall";

            foreach ($metrics as $m => $metric) {
                $qa_utilization['overall']['metrics'][$m]['name'] = $metric;
                $qa_utilization['overall']['metrics'][$m]['cols'][$length] = $qa_metrics[$m];
                $qa_utilization['overall']['metrics'][$m]['graph'][] = $qa_metrics[$m];
            }

            $qa_utilization['overall']['rowspan'] = count($qa_utilization['overall']['metrics']);
            $qa_utilization['rowspan'] = count($qa_utilization['overall']['metrics']);

            $dates[] = [$scope->start->copy(), $scope->end->copy()];
        }

        array_push($utilization, $qa_utilization);

        foreach ($utilization as $key => $team) {
            $metrics = $team['overall']['metrics'];

            foreach ($metrics as $key2 => $metric) {
                $cols = array_reverse($metric['cols']);

                $ctr = 0;
                $tmp_cols = [];

                while($ctr < count($cols) - 1){
                    $val = $cols[$ctr] - $cols[$ctr + 1];
                    $percentage = ($cols[$ctr + 1]) ? round(($val / $cols[$ctr + 1]) * 100, 2) . "%" : "-";
                    $tmp_cols[] = $cols[$ctr];
                    $tmp_cols[] = $percentage;
                    $ctr++;
                }

                $tmp_cols[] = $cols[count($cols) - 1];

                $cols = array_reverse($tmp_cols);
                $graph = $metric['graph'];

                $utilization[$key]['overall']['metrics'][$key2]['cols'] = $cols;
                $utilization[$key]['overall']['metrics'][$key2]['graph'] = implode(",", $graph);
            }
        }

        $data = [
            'dates' => $dates,
            'utilization' => $utilization,
            'timespan' => $timespan,
        ];

        return view('metrics.utilization', $data);
    }

    public function efficiency(Request $request)
    {
        $timespan = $request->input('timespan', 'weeks');

        $metrics = [
            "OTD Level",
            "Average TAT per Unit",
            "Average Task Time per Unit"
        ];

        $efficiency = [];
        $qa_efficiency = [];

        for ($length = self::WEEKS_AGO; $length <= 3; $length++) {
            $scope = dateScope($timespan, $length, false);

            $teams = Team::where('type_tag', Team::TYPE_ACCOUNT_BASED)
                ->with(['accounts.campaigns.units' => function ($query) use ($scope) {
                    $query->closedFrom($scope->start)
                        ->closedTo($scope->end);
                }, 'accounts.campaigns.units.unitTimes' => function ($query) {
                    $query->actionType(UnitTime::ACTION_IN_PROG);
                }])->get();

            foreach ($teams as $t => $team) {
                $efficiency[$t]['name'] = $team->name;

                foreach ($team->accounts as $a => $account) {
                    $efficiency[$t]['accounts'][$a]['name'] = $account->name;

                    $closed_campaign_unit_time = [];
                    $closed_unit_time = [];
                    $average_tat_per_tickets = [];
                    $average_tat_per_units = [];
                    $otd_levels = [];

                    foreach ($account->campaigns as $c => $campaign) {
                        $closed_in_prog_campaign = [];

                        // TAT
                        if ($campaign->units->count()) {
//                            dd($campaign->units->first()->tat);
                            $avg_tat_per_ticket = $campaign->tat;
                            $avg_tat_per_unit = 0;
                            foreach ($campaign->units as $unit) {
                                $avg_tat_per_unit += $unit->tat;
                            }
                        } else {
                            $avg_tat_per_ticket = 0;
                            $avg_tat_per_unit = 0;
                        }

                        $average_tat_per_tickets[] = $avg_tat_per_ticket;
                        $average_tat_per_units[] = $avg_tat_per_unit;

                        // OTD
                        $late_units = $campaign->units()
                            ->closedFrom($scope->start)
                            ->closedTo($scope->end)
                            ->late()
                            ->count();

                        $closed_units = $campaign->units()
                            ->closedFrom($scope->start)
                            ->closedTo($scope->end)
                            ->count();

                        if ($closed_units) {
                            $otd_levels[] = round($late_units / $closed_units, self::TWO_DECIMAL_PLACES);
                        } else {
                            $otd_levels[] = 0;
                        }

                        if ($campaign->status_tag == Campaign::STATUS_CLOSED) {
                            foreach ($campaign->units as $unit) {
                                if ($unit->unitTimes->count()) {
                                    foreach ($unit->unitTimes as $unit_time) {
                                        $closed_unit_time[] = $unit_time->task_time;
                                        $closed_in_prog_campaign[] = $unit_time->task_time;
                                    }
                                }
                            }
                        } else {
                            foreach ($campaign->units as $unit) {
                                if ($unit->unitTimes->count()) {
                                    foreach ($unit->unitTimes as $unit_time) {
                                        $closed_unit_time[] = $unit_time->task_time;
                                    }
                                }
                            }
                        }

                        $closed_campaign_unit_time[] = collect($closed_in_prog_campaign)->sum();
                    }

                    $closed_unit_time_arr = collect($closed_unit_time);
                    $closed_campaign_unit_time_arr = collect($closed_campaign_unit_time);
                    $average_tat_per_ticket_arr = collect($average_tat_per_tickets);
                    $average_tat_per_unit_arr = collect($average_tat_per_units);

                    $average_task_time_per_unit = ($closed_unit_time_arr->count()) ? round(($closed_unit_time_arr->sum() / self::SECONDS_IN_HOUR) / $closed_unit_time_arr->count(), self::TWO_DECIMAL_PLACES) : 0;
                    $average_task_time_per_ticket = ($closed_campaign_unit_time_arr->count()) ? round(($closed_campaign_unit_time_arr->sum() / self::SECONDS_IN_HOUR) / $closed_campaign_unit_time_arr->count(), self::TWO_DECIMAL_PLACES) : 0;
                    $average_tat_per_ticket = ($average_tat_per_ticket_arr->count()) ? round($average_tat_per_ticket_arr->sum() / $average_tat_per_ticket_arr->count(), self::TWO_DECIMAL_PLACES) : 0;
                    $average_tat_per_unit = ($average_tat_per_unit_arr->count()) ? round($average_tat_per_unit_arr->sum() / $average_tat_per_unit_arr->count(), self::TWO_DECIMAL_PLACES) : 0;
                    $otd_level = collect($otd_levels)->sum();

//                    $efficiency[$t]['accounts'][$a]['metrics'][0]['name'] = "Average Task Time per Unit";
//                    $efficiency[$t]['accounts'][$a]['metrics'][0]['cols'][$length] = $average_task_time_per_unit;
//                    $efficiency[$t]['accounts'][$a]['metrics'][0]['graph'][] = $average_task_time_per_unit;
//                    $efficiency[$t]['accounts'][$a]['metrics'][1]['name'] = "Average Task Time per Ticket";
//                    $efficiency[$t]['accounts'][$a]['metrics'][1]['cols'][$length] = $average_task_time_per_ticket;
//                    $efficiency[$t]['accounts'][$a]['metrics'][1]['graph'][] = $average_task_time_per_ticket;
//                    $efficiency[$t]['accounts'][$a]['metrics'][2]['name'] = "Average TAT per Ticket";
//                    $efficiency[$t]['accounts'][$a]['metrics'][2]['cols'][$length] = $average_tat_per_ticket;
//                    $efficiency[$t]['accounts'][$a]['metrics'][2]['graph'][] = $average_tat_per_ticket;
//                    $efficiency[$t]['accounts'][$a]['metrics'][3]['name'] = "OTD Level";
//                    $efficiency[$t]['accounts'][$a]['metrics'][3]['cols'][$length] = $otd_level;
//                    $efficiency[$t]['accounts'][$a]['metrics'][3]['graph'][] = $otd_level;

                    $efficiency[$t]['accounts'][$a]['metrics'][0]['name'] = "OTD Level";
                    $efficiency[$t]['accounts'][$a]['metrics'][0]['cols'][$length] = $otd_level;
                    $efficiency[$t]['accounts'][$a]['metrics'][0]['graph'][] = $otd_level;
                    $efficiency[$t]['accounts'][$a]['metrics'][1]['name'] = "Average TAT per Unit";
                    $efficiency[$t]['accounts'][$a]['metrics'][1]['cols'][$length] = $average_tat_per_unit;
                    $efficiency[$t]['accounts'][$a]['metrics'][1]['graph'][] = $average_tat_per_unit;
                    $efficiency[$t]['accounts'][$a]['metrics'][2]['name'] = "Average Task Time per Unit";
                    $efficiency[$t]['accounts'][$a]['metrics'][2]['cols'][$length] = $average_task_time_per_unit;
                    $efficiency[$t]['accounts'][$a]['metrics'][2]['graph'][] = $average_task_time_per_unit;

                    $efficiency[$t]['accounts'][$a]['rowspan'] = count($efficiency[$t]['accounts'][$a]['metrics']);
                }

                $efficiency[$t]['rowspan'] = count($efficiency[$t]['accounts']) * count($efficiency[$t]['accounts'][0]['metrics']);
            }

            // QA Team
            $qa_efficiency['name'] = "QA Pool";

            foreach ($this->schedules as $s => $schedule) {
                $qa_campaigns = Campaign::with(['units.unitTimes' => function ($query) use ($scope, $schedule) {
                    $query->teamTimes(Team::TEAM_QA)
                        ->schedule($schedule['value'])
                        ->workType(UnitTime::WORK_QA)
                        ->actionType(UnitTime::ACTION_IN_PROG)
                        ->endedFrom($scope->start)
                        ->endedTo($scope->end);
                }])->get();

                $unit_task_times = [];
                $campaign_unit_task_times = [];

                foreach ($qa_campaigns as $qa_campaign) {
                    $campaign_unit_task_time = [];

                    foreach ($qa_campaign->units as $qa_unit) {
                        $unit_task_time = 0;

                        if ($qa_unit->unitTimes->count()) {
                            foreach ($qa_unit->unitTimes as $qa_unit_time) {
                                $unit_task_time += $qa_unit_time->task_time;
                            }
                        }

                        $unit_task_times[] = $unit_task_time;
                        $campaign_unit_task_time[] = $unit_task_time;
                    }

                    $campaign_unit_task_times = collect($campaign_unit_task_time)->avg();
                }

                $average_task_time_per_unit = collect($unit_task_times)->avg();
                $average_task_time_per_ticket = collect($campaign_unit_task_times)->avg();

                $qa_metrics = [
                    round($average_task_time_per_unit, 2),
                    round($average_task_time_per_ticket, 2)
                ];

                $qa_efficiency['accounts'][$s]['name'] = $schedule['name'];

                foreach ($qa_metrics as $qm => $qa_metric) {
                    $qa_efficiency['accounts'][$s]['metrics'][$qm]['name'] = $metrics[$qm];
                    $qa_efficiency['accounts'][$s]['metrics'][$qm]['cols'][$length] = $qa_metric;
                    $qa_efficiency['accounts'][$s]['metrics'][$qm]['graph'][] = $qa_metric;

                    $qa_efficiency['accounts'][$s]['rowspan'] = count($qa_efficiency['accounts'][$s]['metrics']);
                }
            }

            $qa_efficiency['rowspan'] = count($qa_efficiency['accounts']) * count($qa_efficiency['accounts'][0]['metrics']);
        }

        array_push($efficiency, $qa_efficiency);

        foreach ($efficiency as $key => $team) {
            $accounts = $team['accounts'];

            foreach ($accounts as $key2 => $account) {
                $metrics = $account['metrics'];

                foreach ($metrics as $key3 => $metric) {
                    $cols = array_reverse($metric['cols']);

                    $value_1 = $cols[0] - $cols[1];
                    $percentage_1 = ($cols[1]) ? round(($value_1 / $cols[1]) * self::PERCENTAGE_MULTIPLIER, self::TWO_DECIMAL_PLACES) . "%" : "-";
                    $value_2 = $cols[1] - $cols[2];
                    $percentage_2 = ($cols[2]) ? round(($value_2 / $cols[2]) * self::PERCENTAGE_MULTIPLIER, self::TWO_DECIMAL_PLACES) . "%" : "-";
                    $value_3 = $cols[2] - $cols[3];
                    $percentage_3 = ($cols[3]) ? round(($value_3 / $cols[3]) * self::PERCENTAGE_MULTIPLIER, self::TWO_DECIMAL_PLACES) . "%" : "-";

//                    array_splice($cols, 1, 0, [$percentage_1, $value_1]);
//                    array_splice($cols, 4, 0, [$percentage_2, $value_2]);
//                    array_splice($cols, 7, 0, [$percentage_3, $value_3]);

                    array_splice($cols, 1, 0, [$percentage_1]);
                    array_splice($cols, 3, 0, [$percentage_2]);
                    array_splice($cols, 5, 0, [$percentage_3]);

                    $cols = array_reverse($cols);
                    $graph = $metric['graph'];

                    $efficiency[$key]['accounts'][$key2]['metrics'][$key3]['cols'] = $cols;
                    $efficiency[$key]['accounts'][$key2]['metrics'][$key3]['graph'] = implode(",", $graph);
                }
            }
        }
        $nows = Carbon::today();
        $now = Carbon::today()->format('F j');
        $now1 = Carbon::today()->subDays(1)->format('F j');
        $now2 = Carbon::today()->subDays(2)->format('F j');
        $now3 = Carbon::today()->subDays(3)->format('F j');
        $yesterday = Carbon::yesterday()->format('F j');
        $prev_month = $nows->month - 1;
        $prev_prev_month = $nows->month - 2;
        $prev_prev_prev_month = $nows->month - 3;
//        $scope->start->format('F j')
        $startOfWeek = Carbon::now()->startOfWeek()->subDay()->format('F j');
        $startOfWeek1 = Carbon::now()->startOfWeek()->subWeek(1)->subDay()->format('F j');
        $startOfWeek2 = Carbon::now()->startOfWeek()->subWeek(2)->subDay()->format('F j');
        $startOfWeek3 = Carbon::now()->startOfWeek()->subWeek(3)->subDay()->format('F j');
        $endOfWeek = Carbon::now()->endOfWeek()->subDay()->format('F j');
        $endOfWeek1 = Carbon::now()->endOfWeek()->subWeek(1)->subDay()->format('F j');
        $endOfWeek2 = Carbon::now()->endOfWeek()->subWeek(2)->subDay()->format('F j');
        $endOfWeek3 = Carbon::now()->endOfWeek()->subWeek(3)->subDay()->format('F j');
//        dd($startOfWeek);

        $data = [
            'efficiency' => $efficiency,
            'timespan' => $timespan,
            'month' => $nows->month,
            'prev_month' => $prev_month,
            'prev_prev_month' => $prev_prev_month,
            'prev_prev_prev_month' => $prev_prev_prev_month,
            'now' => $now,
            'now1' => $now1,
            'now2' => $now2,
            'now3' => $now3,
            'yesterday' => $yesterday,
            'startOfWeek' => $startOfWeek,
            'startOfWeek1' => $startOfWeek1,
            'startOfWeek2' => $startOfWeek2,
            'startOfWeek3' => $startOfWeek3,
            'endOfWeek' => $endOfWeek,
            'endOfWeek1' => $endOfWeek1,
            'endOfWeek2' => $endOfWeek2,
            'endOfWeek3' => $endOfWeek3,
        ];

        return view('metrics.efficiency', $data);
    }
}
