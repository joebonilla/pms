<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\Models\Employee::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'google' => [
        'client_id' => '713361174408-poeqo73uilek17e67dnqd4hs03jbse61.apps.googleusercontent.com',
        'client_secret' => 'E0gHut61zdnNpVgzL27eqSQ9',
        'redirect' => env('APP_URL').'/auth/google/callback',
    ],
    'google_insight' => [
        'api_key' => 'AIzaSyD2_vu_47opZm56joqllCxn8v0XxR3ACFc',
        'prefix' => 'https://www.googleapis.com/pagespeedonline/v1/runPagespeed?url='
    ]
];
